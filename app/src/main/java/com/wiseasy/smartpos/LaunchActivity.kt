package com.wiseasy.smartpos

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Process
import com.tbruyelle.rxpermissions2.RxPermissions
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.databinding.ActivityLaunchBinding
import com.wiseasy.smartpos.login.firstlogin.FirstLoginActivity
import com.wiseasy.smartpos.login.init.InitManager
import com.wiseasy.smartpos.login.login.LoginActivity

class LaunchActivity : CommonActivity<ActivityLaunchBinding>() {

    private lateinit var mContext: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)

        initDate()
        initView()
    }

    override fun getBinding(): ActivityLaunchBinding {
        return ActivityLaunchBinding.inflate(layoutInflater)
    }

    private fun initDate() {
        mContext = this
        SPUtil.setIsChangeLanguage(mContext, false)
    }

    private fun initView() {
        if (InitManager.initStatus === InitManager.INIT_STATUS_SUCCESS) {
            log("初始化成功")
            requestPermissions()
        } else {
            log("初始化未成功，等待中")
            InitManager.listener = object : InitManager.OnInitListener {
                override fun onSuccess() {
                    requestPermissions()
                }

                override fun onFail(msg: String?) {
                    runOnUiThread {
                        mDialog = CommonDialog.Builder(mContext)
                            .setTitle(msg)
                            .setButton(getString(R.string.base_got_it))
                            .setListener(object : CommonDialog.OnClickListener {
                                override fun onConfirm() {
                                    finish()
                                    Process.killProcess(Process.myPid())
                                }

                                override fun onCancel() {}
                            }).createAndShow()
                    }
                }
            }
        }
    }

    /**
     * 正式启动应用
     */
    private fun startApp() {
        val isFirstLogin = SPUtil.getIsFirstLogin(mContext)
        if (isFirstLogin) {
            startActivity(Intent(mContext, FirstLoginActivity::class.java))
            finish()
        } else {
            startActivity(Intent(mContext, LoginActivity::class.java))
            finish()
        }
    }

    /**
     * 申请权限
     */
    @SuppressLint("CheckResult")
    private fun requestPermissions() {
        log("requestPermissions")
        val rxPermissions = RxPermissions(this)
        rxPermissions.request(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_PHONE_STATE
        )
            .subscribe { granted ->
                log("权限申请结果：$granted")
                if (!granted) {
                    showInitFail(getString(R.string.login_permission))
                } else {
                    startApp()
                }
            }
    }

    private fun showInitFail(msg: String) {
        mDialog = CommonDialog.Builder(this)
            .setTitle(msg)
            .setButton(getString(R.string.base_got_it))
            .setListener(object : CommonDialog.OnClickListener {
                override fun onConfirm() {
                    finish()
                    Process.killProcess(Process.myPid())
                }

                override fun onCancel() {}
            }).createAndShow()
    }

}