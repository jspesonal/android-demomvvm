package com.wiseasy.smartpos

import android.content.Context
import android.content.pm.PackageManager
import com.wiseasy.smartpos.base.BaseApplication
import com.wiseasy.smartpos.base.IApplicationLike
import com.wiseasy.smartpos.base.speech.SpeakManager

class MyApplication : BaseApplication() {
    private val TAG = "MyApplication"

    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext

        //语音初始化
        SpeakManager.getInstance().init(this)

        initModule()
    }

    /**
     * 初始化各个Module
     */
    private fun initModule() {
        val metaData = packageManager.getApplicationInfo(
            packageName,
            PackageManager.GET_META_DATA
        ).metaData

        metaData.keySet().forEach {
            if (it.startsWith(packageName)) {
                try {
                    val clazz = Class.forName(it)
                    val application = clazz.newInstance() as IApplicationLike
                    application.init(this)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

}