package com.wiseasy.smartpos.base

import android.content.Context
import androidx.multidex.MultiDexApplication

/**
 * Created by Bella on 2021/12/20.
 */
open class BaseApplication: MultiDexApplication() {

    companion object{
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this
    }
}