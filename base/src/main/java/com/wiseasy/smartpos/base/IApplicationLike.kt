package com.wiseasy.smartpos.base

import android.app.Application
import org.jetbrains.annotations.NotNull

/**
 * Created by Bella on 2021/12/20.
 */
interface IApplicationLike {
    fun init(@NotNull application: Application)
}