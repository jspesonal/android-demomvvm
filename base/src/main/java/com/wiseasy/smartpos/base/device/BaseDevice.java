package com.wiseasy.smartpos.base.device;

/**
 * 设备对象
 */
public abstract class BaseDevice {

    /**
     * 是否支持银行卡功能
     */
    public Boolean isSupportBankCard() {
        return true;
    }

    /**
     * 是否支持（自带）打印机功能
     */
    public Boolean isSupportPrint() {
        return true;
    }
}
