package com.wiseasy.smartpos.base.device;

import android.os.Build;
import android.util.Log;

import com.wiseasy.smartpos.base.util.DeviceUtil;

public class DeviceProxy {

    public static DeviceProxy getInstance() {
        return Holder.deviceProxy;
    }

    static class Holder {
        static final DeviceProxy deviceProxy = new DeviceProxy();
    }

    private BaseDevice device;

    public BaseDevice getDevice() {
        return device;
    }

    public DeviceProxy() {

        String model = Build.MODEL;
        Log.i("DeviceProxy", "Build.MODEL=" + model);

        if (model.startsWith("WPOS-3")) {
            //通过反射获取设备信息，判断是否为WPOS-3 X
            String product_model_internal = DeviceUtil.getStringMethod("ro.product.model.internal", "");
            if ("WPOS-3 X".equals(product_model_internal)) {
                Log.i("DeviceProxy", "Build.MODEL.INTERNAL = WPOS-3 X");
                device = new WPOS3X();
            } else {
                device = new WPOS3();
            }
        } else if (model.startsWith("WPOS-MINI")) {
            device = new WPOSMINI();
        } else if (model.startsWith("WISEASY MINI2")) {
            device = new WPOSMINI2();
        } else if (model.startsWith("WISENET5")) {
            device = new WISENET5();
        } else if (model.startsWith("WPOS-TAB")) {
            device = new WPOSTAB();
        } else if (model.startsWith("WPOS-QT")) {
            device = new WPOSQT();
        } else if (model.startsWith("Realname_authentication_H1")) {
            device = new REALNAMEAUTHENTICATIONH1();
        } else {
            // 默认是POS3
            device = new WPOS3();
        }

        Log.i("DeviceProxy", "识别设备为：" + device.getClass().getSimpleName());
    }

    /**
     * 是否是WPOS3
     */
    public boolean isWOPS3() {
        return device instanceof WPOS3;
    }

    /**
     * 是否是WPOSMINI
     */
    public boolean isWPOSMINI() {
        return device instanceof WPOSMINI;
    }

    /**
     * 是否是WISENET5
     */
    public boolean isWISENET5() {
        return device instanceof WISENET5;
    }

    /**
     * 是否是TAB
     */
    public boolean isWPOSTAB() {
        return device instanceof WPOSTAB;
    }

    /**
     * 是否是QT
     */
    public boolean isWPOSQT() {
        return device instanceof WPOSQT;
    }

    /**
     * 是否是3X
     */
    public boolean isWPOS3X() {
        return device instanceof WPOS3X;
    }
}
