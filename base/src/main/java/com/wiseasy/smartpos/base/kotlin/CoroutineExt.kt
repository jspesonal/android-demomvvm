package com.wiseasy.smartpos.base.kotlin

import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * @author Hugo
 * @date 2021/10/4
 * launch Rx 调用封装
 * CoroutineExceptionHandler 异常处理
 */
fun CoroutineScope.launchRx(
    context: CoroutineContext = EmptyCoroutineContext,
    block: suspend CoroutineScope.() -> Unit
): CoroutineBuilder {
    return CoroutineBuilder(this, context, block)
}


class CoroutineBuilder(
    private val coroutineScope: CoroutineScope,
    private val context: CoroutineContext,
    private val block: suspend CoroutineScope.() -> Unit
) {

    fun await(onError: ((Throwable) -> Unit)? = null) {
        val coroutineExceptionHandler = CoroutineExceptionHandler { _, exception ->
            exception.printStackTrace()
            onError?.invoke(exception)
        }

        coroutineScope.launch(context = coroutineExceptionHandler + context, block = block)
    }

}


/**
 * 运行在IO线程的withContext，带有重试功能，执行次数为 times
 * @param times 运行次数
 * @param period 重试间隔, timeMillis
 * @param condition 重试条件
 */
suspend fun <T> withIOContext(
    times: Int = 0,
    period: Long = 0,
    condition: (Throwable) -> Boolean = { true },
    block: suspend CoroutineScope.() -> T
): T {

    var retryTime = times - 1
    var run = retryTime > 0

    while (run) {
        try {
            return withContext(Dispatchers.IO, block)
        } catch (e: Exception) {
            retryTime--
            run = retryTime > 0 && condition(e)
            delay(period)
        }
    }

    return withContext(Dispatchers.IO, block)
}


suspend fun <T> retry(
    times: Int = 0,
    period: Long = 0,
    block: suspend () -> T
): T {

    repeat(times - 1) {
        try {
            return block()
        } catch (e: Exception) {
        }
        delay(period)
    }

    return block()
}