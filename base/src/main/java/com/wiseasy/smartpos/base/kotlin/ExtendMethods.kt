package com.wiseasy.cashier.base.kotlin

import android.app.Activity
import android.app.Service
import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.View
import android.view.ViewTreeObserver
import android.widget.Button
import android.widget.EditText
import com.wiseasy.smartpos.base.util.DefaultTextWatcher
import java.security.MessageDigest

/**
 * @author Hugo
 * @date 2021/10/4
 * 扩展方法Kotlin文件
 */


/**
 * 源自：anko
 */
inline fun <reified T : Activity> Context.startActivity() {
    this.startActivity(Intent(this, T::class.java))
}

/**
 * 源自：anko
 */
inline fun <reified T : Service> Context.startService() {
    this.startService(Intent(this, T::class.java))
}


inline fun View.onClick(listener: View.OnClickListener): View {
    setOnClickListener(listener)
    return this
}


inline fun View.OnClick(crossinline method: () -> Unit): View {
    setOnClickListener { method() }
    return this
}


/**
 * 通过监听EditText 判断Button是否可用
 */
inline fun Button.watchEnable(et: EditText, crossinline method: () -> Boolean) {
    val btn = this
    et.addTextChangedListener(object : DefaultTextWatcher() {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            btn.isEnabled = method()
        }
    })
}

/**
 * 当变量为空时执行
 */
inline fun Any?.nullDo(method: () -> Unit) {
    if (this == null) {
        method.invoke()
    } else if (this is String) {
        if (TextUtils.isEmpty(this)) {
            method.invoke()
        }
    }
}

/**
 * 数据类型转换
 */
inline fun <T> Any.to(): T {
    return this as T
}

/**
 * 用于代码规范化 有助于代码美观 一般用于配置对象时对代码格式化
 * 示例: val linearLayout = LinearLayout(context).then {
 *         lay ->
 *         lay.orientation = LinearLayout.HORIZONTAL
 *         lay.setBackgroundColor(Color.RED)
 *       }.removeAllViews()
 */
inline fun <T> T.then(method: (T) -> Unit): T {
    method.invoke(this)
    return this
}


fun String.md5(): String {
    val bytes = MessageDigest.getInstance("MD5").digest(this.toByteArray())
    return bytes.hex()
}

fun ByteArray.hex(): String {
    return joinToString("") { "%02X".format(it) }
}


/**
 * 设置View的最大高度，常用在ListView/RecycleView
 */
fun View.setMaxHeight(maxHeight: Int) {
    this.viewTreeObserver.addOnGlobalLayoutListener(object :
        ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            val view = this@setMaxHeight

            view.viewTreeObserver.removeOnGlobalLayoutListener(this)

            if (view.height > maxHeight) {
                view.layoutParams.height = maxHeight
            }

        }

    })
}