package com.wiseasy.smartpos.base.mvp;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.wiseasy.smartpos.base.util.ToastUtil;
import com.wiseasy.smartpos.base.view.BaseActionBar;
import com.wiseasy.smartpos.base.view.BaseActionBarHavaBg;
import com.wiseasy.smartpos.base.view.BaseActionBarWhite;
import com.wiseasy.smartpos.base.view.BaseDialog;
import com.wiseasy.smartpos.base.view.LoadingDialog;
import com.wiseasy.smartpos.base.view.SystemStatusBarManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.WindowCompat;
import androidx.viewbinding.ViewBinding;


/**
 * Activity 基类
 */
public abstract class BaseActivity<T extends ViewBinding> extends AppCompatActivity implements View.OnClickListener, BaseActionBar.ActionBarClickListener, BaseActionBarHavaBg.ActionBarClickListener, BaseActionBarWhite.ActionBarClickListener, BaseIView {
    protected String TAG = this.getClass().getSimpleName();
    protected SystemStatusBarManager mSystemBar;
    protected LoadingDialog mLoadingDialog;
    protected Dialog mDialog;
    protected T mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        //如果是继承ActionBar或AppCompatActivity则使用supportRequestWindowFeature
        supportRequestWindowFeature(WindowCompat.FEATURE_ACTION_MODE_OVERLAY);

        mBinding = getBinding();
        setContentView(mBinding.getRoot());

        initSystemStatusBar();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy: ");
        if (mLoadingDialog != null) mLoadingDialog.dismiss();
        if (mDialog != null) mDialog.dismiss();
        super.onDestroy();
    }

    protected abstract T getBinding();

    /**
     * 设置状态栏背景
     * Set the status bar background
     */
    private void initSystemStatusBar() {
        mSystemBar = new SystemStatusBarManager(this);
        mSystemBar.setStatusBarColor(0x00000000);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onActionBarLeft() {
        finish();
    }

    @Override
    public void onActionBarRight() {

    }

    @Override
    public void showLoading() {
        showLoading(null);
    }

    @Override
    public void showLoading(String msg) {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this);
        }
        if (msg != null) {
            mLoadingDialog.setMsg(msg);
        }
        mLoadingDialog.show();
    }

    @Override
    public void showLoading(String msg, boolean isCancel) {
        if (mLoadingDialog != null) mLoadingDialog.dismiss();
        mLoadingDialog = new LoadingDialog(this, msg);
        mLoadingDialog.setCancelable(isCancel);
        mLoadingDialog.show();
    }

    @Override
    public void showLoading(String msg, int drawableId, boolean isCancel) {
        if (mLoadingDialog != null) mLoadingDialog.dismiss();
        mLoadingDialog = new LoadingDialog(this, msg);
        mLoadingDialog.setImageViewAnim(drawableId);
        mLoadingDialog.setCancelable(isCancel);
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        if (mLoadingDialog != null) {
            mLoadingDialog.dismiss();
        }
    }

    protected void log(String msg) {
        Log.d(TAG, msg);
    }

    protected void loge(String msg) {
        Log.e(TAG, msg);
    }

    protected void showSoftInput(View view) {
        ((InputMethodManager) getApplicationContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE))
                .showSoftInput(view, 0);

    }

    protected void toast(String msg) {
        ToastUtil.showShort(this, msg);
    }

    /**
     * 设置Back按键是否可用
     */
    @Override
    public void onBackPressed() {
        //todo:增加系统屏蔽home键
        super.onBackPressed();
    }

    /**
     * 设置全屏（包括状态栏）
     */
    public void setFillScreen() {
        mSystemBar.setStatusBarColor(0x00000000);
        //全屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
