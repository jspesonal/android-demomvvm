package com.wiseasy.smartpos.base.mvp;

/**
 * Created by Bella on 2021/12/15.
 */
public interface BaseIView {
    void showLoading();

    void showLoading(String msg);

    void showLoading(String msg, boolean isCancel);

    void showLoading(String msg, int drawableId, boolean isCancel);

    void hideLoading();
}
