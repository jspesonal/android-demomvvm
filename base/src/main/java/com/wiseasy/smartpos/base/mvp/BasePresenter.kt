package com.wiseasy.smartpos.base.mvp

import android.content.Context
import android.util.Log
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel

abstract class BasePresenter : CoroutineScope by MainScope() {

    protected var TAG = this.javaClass.simpleName
    private var mCompositeDisposable: CompositeDisposable? = null
    protected var mContext: Context? = null

    fun onCreate() {

    }

    protected fun addDisposable(disposable: Disposable?) {
        if (mCompositeDisposable == null) {
            mCompositeDisposable = CompositeDisposable()
        }
        mCompositeDisposable!!.add(disposable!!)
    }

    private fun dispose() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable!!.dispose()
        }
    }

    fun onDestroy() {
        mContext = null
        dispose()
        cancel()
    }

    protected fun log(msg: String) {
        Log.i(TAG, "log: $msg")
    }

    protected fun loge(msg: String) {
        Log.e(TAG, "log: $msg")
    }
}