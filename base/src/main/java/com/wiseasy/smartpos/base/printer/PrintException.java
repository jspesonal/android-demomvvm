package com.wiseasy.smartpos.base.printer;

/**
 * 打印相关异常
 * - 格式：4****
 */
public class PrintException extends Exception {

    // 打印过程中出现的通用异常
    public static PrintException PRINT_FAIL = new PrintException(40001, "打印失败");
    public static final String ERROR_MSG_DEFAULT = "未知异常";
    public static final String ERROR_MSG_API_CALL_FAIL = "API调用失败";

    public int code;
    public String msg;

    public PrintException() {
    }

    public PrintException(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "PrintException{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                '}';
    }
}
