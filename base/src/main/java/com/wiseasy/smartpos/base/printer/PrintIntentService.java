package com.wiseasy.smartpos.base.printer;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.wiseasy.smartpos.base.printer.print.AbsPrint;
import com.wiseasy.smartpos.base.printer.print.BaseAdapter;
import com.wiseasy.smartpos.base.printer.print.PrintPage;
import com.wiseasy.smartpos.base.printer.print.WiseCashierPrint;
import com.wiseasy.smartpos.base.printer.printer.AbsPrinter;

import java.util.ArrayList;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class PrintIntentService extends IntentService {

    private static String TAG = "PrintIntentService -> ";
    private static PrintException mPrintException = null;

    private static Context mContext;
    private static OnPrintListener mListener;

    public PrintIntentService() {
        super("PrintIntentService");
    }

    public static void launch(Context context, OnPrintListener listener) {
        mContext = context.getApplicationContext();
        mListener = listener;
        log("启动打印服务 launch()");
        context.startService(new Intent(context, PrintIntentService.class));
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        PrinterManager pm = PrinterManager.getInstance();
        AbsPrinter printer = pm.getPrinter(getApplicationContext());
        BaseAdapter adapter = pm.getPrintDataAdapter();
        Object bean = pm.getPrintData();

        try {
            ArrayList<PrintPage> pages = adapter.getPrintPage(bean);
            log("打印：共" + pages.size() + "联");
            for (int i = 0; i < pages.size(); i++) {
                printPage(printer, i, pages.get(i));
                if (i != pages.size() - 1)
                    Thread.sleep(5000);
            }

        } catch (Exception e) {
            e.printStackTrace();
            // 打印失败
            log("打印失败");
            if (e instanceof PrintException) {
                mPrintException = (PrintException) e;
                loge(mPrintException.toString());
            }
        }

        Single.just(true)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(Boolean aBoolean) {
                        mListener.event(mPrintException);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });

    }

    private void printPage(AbsPrinter printer, int printIndex, PrintPage printPage) throws Exception {
        log("打印前期准备 printPage()");
        // 获取打印机状态
        printer.getStatus();

        // 打印机初始化
        printer.init();

        // 清理打印缓存
        printer.clearCache();

        AbsPrint print = new WiseCashierPrint();
        for (int j = 0; j < printPage.getRows().size(); j++) {
            try {
                print.printRow(mContext, printPage.getRows().get(j));
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }

        // 打印机开始打印
        log("第" + (printIndex + 1) + "联开始打印");
        mPrintException = null;
        printer.start();
        log("第" + (printIndex + 1) + "联开始打印打印结束");
    }

    public interface OnPrintListener {
        void event(PrintException e);
    }

    private static void log(String msg) {
        Log.d(TAG, msg);
    }

    private static void loge(String msg) {
        Log.e(TAG, msg);
    }
}
