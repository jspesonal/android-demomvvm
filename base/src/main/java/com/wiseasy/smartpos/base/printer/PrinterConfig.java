package com.wiseasy.smartpos.base.printer;

import com.wiseasy.smartpos.base.printer.printer.AbsPrinter;

import java.util.List;

/**
 * 打印机配置
 * - 主要包含当前打印机类型，如果是设置过IP打印机，还需要包含设置过的IP打印机配置列表。
 */
public class PrinterConfig {

    /**
     * 打印机类型，详见{@link AbsPrinter}
     */
    private int currentPrinterType;
    /**
     * 已连接过的IP打印机列表
     */
    private List<IPPrinterConfig> ipPrinterConfigs;
    /**
     * 上次连接的IP打印机
     */
    private IPPrinterConfig lastSelectedIpPrinterConfig;

    public PrinterConfig() {
    }

    public int getCurrentPrinterType() {
        return currentPrinterType;
    }

    public void setCurrentPrinterType(int currentPrinterType) {
        this.currentPrinterType = currentPrinterType;
    }

    public List<IPPrinterConfig> getIpPrinterConfigs() {
        return ipPrinterConfigs;
    }

    public void setIpPrinterConfigs(List<IPPrinterConfig> ipPrinterConfigs) {
        this.ipPrinterConfigs = ipPrinterConfigs;
    }

    public IPPrinterConfig getLastSelectedIpPrinterConfig() {
        return lastSelectedIpPrinterConfig;
    }

    public void setLastSelectedIpPrinterConfig(IPPrinterConfig lastSelectedIpPrinterConfig) {
        this.lastSelectedIpPrinterConfig = lastSelectedIpPrinterConfig;
    }

    @Override
    public String toString() {
        switch (currentPrinterType) {
            case AbsPrinter.PRINTER_TYPE_NONE:
                return "未配置任何打印机";
            case AbsPrinter.PRINTER_TYPE_LOCAL:
                return "本机打印机";
            case AbsPrinter.PRINTER_TYPE_BLUETOOTH:
                return "蓝牙打印机";
            case AbsPrinter.PRINTER_TYPE_IP:
                return "IP打印机, " + lastSelectedIpPrinterConfig;
        }
        return "";
    }

    /**
     * IP打印机配置
     */
    class IPPrinterConfig {
        private String name;
        private String ip;
        private String port;
        private int paperWidth;

        public IPPrinterConfig() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getPort() {
            return port;
        }

        public void setPort(String port) {
            this.port = port;
        }

        public int getPaperWidth() {
            return paperWidth;
        }

        public void setPaperWidth(int paperWidth) {
            this.paperWidth = paperWidth;
        }
    }
}
