package com.wiseasy.smartpos.base.printer;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.wiseasy.smartpos.base.R;
import com.wiseasy.smartpos.base.printer.print.BaseAdapter;
import com.wiseasy.smartpos.base.printer.printer.AbsPrinter;
import com.wiseasy.smartpos.base.printer.printer.BluetoothPrinter;
import com.wiseasy.smartpos.base.printer.printer.LocalPrinter;
import com.wiseasy.smartpos.base.util.SPUtil;
import com.wiseasy.smartpos.base.util.ToastUtil;

/**
 * 打印机管理类
 */
public class PrinterManager {

    private static final String TAG = PrinterManager.class.getSimpleName();

    public static PrinterManager getInstance() {
        return Holder.instance;
    }

    private static class Holder {
        private static final PrinterManager instance = new PrinterManager();
    }

    // 当前打印机
    private AbsPrinter mPrinter;
    // 当前打印机配置
    private PrinterConfig printerConfig;

    public AbsPrinter getPrinter(Context context) {
        if (mPrinter == null) {
            createPrinter(context);
        }
        return mPrinter;
    }

    private void createPrinter(final Context context) {
        AbsPrinter.OnPrinterListener listener = new AbsPrinter.OnPrinterListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFail(int errorCode, String errorMsg) {
                loge( "errorCode=" + errorCode + ", errorMsg=" + errorMsg);
                ToastUtil.showShort(context, context.getString(R.string.print_fail) + ":" + errorCode + ", " + errorMsg);
            }
        };

        switch (printerConfig.getCurrentPrinterType()) {
            case AbsPrinter.PRINTER_TYPE_LOCAL:
                mPrinter = new LocalPrinter(context, listener);
                break;
            case AbsPrinter.PRINTER_TYPE_BLUETOOTH:
                mPrinter = new BluetoothPrinter(context, listener);
                break;
            case AbsPrinter.PRINTER_TYPE_IP:
                // TODO: 2019-08-01 尚不支持IP打印机
                loge( "尚不支持IP打印机");
                break;
        }
    }

    public void clearPrinter() {
        mPrinter = null;
        printerConfig = null;
    }

    /**
     * 是否需要打印
     */
    public boolean isNeedPrint(Context context) {
        log( "isNeedPrint");
        String settingPrinter = SPUtil.getSettingPrinter(context);
        if (TextUtils.isEmpty(settingPrinter)) {
            loge( "未配置任何打印机");
            return false;
        } else {
            printerConfig = JSON.parseObject(settingPrinter, PrinterConfig.class);
            log( "当前配置打印机：" + printerConfig);
            if (printerConfig.getCurrentPrinterType() == AbsPrinter.PRINTER_TYPE_NONE) {
                loge( "未配置任何打印机");
                return false;
            } else {
                if (printerConfig.getCurrentPrinterType() == AbsPrinter.PRINTER_TYPE_IP
                        && printerConfig.getLastSelectedIpPrinterConfig() == null) {
                    log( "选择了IP打印机，但是尚未添加或者选择哪个IP打印机");
                    return false;
                }
                return true;
            }
        }
    }

    /**
     * 打印
     *
     * @param context
     * @param mPrintData        打印数据
     * @param mPrintDataAdapter 打印数据适配类
     * @param listener          打印回调
     */
    public void print(Context context, Object mPrintData, BaseAdapter mPrintDataAdapter, PrintIntentService.OnPrintListener listener) {
        log( "print");
        if (isNeedPrint(context)) {
            this.printData = mPrintData;
            this.printDataAdapter = mPrintDataAdapter;
            PrintIntentService.launch(context, listener);
        }
    }

    // 当前打印数据适配类
    private BaseAdapter printDataAdapter;
    // 当前打印数据
    private Object printData;

    public BaseAdapter getPrintDataAdapter() {
        return printDataAdapter;
    }

    public Object getPrintData() {
        return printData;
    }

    private void log(String msg) {
        Log.d(TAG, msg);
    }

    private void loge(String msg) {
        Log.e(TAG, msg);
    }
}
