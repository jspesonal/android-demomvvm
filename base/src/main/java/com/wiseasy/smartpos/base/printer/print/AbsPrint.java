package com.wiseasy.smartpos.base.printer.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;

import com.wiseasy.smartpos.base.printer.PrintException;
import com.wiseasy.smartpos.base.util.BCConvert;
import com.wiseasy.smartpos.base.util.QRUtils;
import com.wiseasy.smartpos.base.util.StringUtils;

/**
 * 关于打印的抽象类
 * 对待打印进行分类处理
 */
public abstract class AbsPrint implements IPrint {

    @Override
    public void printRow(Context context, Row row) throws Throwable {
        switch (row.getType()) {
            case Row.TYPE_IMAGE:
                dealTypeImage(context, row);
                break;
            case Row.TYPE_TEXT:
                dealTypeText(row);
                break;
            case Row.TYPE_NEW_LINE:
                dealTypeNewLine(row);
                break;
            case Row.TYPE_LINE_CONTENT_REPETITION:
                dealTypeLineContentRepetition(row);
                break;
            case Row.TYPE_SUBMIT:
                submitPrint();
                break;
            default:
                throw PrintException.PRINT_FAIL;
        }
    }

    private void dealTypeLineContentRepetition(Row row) throws Throwable {
        printText(StringUtils.leftPadding(row.getContent(), getOneLineByteLength_FontSizeMedium()),
                FontFamily.SONG,
                FontSize.MEDIUM,
                FontStyle.NORMAL,
                FontAlign.LEFT,
                false);
    }

    private void dealTypeNewLine(Row row) throws Throwable {
        int count = Integer.parseInt(row.getContent());
        for (int i = 0; i < count; i++) {
            startNewLine();
        }
    }

    private void dealTypeText(Row row) throws Throwable {

        FontSize fontSize;
        FontStyle fontStyle;
        FontAlign fontAlign;
        int oneLineByteLength;

        switch (row.getFontSize()) {
            case Row.FONGSIZE_SMALL:
                fontSize = FontSize.SMALL;
                oneLineByteLength = getOneLineByteLength_FontSizeSmall();
                break;

            case Row.FONGSIZE_MEDIUM:
                fontSize = FontSize.MEDIUM;
                oneLineByteLength = getOneLineByteLength_FontSizeMedium();
                break;

            case Row.FONGSIZE_LARGE:
                fontSize = FontSize.LARGE;
                oneLineByteLength = getOneLineByteLength_FontSizeLarge();
                break;

            case Row.FONGSIZE_EXTRALARGE:
                fontSize = FontSize.EXTRALARGE;
                oneLineByteLength = getOneLineByteLength_FontSizeExLarge();
                break;
            default:
                fontSize = FontSize.MEDIUM;
                oneLineByteLength = getOneLineByteLength_FontSizeMedium();

                break;
        }
        switch (row.getFontStyle()) {
            case Row.FONTSTYLE_NORMAL:
                fontStyle = FontStyle.NORMAL;
                break;
            case Row.FONTSTYLE_BOLD:
                fontStyle = FontStyle.BOLD;
                break;
            case Row.FONTSTYLE_ITALIC:
                fontStyle = FontStyle.ITALIC;
                break;
            default:
                fontStyle = FontStyle.NORMAL;
                break;
        }

        String content = row.getContent();
        switch (row.getAlign()) {
            case Row.ALIGN_RIGHT:
                fontAlign = FontAlign.RIGHT;
                break;
            case Row.ALIGN_CENTER:
                fontAlign = FontAlign.CENTER;
                break;
            case Row.ALIGN_BOTH:
                fontAlign = FontAlign.BOTH;
                break;
            case Row.ALIGN_LEFT:
                fontAlign = FontAlign.LEFT;
                break;
            case Row.THREE_BISECTION:
                content = threeBisectionFormat(content, oneLineByteLength);
                fontAlign = FontAlign.BOTH;
                break;
            case Row.ALIGN_LEFT_12SIZE:
            case Row.FIVE_BISECTION:
            default:
                fontAlign = FontAlign.LEFT;
                break;
        }
        printText(content, FontFamily.SONG, fontSize, fontStyle, fontAlign, row.getIsNeedChangeLine());
    }

    private String alignBothFormat(String content, int oneLineByteLength) {
        String[] split = content.split(Row.TEXT_SEPARATOR);
        String leftContent = "";
        String rightContent = "";


        if (split != null && split.length == 2) {
            leftContent = split[0];
            rightContent = split[1];
        }
        int left = StringUtils.getContentByteLength(leftContent);
        int right = StringUtils.getContentByteLength(rightContent);
        int total = left + right;
        Log.d("alignBothFormat", "left = " + left + " right = " + right + " total = " + total);
        content = StringUtils.rightAppend(" ", oneLineByteLength - total, leftContent) + rightContent;
        return content;
    }

    private String alignRightFormat(String content, int byteLength) {
        int right = StringUtils.getContentByteLength(content);
        if (right < byteLength) {
            content = StringUtils.rightAppend(" ", byteLength - right, "") + content;
        }
        return content;
    }

    private String alignCenterFormat(String content, int byteLength) {

        int length = StringUtils.getContentByteLength(content);
        int empty = (byteLength - length) / 2;

        content = StringUtils.leftAppend(" ", empty, content);
        content = StringUtils.rightAppend(" ", empty, content);
        return content;
    }

    private String marginLeft12Size(String content) {
        String[] split = content.split(":");
        String leftContent = "";
        String rightContent = "";

        if (split != null && split.length == 2) {
            leftContent = split[0];
            rightContent = split[1];
            int left = StringUtils.getContentByteLength(leftContent);
            content = StringUtils.rightAppend(" ", 12 - left, leftContent) + rightContent;
        }
        return content;
    }

    private String threeBisectionFormat(String content, int oneLineByteLength) {
        String[] split = content.split(":");
        String TAG = "threeBisectionFormat";

        String leftContent = "";
        String middleContent = "";
        String rightContent = "";
        int threeBisection = oneLineByteLength / 3;
        leftContent = split[0].replace(":", "");
        middleContent = split[1].replace(":", "");
        rightContent = split[2].replace(":", "");
        int left = StringUtils.getContentByteLength(leftContent);
        int right = StringUtils.getContentByteLength(rightContent);
        int middle = StringUtils.getContentByteLength(middleContent);
//        int middleLeftLength = left >= 4 ? threeBisection - left + 6 : 1;
//
//        String middleLeftContent = StringUtils.leftAppend(" ", middleLeftLength, middleContent);
//        int middleRightLength = oneLineByteLength - left - StringUtils.getContentByteLength(middleLeftContent) - right;
//        //转换为两等分处理
//        content = leftContent
//                + middleLeftContent + StringUtils.rightAppend(" ", middleRightLength, "")
//                + Row.TEXT_SEPARATOR + rightContent;

        /**
         * 整个字符串平分居中
         */
        int halfLineLength = oneLineByteLength / 2;//一行的一半长度
        int middleLeftSpaceLength = halfLineLength - left - middle / 2;//中间文字左边的空格长度 = 一半长度 - 左边内容长度 -中间内容长度的一半
        int middleRightSpaceLength = halfLineLength - right - middle / 2;//中间文字右边的空格长度 = 一半长度 - 右边内容长度 -中间内容长度的一半
        String middleIncludeLeftContent = StringUtils.leftAppend(" ", middleLeftSpaceLength, middleContent);//中间文字左边填充空格后
        String middleIncludeLeftAndRightContent = middleIncludeLeftContent + StringUtils.rightAppend(" ", middleRightSpaceLength, "");//中间文字填充后边空格后
        content = leftContent
                + Row.TEXT_SEPARATOR
                + middleIncludeLeftAndRightContent + rightContent;

        return BCConvert.full2Half(content);
    }

    private String fiveBisectionFormat(String content, int oneLineByteLength) {
        String[] split = content.split(":");
        int fiveBisection = oneLineByteLength / 5;
        StringBuilder buffer = new StringBuilder();
        //左边对齐
        buffer.append(StringUtils.rightAppend(" ", fiveBisection - StringUtils.getContentByteLength(split[0]), split[0].replace(":", "")));
        for (int i = 1; i < split.length - 1; i++) { //中间居中
            String body = split[i].replace(":", "");
            int length = StringUtils.getContentByteLength(body);
            int middleLength = fiveBisection - length > 3 ? 3 : 1;
            if (length == 0) {
                buffer.append(StringUtils.leftAppend(" ", fiveBisection, ""));
            } else {
                buffer.append(StringUtils.leftAppend(" ", middleLength, body) + StringUtils.rightAppend(" ", fiveBisection - middleLength - length, ""));
            }
        }
        //右边对齐
        buffer.append(StringUtils.leftAppend(" ", fiveBisection - StringUtils.getContentByteLength(split[split.length - 1].replace(":", "")), split[split.length - 1]));
        return buffer.toString();
    }

    private void dealTypeImage(Context context, Row row) throws Throwable {

        Bitmap bitmap = null;
        switch (row.getImageType()) {
            case Row.IMAGETYPE_QR_CODE:
                int w = 280;
                bitmap = QRUtils.createQrcode(row.getContent(), w, w);
                break;
            case Row.IMAGETYPE_BAR_CODE:

                break;
//            case Row.IMAGETYPE_SIGNATURE:
//                bitmap = ElectronicSignatureUtils.get(row.getContent());
//                break;
            case Row.IMAGETYPE_LOGO:
                bitmap = BitmapFactory.decodeResource(context.getResources(), Integer.parseInt(row.getContent()));
                break;
            default:
                throw PrintException.PRINT_FAIL;
        }
        if (bitmap != null) {
            printImage(bitmap, printPosition(row.getAlign()));
        } else {
            if (!TextUtils.isEmpty(row.getContent())) {
                printBarCode(row.getContent());
            }
        }
//        startNewLine();
    }

    private String strToSBC(String input) {

        char c[] = input.toCharArray();
        StringBuilder res = new StringBuilder("");
        boolean space = false;
        int index = 0;
        boolean flag = false;
        boolean isflag = false;
        for (int i = 0; i < c.length; i++) {
            if (c[i] != ' ') {
                if (!flag && (res.toString().contains("扫码预授权撤销") || res.toString().contains("扫码预授权完成"))) {
                    res = new StringBuilder(res.toString().substring(0, res.toString().length() - 1));
                    flag = true;
                }
                if (!isflag && (res.toString().contains("扫码预授权完成") && (res.toString().contains("0")
                        || res.toString().contains("1") || res.toString().contains("2") || res.toString().contains("3")
                        || res.toString().contains("4") || res.toString().contains("5") || res.toString().contains("6") ||
                        res.toString().contains("7") || res.toString().contains("8") || res.toString().contains("9")))) {
                    res.append(' ');
                    res.append(' ');
                    res.append(' ');
                    isflag = true;
                }
                if (index > 0) {
                    res.append(' ');
                }
                res.append(c[i]);
                space = false;
                index = 0;
            } else {

                if (space && index == 1) {
                    res.append('　');
                    index = 0;
                    space = false;
                } else {
                    space = true;
                    index++;
                }
            }
        }
        Log.d("打印数据", res.toString().trim());
        return res.toString().trim();
    }
}
