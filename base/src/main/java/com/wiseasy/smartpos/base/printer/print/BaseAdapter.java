package com.wiseasy.smartpos.base.printer.print;

import android.content.Context;

import com.wiseasy.smartpos.base.printer.PrinterManager;
import com.wiseasy.smartpos.base.printer.printer.BluetoothPrinter;

import java.util.ArrayList;

/**
 * 打印不同实体类的适配器
 */

public abstract class BaseAdapter<T> {
    public abstract ArrayList<PrintPage> getPrintPage(T t);

    public abstract PrintPage createPage(T t, int printIndex);

    /**
     * 打印底部留白
     * - 由于打印机切刀的深浅，需要打印完毕后多打印几行空白，来保证撕掉小票时不会影响内容区域。
     * - Net5打印机的切刀比其他POS较深；
     * - 蓝牙打印机切刀非常浅；
     */
    protected void buildBottomWhiteSpace(ArrayList<Row> rows, Context mContext) {
        // 如果是蓝牙打印机，末尾则只打印1行
        if (PrinterManager.getInstance().getPrinter(mContext) instanceof BluetoothPrinter) {
            rows.add(Row.newLine(1));
        } else {
//            if (DeviceProxy.getInstance().isWISENET5()) {
//                // 如果是NET5，末尾打印5行
//                rows.add(Row.newLine(5));
//            } else if (DeviceProxy.getInstance().isWPOS3X()) {
//                //如果是3X，末尾打印1行
//                rows.add(Row.newLine(1));
//            } else {
            // 如果是其他POS，末尾多打印几行
            rows.add(Row.newLine(4));
//            }
        }
    }
}
