package com.wiseasy.smartpos.base.printer.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.RemoteException;

/**
 * 关于打印的接口
 * 抽象方法、枚举
 */
public interface IPrint {

    void submitPrint() throws  RemoteException;

    void printText(String content, FontFamily fontFamily, FontSize fontSize, FontStyle fontStyle, FontAlign fontAlign, boolean isNeedChangeLine) throws  RemoteException;

    void printImage(Bitmap bm, Object obj) throws RemoteException;

    void printBarCode(String code) throws  RemoteException;

    void startNewLine() throws  RemoteException;

    void printRow(Context context, Row row) throws Throwable;

    Object printPosition(int gravity);

    enum FontFamily {
        SONG;

        private FontFamily() {
        }
    }

    enum FontStyle {
        NORMAL,
        BOLD,
        ITALIC,
        BOTH;//加粗&斜体

        private FontStyle() {
        }
    }

    enum FontSize {
        SMALL,
        MEDIUM,
        LARGE,
        EXTRALARGE;

        private FontSize() {
        }
    }

    enum FontAlign {
        LEFT,
        RIGHT,
        CENTER,
        BOTH;

        private FontAlign() {
        }
    }

    enum PageWidth {
        WIDTH_58,
        WIDTH_80;

        private PageWidth() {
        }
    }


    interface OnEventListener {
        void onEvent(int eventId, String msg);
    }

    int getOneLineByteLength_FontSizeSmall();

    int getOneLineByteLength_FontSizeMedium();

    int getOneLineByteLength_FontSizeLarge();

    int getOneLineByteLength_FontSizeExLarge();


}
