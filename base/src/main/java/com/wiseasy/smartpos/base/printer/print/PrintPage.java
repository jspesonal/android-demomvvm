package com.wiseasy.smartpos.base.printer.print;

import java.io.Serializable;
import java.util.ArrayList;

public class PrintPage implements Serializable {

    public PrintPage() {
        mRows = new ArrayList<>();
    }

    private ArrayList<Row> mRows;

    public ArrayList<Row> getRows() {
        return mRows;
    }

    public void addRow(Row row) {
        mRows.add(row);
    }
}
