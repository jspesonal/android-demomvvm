package com.wiseasy.smartpos.base.printer.print;

import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.IntDef;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 每行的实体类
 */
public class Row implements Serializable {
    public static final int TYPE_IMAGE = 0x01;
    public static final int TYPE_TEXT = 0x02;
    public static final int TYPE_NEW_LINE = 0x03;
    public static final int TYPE_LINE_CONTENT_REPETITION = 0x04;
    public static final int TYPE_SUBMIT = 0x05;

    public static final int ALIGN_LEFT = 0x11;
    public static final int ALIGN_RIGHT = 0x12;
    public static final int ALIGN_CENTER = 0x13;
    public static final int ALIGN_BOTH = 0x14;
    public static final int ALIGN_LEFT_12SIZE = 0x15; //用于两列字体距左边12个单位的内容
    public static final int THREE_BISECTION = 0x16; //用于三列字体 左边靠左 中间等分 右边靠右
    public static final int FIVE_BISECTION = 0x17; //用于五列字体

    public static final int FONGSIZE_SMALL = 0x21;
    public static final int FONGSIZE_MEDIUM = 0x22;
    public static final int FONGSIZE_LARGE = 0x23;
    public static final int FONGSIZE_EXTRALARGE = 0x24;

    public static final int FONTSTYLE_NORMAL = 0x31;
    public static final int FONTSTYLE_BOLD = 0x32;
    public static final int FONTSTYLE_ITALIC = 0x33;

    public static final int IMAGETYPE_QR_CODE = 0x41;
    public static final int IMAGETYPE_BAR_CODE = 0x42;
    public static final int IMAGETYPE_SIGNATURE = 0x43;
    public static final int IMAGETYPE_LOGO = 0x44;

    public static final int NONE = -1;
    public static final int FIRST = 1;
    public static final int REPEAT = 2;

    public static final String TEXT_SEPARATOR = "\r";

    @IntDef({FIRST, REPEAT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface RepeatFlag {
    }


    private int type;
    private int align;
    private String content;
    private int fontSize;
    private int fontStyle;
    private int imageType;
    private boolean isNeedChangeLine = false;

    public int getType() {
        return type;
    }

    public int getAlign() {
        return align;
    }

    public String getContent() {
        return content;
    }

    public int getFontSize() {
        return fontSize;
    }

    public int getFontStyle() {
        return fontStyle;
    }

    public int getImageType() {
        return imageType;
    }

    public boolean getIsNeedChangeLine() {
        return isNeedChangeLine;
    }

    public Row(int type, int align, String content, int fontSize, int fontStyle, int imageType) {
        this.type = type;
        this.align = align;
        this.content = content;
        this.fontSize = fontSize;
        this.fontStyle = fontStyle;
        this.imageType = imageType;
    }

    public Row(int type, int align, String content, int fontSize, int fontStyle, int imageType, boolean isNeedChangeLine) {
        this.type = type;
        this.align = align;
        this.content = content;
        this.fontSize = fontSize;
        this.fontStyle = fontStyle;
        this.imageType = imageType;
        this.isNeedChangeLine = isNeedChangeLine;
    }

    public static Row textLargeNormalLeftRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_LEFT, content, FONGSIZE_LARGE, FONTSTYLE_NORMAL, NONE);
    }

    public static Row textLargeNormalCenterRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_CENTER, content, FONGSIZE_LARGE, FONTSTYLE_NORMAL, NONE);
    }

    public static Row textLargeBoldLeftRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_LEFT, content, FONGSIZE_LARGE, FONTSTYLE_BOLD, NONE);
    }

    public static Row textLargeBoldRightRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_RIGHT, content, FONGSIZE_LARGE, FONTSTYLE_BOLD, NONE);
    }

    public static Row textLargeBoldCenterRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_CENTER, content, FONGSIZE_LARGE, FONTSTYLE_BOLD, NONE);
    }

    public static Row textMediumNormalCenterRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_CENTER, content, FONGSIZE_MEDIUM, FONTSTYLE_NORMAL, NONE);
    }

    public static Row textMediumNormalLeftRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_LEFT, content, FONGSIZE_MEDIUM, FONTSTYLE_NORMAL, NONE);
    }

    /**
     * 需要打印多行的文字，调用此方法(靠左换行)
     *
     * @param content
     * @return
     */
    public static Row textMediumNormalLeftChangeLineRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_LEFT, content, FONGSIZE_MEDIUM, FONTSTYLE_NORMAL, NONE, true);
    }

    /**
     * 需要打印多行的文字，调用此方法(靠右换行)
     *
     * @param content
     * @return
     */
    public static Row textMediumNormalRightChangeLineRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_RIGHT, content, FONGSIZE_MEDIUM, FONTSTYLE_NORMAL, NONE, true);
    }

    /**
     * 需要打印多行的文字，调用此方法(靠右换行加粗)
     *
     * @param content
     * @return
     */
    public static Row textMediumBoldRightChangeLineRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_RIGHT, content, FONGSIZE_MEDIUM, FONTSTYLE_BOLD, NONE, true);
    }

    /**
     * 需要打印多行的文字，调用此方法(居中换行)
     */
    public static Row textMediumNormalCenterChangeLineRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_CENTER, content, FONGSIZE_MEDIUM, FONTSTYLE_NORMAL, NONE, true);
    }

    public static Row textMediumNormalMargin12Row(String content) {
        return new Row(TYPE_TEXT, ALIGN_LEFT_12SIZE, content, FONGSIZE_MEDIUM, FONTSTYLE_NORMAL, NONE);
    }

    public static Row textMediumNormalThreeBisection(String content) {
        return new Row(TYPE_TEXT, THREE_BISECTION, content, FONGSIZE_MEDIUM, FONTSTYLE_NORMAL, NONE);
    }

    public static Row textMediumNormalRightRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_RIGHT, content, FONGSIZE_MEDIUM, FONTSTYLE_NORMAL, NONE);
    }

    public static Row textMediumBoldLeftRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_LEFT, content, FONGSIZE_MEDIUM, FONTSTYLE_BOLD, NONE);
    }

    public static Row textMediumBoldRightRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_RIGHT, content, FONGSIZE_MEDIUM, FONTSTYLE_BOLD, NONE);
    }

    public static Row textMediumBoldCenterRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_CENTER, content, FONGSIZE_MEDIUM, FONTSTYLE_BOLD, NONE);
    }

    public static Row textMediumNormalFiveBisection(String content) {
        return new Row(TYPE_TEXT, FIVE_BISECTION, content, FONGSIZE_MEDIUM, FONTSTYLE_NORMAL, NONE);
    }

    public static Row textSmallNormalLeftRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_LEFT, content, FONGSIZE_SMALL, FONTSTYLE_NORMAL, NONE);
    }

    public static Row textSmallNormalRightRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_RIGHT, content, FONGSIZE_SMALL, FONTSTYLE_NORMAL, NONE);
    }

    public static Row textSmallNormalCenterRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_CENTER, content, FONGSIZE_SMALL, FONTSTYLE_NORMAL, NONE);
    }

    public static Row textSmallNormalThreeBisection(String content) {
        return new Row(TYPE_TEXT, THREE_BISECTION, content, FONGSIZE_SMALL, FONTSTYLE_NORMAL, NONE);
    }

    public static Row textSmallBoldLeftRow(String content) {
        return new Row(TYPE_TEXT, ALIGN_LEFT, content, FONGSIZE_SMALL, FONTSTYLE_BOLD, NONE);
    }

    public static Row textSmallNormalFiveBisection(String content) {
        return new Row(TYPE_TEXT, FIVE_BISECTION, content, FONGSIZE_SMALL, FONTSTYLE_NORMAL, NONE);
    }

    public static Row repeatContentLine(String content) {
        return new Row(TYPE_LINE_CONTENT_REPETITION, NONE, content, NONE, NONE, NONE);
    }

    public static Row newLine(int count) {
        return new Row(TYPE_NEW_LINE, NONE, count + "", NONE, NONE, NONE);
    }

    public static Row imageRow(String content, int imageType) {
        return new Row(TYPE_IMAGE, ALIGN_CENTER, content, NONE, NONE, imageType);
    }

    public static Row imageRowRight(String content, int imageType) {
        return new Row(TYPE_IMAGE, ALIGN_RIGHT, content, NONE, NONE, imageType);
    }

    public static Row textSmallNormalBothAligned(String left, String right) {
        return new Row(TYPE_TEXT, ALIGN_BOTH, left + TEXT_SEPARATOR + right, FONGSIZE_SMALL, FONTSTYLE_NORMAL, NONE);
    }

    public static Row textMediumNormalBothAligned(String left, String right) {
        if (TextUtils.isEmpty(right)) right = " ";
        Log.d("row ----> ", left + TEXT_SEPARATOR + right);
        return new Row(TYPE_TEXT, ALIGN_BOTH, left + TEXT_SEPARATOR + right, FONGSIZE_MEDIUM, FONTSTYLE_NORMAL, NONE);
    }

    public static Row textLargeNormalBothAligned(String left, String right) {
        return new Row(TYPE_TEXT, ALIGN_BOTH, left + TEXT_SEPARATOR + right, FONGSIZE_LARGE, FONTSTYLE_NORMAL, NONE);
    }

    public static Row submit() {
        return new Row(TYPE_SUBMIT, NONE, NONE + "", NONE, NONE, NONE);
    }


}
