package com.wiseasy.smartpos.base.printer.print;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.RemoteException;

import com.google.zxing.BarcodeFormat;
import com.wiseasy.emvprocess.SDKInstance;
import com.wiseasy.smartpos.base.util.QRUtils;

import wangpos.sdk4.libbasebinder.Printer;

/**
 * 建立属于自己的一套规范的Print，也是最终会用到的一个print
 * 文字大小、位置、样式、图片设置等
 */
public class WiseCashierPrint extends AbsPrint {

    private Printer mPrinter = SDKInstance.mPrinter;

    @Override
    public void submitPrint() throws RemoteException {
    }

    @Override
    public void printText(String content, FontFamily fontFamily
            , FontSize fontSize, FontStyle fontStyle
            , FontAlign fontAlign, boolean isNeedChangeLine) throws RemoteException {

        //文字大小
        int fontSize1;
        switch (fontSize) {
            case SMALL:
                fontSize1 = 15;
                break;
            case LARGE:
                fontSize1 = 27;
                break;
            case EXTRALARGE:
                fontSize1 = 30;
                break;
            default:
                fontSize1 = 20;
                break;
        }

        //样式是否加粗、斜体
        boolean isBold;
        boolean isItalic;
        switch (fontStyle) {
            case BOTH:
                isBold = true;
                isItalic = true;
                break;
            case BOLD:
                isBold = true;
                isItalic = false;
                break;
            case ITALIC:
                isBold = false;
                isItalic = true;
                break;
            default:
                isBold = false;
                isItalic = false;
                break;
        }

        //文字位置
        switch (fontAlign) {
            //下面的  Printer.Font.XXX则是设置的字体，目前是Printer.Font.MONOSPACE等宽字体，如果想要宋体，这是为SONG即可
            case RIGHT:
                //printString方法可以自动换行
                if (isNeedChangeLine) {
                    mPrinter.printString(content, fontSize1, Printer.Align.RIGHT, isBold, isItalic);
                } else {
                    mPrinter.printStringExt(content, 0, 0f, 1f, Printer.Font.MONOSPACE,
                            fontSize1, Printer.Align.RIGHT, isBold, isItalic, false);
                }

                break;

            case LEFT:
                //printString方法可以自动换行，目前退款备注需要自动换行
                if (isNeedChangeLine) {
                    mPrinter.printString(content, fontSize1, Printer.Align.LEFT, isBold, isItalic);
                } else {
                    mPrinter.printStringExt(content, 0, 0f, 1f, Printer.Font.MONOSPACE,
                            fontSize1, Printer.Align.LEFT, isBold, isItalic, false);
                }
                break;

            case CENTER:
                //printString方法可以自动换行，目前小票头部商户地址需要自动换行
                if (isNeedChangeLine) {
                    mPrinter.printString(content, fontSize1, Printer.Align.CENTER, isBold, isItalic);
                } else {
                    mPrinter.printStringExt(content, 0, 0f, 1f, Printer.Font.MONOSPACE,
                            fontSize1, Printer.Align.CENTER, isBold, isItalic, false);
                }
                break;

            case BOTH:
                String[] split = content.split(Row.TEXT_SEPARATOR);
                String leftContent = "";
                String rightContent = "";
                if (split.length == 2) {
                    leftContent = split[0];
                    rightContent = split[1];
                }
                mPrinter.print2StringInLine(leftContent, rightContent, 1f, Printer.Font.MONOSPACE, fontSize1, Printer.Align.LEFT, isBold, isItalic, false);
                break;
        }


    }

    @Override
    public void printImage(Bitmap bm, Object obj) throws RemoteException {
//        SDKInstance.mPrinter.printImage(bm, bm.getHeight(), Printer.Align.CENTER);
        mPrinter.printImageBase(bm, bm.getWidth(), bm.getHeight(), (Printer.Align) obj, 0);
        bm.recycle();
    }

    @Override
    public void printBarCode(String code) throws RemoteException {
        Bitmap bm = QRUtils.encodeAsBitmap(code, BarcodeFormat.CODE_128, 50, 50);
        int width = bm.getWidth();
        int height = bm.getHeight();
        Matrix matrix = new Matrix();
        float scale = 320f / width;
        matrix.postScale(scale, scale);
        Bitmap result = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
        if (!bm.isRecycled()) {
            bm.recycle();
        }
        printImage(result, Printer.Align.CENTER);
    }

    @Override
    public void startNewLine() throws RemoteException {
        mPrinter.printString("\r\n", 20, Printer.Align.LEFT, false, false);
    }

    @Override
    public Object printPosition(int gravity) {
        switch (gravity) {
            case Row.ALIGN_LEFT:
                return Printer.Align.LEFT;
            case Row.ALIGN_RIGHT:
                return Printer.Align.RIGHT;
            default:
                return Printer.Align.CENTER;
        }
    }

    @Override
    public int getOneLineByteLength_FontSizeSmall() {
        return 48;
    }

    @Override
    public int getOneLineByteLength_FontSizeMedium() {
        return 32;
    }

    @Override
    public int getOneLineByteLength_FontSizeLarge() {
        return 24;
    }

    @Override
    public int getOneLineByteLength_FontSizeExLarge() {
        return 16;
    }
}
