package com.wiseasy.smartpos.base.printer.printer;

import android.content.Context;

public abstract class AbsPrinter {

    // 打印机类型
    public static final int PRINTER_TYPE_NONE = 0;  // 未配置任何打印机，即不启用打印机
    public static final int PRINTER_TYPE_LOCAL = 1;
    public static final int PRINTER_TYPE_BLUETOOTH = 2;
    public static final int PRINTER_TYPE_IP = 3;

    protected String tag = this.getClass().getSimpleName();
    protected int printType = PRINTER_TYPE_LOCAL;
    protected OnPrinterListener listener;
    protected Context mContext;

    public AbsPrinter(Context mContext, OnPrinterListener listener) {
        this.mContext = mContext;
        this.listener = listener;
    }

    public abstract Result getStatus() throws Exception;

    public abstract Result init() throws Exception ;

    public abstract Result clearCache() throws Exception ;

    public abstract Result start() throws Exception ;

    abstract Result finish() throws Exception ;

    public interface OnPrinterListener {
        void onSuccess();
        void onFail(int errorCode, String errorMsg);
    }
}
