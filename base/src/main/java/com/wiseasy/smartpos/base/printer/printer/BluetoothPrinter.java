package com.wiseasy.smartpos.base.printer.printer;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;

import com.wiseasy.emvprocess.SDKInstance;
import com.wiseasy.smartpos.base.R;
import com.wiseasy.smartpos.base.printer.PrintException;

import wangpos.sdk4.base.ICommonCallback;

/**
 * 蓝牙打印机
 */
public class BluetoothPrinter extends AbsPrinter {

    public BluetoothPrinter(Context mContext, OnPrinterListener listener) {
        super(mContext, listener);
        printType = PRINTER_TYPE_LOCAL;
    }

    @Override
    public Result getStatus() throws Exception {
        return new Result(0);
    }

    @Override
    public Result init() throws Exception {
        log( "init");
        /*
            蓝牙打印机初始化
            - 注意此处写法，一定要new ICommonCallback.Stub()，而不要new ICommonCallback()，SDK的问题。
         */
        ICommonCallback callback = new ICommonCallback.Stub() {
            @Override
            public int CommonCallback(int status) throws RemoteException {
                log( "打印机状态回调, status=" + status);
                switch (status) {
                    case 0:
                        log( "正常");
                        break;
                    case 1:
                        // 连接错误
                        listener.onFail(status, mContext.getString(R.string.print_connect_error));
                        break;
                    case 2:
                        // 缺纸
                        listener.onFail(status, mContext.getString(R.string.print_lack_paper));
                        break;
                    case 3:
                        // 脱机
                        listener.onFail(status, mContext.getString(R.string.print_offline));
                        break;
                    case 4:
                        // 打印对象未连接
                        listener.onFail(status, mContext.getString(R.string.print_unconect));
                        break;
                    case 10:
                        // 打印结束
                        try {
                            log( mContext.getString(R.string.print_finish));
//                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        // 未知状态
                        listener.onFail(status, mContext.getString(R.string.print_unknow_error));
                        break;
                }
                return 0;
            }
        };
        log( "init|initBlueToothPrint");
        int result = SDKInstance.mPrinter.initBlueToothPrint(callback);
        log( "init|initBlueToothPrint, result=" + result);
//        result = SDKInstance.mPrinter.setPrintPaperType(SPUtil.getSettingPrinterPaperType());//baibai
        result = SDKInstance.mPrinter.setPrintPaperType(0);
        log( "init|setPrintPaperType, result=" + result);

        if (result != 0 && result != 10) {
            String msg;
            switch (result) {
//                case 0:
//                    msg = "成功";
//                    break;
                case 1:
                    // 蓝牙未开启
                    msg = mContext.getString(R.string.print_bluetooth_off);
                    break;
                case 2:
                    // 无可连接设备
                    msg = mContext.getString(R.string.print_no_device);
                    break;
                case 3:
                    // 设备连接失败
                    msg = mContext.getString(R.string.print_device_connect_fail);
                    break;
                case 5:
                    // 发生异常
                    msg = mContext.getString(R.string.print_error);
                    break;
                default:
                    msg = mContext.getString(R.string.print_unknow_error) + ", " + result;
                    break;
            }
            throw new PrintException(result, msg);
        }
        return new Result(result);
    }

    @Override
    public Result clearCache() throws Exception {
        log( "clearPrintDataCache");
        int result = SDKInstance.mPrinter.clearPrintDataCache();
        log( "clearPrintDataCache, result=" + result);
        return new Result(result);
    }

    @Override
    public Result start() throws Exception {
        log( "startBlueToothPrint");
        int result = SDKInstance.mPrinter.startBlueToothPrint();
        log( "startBlueToothPrint, result=" + result);
        if (result == 0) {
            finish();
        }
        return new Result(result);
    }

    @Override
    Result finish() throws Exception {
        log( "finishBlueToothPrint");
        int result = SDKInstance.mPrinter.finishBlueToothPrint();
        log("finishBlueToothPrint, result=" + result);
        return new Result(result);
    }
    
    private void log(String msg) {
        Log.d(tag, msg);
    }
}