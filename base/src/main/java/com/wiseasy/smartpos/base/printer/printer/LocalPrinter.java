package com.wiseasy.smartpos.base.printer.printer;

import android.content.Context;
import android.util.Log;

import com.wiseasy.emvprocess.SDKInstance;
import com.wiseasy.smartpos.base.R;
import com.wiseasy.smartpos.base.printer.PrintException;

/**
 * POS内置打印机
 */
public class LocalPrinter extends AbsPrinter {

    public LocalPrinter(Context mContext, OnPrinterListener listener) {
        super(mContext, listener);
        printType = PRINTER_TYPE_LOCAL;
    }

    @Override
    public Result getStatus() throws Exception {
        int[] status = new int[1];
        int result = SDKInstance.mPrinter.getPrinterStatus(status);
        if (result == 0) {
            if (status[0] != 0) {
                String msg = "";
                msg = getPrintStatusMsg(status[0]);
                switch (status[0]) {
//                    case 0x00:
//                        msg = "打印机正常";
//                        break;
                    case 0x01:
                        // 参数错误
                        msg = mContext.getString(R.string.print_parameter_error);
                        break;
                    case 0x06:
                        // 不可执行
                        msg = mContext.getString(R.string.print_cannot_exe);
                        break;
                    case 0x8A:
                        // 缺纸
                        msg = mContext.getString(R.string.print_lack_paper);
                        break;
                    case 0x8B:
                        // 过热
                        msg = mContext.getString(R.string.print_over_hot);
                        break;
                    default:
                        msg = mContext.getString(R.string.print_unknow_error);
                        break;
                }

                throw new PrintException(status[0], msg);
            }
        } else {
            throw new PrintException(result, PrintException.ERROR_MSG_DEFAULT);
        }
        return new Result(result, "", status[0], "");
    }

    @Override
    public Result init() throws Exception {
        int result = SDKInstance.mPrinter.printInit();
        return new Result(result);
    }

    @Override
    public Result clearCache() throws Exception {
        int result = SDKInstance.mPrinter.clearPrintDataCache();
        return new Result(result);
    }

    @Override
    public Result start() throws Exception {
        // 开始打印，同步返回结果
//        SDKInstance.mPrinter.setPrintLineSpacing(0);//设置行间距
        int result = SDKInstance.mPrinter.printPaper(50);
        Log.i("LocalPrinter", "start result:" + result);
        if (result != 0) {
            String msg = getPrintStatusMsg(result);
            throw new PrintException(result, msg);
        }
        // POS内置打印机，调用打印方法，同步返回打印结果，所以，在收到结果后自动调用打印完成方法
        finish();
        return new Result(result);
    }

    @Override
    Result finish() throws Exception {
        int result = SDKInstance.mPrinter.printFinish();
        Log.i("LocalPrinter", "finish: result:" + result);
        if (result != 0) {
            String msg = getPrintStatusMsg(result);
            throw new PrintException(result, msg);
        }
        return new Result(result);
    }

    private String getPrintStatusMsg(int code) {
        String msg = "";
        switch (code) {
//                    case 0x00:
//                        msg = "打印机正常";
//                        break;
            case 0x01:
                // 参数错误
                msg = mContext.getString(R.string.print_parameter_error);
                break;
            case 0x06:
                // 不可执行
                msg = mContext.getString(R.string.print_cannot_exe);
                break;
            case 0x8A:
                // 缺纸
                msg = mContext.getString(R.string.print_lack_paper);
                break;
            case 0x8B:
                // 过热
                msg = mContext.getString(R.string.print_over_hot);
                break;
            default:
                msg = mContext.getString(R.string.print_unknow_error);
                break;
        }
        return msg;
    }
}