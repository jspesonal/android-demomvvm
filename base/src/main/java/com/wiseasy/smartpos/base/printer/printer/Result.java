package com.wiseasy.smartpos.base.printer.printer;

public class Result {

    public int resultCode = 0;
    public String resultMsg = "";

    public int statusCode = 0;
    public String statusMsg = "";

    public Result() {
    }

    public Result(int resultCode) {
        this(resultCode, "");
    }

    public Result(int resultCode, String resultMsg) {
        this(resultCode, "", 0, "");
    }

    public Result(int resultCode, String resultMsg, int statusCode, String statusMsg) {
        this.resultCode = resultCode;
        this.resultMsg = resultMsg;
        this.statusCode = statusCode;
        this.statusMsg = statusMsg;
    }
}
