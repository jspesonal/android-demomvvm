package com.wiseasy.smartpos.base.router

import android.util.Log
import com.wiseasy.cashier.base.kotlin.to

/**
 * Created by Bella on 2021/12/20.
 * 路由服务，暂时只有组件间通信的service
 */
class Router private constructor() {
    private val TAG = "Router"

    companion object {
        fun getInstance() = Holder.instance
    }

    private object Holder {
        val instance = Router()
    }

    private val services: HashMap<Class<*>, Any> = HashMap()

    fun addService(name: Class<*>, serviceImpl: Any) {
        if (name == null || serviceImpl == null) {
            return
        }

        Log.i(TAG, "addService: ")
        services.put(name, serviceImpl)
    }

    fun <T> getService(name: Class<T>): T? {
        if (name == null) {
            return null
        }
        return services[name]?.to()
    }
}