package com.wiseasy.smartpos.base.scanner;

public interface DecodeListener {
    void onDecode(String result);
}
