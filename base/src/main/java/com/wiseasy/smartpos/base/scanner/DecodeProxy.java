package com.wiseasy.smartpos.base.scanner;


import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class DecodeProxy implements DecodeListener {
    private String TAG = "DecodeProxy ->";

    private DecodeListener mDecodeListener;
    private IDecoder mDecoder = DecoderFactory.create();
    private Handler mMainHandler = new Handler(Looper.getMainLooper());
    private ThreadPoolExecutor mThreadPoolExecutor = new ThreadPoolExecutor(1, 1, 1000, TimeUnit.MILLISECONDS, new SynchronousQueue());

    public DecodeProxy(DecodeListener mDecodeListener) {
        this.mDecodeListener = mDecodeListener;
    }

    @Override
    public void onDecode(final String result) {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mDecodeListener.onDecode(result);
            }
        });
    }

    public boolean aidlUsed() {
        return mDecoder.aidlUsed();
    }

    public void onStart() {
        mDecoder.onStart();
    }

    public void decode(final byte[] bytes, final int width, final int height) {
        if (mDecoder.isDecoding()) {
            return;
        }
        try {
            Log.d(TAG, "解码：线程池->开启解码线程...");
            mThreadPoolExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    mDecoder.decode(bytes, width, height, DecodeProxy.this);
                }
            });
        } catch (RejectedExecutionException e) {
            Log.d(TAG, "解码中...");
        } catch (Exception ex) {
            Log.d(TAG, "解码中，发生其他错误...");
        }
    }

}
