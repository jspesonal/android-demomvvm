package com.wiseasy.smartpos.base.scanner;

import android.util.Log;

import cn.weipass.pos.sdk.impl.WeiposImpl;


public class DecoderFactory {

    private static String TAG = "DecoderFactory -> ";

    public static IDecoder create() {
        boolean enhanced = enhancedActive();
         if (enhanced) {
             //目前不支持高级算法 --2021.12.16
            Log.i(TAG, "高级解码算法已激活，使用高级解码算法");
            return new DecoderImplEnhanced();
        } else {
            Log.i(TAG, "高级解码算法未激活，使用Zxing算法");
            return new DecoderImplZxing();
        }
    }

    private static boolean enhancedActive() {
        try {
            return WeiposImpl.as().openScanner().initScanpEngine();
        } catch (Exception e) {
            Log.e(TAG, "调用WangUI SDK获取高级解码算法状态时发生异常");
            return false;
        }
    }

}
