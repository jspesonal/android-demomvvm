package com.wiseasy.smartpos.base.scanner;

import android.util.Log;

import cn.weipass.pos.sdk.Scanner;
import cn.weipass.pos.sdk.impl.WeiposImpl;


public class DecoderImplEnhanced implements IDecoder, Scanner.OnResultListener {

    private Scanner mScanner = WeiposImpl.as().openScanner();
    private DecodeListener mDecodeListener;

    public DecoderImplEnhanced() {
        mScanner.setOnResultListener(this);
    }

    @Override
    public boolean aidlUsed() {
        return true;
    }

    @Override
    public boolean isDecoding() {
        return false;
    }

    @Override
    public void onStart() {
        mScanner.setOnResultListener(this);
    }

    @Override
    public void onResult(int i, String s) {
        if (i == 0 && s != null) {
            mDecodeListener.onDecode(s);
            mScanner.destoryEngine();
        }
    }

    @Override
    public void decode(byte[] bytes, int width, int height, DecodeListener decodeListener) {
        this.mDecodeListener = decodeListener;
        Log.d("DecoderImplEnhanced -> ", "扫码：开始解码 -> " + (width - height));
        mScanner.doDecode(bytes, width, height);
    }
}
