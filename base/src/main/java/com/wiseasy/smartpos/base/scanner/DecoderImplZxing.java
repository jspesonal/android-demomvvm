package com.wiseasy.smartpos.base.scanner;

import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;


public class DecoderImplZxing implements IDecoder {

    private String TAG = "DecoderImplZxing -> ";

    private MultiFormatReader mMultiFormatReader = new MultiFormatReader();
    private boolean mDecoding = false;

    public DecoderImplZxing() {
        List<BarcodeFormat> list = new ArrayList<>();
        list.add(BarcodeFormat.CODE_128);
        list.add(BarcodeFormat.QR_CODE);

        Hashtable<DecodeHintType, Object> hints = new Hashtable<>();
        hints.put(DecodeHintType.CHARACTER_SET, "utf-8");
        hints.put(DecodeHintType.POSSIBLE_FORMATS, list);

        mMultiFormatReader.setHints(hints);
    }

    @Override
    public boolean isDecoding() {
        return mDecoding;
    }

    @Override
    public boolean aidlUsed() {
        return false;
    }

    @Override
    public void onStart() {

    }


    @Override
    public void decode(byte[] bytes1, int width1, int height1, DecodeListener decodeListener) {
        if (mDecoding) {
            return;
        }
        try {
            mDecoding = true;
            Log.d(TAG, "扫码：" + width1 + "**" + height1);
            long step0 = System.currentTimeMillis();

            int step = width1 / 3; //12
            int start = step;
            int width = step;
            int height = height1;
            Log.d(TAG, "扫码：新数组大小：" + width + "**" + height);
            byte[] bytes = new byte[(width * height)];

            for (int i = 0; i < height; i++) {
                System.arraycopy(bytes1, i * width1 + start, bytes, i * width, width);
            }
            long step00 = System.currentTimeMillis();
            Log.d(TAG, "扫码：数组Copy耗时：" + (step00 - step0));

            String result;
            byte[] rotatedData = new byte[bytes.length];
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    rotatedData[x * height + height - y - 1] = bytes[x + y * width];
                }
            }
            long step1 = System.currentTimeMillis();
            Log.d(TAG, "扫码：数组变换耗时：" + (step1 - step00));
            int newWidth = height;
            int newHeight = width;

            result = realDecode(rotatedData, newWidth, newHeight);

            if (result == null) {
                result = realDecode(bytes, width, height);
            }

            if (result != null) {
                decodeListener.onDecode(result);
            }
        } catch (Exception e) {
            Log.d(TAG, "扫码结果：解码异常：" + e.getMessage());
        } finally {
            mDecoding = false;
        }
    }

    private String realDecode(byte[] bytes, int width, int height) {
        try {
            long start = System.currentTimeMillis();

            PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(bytes, width, height, 0, 0,
                    width, height, false);

            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            long step2 = System.currentTimeMillis();
            Log.d(TAG, "扫码：对象创建耗时：" + (step2 - start));
            Log.d(TAG, "扫码中，开始解码");
            Result rawResult = mMultiFormatReader.decodeWithState(bitmap);

            if (rawResult != null) {
                long end = System.currentTimeMillis();
                Log.d("扫码中", "Found barcode (" + (end - start) + " ms):\n" + rawResult.toString());
                Log.d(TAG, "扫码耗时：" + (end - start));
                return rawResult.getText();
            } else {
                Log.d(TAG, "扫码中:解码失败");
                return null;
            }
        } catch (ReaderException re) {
            // continue
            Log.d(TAG, "扫码结果：解码失败：" + re.getMessage());
            return null;
        } finally {
            mMultiFormatReader.reset();
        }
    }

}
