package com.wiseasy.smartpos.base.scanner;

public interface IDecoder {
    boolean aidlUsed();

    void onStart();

    boolean isDecoding();

    void decode(byte[] bytes, int width, int height, DecodeListener decodeListener);
}
