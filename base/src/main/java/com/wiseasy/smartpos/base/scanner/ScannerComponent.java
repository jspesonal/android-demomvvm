package com.wiseasy.smartpos.base.scanner;

import android.app.Activity;
import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;


import com.wiseasy.smartpos.base.R;
import com.wiseasy.smartpos.base.util.ToastUtil;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ScannerComponent implements SurfaceHolder.Callback, DecodeListener {

    private String TAG = "ScannerComponent -> ";

    private final SurfaceView mSurfaceView;
    private final DecodeListener mDecodeListener;
    private final boolean mDisposable;

    private final int MSG_TYPE_AUTO_FOCUS = 0x01;

    private boolean mDecodeSuccess = false;
    private Camera camera = null;
    private final Context mContext;
    private final SurfaceHolder mSurfaceHolder;
    private final DecodeProxy mDecodeProxy = new DecodeProxy(this);
    private boolean mReadyToDecode = false;
    private Point mBestSize = null;

    private final Handler mAutoFocusHandler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == MSG_TYPE_AUTO_FOCUS && (camera != null)) {
                camera.autoFocus(mAutoFocusCallback);
            }
            return true;
        }
    });

    private final Camera.AutoFocusCallback mAutoFocusCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            mAutoFocusHandler.sendMessageDelayed(Message.obtain(mAutoFocusHandler, MSG_TYPE_AUTO_FOCUS), 1800L);
        }
    };

    private final Camera.PreviewCallback previewCallback = new Camera.PreviewCallback() {
        @Override
        public void onPreviewFrame(byte[] bytes, Camera camera) {
            if (camera != null) {
                if (mReadyToDecode) {
                    Camera.Parameters parameters = camera.getParameters();
                    if (parameters != null) {
                        Camera.Size si = parameters.getPreviewSize();
                        if (bytes != null && si != null) {
                            mDecodeProxy.decode(bytes, si.width, si.height);
                        }
                    }
                } else {
                    camera.stopPreview();
                }
            }
        }
    };

    public ScannerComponent(SurfaceView surfaceView, DecodeListener decodeListener) {
        this(surfaceView, decodeListener, true);
    }

    public ScannerComponent(SurfaceView surfaceView, DecodeListener decodeListener, boolean disposable) {
        this.mSurfaceView = surfaceView;
        this.mDecodeListener = decodeListener;
        this.mDisposable = disposable;
        mContext = mSurfaceView.getContext();
        mSurfaceHolder = mSurfaceView.getHolder();

        mSurfaceHolder.addCallback(this);
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void onDecode(String result) {
        String realResult = result;
//        val resultArray = result.split(",")
//        if (resultArray.isEmpty()){
//            return
//        } else {
//            realResult = resultArray[0]
//        }

        if (mDisposable && !mDecodeSuccess) {
            //一次性解码：解码之后不再继续解码
            mDecodeSuccess = true;
            Log.i(TAG, "解码结果：" + result);
            stopDecode();
            mDecodeListener.onDecode(realResult);
        } else if (mDisposable && mDecodeSuccess) {
            //一次性解码：解码之后不再处理解码结果（Zxing出现过十毫秒级连续两次解码）
            Log.i(TAG, "已经处理过解码结果，本次解码结果被忽略：" + result);
        } else {
            Log.i(TAG, "解码结果：" + result);
            mDecodeListener.onDecode(realResult);
        }
    }

    public void onStart(Context context) {
        mDecodeProxy.onStart();
        if (camera == null) {
            return;
        }
        camera.setPreviewCallback(previewCallback);
        camera.autoFocus(mAutoFocusCallback);
        camera.startPreview();
    }

    private int getRealSize(int n) {
        return ((n - 1) / 16 + 1) * 16;
    }

    public void onStop(Context context) {
        if (camera == null) {
            return;
        }
        camera.setPreviewCallback(null);
        camera.stopPreview();
    }

    public void onDestroy(Context context) {
        stopDecode();
        mAutoFocusHandler.removeMessages(MSG_TYPE_AUTO_FOCUS);
        releaseCamera();
    }

    public void startDecode() {
        if (camera != null) {
            camera.setPreviewCallback(previewCallback);
            camera.startPreview();
            camera.autoFocus(mAutoFocusCallback);
        }
        mDecodeSuccess = false;
        mReadyToDecode = true;
    }

    public void stopDecode() {
        mReadyToDecode = false;
        mDecodeSuccess = true;
        if (camera != null) {
            camera.setPreviewCallback(null);
            camera.stopPreview();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "扫码：surfaceChanged");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "扫码：surfaceDestroyed");
        releaseCamera();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "扫码：surfaceCreated：" + mSurfaceView.getContext().getPackageName());
        onCreate();
    }

    private void onCreate() {
        initCamera();
    }

    public void initCamera() {
        if (camera == null) {
            try {
                camera = Camera.open(0);
            } catch (Exception e) {
                //无法打开相机,请检查系统相机是否可用，或检查权限
                ToastUtil.showShort(mContext, mContext.getString(R.string.mobile_scan_camera_can_not_open));
                if (mContext instanceof Activity) {
                    ((Activity) mContext).finish();
                }
                return;
            }

        }
        if (camera != null) {
            try {
                Camera.Parameters parameters = camera.getParameters();
                List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
                WindowManager manager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
                Display display = manager.getDefaultDisplay();
                Point screenResolution = new Point(display.getWidth(), display.getHeight());
                Point screenResolutionForCamera = new Point();
                screenResolutionForCamera.x = screenResolution.x;
                screenResolutionForCamera.y = screenResolution.y;
                if (screenResolution.x < screenResolution.y) {
                    screenResolutionForCamera.x = screenResolution.y;
                    screenResolutionForCamera.y = screenResolution.x;
                }
                Point pointForPreview = findBestPreviewSizeValue(supportedPreviewSizes, screenResolutionForCamera);
                parameters.setPreviewSize(pointForPreview.x, pointForPreview.y);
                //                parameters.setPreviewSize(1280, 720);
                parameters.setPreviewFormat(ImageFormat.NV21);
//                parameters.whiteBalance = Camera.Parameters.WHITE_BALANCE_AUTO
                parameters.setFlashMode(Camera.Parameters.FOCUS_MODE_AUTO);
                camera.setPreviewDisplay(mSurfaceHolder);
                camera.setParameters(parameters);
                // 设置预览方向
                // 如果是横屏（TAB），摄像头预览方向不用转动，
                // 如果是竖屏，摄像头预览方向顺时针旋转90度
//                if (DeviceProxy.getInstance().isWPOSTAB()) {
//                    camera.setDisplayOrientation(0);
//                } else {
                    camera.setDisplayOrientation(90);
//                }
                //开始预览
//                camera.setPreviewCallbackWithBuffer(previewCallback)
                camera.setPreviewCallback(previewCallback);
                camera.startPreview();
//                camera.autoFocus(mAutoFocusCallback);
                mAutoFocusHandler.sendMessageDelayed(Message.obtain(mAutoFocusHandler, MSG_TYPE_AUTO_FOCUS), 1800L);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }

        }
    }

    private Point findBestPreviewSizeValue(List<Camera.Size> sizes, Point screenResolution) {
        int bestX = screenResolution.x;
        int bestY = screenResolution.y;
        double screenRatio = screenResolution.x * 1.0 / screenResolution.y;

        Collections.sort(sizes, new Comparator<Camera.Size>() {//从大到小排序
            @Override
            public int compare(Camera.Size o1, Camera.Size o2) {
                return o2.width - o1.width;
            }
        });

        double diff = Double.MAX_VALUE;

        for (int i = 0; i < sizes.size(); i++) {
            Camera.Size size = sizes.get(i);
            if (size.width < size.height ||
                    (mDecodeProxy.aidlUsed() && size.width * size.height > 1024 * 1024 / 2)) {//如果byte[]大于1M，aidl会调用失败(按1024*1024貌似不对，1280*720依然超限)
                continue;
            }
            int newX = size.width;
            int newY = size.height;
            double ratio = newX * 1.0 / newY;
            double newDiff = Math.abs(screenRatio - ratio);
            if (newDiff == 0.0) {
                bestX = newX;
                bestY = newY;
                break;
            } else if (newDiff < diff) {
                bestX = newX;
                bestY = newY;
                diff = newDiff;
            }
        }

        mBestSize = new Point(bestX, bestY);
        return mBestSize;
    }

    public void releaseCamera() {
        if (camera != null) {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }


}
