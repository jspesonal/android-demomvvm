package com.wiseasy.smartpos.base.speech


interface ISpeaker {

     fun speak(content: String, listener: SpeakCompleteListener)

     fun stop()

}