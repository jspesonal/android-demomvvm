package com.wiseasy.smartpos.base.speech;

import android.annotation.SuppressLint;
import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

public class LocalTextSpeaker implements ISpeaker {

    private static final String TAG = "LocalTextSpeaker";

    private TextToSpeech textToSpeech;
    private Context context;
    private boolean inited = false;

    public LocalTextSpeaker(Context context) {
        this.context = context;
        init();
    }

    private void init() {
        textToSpeech = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @SuppressLint("NewApi")
            @Override
            public void onInit(int status) {

                Log.d(TAG, "init status " + status);

                if (TextToSpeech.SUCCESS != status) {
                    return;
                }

                Locale aDefault = Locale.getDefault();
                //setLanguage会触发对应语音包的下载
                int result = textToSpeech.setLanguage(aDefault);

                if (TextToSpeech.LANG_MISSING_DATA == result || TextToSpeech.LANG_NOT_SUPPORTED == result) {
                    Log.d(TAG, "language not support");
                    return;
                } else {
                    inited = true;
                    Log.d(TAG, "init language " + aDefault.getDisplayName() + "success");
                }
            }
        });
    }

    @Override
    public void speak(@NotNull final String content, @NotNull final SpeakCompleteListener listener) {

        Log.d(TAG, "speak|content=" + content);

        if (!inited) {
            Log.d(TAG, "TextToSpeech is not initialized");
            return;
        }

        if (textToSpeech.isSpeaking()) {
            return;
        }

        textToSpeech.speak(content, TextToSpeech.QUEUE_FLUSH, null, String.valueOf(System.currentTimeMillis()));

        textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {

            @Override
            public void onStart(String utteranceId) {
            }

            @Override
            public void onDone(String utteranceId) {
                Log.d(TAG, "speak onDone");
                if (listener != null) {
                    listener.onDone();
                }
            }

            @Override
            public void onError(String utteranceId) {
                Log.d(TAG, "speak onError");
                if (listener != null) {
                    listener.onError();
                }
            }
        });
    }

    @Override
    public void stop() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }
}
