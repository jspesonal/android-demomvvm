package com.wiseasy.smartpos.base.speech

interface SpeakCompleteListener {

    fun onDone()

    fun onError()
}