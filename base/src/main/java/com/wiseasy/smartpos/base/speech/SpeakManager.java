package com.wiseasy.smartpos.base.speech;

import android.content.Context;

/**
 * @author hongge
 * @Date
 * @Note
 */
public class SpeakManager {

    public static SpeakManager getInstance(){
        return Holder.speakManager;
    }

    private static class Holder{
        private static SpeakManager speakManager = new SpeakManager();
    }

    private LocalTextSpeaker textSpeaker;
    private Context context;

    public void init(Context ctx){
        this.context = ctx.getApplicationContext();

        if(textSpeaker != null){
            textSpeaker.stop();
            textSpeaker = null;
        }

        textSpeaker = new LocalTextSpeaker(context);
    }

    private void checkInit(){
        if(context == null){
            throw new RuntimeException("Please initialize first");
        }
    }

    public void speak(String msg){
        checkInit();
        textSpeaker.speak(msg, null);
    }
}