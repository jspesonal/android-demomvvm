package com.wiseasy.smartpos.base.util

import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Camera
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.graphics.drawable.AnimationDrawable
import android.view.animation.*


/**
 * 关于动画的类
 */

//平移动画常量
val TRANS_ENTER_FROM_LEFT = 0//平移动画左侧进入
val TRANS_ENTER_FROM_RIGHT = 1//平移动画右侧进入
val TRANS_EXIT_TO_LEFT = 2//平移动画退出到左侧
val TRANS_EXIT_TO_RIGHT = 3//平移动画退出到右侧


/**
 * 控件的左右平移动画
 */
fun transAnim(context: Context, view: View, transType: Int) {
    val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val width = windowManager.defaultDisplay.width.toFloat()
    val animator: ObjectAnimator
    animator = when (transType) {
        TRANS_ENTER_FROM_LEFT -> ObjectAnimator.ofFloat(view, "translationX", -width, 0f)
        TRANS_ENTER_FROM_RIGHT -> ObjectAnimator.ofFloat(view, "translationX", width, 0f)
        TRANS_EXIT_TO_LEFT -> ObjectAnimator.ofFloat(view, "translationX", 0f, -width)
        TRANS_EXIT_TO_RIGHT -> ObjectAnimator.ofFloat(view, "translationX", 0f, width)
        else -> ObjectAnimator.ofFloat(view, "translationX", 0f, -width)
    }
    animator.duration = 500
    animator.start()
}

/**
 * 控件的上下平移动画
 */
fun transAnimViewVertical(context: Context, view: View, fromLocation: Float, toLocation: Float) {
    val animator = ObjectAnimator.ofFloat(view, "translationY", fromLocation, toLocation)
    animator.duration = 1000
    animator.start()
}

/**
 * 帧动画
 */
fun drawableAnim(imageView: ImageView, drawableId: Int) {
    imageView.setBackgroundResource(drawableId)
    val mAnimationDrawable = imageView.getBackground() as AnimationDrawable
    mAnimationDrawable.start()
}

/**
 * 渐现动画
 */
fun showAnimation(view: View, duration: Long) {
    if (duration < 0) {
        return
    }

    val showAnimation = AlphaAnimation(0.0f, 1.0f)
    showAnimation.duration = duration
    showAnimation.fillAfter = true
    showAnimation.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(animation: Animation?) {
        }

        override fun onAnimationEnd(animation: Animation?) {
        }

        override fun onAnimationStart(animation: Animation?) {
            view.visibility = View.VISIBLE
        }

    })
    view.animation = showAnimation
    view.startAnimation(showAnimation)
}

/**
 * 旋转动画
 */
class RotateAnim : Animation {

    //对称轴
    val ROTATE_AXIS_X = "X"
    val ROTATE_AXIS_Y = "Y"
    val ROTATE_AXIS_Z = "Z"

    private var mRotateAxis: String//旋转中心轴
    private var mInterpolator: Interpolator//插值器
    private var mDegrees: Int//旋转角度
    private val mDuration: Long
    private var centerX: Int = 0
    private var centerY: Int = 0
    private var camera = Camera()

    /**
     * 默认绕Y轴顺时针线性旋转360度，时间为1000毫秒
     */
    constructor(
        axis: String = "Y", duretion: Long = 1000, interpolator: Interpolator = LinearInterpolator(), degrees: Int = -360
    ) {
        mRotateAxis = axis
        mInterpolator = interpolator
        mDuration = duretion
        mDegrees = degrees
    }

    override fun initialize(width: Int, height: Int, parentWidth: Int, parentHeight: Int) {
        super.initialize(width, height, parentWidth, parentHeight)
        //获取中心点坐标
        centerX = width / 2
        centerY = height / 2
        repeatCount = Animation.INFINITE
        duration = mDuration
        interpolator = mInterpolator
    }

    override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
        val matrix = t.matrix
        camera.save()
        //中心是绕Y轴旋转  这里可以自行设置X轴 Y轴 Z轴
        when (mRotateAxis) {
            ROTATE_AXIS_X -> camera.rotateX(mDegrees * interpolatedTime)
            ROTATE_AXIS_Y -> camera.rotateY(mDegrees * interpolatedTime)
            ROTATE_AXIS_Z -> camera.rotateZ(mDegrees * interpolatedTime)
        }
        //把我们的摄像头加在变换矩阵上
        camera.getMatrix(matrix)
        //设置翻转中心点
        matrix.preTranslate((-centerX).toFloat(), (-centerY).toFloat())
        matrix.postTranslate(centerX.toFloat(), centerY.toFloat())
        camera.restore()
    }
}