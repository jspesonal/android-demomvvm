package com.wiseasy.smartpos.base.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import java.util.Locale;

public class CommonUtil {

    private static String TAG = "CommonUtil";

    public static String getPackageName(Context context) {
        return context.getApplicationContext().getPackageName();
    }

    public static String getVersionName(Context context) {
        PackageInfo packageInfo = getPackageInfo(context);
        return packageInfo == null ? "" : packageInfo.versionName;
    }

    public static int getVersionCode(Context context) {
        PackageInfo packageInfo = getPackageInfo(context);
        return packageInfo == null ? -1 : packageInfo.versionCode;
    }

    /**
     * 获取应用包信息
     * - 不能直接通过 BuildConfig来获取VersionName和VersionCode，可能会为空或者非app包名
     *
     * @param context 上下文
     * @return PackageInfo
     */
    public static PackageInfo getPackageInfo(Context context) {
        try {
            return context.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(context.getApplicationContext().getPackageName(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 中文   语言:zh 国家:cn
     * 英文   语言:en 国家:us
     * 法文   语言:fr 国家:fr
     */
    public static String getLanguageEnv() {
        Locale l = Locale.getDefault();
        String language = l.getLanguage();
        String country = l.getCountry().toLowerCase();
        Log.d(TAG, "当前系统语言:" + language);
        Log.d(TAG, "当前系统国家:" + country);
        if ("zh".equals(language)) {
            if ("cn".equals(country)) {
                language = "zh-CN";
            } else if ("tw".equals(country)) {
                language = "zh-TW";
            }
        } else if ("pt".equals(language)) {
            if ("br".equals(country)) {
                language = "pt-BR";
            } else if ("pt".equals(country)) {
                language = "pt-PT";
            }
        }
        return language;
    }

    /**
     * 获取SN号
     */
    public static String getSN(Context context) {
//        if (true) {
//            return "WPOSQT2070000091";
//        }
        Log.d(TAG, "当前VERSION.SDK_INT = " + Build.VERSION.SDK_INT);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O || Build.VERSION.SDK_INT >= 29) {
            return Build.SERIAL;
        } else {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return Build.SERIAL;
            }
            return Build.getSerial();
        }
    }

    /**
     * 获取当前手机系统版本号
     */
    public static String getSystemVersion() {
        return android.os.Build.VERSION.RELEASE;
    }
}