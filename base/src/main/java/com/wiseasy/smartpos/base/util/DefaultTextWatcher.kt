package com.wiseasy.smartpos.base.util

import android.text.Editable
import android.text.TextWatcher

/**
 * @author Hugo
 * @date 2021/10/4
 */
open class DefaultTextWatcher : TextWatcher {
    override fun afterTextChanged(s: Editable?) {
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }
}