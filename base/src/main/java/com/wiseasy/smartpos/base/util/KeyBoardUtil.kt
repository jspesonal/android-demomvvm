package com.wiseasy.smartpos.base.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

/**
 * 关于系统输入法
 */

/**
 * 关闭输入法（隐藏软键盘）
 */
fun closeKeyboard(activity: Activity, view: View) {
    val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

/**
 * 判断软键盘是否弹出
 */
fun isShowKeyboard(context: Context, view: View): Boolean {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    return imm.hideSoftInputFromWindow(view.windowToken, 0)
}