package com.wiseasy.smartpos.base.util

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.os.LocaleList
import android.util.DisplayMetrics
import android.util.Log
import java.util.*

/**
 * 语言工具包
 * @author Belle(Baiyunyan)
 * @date 2022/01/21
 */
object LanguageUtil {

    const val TAG = "LanguageUtil"

    /**
     * 获取Locale
     */
    fun getLanguageLocale(context: Context): Locale {
        return when (SPUtil.getSettingLanguage(context)) {
            0 -> Locale("vi", "VN")
            1 -> Locale("en", "US")
            else -> Locale("vi", "VN")
        }
    }

    fun getLanguage(): String {
        //Android 7.0 起，Android 系统语言的规则变了
        val locale: Locale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            LocaleList.getDefault().get(0)
        } else
            Locale.getDefault()

        return locale.language
    }

    /**
     * 根据Locale获取语言内容
     */
    fun getContextByLocale(context: Context, locale: Locale): Context {
        val resources = context.packageManager.getResourcesForApplication(context.packageName)
        val configuration = resources.configuration
        configuration.setLocale(locale)
        return context.createConfigurationContext(configuration)
    }

}