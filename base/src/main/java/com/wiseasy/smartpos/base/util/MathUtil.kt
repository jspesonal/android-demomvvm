package com.wiseasy.android.utils

import android.text.TextUtils
import java.text.DecimalFormat
import java.text.NumberFormat

import java.math.BigDecimal
import java.util.*

/**
 * 数学中各个类型转换
 */


/**
 * double类型转百分号
 */
fun doubleToPercent(double: Double): String {
    val numberFormat = NumberFormat.getPercentInstance(Locale("en", "US"))
    numberFormat.maximumFractionDigits = 3 //精确到3位。
    val percent = numberFormat.format(double)
    return percent
}

/**
 * 百分号转double类型
 */
fun percentToDouble(percent: String): Double {
    val numberFormat = NumberFormat.getPercentInstance(Locale("en", "US"))
    numberFormat.maximumFractionDigits = 2 //精确到2位。
    val number = numberFormat.parse(percent)
    return number.toDouble()
}

/**
 * 规范小数点后位数及是否添加千位分隔符
 */
fun decimalFormat(number: String, decimalDigits: Int = 0, isSeparator: Boolean = true): String {
    val number = number.replace(",", "").toDouble()
    //force to US locale,  otherwise， it will cause serious error when device change language setting to French
    val decimalFormat = NumberFormat.getInstance(Locale("en", "US")) as DecimalFormat
    if (isSeparator) {
        when (decimalDigits) {
            0, -1 -> decimalFormat.applyPattern("#,##0")
            1 -> decimalFormat.applyPattern("#,##0.0")
            2 -> decimalFormat.applyPattern("#,##0.00")
            3 -> decimalFormat.applyPattern("#,##0.000")
            4 -> decimalFormat.applyPattern("#,##0.0000")
            else -> decimalFormat.applyPattern("#,##0")
        }
        return decimalFormat.format(number)
    } else {
        when (decimalDigits) {
            0, -1 -> decimalFormat.applyPattern("0")
            1 -> decimalFormat.applyPattern("0.0")
            2 -> decimalFormat.applyPattern("0.00")
            3 -> decimalFormat.applyPattern("0.000")
            4 -> decimalFormat.applyPattern("0.0000")
            else -> decimalFormat.applyPattern("0.00")
        }
        return decimalFormat.format(number)
    }
}

/**
 * 获取小数点后有多少位
 */
fun getDecimalPlaces(num: String): Int {
    if (num.contains(".")) {
        when (num.indexOf(".")) {
            num.length - 1 -> return 0
            num.length - 2 -> return 1
            num.length - 3 -> return 2
            num.length - 4 -> return 3
        }
    }
    return -1
}

/**
 * 通过百分号计算出小费
 */
fun getTipFromPercent(percent: String, amount: String, decimalDigits: Int, isSeparator: Boolean): String {
    var tip = decimalFormat((percentToDouble(percent) * amount.replace(",", "").toFloat()).toString(), decimalDigits, isSeparator)
    return tip
}

/**
 * 加法
 */
fun plusUtil(str1: String, str2: String): Double {
    var str1 = str1
    var str2 = str2
    if (TextUtils.isEmpty(str1)) str1 = "0"
    if (TextUtils.isEmpty(str2)) str2 = "0"
    str1 = str1.replace(",", "")
    str2 = str2.replace(",", "")
    val bd1 = BigDecimal(str1)
    val bd2 = BigDecimal(str2)
    return bd1.add(bd2).toDouble()
}

/**
 * 减法
 */

fun minusUtil(str1: String, str2: String): Double {
    var str1 = str1
    var str2 = str2
    if (TextUtils.isEmpty(str1)) str1 = "0"
    if (TextUtils.isEmpty(str2)) str2 = "0"
    str1 = str1.replace(",", "")
    str2 = str2.replace(",", "")
    val bd1 = BigDecimal(str1)
    val bd2 = BigDecimal(str2)
    return bd1.subtract(bd2).toDouble()
}

/**
 * 减法2
 */

fun minusUtil2(str1: String, str2: String): BigDecimal {
    var str1 = str1
    var str2 = str2
    if (TextUtils.isEmpty(str1)) str1 = "0"
    if (TextUtils.isEmpty(str2)) str2 = "0"
    str1 = str1.replace(",", "")
    str2 = str2.replace(",", "")
    val bd1 = BigDecimal(str1)
    val bd2 = BigDecimal(str2)
    return bd1.subtract(bd2)
}

/**
 * 乘法
 */
fun multiplyUtil(str1: String, str2: String): Double {
    var str1 = str1
    var str2 = str2
    if (TextUtils.isEmpty(str1)) str1 = "0"
    if (TextUtils.isEmpty(str2)) str2 = "0"
    str1 = str1.replace(",", "")
    str2 = str2.replace(",", "")
    val bd1 = BigDecimal(str1)
    val bd2 = BigDecimal(str2)
    return bd1.multiply(bd2).toDouble()
}