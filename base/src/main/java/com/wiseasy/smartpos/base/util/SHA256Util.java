package com.wiseasy.smartpos.base.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * SHA256加密
 *
 * @author Belle(Baiyunyan)
 * @date 2021/12/31
 */
public class SHA256Util {

    private static String TAG = "SHA256Util -> ";

    /**
     * SHA加密
     *
     * @param strSrc 明文
     * @return 加密之后的密文
     */
    public static String sha256Encrypt(String strSrc) {
        MessageDigest md = null;
        String strDes = null;
        byte[] bt = strSrc.getBytes();
        try {
            md = MessageDigest.getInstance("SHA-256");// 将此换成SHA-1、SHA-512、SHA-384等参数
            md.update(bt);
            strDes = bytes2Hex(md.digest()); // to HexString
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
//        Log.d(TAG, "SHA-256计算之后：" + strDes);
        return strDes;
    }

    /**
     * byte数组转换为16进制字符串
     *
     * @param bts 数据源
     * @return 16进制字符串
     */
    private static String bytes2Hex(byte[] bts) {
        String des = "";
        String tmp = null;
        for (int i = 0; i < bts.length; i++) {
            tmp = (Integer.toHexString(bts[i] & 0xFF));
            if (tmp.length() == 1) {
                des += "0";
            }
            des += tmp;
        }
        return des;
    }

    public static void main(String[] args) {
        String a = "eyJicmFuY2hDb2RlIjoiYnJhbmNoQ29kZTEiLCJhbW91bnQiOjEwLCJyZXF1ZXN0VHlwZSI6InFyY29kZSIsImJhc2ljQXV0aCI6eyJzcE1JRCI6IjAwMjU2NDg5Iiwid2lkIjoiMTAwMDAwMDAxNTg3MyIsInBob25lIjoiMDkzNTI2Mzk4OSIsInNwVElEIjoidGlkMTIzIiwiZGV2aWNlU04iOiJQUDM1MjcxOTA5MDAwMDc5In0sImNyZWF0ZWQiOiIyMDIxLTEyLTMxVDExOjI4OjExIiwiY2hhbm5lbCI6IldTUCIsImRlc2MiOiJUaGlzIGlzIGEgYmlnIGN1c3RvbWVyICJ9|04031BE8827DAD8FCBD7C1BCECC04FFF07FAA07468CC4B13AD684DAFFC466421";
        System.out.println("encrypt data = " + sha256Encrypt(a));
    }

}
