package com.wiseasy.smartpos.base.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.wiseasy.smartpos.base.BaseApplication;

/**
 * SharedPreferences 工具类
 */
public class SPUtil {

    private static final String DEFAULT_FILE_NAME = "SP_DEFAULT";
    private static SharedPreferences sharedPreferences;

    private static SharedPreferences getInstance() {
        if (sharedPreferences == null) {
            synchronized (SPUtil.class) {
                if (sharedPreferences == null) {
                    if (BaseApplication.context != null) {
                        sharedPreferences = BaseApplication.context.getSharedPreferences(DEFAULT_FILE_NAME, Context.MODE_PRIVATE);
                    } else {
                        Log.i("SPUtil", "BaseBaseApplication.context == null");
                    }
                }
            }
        }
        return sharedPreferences;
    }

    /**
     * 清空数据
     */
    public static void clear(Context context) {
        getInstance().edit().clear().apply();
    }

    private static void putString(Context context, String key, String value) {
        getInstance().edit().putString(key, value).apply();
    }

    private static String getString(Context context, String key) {
        return getString(context, key, "");
    }

    private static String getString(Context context, String key, String defValue) {
        return getInstance().getString(key, defValue);
    }

    private static void putBoolean(Context context, String key, boolean value) {
        getInstance().edit().putBoolean(key, value).apply();
    }

    private static boolean getBoolean(Context context, String key) {
        return getBoolean(context, key, false);
    }

    private static boolean getBoolean(Context context, String key, boolean defValue) {
        return getInstance().getBoolean(key, defValue);
    }

    private static void putInt(Context context, String key, int value) {
        getInstance().edit().putInt(key, value).apply();
    }

    private static int getInt(Context context, String key) {
        return getInt(context, key, 0);
    }

    private static int getInt(Context context, String key, int defValue) {
        return getInstance().getInt(key, defValue);
    }

    private static void putLong(Context context, String key, long value) {
        getInstance().edit().putLong(key, value).apply();
    }

    private static long getLong(Context context, String key) {
        return getLong(context, key, 0L);
    }

    private static long getLong(Context context, String key, long defValue) {
        return getInstance().getLong(key, defValue);
    }

    private static void putFloat(Context context, String key, float value) {
        getInstance().edit().putFloat(key, value).apply();
    }

    private static float getFloat(Context context, String key) {
        return getFloat(context, key, 0F);
    }

    private static float getFloat(Context context, String key, float defValue) {
        return getInstance().getFloat(key, defValue);
    }

    /**
     * 是否第一次登录
     */
    private static final String IS_FIRST_LOGIN = "is_first_login";

    public static boolean getIsFirstLogin(Context context) {
        return getBoolean(context, IS_FIRST_LOGIN, true);
    }

    public static void setIsFirstLogin(Context context, boolean value) {
        putBoolean(context, IS_FIRST_LOGIN, value);
    }

    /**
     * smartpay后台在登录时返回的firstlogin状态
     * 1. first login
     * 2. not first login
     */
    private static final String IS_FIRST_LOGIN_SMART_PAY = "is_first_login_smart_pay";

    public static Boolean getIsFirstLoginSmartPay(Context context) {
        return getBoolean(context, IS_FIRST_LOGIN_SMART_PAY, false);
    }

    public static void setIsFirstLoginSmartPay(Context context, boolean value) {
        putBoolean(context, IS_FIRST_LOGIN_SMART_PAY, value);
    }

    /**
     * SmartPay返回的token
     */
    private static final String SMART_PAY_TOKEN = "smartPay_token";

    public static String getSmartPayToken(Context context) {
        return getString(context, SMART_PAY_TOKEN, "");
    }

    public static void setSmartPayToken(Context context, String value) {
        putString(context, SMART_PAY_TOKEN, value);
    }

    /**
     * Token的有效期
     * 时间戳
     */
    private static final String SMART_PAY_VALID_TILL = "smartpay_valid_till";

    public static Long getSmartPayValidTill(Context context) {
        return getLong(context, SMART_PAY_VALID_TILL, 0);
    }

    public static void setSmartPayValidTill(Context context, Long value) {
        putLong(context, SMART_PAY_VALID_TILL, value);
    }

    /**
     * 登录信息（包括MID, TID, SN, WID, PHONE等信息）
     */
    private static final String SMART_PAY_BASIC_AUTH = "smart_pay_basic_auth";

    public static String getSmartPayBasicAuth(Context context) {
        return getString(context, SMART_PAY_BASIC_AUTH, "");
    }

    public static void setSmartPayBasicAuth(Context context, String value) {
        putString(context, SMART_PAY_BASIC_AUTH, value);
    }

    /**
     * 登录的手机号
     */
    private static final String SMART_PAY_PHONE = "smart_pay_phone";

    public static String getSmartPayPhone(Context context) {
        return getString(context, SMART_PAY_PHONE, "");
    }

    public static void setSmartPayPhone(Context context, String value) {
        putString(context, SMART_PAY_PHONE, value);
    }

    /**
     * SmartPay的钱包ID(wid)
     */
    private static final String SMART_PAY_WID = "smart_pay_wid";

    public static String getSmartPayWid(Context context) {
        return getString(context, SMART_PAY_WID, "");
    }

    public static void setSmartPayWid(Context context, String value) {
        putString(context, SMART_PAY_WID, value);
    }

    /**
     * SmartPay的spMID
     */
    private static final String SMART_PAY_SP_MID = "smart_pay_sp_mid";

    public static String getSmartPaySpMid(Context context) {
        return getString(context, SMART_PAY_SP_MID, "");
    }

    public static void setSmartPaySpMid(Context context, String value) {
        putString(context, SMART_PAY_SP_MID, value);
    }

    /**
     * SmartPay的spTID
     */
    private static final String SMART_PAY_SP_TID = "smart_pay_sp_tid";

    public static String getSmartPaySpTid(Context context) {
        return getString(context, SMART_PAY_SP_TID, "");
    }

    public static void setSmartPaySpTid(Context context, String value) {
        putString(context, SMART_PAY_SP_TID, value);
    }

    /**
     * 商户信息（WiseCloud返回的各个mapping关系）
     */
    private static final String WISE_CLOUD_MERCHANT_INFO = "WISE_CLOUD_MERCHANT_INFO";

    public static String getWiseCloudMerchantInfo(Context context) {
        return getString(context, WISE_CLOUD_MERCHANT_INFO, "");
    }

    public static void setWiseCloudMerchantInfo(Context context, String value) {
        putString(context, WISE_CLOUD_MERCHANT_INFO, value);
    }

    /**
     * 当前打印机设置
     */
    private static final String SETTING_PRINTER = "setting_printer";

    public static String getSettingPrinter(Context context) {
        return getString(context, SETTING_PRINTER, "");
    }

    public static void setSettingPrinter(Context context, String value) {
        putString(context, SETTING_PRINTER, value);
    }

    /**
     * 设置语言类型   0：越南语；1：英文
     */
    private static final String SETTING_LANGUAGE = "setting_language";

    public static int getSettingLanguage(Context context) {
        return getInt(context, SETTING_LANGUAGE, 0);
    }

    public static void setSettingLanguage(Context context, int value) {
        putInt(context, SETTING_LANGUAGE, value);
    }

    /**
     * 是否修改了语言
     */
    private static final String IS_CHANGE_LANGUAGE = "is_change_language";

    public static Boolean getIsChangeLanguage(Context context) {
        return getBoolean(context, IS_CHANGE_LANGUAGE, false);
    }

    public static void setIsChangeLanguage(Context context, boolean value) {
        putBoolean(context, IS_CHANGE_LANGUAGE, value);
    }

    /**
     * 接口返回超时时间
     * verify otp/ resend
     */
    private static final String SMART_PAY_OTP_DURATION = "smart_pay_otp_duration";

    public static Long getSmartPayOtpDuration(Context context) {
        return getLong(context, SMART_PAY_OTP_DURATION, 180L);
    }

    public static void setSmartPayOtpDuration(Context context, Long value) {
        putLong(context, SMART_PAY_OTP_DURATION, 180L);
    }

    /**
     * 当前用户的角色
     * 0.admin
     * 1.店长
     * 2.普通店员
     */
    private static final String SMART_PAY_USER_POSITION = "smart_pay_user_position";

    public static int getSmartPayUserPosition(Context context) {
        return getInt(context, SMART_PAY_USER_POSITION);
    }

    public static void setSmartPayUserPosition(Context context, int value) {
        putInt(context, SMART_PAY_USER_POSITION, value);
    }

    /**
     * 当前用户姓名
     */
    private static final String SMART_PAY_USER_NAME = "smart_pay_user_name";

    public static String getSmartPayUserName(Context context) {
        return getString(context, SMART_PAY_USER_NAME);
    }

    public static void setSmartPayUserName(Context context, String value) {
        putString(context, SMART_PAY_USER_NAME, value);
    }

    /**
     * 应用版本号
     */
    private static final String App_Version = "app_version";

    public static String getAppVersion(Context context) {
        return getString(context, App_Version);
    }

    public static void setAppVersion(Context context, String value) {
        putString(context, App_Version, value);
    }

    /**
     * 是否禁屏
     */
    private static final String IS_SCREEN_BAN = "is_screen_ban";

    public static boolean getIsScreenBan(Context context) {
        return getBoolean(context, IS_SCREEN_BAN);
    }

    public static void setIsScreenBan(Context context, boolean value) {
        putBoolean(context, IS_SCREEN_BAN, value);
    }

}