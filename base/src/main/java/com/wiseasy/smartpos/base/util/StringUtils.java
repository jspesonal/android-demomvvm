package com.wiseasy.smartpos.base.util;

import android.text.TextUtils;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Administrator on 2017/4/10.
 */

public class StringUtils {

    /**
     * String 非空判断
     */
    public static boolean isEmpty(String msg) {
        boolean is;
        is = !(msg != null && !"".equals(msg));
        return is;
    }

    /**
     * 字符串转BCD，只传字符串即可
     */
    public static byte[] string2BCD(String str) {

        return string2BCD(str, str.length());
    }

    /**
     * 字符串转BCD
     */
    public static byte[] string2BCD(String str, int numlen) {
        if (numlen % 2 != 0)
            numlen++;

        while (str.length() < numlen) {
            str = "0" + str;
        }

        byte[] bStr = new byte[str.length() / 2];
        char[] cs = str.toCharArray();
        int i = 0;
        int iNum = 0;
        for (i = 0; i < cs.length; i += 2) {

            int iTemp = 0;
            if (cs[i] >= '0' && cs[i] <= '9') {
                iTemp = (cs[i] - '0') << 4;
            } else {

                if (cs[i] >= 'a' && cs[i] <= 'f') {
                    cs[i] -= 32;
                }
                iTemp = (cs[i] - '0' - 7) << 4;
            }

            if (cs[i + 1] >= '0' && cs[i + 1] <= '9') {
                iTemp += cs[i + 1] - '0';
            } else {

                if (cs[i + 1] >= 'a' && cs[i + 1] <= 'f') {
                    cs[i + 1] -= 32;
                }
                iTemp += cs[i + 1] - '0' - 7;
            }
            bStr[iNum] = (byte) iTemp;
            iNum++;

        }
        return bStr;

    }

    public static String leftPadding(String fill, int totalLength) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < totalLength; i++) {
            buffer.append(fill);
        }
        return buffer.toString();
    }

    /**
     * 得到字符串的字节长度
     */
    public static int getContentByteLength(String content) {
        if (content == null || content.length() == 0)
            return 0;
        int length = 0;
        for (int i = 0; i < content.length(); i++) {
            length += getByteLength(content.charAt(i));
        }
        return length;
    }

    /**
     * 得到几位字节长度
     */
    private static int getByteLength(char a) {
        String tmp = Integer.toHexString(a);
        return tmp.length() >> 1;
    }

    /**
     * 左边增加一定长度的字符串
     */
    public static String leftAppend(String fill, int appendLength, String str) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < appendLength; i++) {
            buffer.append(fill);
        }
        buffer.append(str);
        return buffer.toString();
    }

    /**
     * 右边增加一定长度的字符串
     */
    public static String rightAppend(String fill, int appendLength, String str) {
        StringBuilder buffer = new StringBuilder(str);
        for (int i = 0; i < appendLength; i++) {
            buffer.append(fill);
        }
        return buffer.toString();
    }

    /**
     * 组47域数据
     * @param map
     * @return
     */
    public static String TLVPack(Map<String, String> map) {
        Set<String> tags = map.keySet();
        StringBuilder str = new StringBuilder();
        Iterator var3 = tags.iterator();

        while(var3.hasNext()) {
            String tag = (String)var3.next();
            String vaule = (String)map.get(tag);
            if (!TextUtils.isEmpty(vaule)) {
                String length = getLength(vaule.length());
                str.append(tag).append(length).append(vaule);
            }
        }

        return str.toString();
    }

    public static String getLength(int len) {
        String temp = "00000";
        temp = temp + len;
        return temp.substring(temp.length() - 3, temp.length());
    }

}
