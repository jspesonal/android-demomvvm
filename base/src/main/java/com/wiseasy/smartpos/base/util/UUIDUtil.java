package com.wiseasy.smartpos.base.util;

import java.util.Objects;
import java.util.UUID;

/**
 * UUID生成器
 *
 * @author Belle(Baiyunyan)
 * @date 2022/01/04
 */
public class UUIDUtil {

    public UUIDUtil() {
    }

    public static String getUUID() {
        UUID uuid = UUID.randomUUID();
        String uuidStr = uuid.toString();
        // 去掉"-"符号
        String finalUUID = uuidStr.substring(0, 8) + uuidStr.substring(9, 13) + uuidStr.substring(14, 18) + uuidStr.substring(19, 23) + uuidStr.substring(24);
        System.out.println("最后生成的去掉uuid = " + finalUUID);
        return finalUUID;
    }

    //获得指定数量的UUID
    public static String[] getUUID(int number) {
        if (number < 1) {
            return null;
        }
        String[] ss = new String[number];
        for (int i = 0; i < number; i++) {
            ss[i] = getUUID();
        }
        return ss;
    }

    public static void main(String[] args) {
        String[] ss = getUUID(10);
        for (int i = 0; i < Objects.requireNonNull(ss).length; i++) {
            System.out.println("ss[" + i + "]=====" + ss[i]);
        }
    }
}
