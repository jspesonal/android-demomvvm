package com.wiseasy.smartpos.base.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiseasy.smartpos.base.R;
import com.wiseasy.smartpos.base.databinding.BaseViewActionBarWhiteBinding;


/**
 * 自定义的白色背景ActionBar
 * Custom ActionBar
 */
public class BaseActionBarWhite extends RelativeLayout implements View.OnClickListener {

    private ActionBarClickListener mListener;
    private TextView mTvTitle, mTvRightMsg;
    private ImageView ivRightIcon;

    public BaseActionBarWhite(Context context) {
        this(context, null);
    }

    public BaseActionBarWhite(Context context, AttributeSet attrs) {
        super(context, attrs);
        //loadData
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.BaseActionBarWhite);
        String title = array.getString(R.styleable.BaseActionBarWhite_title2);
        String rightMsg = array.getString(R.styleable.BaseActionBarWhite_rightMessage2);
        boolean isShowLeftBackIcon = array.getBoolean(R.styleable.BaseActionBarWhite_isShowLeftBackIcon2, true);
        boolean isShowRightIcon = array.getBoolean(R.styleable.BaseActionBarWhite_isShowRightIcon2, false);
        boolean isShowRightMsg = array.getBoolean(R.styleable.BaseActionBarWhite_isShowRightMessage2, false);
        int rightIconId = array.getResourceId(R.styleable.BaseActionBarWhite_rightIcon2, R.mipmap.smartpay_icon_question);

        //initView
        BaseViewActionBarWhiteBinding mBinding = BaseViewActionBarWhiteBinding.inflate(LayoutInflater.from(context));
        mTvTitle = mBinding.tvTitle;
        ivRightIcon = mBinding.ivRightIcon;
        ImageView ivBack = mBinding.ivBack;
        mTvRightMsg = mBinding.tvRightMsg;

        //setViewData
        isShowView(isShowLeftBackIcon, ivBack);
        isShowView(isShowRightIcon, ivRightIcon);
        isShowView(isShowRightMsg, mTvRightMsg);
        if (title != null) mTvTitle.setText(title);
        mTvRightMsg.setText(rightMsg);
        ivRightIcon.setImageResource(rightIconId);

        addView(mBinding.getRoot());
        array.recycle();

        if (context instanceof ActionBarClickListener) {
            setActionBarClickListener((ActionBarClickListener) context);
        }
    }

    public void isShowView(boolean isShow, View view) {
        if (isShow) {
            view.setVisibility(VISIBLE);
            view.setOnClickListener(this);
        } else {
            view.setVisibility(INVISIBLE);
        }
    }

    public void setTitle(String title) {
        mTvTitle.setText(title);
    }

    public void setRightText(String rightMassage) {
        mTvRightMsg.setText(rightMassage);
    }

    public void setRightTextVisibility(int visibility) {
        mTvRightMsg.setVisibility(visibility);
    }

    public void setRightIconVisibility(int visibility) {
        ivRightIcon.setVisibility(visibility);
    }

    public void setActionBarClickListener(ActionBarClickListener listener) {
        mListener = listener;
    }

    @Override
    public void onClick(View v) {

        // Resource IDs cannot be used in a switch statement in Android library modules
        if (v.getId() == R.id.iv_back) {
            if (mListener == null) return;
            mListener.onActionBarLeft();
        } else if (v.getId() == R.id.tv_right_msg) {
            if (mListener == null) return;
            mListener.onActionBarRight();
        } else if (v.getId() == R.id.iv_right_icon) {
            if (mListener == null) return;
            mListener.onActionBarRight();
        }
    }

    public interface ActionBarClickListener {
        void onActionBarLeft();

        void onActionBarRight();
    }

}