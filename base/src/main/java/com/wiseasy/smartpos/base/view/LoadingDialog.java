package com.wiseasy.smartpos.base.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wiseasy.smartpos.base.R;
import com.wiseasy.smartpos.base.databinding.BaseViewLoadingDialogBinding;


public class LoadingDialog extends Dialog {

    private TextView mTvMsg;
    private ProgressBar mPbLoading;
    private ImageView mIvIcon;
    private AnimationDrawable mAnimation;

    public LoadingDialog(Context context) {
        this(context, "");
    }

    public LoadingDialog(Context context, String msg) {
        super(context, R.style.custom_dialog_light_style);
        BaseViewLoadingDialogBinding mBinding = BaseViewLoadingDialogBinding.inflate(LayoutInflater.from(context));
        mTvMsg = mBinding.tvMsg;
        mPbLoading = mBinding.pbLoading;
        mIvIcon = mBinding.ivIcon;
        setMsg(msg);
        setContentView(mBinding.getRoot());
        this.setCanceledOnTouchOutside(false);
    }

    public void setMsg(String msg) {
        if (msg != null && !msg.equals("")) {
            mTvMsg.setVisibility(View.VISIBLE);
            mTvMsg.setText(msg);
        } else {
            mTvMsg.setVisibility(View.GONE);
        }
    }

    public void setImageViewAnim(int drawableId) {
        mPbLoading.setVisibility(View.INVISIBLE);
        mIvIcon.setBackgroundResource(drawableId);
        mAnimation = (AnimationDrawable) mIvIcon.getBackground();
        mAnimation.start();
    }

    @Override
    public void hide() {
        super.hide();
        if (mAnimation != null) {
            mAnimation.stop();
        }
    }
}
