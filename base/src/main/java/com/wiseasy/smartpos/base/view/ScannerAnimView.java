package com.wiseasy.smartpos.base.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;

import com.wiseasy.smartpos.base.R;
import com.wiseasy.smartpos.base.device.DeviceProxy;


public class ScannerAnimView extends View {

    private int finalRectLeft;//框的宽是445
    private int finalRectTop;
    private int finalRectRight;
    private int finalRectBottom;

    private int rectLeft;
    private int rectTop;
    private int rectRight;
    private int rectBottom;

    private int CORNER_THICK = 3;//边框的厚度
    private int CORNER_WIDTH = 80;//边框的长度

    private boolean drawCenterRect = false;
    private boolean lineAnimationEnabled = true;

    private int LINE_MARGIN = 10;
    private int LINE_STEP = 10;
    private long LINE_ANIM_DURATION = 12L;
    private Bitmap lineBitmap = ((BitmapDrawable) getResources().getDrawable(R.mipmap.base_scan_line)).getBitmap();
    private Rect lineSrcRect = new Rect(0, 0, lineBitmap.getWidth(), lineBitmap.getHeight());
    private Rect lineDestRect = new Rect(0, 0, 0, 0);

    private int lineStartTop;

    private Rect leftRect;
    private Rect topRect;
    private Rect rightRect;
    private Rect bottomRect;

    private Rect leftTopCorner1;
    private Rect leftTopCorner2;
    private Rect rightTopCorner1;
    private Rect rightTopCorner2;
    private Rect leftBottomCorner1;
    private Rect leftBottomCorner2;
    private Rect rightBottomCorner1;
    private Rect rightBottomCorner2;

    private Paint paint = new Paint();
    private Rect scanRect = new Rect();
    private Rect originRect = new Rect();

    public ScannerAnimView(Context context) {
        this(context, null);

    }

    public ScannerAnimView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initData(context);

    }

    private void initData(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();

        if (width < height) {//竖屏
            //如果是QT设备
            if (DeviceProxy.getInstance().isWPOSQT()) {
                finalRectLeft = width / 5;//设定框的左边沿width / 5
                finalRectTop = (height - width * 3 / 5) / 2 - 50;//上边沿位置 = （总长度 - 框的宽度）/ 2 再上移50px
                finalRectRight = width * 4 / 5;
                finalRectBottom = finalRectTop + width * 3 / 5;
            } else {
                finalRectLeft = width / 6;//设定框的宽度是width * 2 / 3 ,备注：1/6 + 2/3 + 1/6 = 1
                finalRectTop = (height - width * 2 / 3) / 2;//上边沿位置 = （总长度 - 框的宽度）/ 2
                finalRectRight = width * 5 / 6;
                finalRectBottom = finalRectTop + width * 2 / 3;
            }

        } else {//横屏
            finalRectLeft = (width - height / 2) / 2;//设定框的宽度是height / 2
            finalRectTop = height / 4;
            finalRectRight = finalRectLeft + height / 2;
            finalRectBottom = height * 3 / 4;
        }

        rectLeft = finalRectLeft;
        rectTop = finalRectTop;
        rectRight = finalRectRight;
        rectBottom = finalRectBottom;

        lineStartTop = rectTop;

        leftRect = new Rect(0, 0, rectLeft, getHeight());
        topRect = new Rect(rectLeft, 0, rectRight, rectTop);
        rightRect = new Rect(rectRight, 0, getWidth(), getHeight());
        bottomRect = new Rect(rectLeft, rectBottom, rectRight, getHeight());

        leftTopCorner1 = new Rect(rectLeft - CORNER_THICK, rectTop - CORNER_THICK, rectLeft, rectTop - CORNER_THICK + CORNER_WIDTH);
        leftTopCorner2 = new Rect(rectLeft, rectTop - CORNER_THICK, rectLeft + CORNER_WIDTH - CORNER_THICK, rectTop);
        rightTopCorner1 = new Rect(rectRight, rectTop - CORNER_THICK, rectRight + CORNER_THICK, rectTop - CORNER_THICK + CORNER_WIDTH);
        rightTopCorner2 = new Rect(rectRight + CORNER_THICK - CORNER_WIDTH, rectTop - CORNER_THICK, rectRight, rectTop);
        leftBottomCorner1 = new Rect(rectLeft - CORNER_THICK, rectBottom + CORNER_THICK - CORNER_WIDTH, rectLeft, rectBottom + CORNER_THICK);
        leftBottomCorner2 = new Rect(rectLeft, rectBottom, rectLeft - CORNER_THICK + CORNER_WIDTH, rectBottom + CORNER_THICK);
        rightBottomCorner1 = new Rect(rectRight, rectBottom + CORNER_THICK - CORNER_WIDTH, rectRight + CORNER_THICK, rectBottom + CORNER_THICK);
        rightBottomCorner2 = new Rect(rectRight + CORNER_THICK - CORNER_WIDTH, rectBottom, rectRight, rectBottom + CORNER_THICK);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

//        paint.setColor(Color.parseColor(SPUtil.getScanIsDockModule() ? "#cf000000" : "#7f000000"));
//        paint.setColor(Color.parseColor("#cf000000"));
        paint.setColor(Color.parseColor("#7f000000"));
        // 画左边
        leftRect.right = rectLeft;
        leftRect.bottom = getHeight();
        canvas.drawRect(leftRect, paint);
        // 画上边
        topRect.left = rectLeft;
        topRect.right = rectRight;
        topRect.bottom = rectTop;
        canvas.drawRect(topRect, paint);
        // 画右边
        rightRect.left = rectRight;
        rightRect.right = getWidth();
        rightRect.bottom = getHeight();
        canvas.drawRect(rightRect, paint);
        // 画下边
        bottomRect.left = rectLeft;
        bottomRect.top = rectBottom;
        bottomRect.right = rectRight;
        bottomRect.bottom = getHeight();
        canvas.drawRect(bottomRect, paint);

        if (drawCenterRect) {
            canvas.drawRect(getScanAreaRect(), paint);

        } else {
            //开始画4个角
            paint.setColor(Color.parseColor("#FFFFFF"));
            int distance = 20;//白色的边框距离中间透明边框的距离

            leftTopCorner1.left = rectLeft - CORNER_THICK - distance;
            leftTopCorner1.top = rectTop - CORNER_THICK - distance;
            leftTopCorner1.right = rectLeft - distance;
            leftTopCorner1.bottom = rectTop - CORNER_THICK + CORNER_WIDTH;
            leftTopCorner2.left = rectLeft - distance;
            leftTopCorner2.top = rectTop - CORNER_THICK - distance;
            leftTopCorner2.right = rectLeft + CORNER_WIDTH - CORNER_THICK;
            leftTopCorner2.bottom = rectTop - distance;
            canvas.drawRect(leftTopCorner1, paint);
            canvas.drawRect(leftTopCorner2, paint);

            rightTopCorner1.left = rectRight + distance;
            rightTopCorner1.top = rectTop - CORNER_THICK - distance;
            rightTopCorner1.right = rectRight + CORNER_THICK + distance;
            rightTopCorner1.bottom = rectTop - CORNER_THICK + CORNER_WIDTH;
            rightTopCorner2.left = rectRight + CORNER_THICK - CORNER_WIDTH;
            rightTopCorner2.top = rectTop - CORNER_THICK - distance;
            rightTopCorner2.right = rectRight + distance;
            rightTopCorner2.bottom = rectTop - distance;
            canvas.drawRect(rightTopCorner1, paint);
            canvas.drawRect(rightTopCorner2, paint);

            leftBottomCorner1.left = rectLeft - CORNER_THICK - distance;
            leftBottomCorner1.top = rectBottom + CORNER_THICK - CORNER_WIDTH;
            leftBottomCorner1.right = rectLeft - distance;
            leftBottomCorner1.bottom = rectBottom + CORNER_THICK + distance;
            leftBottomCorner2.left = rectLeft - distance;
            leftBottomCorner2.top = rectBottom + distance;
            leftBottomCorner2.right = rectLeft - CORNER_THICK + CORNER_WIDTH;
            leftBottomCorner2.bottom = rectBottom + CORNER_THICK + distance;
            canvas.drawRect(leftBottomCorner1, paint);
            canvas.drawRect(leftBottomCorner2, paint);

            rightBottomCorner1.left = rectRight + distance;
            rightBottomCorner1.top = rectBottom + CORNER_THICK - CORNER_WIDTH;
            rightBottomCorner1.right = rectRight + CORNER_THICK + distance;
            rightBottomCorner1.bottom = rectBottom + CORNER_THICK + distance;
            rightBottomCorner2.left = rectRight + CORNER_THICK - CORNER_WIDTH;
            rightBottomCorner2.top = rectBottom + distance;
            rightBottomCorner2.right = rectRight + distance;
            rightBottomCorner2.bottom = rectBottom + CORNER_THICK + distance;
            canvas.drawRect(rightBottomCorner1, paint);
            canvas.drawRect(rightBottomCorner2, paint);
        }

//        (1 until H step step).forEach {
//            canvas?.drawRect(0f, it.toFloat()-2, W.toFloat(), it.toFloat()+2, paint)
//        }


        if (lineAnimationEnabled) {
            if (lineStartTop >= rectBottom) {
                lineStartTop = rectTop;
            }
            lineDestRect.left = rectLeft + LINE_MARGIN;
            lineDestRect.top = lineStartTop;
            lineDestRect.right = rectRight - LINE_MARGIN;
            lineDestRect.bottom = lineStartTop + lineBitmap.getHeight() / 2;
            canvas.drawBitmap(lineBitmap, lineSrcRect, lineDestRect, paint);
            lineStartTop += LINE_STEP;

            postInvalidateDelayed(LINE_ANIM_DURATION, rectLeft, rectTop,
                    rectRight, rectBottom);
        } else {
            lineStartTop = rectTop;
        }


    }

    public Rect getScanAreaRect() {
        scanRect.set(rectLeft, rectTop, rectRight, rectBottom);
        return scanRect;
    }

    public Rect getOriginScanAreaRect() {
        originRect.set(finalRectLeft, finalRectTop, finalRectRight, finalRectBottom);
        return originRect;
    }

    public void setScanAreaRect(Rect rect) {
        rectLeft = rect.left;
        rectTop = rect.top;
        rectRight = rect.right;
        rectBottom = rect.bottom;
        postInvalidate();
    }

    public void setDrawCenterRect(boolean drawCenterRect) {
        this.drawCenterRect = drawCenterRect;
        postInvalidate();
    }

    public boolean getDrawCenterRect() {
        return drawCenterRect;
    }

    public void setLineAnimationEnabled(boolean lineAnimationEnabled) {
        this.lineAnimationEnabled = lineAnimationEnabled;
        postInvalidate();
    }

    public boolean getLineAnimationEnabled() {
        return lineAnimationEnabled;
    }

}
