package com.wiseasy.smartpos.base.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.wiseasy.smartpos.base.R;


/**
 * 系统状态栏管理器
 */
public class SystemStatusBarManager {

    private View mStatusBarView;
    private int DEFAULT_COLOR = Color.GRAY;

    public SystemStatusBarManager(Activity activity) {

        Window win = activity.getWindow();
        ViewGroup decorViewGroup = (ViewGroup) win.getDecorView();
        if (Build.VERSION.SDK_INT >= 21) {//android 5.0
//            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            //后面这个属性为将状态栏的文字颜色改为深色，只有6.0以后的才有这个属性
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            decorViewGroup.setSystemUiVisibility(option);
            win.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            win.setStatusBarColor(Color.TRANSPARENT);
        } else if (Build.VERSION.SDK_INT >= 19) {//android 4.4
            WindowManager.LayoutParams winParams = win.getAttributes();
            int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
            winParams.flags = winParams.flags | bits;
            win.setAttributes(winParams);
        }
        setupStatusBarView(activity, decorViewGroup);
        // 设置默认的状态栏为基础的颜色
//        setStatusBarResource(R.drawable.base_shape_blue_bg);
        setStatusBarResource(R.color.base_color_app_primary);
    }

    private void setupStatusBarView(Context context, ViewGroup decorViewGroup) {
        mStatusBarView = new View(context);
        int statusBarHeight = getInternalDimensionSize(context.getResources());
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(-1, statusBarHeight);
        mStatusBarView.setLayoutParams(params);
        decorViewGroup.addView(mStatusBarView);
    }

    private int getInternalDimensionSize(Resources res) {
        int result = 0;
        int resourceId = res.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = res.getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * 设置状态栏的颜色
     */
    public void setStatusBarColor(int color) {
        mStatusBarView.setBackgroundColor(color);
    }

    /**
     * 设置状态栏为某张图片
     * @param res 图片资源Id
     */
    public void setStatusBarResource(int res) {
        mStatusBarView.setBackgroundResource(res);
    }
}