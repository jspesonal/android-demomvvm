package com.wiseasy.smartpos.comm

object Constant {
    /**
     * 支付方式
     */
    const val PAYMENT_METHOD_SCAN_TO_PAY = "quickpay"
    const val PAYMENT_METHOD_QR_CODE = "qrcode"

    /**
     * 交易状态(QueryOrder)
     */
    const val TRANS_STATUS_OPEN = "OPEN"
    const val TRANS_STATUS_PAYED = "PAYED"
    const val TRANS_STATUS_PROCESSING = "PROCESSING"
    const val TRANS_STATUS_PAY_ERROR = "PAY_ERROR"

    /**
     * 交易状态(getTransDetail)
     */
    const val TRANS_STATUS_DETAIL_SUCCESS = "SUCCESS"
    const val TRANS_STATUS_DETAIL_SETTLEMENT = "SETTLEMENT"
    const val TRANS_STATUS_DETAIL_FAIL = "FAIL"
    const val TRANS_STATUS_DETAIL_PROCESSING = "PROCESSING"

    /**
     * 交易状态（getTransList）
     */
    const val TRANS_STATUS_LIST_SUCCESS = "SUCCESS"
    const val TRANS_STATUS_LIST_SETTLEMENT = "SETTLEMENT"
    const val TRANS_STATUS_LIST_FAIL = "FAIL"
    const val TRANS_STATUS_LIST_PROCESSING = "PROCESSING"

    const val UNIT = "₫"
    const val TOMEZONE = "GMT+7:00" //越南时区(东七区)

    const val TIMEZONEZERO = "GMT+0:00" //零时区

    const val PHONE_REGEX = "^0?[1-9]?[0-9]{8}?\$"//SmartPay手机号正则表达式

    const val SCREEN_TIME = 5//禁屏时间

}