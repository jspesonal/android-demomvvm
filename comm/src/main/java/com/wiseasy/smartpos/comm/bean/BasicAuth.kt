package com.wiseasy.smartpos.comm.bean

/**
 * 几乎所有的接口请求数据中都有这个字段
 * @author Belle(Baiyunyan)
 * @date 2022/01/07
 */
class BasicAuth {
    var spMID: String? = ""
    var spTID: String? = ""
    var deviceSN: String? = ""
    var password: String? = ""
    var wid: String? = ""
    var otp: String? = ""
    var phone: String? = ""
}