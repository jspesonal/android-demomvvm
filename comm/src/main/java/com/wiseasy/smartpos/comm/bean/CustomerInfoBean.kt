package com.wiseasy.smartpos.comm.bean

import java.io.Serializable

/**
 * 顾客信息
 * @author Belle(Baiyunyan)
 * @date 2022/01/05
 */
class CustomerInfoBean : Serializable {
    var name: String? = ""
    var phone: String? = ""
    var logo: String? = ""
}