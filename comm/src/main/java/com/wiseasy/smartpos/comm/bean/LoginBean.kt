package com.wiseasy.smartpos.comm.bean

/**
 * Created by Bella on 2022/1/18.
 */
data class LoginBean(
    var accessToken: String,
    var validTill: String,
    var wid: String,
    var firstLogin: Int,
    var staffName: String,
    var position: Int
)
