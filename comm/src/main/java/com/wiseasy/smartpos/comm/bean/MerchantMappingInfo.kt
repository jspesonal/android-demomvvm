package com.wiseasy.smartpos.comm.bean

/**
 * 获取商户信息返回数据
 * @author Belle(Baiyunyan)
 * @date 2022/01/19
 */
class MerchantMappingInfo {
    var merchantAbbreviation: String? = ""
    var merchantNo: String? = ""
    var merchantName: String? = ""
    var merchantAddress: String? = ""
    var sn: String? = ""
    var storeAddress: String? = ""
    var storeNo: String? = ""
    var storeName: String? = ""
    var thirdMid: String? = ""
    var thirdStoreNo: String? = ""
    var thirdTid: String? = ""
    var thirdWalletId: String? = ""
}