package com.wiseasy.smartpos.comm.bean

/**
 * 获取二维码返回数据
 * @author Belle(Baiyunyan)
 * @date 2022/01/04
 */
class QRPayBean {
    var payload: String? = ""
}