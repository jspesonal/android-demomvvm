package com.wiseasy.smartpos.comm.bean

/**
 * 扫一扫支付返回数据
 * @author Belle(Baiyunyan)
 * @date 2022/01/05
 */
class ScanPayBean {
    var transId: String? = ""
    var amount: Int = 0
    var status: String? = ""
    var orderNo: String? = ""
}