package com.wiseasy.smartpos.comm.bean

/**
 * 发送邮件返回数据
 * @author Belle(Baiyunyan)
 * @date 2022/01/07
 */
class SendEmailBean {
    var receiptNo: String? = ""
}