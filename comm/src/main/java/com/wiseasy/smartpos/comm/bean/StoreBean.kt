package com.wiseasy.smartpos.comm.bean

/**
 * Created by Bella on 2022/1/21.
 */
data class StoreBean(
    var merchantNo: String?,
    var storeNo: String?,
    var storeName: String?
)
