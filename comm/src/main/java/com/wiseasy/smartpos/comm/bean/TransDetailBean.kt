package com.wiseasy.smartpos.comm.bean

import java.io.Serializable

/**
 * 查询交易结果返回数据（QueryOrder API）
 * @author Belle(Baiyunyan)
 * @date 2022/01/05
 */
class TransDetailBean : Serializable {
    var orderNo: String? = ""
    var fee: Int = 0
    var paidAmount: Int = 0
    var amount: Int = 0
    var voucherAmount: Int = 0
    var transId: String? = ""
    var status: String? = ""
    var desc: String? = ""
    var created: String? = ""
    var paymentTime: String? = ""
    var requestType: String? = ""
    var partnerCode: String? = ""
    var customerInfo: CustomerInfoBean? = null
    var receiptNo: String? = null

    //不是后台返回的，用于本地数据传递
    var qrcode: String? = ""
    var transNo: String? = ""
}