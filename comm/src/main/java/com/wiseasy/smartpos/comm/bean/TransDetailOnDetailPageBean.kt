package com.wiseasy.smartpos.comm.bean

/**
 * 查询交易详情返回数据(getTransDetail API)
 * @author Belle(Baiyunyan)
 * @date 2022/01/14
 */
class TransDetailOnDetailPageBean {
    var customerName: String? = ""
    var customerPhone: String? = ""
    var customerAvatar: String? = ""
    var status: String? = ""
    var transId: String? = ""
    var amount: String? = ""
    var typeTrans: String? = ""
    var fee: Int = 0
    var paidAmount: Int = 0
    var date: String? = ""
}