package com.wiseasy.smartpos.comm.bean

/**
 * 交易列表返回数据
 * @author Belle(Baiyunyan)
 * @date 2022/01/13
 */
class TransListBean {

    var list: MutableList<TransListDetailBean>? = null

}