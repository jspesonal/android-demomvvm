package com.wiseasy.smartpos.comm.bean

/**
 * 交易列表返回数据的每一个item
 * @author Belle(Baiyunyan)
 * @date 2022/01/12
 */
class TransListDetailBean {
    var customerName: String? = ""
    var amount: Int = 0
    var status: String? = ""
    var date: String? = ""
    var refNo: String? = ""
    var typeTrans: String? = ""
}