package com.wiseasy.smartpos.comm.bean

/**
 * @author Belle(Baiyunyan)
 * @date 2021/12/28
 */
class TransactionBean {

    var type: String? = ""
    var name: String? = ""
    var status: String? = ""
    var amount: String? = ""
    var time: String? = ""

    constructor(type: String?, name: String?, status: String?, amount: String?, time: String?) {
        this.type = type
        this.name = name
        this.status = status
        this.amount = amount
        this.time = time
    }
}