package com.wiseasy.smartpos.comm.bean

/**
 * 升级APP返回数据
 * @author Belle(Baiyunyan)
 * @date 2022/02/08
 */
class UpdateApp {
    var appList: MutableList<UpdateAppDetail>? = null
}