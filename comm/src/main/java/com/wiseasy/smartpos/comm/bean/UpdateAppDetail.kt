package com.wiseasy.smartpos.comm.bean

/**
 * 升级APP返回数据详情
 * @author Belle(Baiyunyan)
 * @date 2022/02/08
 */
class UpdateAppDetail {
    var marketId: Int = 0
    var versionMD5: String? = ""
    var appSize: Int = 0
    var appId: Int = 0
    var versionNumber: Long = 0L
    var appIconUrl: String? = ""
    var versionName: String? = ""
    var appAlias: String? = ""
    var appPackage: String? = ""
    var updateStrategy: Int = 0
    var grayStatus: Int = 0
}