package com.wiseasy.smartpos.comm.bean

/**
 * Created by Bella on 2022/1/7.
 */
data class UserBean(
    var id: Int = 0,
    var loginAccount: String? = "",
    var lastLoginTime: String? = "",
    var partnerCode: String? = "",
    var merchantNo: String? = "",
    var position: Int = 2,
    var staffName: String? = "",
    var status: Int = 1,
    var storeName: String? = "",
    var storeNo: String? = "",
    var createTime: String? = "",
    var updateTime: String? = "",
    var firstLogin: Int = 1
)
