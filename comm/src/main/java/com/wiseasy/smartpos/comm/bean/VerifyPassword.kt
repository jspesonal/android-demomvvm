package com.wiseasy.smartpos.comm.bean

/**
 * 验证身份返回数据
 * @author Belle(Baiyunyan)
 * @date 2022/02/07
 */
class VerifyPassword {
    var staffName: String? = ""
    var position: Int = -1
    var firstLogin: Int = -1
}