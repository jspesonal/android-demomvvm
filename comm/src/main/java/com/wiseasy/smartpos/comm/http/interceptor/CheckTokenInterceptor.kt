package com.wiseasy.smartpos.comm.http.interceptor

import android.content.Context
import android.util.Log
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.base.util.DateUtil
import okhttp3.Interceptor
import kotlin.Throws
import com.wiseasy.smartpos.base.util.NetUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.Constant
import com.wiseasy.smartpos.comm.R
import com.wiseasy.smartpos.comm.module.service.LoginModuleService
import okhttp3.Response
import java.io.IOException

/**
 * 检验token是否过期
 * @author Belle(Baiyunyan)
 * @date 2022/02/07
 */
class CheckTokenInterceptor(private val mContext: Context) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        //对比当前时间和token的有效时间
        val currentTime = DateUtil.getCurDateStrByTimeZone("yyyyMMddHHmmss", Constant.TOMEZONE).toLong()
        val tokenTime = SPUtil.getSmartPayValidTill(mContext)
        Log.d("CheckTokenInterceptor", "currentTime = $currentTime, tokenTime = $tokenTime")
        if (tokenTime != 0L && currentTime > tokenTime) {
            Router.getInstance().getService(LoginModuleService::class.java)?.goToLoginActivity(mContext)
            throw IOException(mContext.getString(R.string.login_token_expired))
        }
        return chain.proceed(chain.request())
    }
}