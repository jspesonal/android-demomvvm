package com.wiseasy.smartpos.comm.http.interceptor

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response

/**
 * 请求头应用拦截器
 * - 添加请求头中的公共数据
 */
class HttpHeadInterceptor(private var headers: MutableMap<String, String>) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        if (headers.isNotEmpty()) {
            // 如果公共Header不为空，则基于原有请求新建请求
            val newRequestBuilder = chain.request().newBuilder()
            headers.keys.forEach {
                Log.d("HttpHeadInterceptor", "key = $it, value = ${headers[it]}")
                newRequestBuilder.addHeader(it, headers[it]!!)
            }
            return chain.proceed(newRequestBuilder.build())
        } else {
            return chain.proceed(chain.request())
        }
    }
}