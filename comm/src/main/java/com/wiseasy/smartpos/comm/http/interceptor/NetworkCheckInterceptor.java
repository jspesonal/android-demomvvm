package com.wiseasy.smartpos.comm.http.interceptor;

import android.content.Context;

import com.wiseasy.smartpos.base.util.NetUtil;
import com.wiseasy.smartpos.comm.R;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class NetworkCheckInterceptor implements Interceptor {

    private Context mContext;

    public NetworkCheckInterceptor(Context context) {
        mContext = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        //检查网络
        if (!NetUtil.isNetworkConnect(mContext)) {
            throw new IOException(mContext.getString(R.string.common_not_net));
        }
        return chain.proceed(chain.request());
    }
}
