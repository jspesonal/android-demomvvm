package com.wiseasy.smartpos.comm.http.smartpay

import android.util.Log
import com.google.gson.Gson
import com.wiseasy.smartpos.base.BaseApplication
import com.wiseasy.smartpos.base.util.Base64Utils
import com.wiseasy.smartpos.comm.R
import com.wiseasy.smartpos.comm.http.smartpay.util.HttpUtil
import java.lang.Exception

/**
 * @author Belle(Baiyunyan)
 * @date 2021/12/29
 */
class BaseResult<T> {
    var code: String? = ""
    var message: String? = ""
    var data: String? = ""
    var checksum: String? = ""

    /**
     * 数据转换，将后台返回的加密数据转换为本地可识别的实体类数据
     * @return T 解密后的实际用到的数据类型
     */
    fun dataConvert(type: Class<T>?): T {
        when (code) {
            "OK" -> {
                val decodeData = Base64Utils.decodeToString(data)
                Log.d("BaseResult", "返回数据(解密后)：$decodeData")
                val finalData: T = Gson().fromJson(decodeData, type)
                //将后台返回的data数据做SHA256加密，如果加密后的数据跟后台返回的checksum一致，则说明数据没有被篡改，否则则说明数据被篡改
                val afterSha256 = HttpUtil.getCheckSum(data!!)
                if (afterSha256 == checksum) {
                    return finalData
                } else {
                    throw Exception(BaseApplication.context.getString(R.string.login_re_login_account))
                }
            }
            "ERROR_OTP_EXCEED_MAX_ATTEMPS" -> {
                //当天请求otp超过最大次数，需要第二天零点解除禁用
                throw Exception(BaseApplication.context.getString(R.string.login_otp_exceed_max_attempts))
            }
            else -> {
                throw Exception(message)
            }
        }
    }
}