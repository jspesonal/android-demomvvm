package com.wiseasy.smartpos.comm.http.smartpay

import com.wiseasy.smartpos.comm.bean.*
import okhttp3.RequestBody
import okhttp3.Response
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * 用于描述网络请求API的接口
 * @author Belle(Baiyunyan)
 * @date 2021/12/29
 */
interface ISmartPayAPI {

    /**
     * 获取二维码
     */
    @POST("createqr")
    suspend fun getQrCode(@Body body: RequestBody): BaseResult<QRPayBean>

    /**
     * 扫一扫
     */
    @POST("createquickpay")
    suspend fun quickPay(@Body body: RequestBody): BaseResult<ScanPayBean>

    /**
     * 查询交易状态
     */
    @POST("queryorder")
    suspend fun queryTransStatus(@Body body: RequestBody): BaseResult<TransDetailBean>

    /**
     * 发送邮件
     */
    @POST("sendreceipt")
    suspend fun sendEmail(@Body body: RequestBody): BaseResult<SendEmailBean>

    /**
     * 获取交易列表
     */
    @POST("gettranslist")
    suspend fun getTransList(@Body body: RequestBody): BaseResult<TransListBean>

    /**
     * 获取交易详情
     */
    @POST("gettransdetail")
    suspend fun getTransDetail(@Body body: RequestBody): BaseResult<TransDetailOnDetailPageBean>

    /**
     * 首次登录
     */
    @POST("auth/firstlogin")
    suspend fun firstLogin(@Body body: RequestBody): BaseResult<FirstLoginBean>

    /**
     * 验证otp
     */
    @POST("auth/verifyotp")
    suspend fun verifyOtp(@Body body: RequestBody): BaseResult<LoginBean>

    /**
     * 保存密码
     */
    @POST("savepassword")
    suspend fun savePassword(@Body body: RequestBody): BaseResult<Response>

    /**
     * 普通登录
     */
    @POST("auth/login")
    suspend fun login(@Body body: RequestBody): BaseResult<LoginBean>

    /**
     * logout
     */
    @POST("logout")
    suspend fun logout(@Body body: RequestBody): BaseResult<Response>

    /**
     * forgetpassword
     */
    @POST("auth/forgotpassword")
    suspend fun forgetPassword(@Body body: RequestBody): BaseResult<FirstLoginBean>
}