package com.wiseasy.smartpos.comm.http.smartpay

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.http.interceptor.CheckTokenInterceptor
import com.wiseasy.smartpos.comm.http.smartpay.util.HttpUtil.getBase64Data
import com.wiseasy.smartpos.comm.http.smartpay.util.HttpUtil.getCheckSum
import com.wiseasy.smartpos.comm.http.interceptor.HttpHeadInterceptor
import com.wiseasy.smartpos.comm.http.interceptor.NetworkCheckInterceptor
import com.wiseasy.smartpos.comm.http.smartpay.util.HeaderUtil
import okhttp3.Headers
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.fastjson.FastJsonConverterFactory
import java.util.concurrent.TimeUnit

@SuppressLint("StaticFieldLeak")
object RetrofitClientSmartPay {

    private lateinit var okHttpClient: OkHttpClient
    private lateinit var retrofit: Retrofit

    private val DEFAULT_MEDIATYPE = MediaType.parse("application/json; charset=utf-8")
    private lateinit var context: Context

    /**
     * 服务端地址
     */
    private var baseUrl: String = " https://sb-smartpos-interface.paysmart.com.vn/v2.0/"

    /**
     * 公共请求头集合
     * - 由于Headers为不可变，所以在此处使用MutableMap
     */
    private val commonHeadersMap = mutableMapOf<String, String>()

    private val apiMap = mutableMapOf<String, Any>()

    /**
     * 初始化
     */
    fun init(context: Context) {
        RetrofitClientSmartPay.context = context
        // 构建OkHttpClient
        okHttpClient = OkHttpClient.Builder()
            .addInterceptor(NetworkCheckInterceptor(context))       // 检查网络状态
            .addInterceptor(CheckTokenInterceptor(context))       // 检验token
            .addInterceptor(HttpHeadInterceptor(commonHeadersMap))  // 添加请求头公共信息
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))   // 日志
            .connectTimeout(60, TimeUnit.SECONDS)   // 超时时间为60s
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build()

        // 构建Retrofit实现类
        retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(FastJsonConverterFactory.create())     // 将数据转换成Json实例对象
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())  // 将结果转换成RxJava数据流
            .build()
    }

    /**
     * 增加公共Header
     */
    fun addCommonHeaders(headers: Headers? = null) {
        if (headers?.size()!! > 0) {
            headers.names().forEach {
                val value = headers[it]
                if (!value.isNullOrEmpty()) {
                    commonHeadersMap[it] = value
                    Log.d("addCommonHeaders", "addCommonHeaders|key=$it, value=$value")
                }
            }
        }
    }

    /**
     * 设置消息头
     */
    private fun setHeaders(context: Context, needToken: Boolean) {
        val requestId = HeaderUtil.getRequestId()
        commonHeadersMap["X-Request-ID"] = requestId
        if (needToken) {
            val token = SPUtil.getSmartPayToken(context)
            commonHeadersMap["X-Access-Token"] = token
        } else {
            commonHeadersMap.remove("X-Access-Token")
        }
    }

    /**
     * 获取RequestId
     */
    fun getRequestId(): String {
        return commonHeadersMap["X-Request-ID"].toString()
    }

    /**
     * 根据给出的Class构建其实例对象
     */
    fun <T> create(service: Class<T>): T {
        setHeaders(context, true)
        if (apiMap.containsKey(service.name)) {
            Log.d("create", "RetrofitClient|create|${service.name} is in the set")
            return apiMap[service.name] as T
        } else {
            Log.d("create", "RetrofitClient|create|${service.name} is creating")
            val t = retrofit.create(service)
            apiMap[service.name] = t as Any
            return t
        }
    }

    /**
     * 根据给出的Class构建其实例对象
     * firstlogin/login/forgetpassword/verifyotp这几个接口都不需要在header中送token
     */
    fun <T> createWithoutToken(service: Class<T>): T {
        setHeaders(context, false)
        if (apiMap.containsKey(service.name)) {
            Log.d("createWithoutToken", "RetrofitClient|create|${service.name} is in the set")
            return apiMap[service.name] as T
        } else {
            Log.d("createWithoutToken", "RetrofitClient|create|${service.name} is creating")
            val t = retrofit.create(service)
            apiMap[service.name] = t as Any
            return t
        }
    }

    fun buildRequestBody(param: JSONObject): RequestBody {
        val finalJson = JSONObject()
        val afterBase64 = getBase64Data(param)
        finalJson["data"] = afterBase64
        finalJson["checksum"] = getCheckSum(afterBase64)
        return RequestBody.create(DEFAULT_MEDIATYPE, finalJson.toJSONString())
    }
}