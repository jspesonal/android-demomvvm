package com.wiseasy.smartpos.comm.http.smartpay.util

import com.wiseasy.smartpos.base.util.UUIDUtil

/**
 * 网络请求关于消息头工具类
 * @author Belle(Baiyunyan)
 * @date 2022/01/04
 */
object HeaderUtil {

    /**
     * 获取requestID
     */
    fun getRequestId(): String {
        return UUIDUtil.getUUID().substring(0, 25)
    }

}