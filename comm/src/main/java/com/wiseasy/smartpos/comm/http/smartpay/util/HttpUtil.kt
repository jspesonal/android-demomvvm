package com.wiseasy.smartpos.comm.http.smartpay.util

import android.util.Log
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.util.Base64Utils
import com.wiseasy.smartpos.base.util.SHA256Util

/**
 * 与smartPay交互时的加解密
 * @author Belle(Baiyunyan)
 * @date 2021/12/31
 */
object HttpUtil {

    private const val TAG = "HttpUtil"
    private const val SMARTPAY_LOCAL_MASTER_KEY = "04031BE8827DAD8FCBD7C1BCECC04FFF07FAA07468CC4B13AD684DAFFC466421"

    /**
     * 加密
     * 加密规则：Base64加密
     */
    fun getBase64Data(data: JSONObject): String {
        val dataString = data.toJSONString()
        Log.d(TAG, "encrypt|base64加密前 before base64Encrypt = $dataString")
        val dataAfterBase64Encrypt = Base64Utils.encode(dataString.toByteArray())
        Log.d(TAG, "encrypt|base64加密后 after base64Encrypt = $dataAfterBase64Encrypt")
        return dataAfterBase64Encrypt
    }

    /**
     * 获取校验值
     * 获取规则：Base64加密后的数据 + "|" + LMK
     */
    fun getCheckSum(encryptData: String): String {
        val spliceDataAndKey = "$encryptData|$SMARTPAY_LOCAL_MASTER_KEY"
        Log.d(TAG, "getCheckSum|sha256加密前 before sha256Encrypt = $spliceDataAndKey")
        val checkSum = SHA256Util.sha256Encrypt(spliceDataAndKey)
        Log.d(TAG, "getCheckSum|sha256加密后 after sha256Encrypt = $checkSum")
        return checkSum
    }


}