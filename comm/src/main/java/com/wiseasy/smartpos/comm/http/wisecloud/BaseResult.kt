package com.wiseasy.smartpos.comm.http.wisecloud

import android.text.TextUtils
import android.util.Log
import com.alibaba.fastjson.JSON
import com.google.gson.Gson
import com.wiseasy.smartpos.base.BaseApplication
import com.wiseasy.smartpos.comm.R
import com.wiseasy.smartpos.comm.http.wisecloud.util.AesUtil
import com.wiseasy.smartpos.comm.http.wisecloud.util.SignUtil
import org.json.JSONArray
import java.lang.Exception
import java.lang.reflect.Type

/**
 * WiseCloud返回的标准数据类型
 * @author Belle(Baiyunyan)
 * @date 2022/01/18
 */
class BaseResult<T> {
    var code: Int = -1
    var count: Int = 0
    var data: String? = ""
    var extra: String? = ""
    var msg: String? = ""
    var path: String? = ""
    var timestamp: Long = 0
    var signatureValue: String? = ""

    /**
     * 数据转换，将后台返回的加密数据转换为本地可识别的实体类数据
     * @return T 解密后的实际用到的数据类型
     */
    fun dataConvert(type: Class<T>?): T {
        when (code) {
            0 -> {
                val decodeData = AesUtil.decrypt(data, RetrofitClientWiseCloud.ACCESS_KEY_SECRET)
                Log.d("BaseResult", "返回数据(解密后)：$decodeData")
                val finalData: T = Gson().fromJson(decodeData, type)
                //将后台返回的data数据做签名加密，如果加密后的数据跟后台返回的signatureValue一致，则说明数据没有被篡改，否则则说明数据被篡改
                val afterSign = SignUtil.createSign(data, RetrofitClientWiseCloud.ACCESS_KEY_SECRET)
                if (TextUtils.isEmpty(data) || afterSign == signatureValue) {
                    return finalData
                } else {
                    throw Exception(BaseApplication.context.getString(R.string.login_re_login_account))
                }
            }
            else -> throw Exception(msg)
        }
    }

    /**
     * 数据转换，将后台返回的加密数据转换为本地可识别的实体类数据
     * @param type:  val typeTemp = object : TypeToken<MutableList<UserBean>>() {}.type
     * @return T 解密后的实际用到的数据类型
     */
    fun dataConvertByList(type: Type): T {
        when (code) {
            0 -> {
                val decodeData = AesUtil.decrypt(data, RetrofitClientWiseCloud.ACCESS_KEY_SECRET)
                Log.d("BaseResult", "返回数据(解密后)：$decodeData")
                val finalData: T = Gson().fromJson(decodeData, type)
                //将后台返回的data数据做签名加密，如果加密后的数据跟后台返回的signatureValue一致，则说明数据没有被篡改，否则则说明数据被篡改
                val afterSign = SignUtil.createSign(data, RetrofitClientWiseCloud.ACCESS_KEY_SECRET)
                if (afterSign == signatureValue) {
                    return finalData
                } else {
                    throw Exception(BaseApplication.context.getString(R.string.login_re_login_account))
                }
            }
            else -> throw Exception(msg)
        }
    }
}