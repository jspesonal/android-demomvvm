package com.wiseasy.smartpos.comm.http.wisecloud

import com.wiseasy.smartpos.comm.bean.*
import okhttp3.RequestBody
import okhttp3.Response
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * 用于描述网络请求API的接口
 * @author Belle(Baiyunyan)
 * @date 2022/01/19
 */
interface IWiseCloudAPI {

    /**
     * 获取商户信息
     */
    @POST("device/service/dms/merchant/mappingInfo")
    suspend fun getMerchantInfo(@Body body: RequestBody): BaseResult<MerchantMappingInfo>

    /**
     * 查询员工信息列表
     */
    @POST("device/service/dms/merchant/staff/list")
    suspend fun getUserList(@Body body: RequestBody): BaseResult<MutableList<UserBean>>

    /**
     * 修改员工信息
     */
    @POST("device/service/dms/merchant/staff/update")
    suspend fun updateUserInfo(@Body body: RequestBody): BaseResult<Response>

    /**
     * 获取商户门店列表
     */
    @POST("device/service/dms/merchant/storelist")
    suspend fun getStoreList(@Body body: RequestBody): BaseResult<MutableList<StoreBean>>

    /**
     * 删除员工信息
     */
    @POST("device/service/dms/merchant/staff/delete")
    suspend fun deleteUser(@Body body: RequestBody): BaseResult<Response>

    /**
     * 增加员工信息
     */
    @POST("device/service/dms/merchant/staff/add")
    suspend fun addUser(@Body body: RequestBody): BaseResult<Response>

    /**
     * 验证当前身份信息
     */
    @POST("device/service/dms/merchant/staff/authentication")
    suspend fun verifyPassword(@Body body: RequestBody): BaseResult<VerifyPassword>

    /**
     * 修改密码
     */
    @POST("device/service/dms/merchant/staff/modifyPassword")
    suspend fun changePassword(@Body body: RequestBody): BaseResult<Response>

    /**
     * 升级APP应用
     */
    @POST("interface/service/market/app/updatable/app")
    suspend fun updateApp(@Body body: RequestBody): BaseResult<UpdateApp>
}