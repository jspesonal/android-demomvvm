package com.wiseasy.smartpos.comm.http.wisecloud

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.comm.http.interceptor.CheckTokenInterceptor
import com.wiseasy.smartpos.comm.http.interceptor.HttpHeadInterceptor
import com.wiseasy.smartpos.comm.http.interceptor.NetworkCheckInterceptor
import com.wiseasy.smartpos.comm.http.wisecloud.util.AesUtil
import com.wiseasy.smartpos.comm.http.wisecloud.util.SignUtil
import okhttp3.Headers
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.fastjson.FastJsonConverterFactory
import java.util.concurrent.TimeUnit

@SuppressLint("StaticFieldLeak")
object RetrofitClientWiseCloud {

    private lateinit var okHttpClient: OkHttpClient
    private lateinit var retrofit: Retrofit

    private val DEFAULT_MEDIATYPE = MediaType.parse("application/json; charset=utf-8")
    private lateinit var context: Context

    /**
     * 服务端地址
     */

//    private var baseUrl: String = "https://wisecloud-pre.wiseasy.com/"//测试环境
//    private var baseUrl: String = "https://wisecloud-pre.wiseasy.com/wisecloud-gateway-open-platform/device/service/"//测试环境
    private var baseUrl: String = "https://wisecloud-pre.wiseasy.com/wisecloud-gateway-open-platform/"//测试环境

    /**
     * 自己生成的access
     */
//    private val ACCESS_KEY_ID = "61e6901ce8cd52000114fe56"
//    val ACCESS_KEY_SECRET = "rtRwtHFPJmBGwVYRbFXqfEcWLKXeMPmf"

    /**
     * SmartPay提供的access
     */
    private val ACCESS_KEY_ID = "61e5591de8cd52000114fe55"
    val ACCESS_KEY_SECRET = "ZUCKyxLWPPpMyPlptWwAyEwUTwcxUfkB"

    /**
     * 公共请求头集合
     * - 由于Headers为不可变，所以在此处使用MutableMap
     */
    private val commonHeadersMap = mutableMapOf<String, String>()

    private val apiMap = mutableMapOf<String, Any>()

    /**
     * 初始化
     */
    fun init(context: Context) {
        this.context = context
        // 构建OkHttpClient
        okHttpClient = OkHttpClient.Builder()
            .addInterceptor(NetworkCheckInterceptor(context))       // 检查网络状态
            .addInterceptor(CheckTokenInterceptor(context))       // 检验token
            .addInterceptor(HttpHeadInterceptor(commonHeadersMap))  // 添加请求头公共信息
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))   // 日志
            .connectTimeout(60, TimeUnit.SECONDS)   // 超时时间为60s
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build()

        // 构建Retrofit实现类
        retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(FastJsonConverterFactory.create())     // 将数据转换成Json实例对象
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())  // 将结果转换成RxJava数据流
            .build()
    }

    /**
     * 增加公共Header
     */
    fun addCommonHeaders(headers: Headers? = null) {
        if (headers?.size()!! > 0) {
            headers.names().forEach {
                val value = headers[it]
                if (!value.isNullOrEmpty()) {
                    commonHeadersMap[it] = value
                    Log.d("addCommonHeaders", "addCommonHeaders|key=$it, value=$value")
                }
            }
        }
    }

    /**
     * 根据给出的Class构建其实例对象
     */
    fun <T> create(service: Class<T>): T {
        if (apiMap.containsKey(service.name)) {
            Log.d("create", "RetrofitClient|create|${service.name} is in the set")
            return apiMap[service.name] as T
        } else {
            Log.d("create", "RetrofitClient|create|${service.name} is creating")
            val t = retrofit.create(service)
            apiMap[service.name] = t as Any
            return t
        }
    }

    fun buildRequestBody(data: JSONObject): RequestBody {
        Log.d("buildRequestBody", "data加密前：$data")
        val param = JSONObject()
        param["version"] = "v1.0"
        param["accessKeyId"] = ACCESS_KEY_ID
        param["timestamp"] = System.currentTimeMillis()
        param["nonce"] = System.currentTimeMillis()
        param["signType"] = "MD5"
        param["encryptionSwitch"] = "on"

//        val dataMap = convertJsonToMap(param.toJSONString())
//        val dataJson = convertMapToJson(dataMap)
        val dataEncrypt = AesUtil.encrypt(data.toString(), ACCESS_KEY_SECRET)//对每个API特有的字段进行加密
        param["data"] = dataEncrypt

        val getSignValue = SignUtil.createSign(dataEncrypt, ACCESS_KEY_SECRET)//只对data的值进行签名
        param["signatureValue"] = getSignValue
        return RequestBody.create(DEFAULT_MEDIATYPE, param.toJSONString())
    }
}