package com.wiseasy.smartpos.comm.http.wisecloud.util;

import android.util.Log;

import com.wiseasy.smartpos.base.util.Base64Utils;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import java.util.Base64;

/**
 * AES commonly used decryption and encryption tool class
 * https://github.com/ourlang
 *
 * @author Kobayashi
 */
public class AesUtil {

    /**
     * Default character encoding
     */
    private static final String DEFAULT_CHARSET = "utf-8";

    /**
     * Algorithm
     */
    private static String ALGORITHM = "AES";


    /**
     * Algorithm/Pattern/Fill
     **/
    private static final String CipherMode = "AES/ECB/PKCS5Padding";


    /**
     * Logging
     **/
    private final static String TAG = "AesUtil ->";

    private AesUtil() {
    }

    /**
     * Decrypt AES 32-bit
     *
     * @param sSrc      decrypted content
     * @param secretKey
     * @return decrypted plain text data
     */
    public static String decrypt(String sSrc, String secretKey) {

        //Determine whether the Key is 32 bits
        if (secretKey.length() != 32) {
            Log.d(TAG, "Key length is not 32 bits");
            return null;
        }
        try {
            byte[] raw = secretKey.getBytes(DEFAULT_CHARSET);
            SecretKeySpec secretKeySpec = new SecretKeySpec(raw, ALGORITHM);
            Cipher cipher = Cipher.getInstance(CipherMode);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            //Decrypt with base64 first
//            byte[] encryptedArr = Base64.getDecoder().decode(sSrc);//baibai
            byte[] encryptedArr = Base64Utils.decode(sSrc);
            byte[] original = cipher.doFinal(encryptedArr);
            return new String(original, DEFAULT_CHARSET);
        } catch (Exception ex) {
            Log.d(TAG, "AES decryption failed");
            return null;
        }
    }


    /**
     * Encrypted 32-bit
     *
     * @param sSrc requires encrypted content
     * @param sKey
     * @return encrypted content
     */
    public static String encrypt(String sSrc, String sKey) {
        //Determine whether the Key is 32 bits
        if (sKey.length() != 32) {
            Log.d(TAG, "Key length is not 32 bits");
            return null;
        }
        try {
            byte[] raw = sKey.getBytes(DEFAULT_CHARSET);
            SecretKeySpec skeySpec = new SecretKeySpec(raw, ALGORITHM);
            Cipher cipher = Cipher.getInstance(CipherMode);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encrypted = cipher.doFinal(sSrc.getBytes(DEFAULT_CHARSET));

//            return Base64.getEncoder().encodeToString(encrypted);//baibai
            return Base64Utils.encode(encrypted);
        } catch (Exception ex) {
            Log.d(TAG, "AES encryption failed", ex);
            return null;
        }
    }
}