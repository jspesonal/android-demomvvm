package com.wiseasy.smartpos.comm.http.wisecloud.util

import java.io.UnsupportedEncodingException
import java.lang.StringBuilder
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/19
 */

/**
 * 8到128位随机数
 */
fun getNonce(): String {
    val str = StringBuilder()
    val random = Random()
    //生成随机位数[8, 128]
//        val bit = random.nextInt(121) + 8
    val bit = random.nextInt(2) + 8
    //轮询每一位上的数[1,bit]
    for (i in 1..bit) {
        str.append(random.nextInt(10))
    }
    return str.toString()
}

/**
 * 获取MD5字符串
 * @param input String
 * @return String?
 */
fun getMD5String(input: String): String {
    var messageDigest: MessageDigest? = null
    try {
        messageDigest = MessageDigest.getInstance("MD5")
        messageDigest.reset()
        messageDigest.update(input.toByteArray(charset("UTF-8")))
    } catch (e: NoSuchAlgorithmException) {
        e.printStackTrace()
    } catch (e: UnsupportedEncodingException) {
        e.printStackTrace()
    }
    val byteArray = messageDigest!!.digest()
    val md5StrBuff = StringBuffer()
    for (i in byteArray.indices) {
        if (Integer.toHexString(0xFF and byteArray[i].toInt()).length == 1) md5StrBuff.append("0").append(
            Integer.toHexString(0xFF and byteArray[i].toInt())
        ) else md5StrBuff.append(Integer.toHexString(0xFF and byteArray[i].toInt()))
    }
//    return md5StrBuff.toString().toUpperCase()
    return md5StrBuff.toString()
}
