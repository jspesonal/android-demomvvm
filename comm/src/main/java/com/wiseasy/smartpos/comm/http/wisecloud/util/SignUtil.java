/**
 * @Title: SignUtil.java
 * @Package com.jushi.test
 * @Description: TODO (describe what the file does in one sentence)
 * @author johnny
 * @Date May 15, 2017, 7:25:48 PM
 * @version V1.0
 */
package com.wiseasy.smartpos.comm.http.wisecloud.util;

import static com.wiseasy.smartpos.comm.http.wisecloud.util.CommonDataUtilKt.getMD5String;

import android.util.Log;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.security.SignatureException;
import java.util.*;


public class SignUtil {
    private static String TAG = "SignUtil ->";

    /**
     * @param content
     * @param charset
     * @return
     * @throws SignatureException
     * @throws UnsupportedEncodingException
     */
    private static byte[] getContentBytes(String content, String charset) {
        if (charset == null || "".equals(charset)) {
            return content.getBytes();
        }
        try {
            return content.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("There was an error in the MD5 signature process, the specified encoding set is wrong, the encoding set you currently specified is:" + charset);
        }
    }

    /**
     * 对数据进行签名，并返回, 入参是Map
     *
     * @param requestBody
     * @param signKey
     * @return
     */
    public static String createSign(Map<String, Object> requestBody, String signKey) {
        String data = convertMapToJson(requestBody);
        Log.d(TAG, "Signature original string:" + data + signKey);
//        String sign = DigestUtils.md5Hex((getContentBytes(data + signKey, "UTF-8")));//baibai
        String sign = getMD5String(data + signKey);
        Log.d(TAG, "Signed result string:" + sign);
        return sign;
    }

    /**
     * 对数据进行签名，并返回 入参是String
     */
    public static String createSign(String data, String signKey) {
        Log.d(TAG, "Signed original string:" + data);
//        String sign = DigestUtils.md5Hex((getContentBytes(data + signKey, "UTF-8")));//baibai
        String sign = getMD5String(data + signKey);//baibai
        Log.d(TAG, "Signed result string:" + sign);
        return sign;
    }

    /**
     * 对数据进行签名，并返回 入参是json
     */
    public static String createSign(JSONObject json, String signKey) {
        Log.d(TAG, "原始Json数据：" + json);
        Map<String, Object> dataMap = jsonToMap(json);
        String sign = createSign(dataMap, signKey);
        return sign;
    }

    public static Boolean signVerify(String data, String sign, String signKey) {
        String newSign = createSign(data, signKey);
        return newSign.equals(sign);
    }

    public static Boolean signVerify(Map<String, Object> data, String sign, String signKey) {
        String newSign = createSign(data, signKey);
        return newSign.equals(sign);
    }

    public static String convertMapToJson(Map<String, Object> requestBody) {
        requestBody.remove("signatureValue");
        TreeSet<String> sortedKey = new TreeSet<String>(requestBody.keySet());
        StringBuilder builer = new StringBuilder();
        for (String key : sortedKey) {
            builer.append(key).append("=").append(convertObjectToJson(requestBody.get(key))).append("&");
        }
        String result = builer.toString();
        //logger.info("result："+result);
        String jsonString = result.substring(0, result.length() - 1);
        Log.d(TAG, "convertMapToJson|Map转成Json后：" + jsonString);
        return jsonString;
    }

    public static String convertObjectToJson(Object obj) {
        //logger.info("obj："+obj);
        if (obj == null) {
            return "";
        }
        if (obj.getClass().isArray()) {
            return StringUtils.join((Object[]) obj, "&");
        } else if (obj instanceof Map) {
            return convertMapToJson((Map<String, Object>) obj);
        } else if (obj.getClass().isPrimitive() || obj.getClass() == String.class) {
            return String.valueOf(obj);
        } else if (obj instanceof Collection) {
            StringBuilder builder = new StringBuilder();
            for (Object _obj : (List<Object>) obj) {
                builder.append(convertObjectToJson(_obj)).append("&");
            }
            String result = builder.toString();
            if (result == null || result.equals("")) {
                return "";
            } else {
                return result.substring(0, result.length() - 1);
            }
        } else {
            return String.valueOf(obj);
        }
    }


    public static Map<String, Object> convertJsonToMap(String jsonStr) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if (jsonStr != null) {
            JSONObject json = JSONObject.parseObject(jsonStr);
            retMap = jsonToMap(json);
        }
        Log.d(TAG, "Json转成Map后：" + retMap);
        return retMap;
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = json.keySet().iterator();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = json.get(key);

            if (value instanceof JSONArray) {
                value = jsonToList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = jsonToMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> jsonToList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.size(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = jsonToList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = jsonToMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
}