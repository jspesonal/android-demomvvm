package com.wiseasy.smartpos.comm.module.service

import android.content.Context
import com.wiseasy.smartpos.comm.bean.MerchantMappingInfo

/**
 * Created by Belle on 2021/12/29.
 */
interface LoginModuleService {

    /**
     * 跳转到主界面
     */
    fun goToHomeActivity(context: Context)

    /**
     * 跳转到登录界面
     */
    fun goToLoginActivity(context: Context)

    /**
     * 获取商户信息
     */
    fun getMerchantInfo(context: Context): MerchantMappingInfo

}