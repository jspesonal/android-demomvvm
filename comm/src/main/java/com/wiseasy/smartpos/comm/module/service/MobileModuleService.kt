package com.wiseasy.smartpos.comm.module.service

import android.content.Context

/**
 * Created by Bella on 2021/12/20.
 */
interface MobileModuleService {
    /**
     * 跳转到SmartPay界面
     */
    fun goToSmartPayActivity(context: Context)

    /**
     * 跳转到详情界面
     * @param orderNo String    类似交易号的唯一标识
     */
    fun goToCodePayDetailActivity(context: Context, orderNo: String)
}