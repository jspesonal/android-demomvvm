package com.wiseasy.smartpos.comm.module.service

import android.content.Context

/**
 * Created by Bella on 2021/12/20.
 */
interface OtherModuleService {

    /**
     * 跳转到Other界面
     */
    fun goToOtherActivity(context: Context)

}