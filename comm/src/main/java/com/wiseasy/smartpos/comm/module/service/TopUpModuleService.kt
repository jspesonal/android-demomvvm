package com.wiseasy.smartpos.comm.module.service

import android.content.Context

interface TopUpModuleService {
    fun goToTopUpActivity(context: Context)

}