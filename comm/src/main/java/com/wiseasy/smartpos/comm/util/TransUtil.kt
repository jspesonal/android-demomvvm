package com.wiseasy.smartpos.comm.util

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.util.Log
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.Constant
import com.wiseasy.smartpos.comm.Constant.PHONE_REGEX
import com.wiseasy.smartpos.comm.R
import com.wiseasy.smartpos.comm.http.wisecloud.util.getMD5String
import java.util.regex.Pattern

/**
 * @author Belle(Baiyunyan)
 * @date 2021/12/28
 */

private val TAG = "TransUitl"

/**
 * 根据交易类型获取图标
 */
fun getIconByType(context: Context, type: String): Drawable? {
    var icon: Drawable? = null
    when (type) {
        Constant.PAYMENT_METHOD_SCAN_TO_PAY -> icon = context.getDrawable(R.mipmap.other_scan)
        Constant.PAYMENT_METHOD_QR_CODE -> icon = context.getDrawable(R.mipmap.other_qr)
    }
    return icon
}

/**
 * 根据交易状态获取图标(交易结果页)
 */
fun getIconByStatusOnResultPage(context: Context, status: String): Drawable? {
    return when (status) {
        Constant.TRANS_STATUS_PROCESSING, Constant.TRANS_STATUS_OPEN -> context.getDrawable(R.mipmap.mobile_result_pending)
        Constant.TRANS_STATUS_PAYED -> context.getDrawable(R.mipmap.mobile_result_success)
        Constant.TRANS_STATUS_PAY_ERROR -> context.getDrawable(R.mipmap.mobile_result_fail)
        else -> context.getDrawable(R.mipmap.mobile_result_pending)
    }
}

/**
 * 根据交易状态获取图标(交易详情页)
 */
fun getIconByStatusOnDetailPage(context: Context, status: String): Drawable? {
    return when (status) {
        Constant.TRANS_STATUS_DETAIL_SETTLEMENT -> context.getDrawable(R.mipmap.mobile_detail_settlement_black)
        Constant.TRANS_STATUS_DETAIL_SUCCESS -> context.getDrawable(R.mipmap.mobile_detail_success)
        Constant.TRANS_STATUS_DETAIL_PROCESSING -> context.getDrawable(R.mipmap.mobile_detail_pending)
        Constant.TRANS_STATUS_DETAIL_FAIL -> context.getDrawable(R.mipmap.mobile_detail_fail)
        else -> context.getDrawable(R.mipmap.mobile_detail_pending)
    }
}

/**
 * 根据支付方式获取支付方式名称(交易详情页)
 */
fun getPaymentMethodNameByPaymentMethod(context: Context, method: String?): String {
    return when (method) {
        Constant.PAYMENT_METHOD_QR_CODE -> context.getString(R.string.mobile_qr_pay)
        Constant.PAYMENT_METHOD_SCAN_TO_PAY -> context.getString(R.string.mobile_scan_to_pay)
        else -> ""
    }
}

/**
 * 根据交易类型获取交易类型名称(交易列表页)
 */
fun getStatusNameByStatus(context: Context, status: String?): String {
    var statusName = ""
    statusName = when (status) {
        Constant.TRANS_STATUS_LIST_SUCCESS -> context.getString(R.string.other_trans_list_result_success)
        Constant.TRANS_STATUS_LIST_FAIL -> context.getString(R.string.other_trans_list_result_fail)
        Constant.TRANS_STATUS_LIST_PROCESSING -> context.getString(R.string.other_trans_list_result_pending)
        Constant.TRANS_STATUS_LIST_SETTLEMENT -> context.getString(R.string.other_trans_list_result_settlement)
        else -> ""
    }
    return statusName
}

/**
 * 根据交易类型获取交易类型名称（交易结果页）
 */
fun getResultByStatus(context: Context, status: String?): String {
    var statusName = ""
    statusName = when (status) {
        Constant.TRANS_STATUS_PROCESSING, Constant.TRANS_STATUS_OPEN -> context.getString(R.string.mobile_pay_result_pending)
        Constant.TRANS_STATUS_PAYED -> context.getString(R.string.mobile_pay_result_success)
        Constant.TRANS_STATUS_PAY_ERROR -> context.getString(R.string.mobile_pay_result_fail)
        else -> status!!
    }
    return statusName
}

/**
 * 设置baseAuth
 * 最后一个参数不要动，不要动，不要动
 */
fun putBasicAuth(context: Context, jsonObject: JSONObject, isNeedWid: Boolean = true) {
    jsonObject["spMID"] = SPUtil.getSmartPaySpMid(context)
    jsonObject["spTID"] = SPUtil.getSmartPaySpTid(context)
    jsonObject["deviceSN"] = CommonUtil.getSN(context)
    //是否是小微商户，如果wid不为空，则说明是小微商户
    val isMicroMerchant = !TextUtils.isEmpty(SPUtil.getSmartPayWid(context))
    //如果时小微商户，且是payment/transaction则说明需要传该字段
    if (isNeedWid && isMicroMerchant) {
        jsonObject["wid"] = SPUtil.getSmartPayWid(context)
    }
    jsonObject["phone"] = SPUtil.getSmartPayPhone(context)
}

/**
 * 设置baseAuth-首次登录
 */
fun putBasicAuthFirstLogin(context: Context, jsonObject: JSONObject) {
    jsonObject["deviceSN"] = CommonUtil.getSN(context)
    //超管手机号
    jsonObject["phone"] = SPUtil.getSmartPayPhone(context)
}

/**
 * 设置baseAuth-验证otp(首次登录时)
 */
fun putBasicAuthVerifyOtp(context: Context, otp: String, jsonObject: JSONObject) {
    jsonObject["deviceSN"] = CommonUtil.getSN(context)
    jsonObject["phone"] = SPUtil.getSmartPayPhone(context)
    jsonObject["otp"] = otp
}

/**
 * 设置baseAuth-保存密码(首次登录)
 */
fun putBasicAuthSavePassword(context: Context, pwd: String, jsonObject: JSONObject) {
    jsonObject["deviceSN"] = CommonUtil.getSN(context)
    jsonObject["phone"] = SPUtil.getSmartPayPhone(context)
    jsonObject["password"] = getMD5String(pwd)
}

/**
 * 检查email是否正确
 */
fun checkEmail(email: String): Boolean {
    val REGEX_EMAIL = "^([a-z0-9A-Z]+[-_|\\.]?)+[a-z0-9A-Z]+[-_|\\.]?@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
    val pattern = Pattern.compile(REGEX_EMAIL)
    return pattern.matcher(email.trim()).matches()
}

fun getUserRoleName(context: Context, position: Int): String {
    var role: String = when (position) {
        0 -> context.getString(R.string.other_user_info_role_admin)
        1 -> context.getString(R.string.other_user_info_role_manager)
        2 -> context.getString(R.string.other_user_info_role_clerk)
        else -> context.getString(R.string.other_user_info_role_clerk)
    }
    return role
}

fun getUserRoleCode(context: Context, role: String): Int {
    var position: Int = when (role) {
        context.getString(R.string.other_user_info_role_admin) -> 0
        context.getString(R.string.other_user_info_role_manager) -> 1
        context.getString(R.string.other_user_info_role_clerk) -> 2
        else -> 2
    }
    return position
}

fun getUserStatusName(context: Context, status: Int): String {
    var userStatus: String = when (status) {
        1 -> context.getString(R.string.other_user_info_status_active)
        2 -> context.getString(R.string.other_user_info_status_disable)
        else -> context.getString(R.string.other_user_info_status_disable)
    }
    return userStatus
}

fun getUserStatusCode(context: Context, status: String): Int {
    var userCode: Int = when (status) {
        context.getString(R.string.other_user_info_status_active) -> 1
        context.getString(R.string.other_user_info_status_disable) -> 2
        else -> 1
    }
    return userCode
}

/**
 * 设置baseAuth login
 */
fun putBasicAuthLogin(context: Context, pwd: String, jsonObject: JSONObject, isNeedWid: Boolean = false) {
    if (!TextUtils.isEmpty(SPUtil.getSmartPaySpMid(context))) {
        jsonObject["spMID"] = SPUtil.getSmartPaySpMid(context)
    }
    if (!TextUtils.isEmpty(SPUtil.getSmartPaySpTid(context))) {
        jsonObject["spTID"] = SPUtil.getSmartPaySpTid(context)
    }
    jsonObject["deviceSN"] = CommonUtil.getSN(context)
    if (isNeedWid) {
        jsonObject["wid"] = SPUtil.getSmartPayWid(context)
    }
    jsonObject["phone"] = SPUtil.getSmartPayPhone(context)
    jsonObject["password"] = getMD5String(pwd)
}

/**
 * 是否是正确的手机号
 */
fun isCorrectPhone(phone: String): Boolean {
    val pattern = Pattern.compile(PHONE_REGEX)
    val isCorrect = pattern.matcher(phone).matches()
    Log.d(TAG, "isCorrectPhone|isCorrect = $isCorrect")
    return isCorrect
}