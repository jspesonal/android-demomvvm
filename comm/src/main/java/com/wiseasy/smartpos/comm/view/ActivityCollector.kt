package com.wiseasy.smartpos.comm.view

import android.app.Activity
import android.util.Log
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Bella on 2022/2/10.
 * 单例类作为Activity的集合
 */
object ActivityCollector {
    private val activities = ArrayList<Activity>()

    fun addActivity(activity: Activity) {
        if (!activities.contains(activity)) {
            activities.add(activity)
        }
    }

    fun removeActivity(activity: Activity) {
        activities.remove(activity)
    }

    fun finishAll() {
        for (activity in activities) {
            if (!activity.isFinishing) {
                activity.finish()
            }
        }
        activities.clear()
    }

    fun finishAllWithoutCurrent(currentActivity: Activity) {
        for (activity in activities) {
            if ((!activity.isFinishing) && (activity != currentActivity)) {
                activity.finish()
            }
        }
        activities.clear()
        activities.add(currentActivity)
    }
}