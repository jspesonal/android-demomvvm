package com.wiseasy.smartpos.comm.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.viewbinding.ViewBinding
import com.wiseasy.smartpos.base.mvp.BaseActivity
import com.wiseasy.smartpos.base.util.ContextWrapper
import com.wiseasy.smartpos.base.util.LanguageUtil.getLanguageLocale
import com.wiseasy.smartpos.base.util.SPUtil

/**
 * Created by Bella on 2022/1/5.
 */
abstract class CommonActivity<T : ViewBinding> : BaseActivity<T>() {
    protected var mLoading: CommonLoadingDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        log("onCreate: ")
        ActivityCollector.addActivity(this)
    }

    override fun onRestart() {
        super.onRestart()
        log("onRestart")
        //如果修改了语言，则重新加载当前界面
        if (SPUtil.getIsChangeLanguage(this)) {
            startActivity(Intent(this, this.javaClass))
            finish()
        }
    }

    override fun onDestroy() {
        log("onDestroy")
        if (mLoading != null) {
            mLoading?.dismiss()
        }
        ActivityCollector.removeActivity(this)
        super.onDestroy()
    }

    override fun attachBaseContext(newBase: Context?) {
        log("attachBaseContext")
        val newLocale = getLanguageLocale(this)
        val context: Context = ContextWrapper.wrap(newBase, newLocale)
        super.attachBaseContext(context)
    }

    override fun showLoading() {
        showLoading(null)
    }

    override fun showLoading(msg: String?) {
        if (mLoading == null) {
            mLoading = CommonLoadingDialog(this)
        }
        if (msg != null) {
            mLoading?.setMsg(msg)
        }
        mLoading?.show()
    }

    override fun showLoading(msg: String, isCancel: Boolean) {
        if (mLoading != null) {
            mLoading?.dismiss()
        }
        mLoading = CommonLoadingDialog(this, msg)
        mLoading?.setCancelable(isCancel)
        mLoading?.show()
    }
//   override fun showLoading(msg: String, drawableId: Int, isCancel: Boolean) {
//        if (mLoading != null) {
//            mLoading.dismiss()
//        }
//       mLoading = CommonLoadingDialog(this, msg)
//       mLoadin
//    }

    override fun hideLoading() {
        if (mLoading != null) {
            mLoading?.dismiss()
        }
    }
}