package com.wiseasy.smartpos.comm.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import com.wiseasy.smartpos.comm.R
import com.wiseasy.smartpos.comm.databinding.CommonViewCustomNumberInputKeyboardBinding
import java.util.ArrayList
import java.util.regex.Pattern

/**
 * 自定义数字键盘
 */
@SuppressLint("ViewConstructor")
class CommonCustomNumberInputKeyboard(context: Context, attrs: AttributeSet? = null) : LinearLayout(context, attrs), View.OnClickListener, View.OnLongClickListener {

    private val mBtnNums: MutableList<Button> = ArrayList()
    private var mItemHeight = 0
    private var mItemTextSize = 0f
    private val mKeyBoardType = 0
    private var mKeyBoardClickListener: KeyBoardClickListener? = null
    private var mInput = "" //已经输入的数字
    private var mPatternCalculation = "^(([1-9]{1}\\d{0,20})|([0]{1}))(\\.(\\d){0,2})?" //计算器模式
    private val mPatternPassword = "^(\\d{0,6})" //密码键盘模式
    private val mBinding: CommonViewCustomNumberInputKeyboardBinding = CommonViewCustomNumberInputKeyboardBinding.inflate(LayoutInflater.from(context))
    private var mIsCanZero = false//输入第一个字符是否可以为0

    init {
        setLinearLayoutStyle(context, attrs)
        addView(mBinding.root, LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT))
    }

    private fun setLinearLayoutStyle(context: Context, attrs: AttributeSet?) {
        val array = context.obtainStyledAttributes(attrs, R.styleable.CommonCustomNumberInputKeyboard)
        mItemHeight = array.getDimensionPixelSize(R.styleable.CommonCustomNumberInputKeyboard_itemHeight, -1)
        mItemTextSize = array.getDimension(R.styleable.CommonCustomNumberInputKeyboard_itemTextSize, 30f)
        val btnText = array.getString(R.styleable.CommonCustomNumberInputKeyboard_buttonText)
        btnText?.let { setButtonText(it) }
        val isShowClear = array.getBoolean(R.styleable.CommonCustomNumberInputKeyboard_isShowClear, true)
        setClearIsShow(isShowClear)

//        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
//        mBinding.btnNum0.setPadding(0, 0, windowManager.defaultDisplay.width / 4, 0)
        mBinding.rlDel.setOnClickListener(this)
        mBinding.rlDel.setOnLongClickListener(this)
        mBinding.btnKeyboardNext.setOnClickListener(this)
        mBtnNums.add(mBinding.btnNum0)
        mBtnNums.add(mBinding.btnNum1)
        mBtnNums.add(mBinding.btnNum2)
        mBtnNums.add(mBinding.btnNum3)
        mBtnNums.add(mBinding.btnNum4)
        mBtnNums.add(mBinding.btnNum5)
        mBtnNums.add(mBinding.btnNum6)
        mBtnNums.add(mBinding.btnNum7)
        mBtnNums.add(mBinding.btnNum8)
        mBtnNums.add(mBinding.btnNum9)
        mBtnNums.add(mBinding.btnNum00)
        mBtnNums.add(mBinding.btnClear)
        if (mItemHeight == -1) {
            for (i in mBtnNums.indices) {
                mBtnNums[i].setOnClickListener(this)
                mBtnNums[i].textSize = mItemTextSize
            }
        } else {
            for (i in mBtnNums.indices) {
                setViewHeight(mBtnNums[i])
                mBtnNums[i].setOnClickListener(this)
                mBtnNums[i].textSize = mItemTextSize
            }
            setViewHeight(mBinding.rlDel)
            mBinding.btnKeyboardNext.height = getViewHeight()
        }
        array.recycle()
    }

    /**
     * 设置子控件的高度
     */
    private fun setViewHeight(view: View) {
        val params = view.layoutParams as LayoutParams
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val height = (windowManager.defaultDisplay.height * mItemHeight / 1280.0).toInt()
        params.height = height
        view.layoutParams = params
    }

    /**
     * 获取每一行控件的高度
     */
    private fun getViewHeight(): Int {
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        return (windowManager.defaultDisplay.height * mItemHeight / 1280.0).toInt()
    }

    override fun onClick(v: View) {
        if (mKeyBoardClickListener == null) return
        val i = v.id
        when (v.id) {
            R.id.btn_num0,
            R.id.btn_num1,
            R.id.btn_num2,
            R.id.btn_num3,
            R.id.btn_num4,
            R.id.btn_num5,
            R.id.btn_num6,
            R.id.btn_num7,
            R.id.btn_num8,
            R.id.btn_num9 -> {
                input((v as Button).text.toString())
            }
            R.id.rl_del -> {
                input("del")
            }
            R.id.btn_keyboard_next -> {
                mKeyBoardClickListener?.onOkTouch()
            }
            R.id.btn_clear -> {
                clear()
            }
        }
    }

    override fun onLongClick(v: View): Boolean {
        input("C")
        return true
    }

    /**
     * 数字键盘输入内容
     */
    fun input(input: String) {
        var tempInput = mInput + input
        var regex: String? = null
        if ("0" == tempInput && !mIsCanZero) return  //第一次输入0
        if ("." == tempInput) tempInput = "0."
        regex = mPatternCalculation
        when (input) {
            "del" -> if (tempInput.length > 4) {
                if (tempInput.startsWith("0.") && tempInput.length == 5) { //此时tempInput == 0.del
                    mInput = ""
                } else {
                    tempInput = tempInput.substring(0, tempInput.length - 4)
                }
            } else mInput = ""
            "C" -> mInput = ""
        }
        val pattern = Pattern.compile(regex)
        if (pattern.matcher(tempInput).matches()) {
            mInput = tempInput
        }
        showNumber(mInput)
    }

    /**
     * 显示内容
     */
    private fun showNumber(inputNumber: String) {
        if (mKeyBoardClickListener == null) return
        mKeyBoardClickListener!!.onInputNumberTouch(inputNumber)
    }

    /**
     * 设置监听
     */
    fun setKeyBoardClickListener(listener: KeyBoardClickListener?) {
        mKeyBoardClickListener = listener
    }

    /**
     * 清空缓存
     */
    fun clear() {
        input("C")
    }

    /**
     * 设置“C”是否显示
     */
    fun setClearButtonVisible(isShow: Boolean) {
        if (isShow) {
            mBinding.btnClear.visibility = VISIBLE
        } else {
            mBinding.btnClear.visibility = INVISIBLE
        }
    }

    /**
     * 设置数字键盘最下方按钮的显示内容
     */
    fun setButtonText(text: String) {
        mBinding.btnKeyboardNext.text = text
    }

    /**
     * 设置数字键盘最下方按钮的是否可点击
     */
    fun setButtonClickable(clickable: Boolean) {
        mBinding.btnKeyboardNext.isClickable = clickable
        mBinding.btnKeyboardNext.isEnabled = clickable
    }

    /**
     * 是否显示清除按钮
     */
    fun setClearIsShow(isShow: Boolean) {
        if (isShow) {
            mBinding.btnClear.visibility = VISIBLE
            mBinding.tvEmpty.visibility = GONE
        } else {
            mBinding.btnClear.visibility = GONE
            mBinding.tvEmpty.visibility = VISIBLE
        }
    }

    /**
     * 设置可输入的最大长度(第一个可输入0，可用于账号、密码等)
     */
    fun setMaxLength(length: Int) {
        mIsCanZero = true
        mPatternCalculation = "^(\\d{0,$length})?"
    }

    /**
     * 设置可输入的最大长度(第一个不可输入0,常用于金额)
     */
    fun setMaxLengthCanNotZero(length: Int) {
        mIsCanZero = false
        mPatternCalculation = "^([1-9]{1}\\d{0,${length - 1}})?"
    }

    /**
     * 修改输入框正则表达式
     */
    fun setPatternCalculation(pattern: String) {
        mPatternCalculation = pattern
    }

    interface KeyBoardClickListener {
        fun onOkTouch()
        fun onInputNumberTouch(inputNumber: String?)
    }

}