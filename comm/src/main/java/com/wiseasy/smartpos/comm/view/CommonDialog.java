package com.wiseasy.smartpos.comm.view;

import android.app.Dialog;
import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiseasy.smartpos.comm.R;
import com.wiseasy.smartpos.comm.databinding.CommonViewDialogBinding;

import androidx.annotation.NonNull;

/**
 * Created by Bella on 2022/1/4.
 */
public class CommonDialog extends Dialog implements View.OnClickListener {
    private Context mContext;
    private OnClickListener mListener;
    private ImageView mIvIcon, mIvLittleIcon, mIvExit;
    private TextView mTvTitle, mTvDescription, mTvNote;
    private Button mBtnConfirm, mBtnBothConfirm, mBtnBothCancel;
    private View mLlBothBtn;

    public CommonDialog(@NonNull Context context) {
        this(context, 0, "", "", "", "");
    }

    public CommonDialog(@NonNull Context context, String title, String btnContent) {
        this(context, 0, title, "", "", btnContent);
    }

    public CommonDialog(@NonNull Context context, int iconId, String title, String btnContent) {
        this(context, iconId, title, "", "", btnContent);
    }

    /**
     * @param context
     * @param iconId      图标id
     * @param title       标题
     * @param description 描述
     * @param note        备注
     * @param btnContent  按钮文字提示
     */
    protected CommonDialog(@NonNull Context context, int iconId, String title,
                           String description, String note,
                           String btnContent) {
        super(context, R.style.custom_base_dialog_style);
        mContext = context;
        CommonViewDialogBinding mBinding = CommonViewDialogBinding.inflate(LayoutInflater.from(context));
        setContentView(mBinding.getRoot());

        mIvIcon = mBinding.ivDialogIcon;
        if (iconId != 0) {
            mIvIcon.setBackground(context.getDrawable(iconId));
        }

        mTvTitle = mBinding.tvDialogTitle;
        mTvTitle.setText(title);
        mTvTitle.setMovementMethod(ScrollingMovementMethod.getInstance());

        mTvDescription = mBinding.tvDialogDescription;
        mTvDescription.setText(description);

        mTvNote = mBinding.tvDialogNote;
        mTvNote.setText(note);

        mBtnConfirm = mBinding.btnConfirm;
        mBtnConfirm.setOnClickListener(this);
        mBtnConfirm.setText(btnContent);

        mIvLittleIcon = mBinding.ivDialogLittleIcon;

        mLlBothBtn = mBinding.llBothBtn;
        mBtnBothConfirm = mBinding.btnBothConfirm;
        mBtnBothConfirm.setOnClickListener(this);
        mBtnBothCancel = mBinding.btnBothCancel;
        mBtnBothCancel.setOnClickListener(this);

        //右上角退出图标
        mIvExit = mBinding.ivExit;
        mIvExit.setOnClickListener(this);

        //dialog弹出后点击屏幕或物理按键，dialog不消失
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_confirm) {
            dismiss();
            if (mListener != null) {
                mListener.onConfirm();
            }
        }
        if (i == R.id.btn_both_confirm) {
            dismiss();
            if (mListener != null) {
                mListener.onConfirm();
            }
        }
        if (i == R.id.btn_both_cancel) {
            dismiss();
            if (mListener != null) {
                mListener.onCancel();
            }
        }
        if (i == R.id.iv_exit) {
            dismiss();
        }
    }

    public void setDialogListener(OnClickListener listener) {
        mListener = listener;
    }

    public void setTitleText(String content) {
        mTvTitle.setText(content);
    }

    public void setDescriptionText(String content) {
        mTvDescription.setText(content);
    }

    public void setNote(String content) {
        mTvNote.setText(content);
    }

    public void setButtonText(String content) {
        mBtnConfirm.setText(content);
    }

    public void clickConfirmButton() {
        mBtnConfirm.performClick();
    }

    public interface OnClickListener {
        void onConfirm();

        void onCancel();
    }

    public static class Builder {
        private CommonDialog commonDialogWithIcon;
        private Context mContext;

        public Builder(Context context) {
            commonDialogWithIcon = new CommonDialog(context);
            mContext = context;
        }

        public Builder setTitle(String title) {
            commonDialogWithIcon.mTvTitle.setVisibility(View.VISIBLE);
            commonDialogWithIcon.mTvTitle.setText(title);
            return this;
        }

        public Builder setDescription(String desc) {
            commonDialogWithIcon.mTvDescription.setVisibility(View.VISIBLE);
            commonDialogWithIcon.mTvDescription.setText(desc);
            return this;
        }

        public Builder setNote(String note) {
            commonDialogWithIcon.mTvNote.setVisibility(View.VISIBLE);
            commonDialogWithIcon.mTvNote.setText(note);
            return this;
        }

        public Builder setIcon(boolean isShow, int drawableId) {
            if (isShow) {
                commonDialogWithIcon.mIvIcon.setVisibility(View.VISIBLE);
                commonDialogWithIcon.mIvIcon.setBackground(mContext.getDrawable(drawableId));
            } else {
                commonDialogWithIcon.mIvIcon.setVisibility(View.GONE);
            }
            return this;
        }

        /**
         * 设置小图标
         */
        public Builder setLittleIcon(int drawableId) {
            commonDialogWithIcon.mIvLittleIcon.setVisibility(View.VISIBLE);
            commonDialogWithIcon.mIvLittleIcon.setImageResource(drawableId);
            return this;
        }

        /**
         * 一个按钮
         */
        public Builder setButton(String text) {
            commonDialogWithIcon.mBtnConfirm.setText(text);
            return this;
        }

        /**
         * 两个按钮
         */
        public Builder setButton(String confirmText, String cancelText) {
            commonDialogWithIcon.mBtnConfirm.setVisibility(View.GONE);
            commonDialogWithIcon.mLlBothBtn.setVisibility(View.VISIBLE);
            commonDialogWithIcon.mBtnBothConfirm.setText(confirmText);
            commonDialogWithIcon.mBtnBothCancel.setText(cancelText);
            return this;
        }

        /**
         * 右上角退出图标是否显示
         */
        public Builder setExitIsShow(boolean isShow) {
            if (isShow) {
                commonDialogWithIcon.mIvExit.setVisibility(View.VISIBLE);
            } else {
                commonDialogWithIcon.mIvExit.setVisibility(View.GONE);
            }

            return this;
        }

        public Builder setListener(OnClickListener listener) {
            commonDialogWithIcon.mListener = listener;
            return this;
        }

        public CommonDialog create() {
            return commonDialogWithIcon;
        }

        public CommonDialog createAndShow() {
            commonDialogWithIcon.show();
            return commonDialogWithIcon;
        }

    }
}
