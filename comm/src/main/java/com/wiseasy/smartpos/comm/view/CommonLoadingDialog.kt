package com.wiseasy.smartpos.comm.view

import android.app.Dialog
import android.content.Context
import android.text.TextUtils
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.wiseasy.smartpos.comm.R
import com.wiseasy.smartpos.comm.databinding.CommonViewLoadingDialogBinding

/**
 * Created by Bella on 2022/1/13.
 */
class CommonLoadingDialog: Dialog {

    lateinit var mContext: Context
    lateinit var mBinding: CommonViewLoadingDialogBinding
    lateinit var operatingAnimation : Animation


    constructor(context: Context): this(context, "")

    constructor(context: Context, message: String): super(context, R.style.custom_common_dialog_style) {
        mContext = context
        mBinding = CommonViewLoadingDialogBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        operatingAnimation = AnimationUtils.loadAnimation(mContext, R.anim.loading)

        openAnim()
        //dialog弹出后点击屏幕或物理按键，dialog不消失
        this.setCanceledOnTouchOutside(false)
        this.setCancelable(false)
    }

    /**
     * 开始旋转
     */
    private fun openAnim() {
        if (operatingAnimation != null) {
            mBinding.ivIcon.startAnimation(operatingAnimation)
        }
    }

    /**
     * 停止旋转
     */
    private fun closeAnim() {
        if (operatingAnimation != null) {
            mBinding.ivIcon.clearAnimation()
        }
    }

    fun setMsg(msg: String) {
        if (!TextUtils.isEmpty(msg)) {
            mBinding.tvMsg.visibility = View.VISIBLE
            mBinding.tvMsg.text = msg
        } else {
            mBinding.tvMsg.visibility = View.GONE
        }
    }

    override fun dismiss() {
        super.dismiss()
        closeAnim()
    }
}