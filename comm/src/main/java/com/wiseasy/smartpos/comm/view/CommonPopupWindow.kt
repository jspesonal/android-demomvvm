package com.wiseasy.smartpos.comm.view

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.PopupWindow
import com.wiseasy.smartpos.base.device.DeviceProxy
import com.wiseasy.smartpos.comm.R

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/11
 */
open class CommonPopupWindow(context: Context) : PopupWindow() {

    private val mContext: Activity = context as Activity

    init {
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val width = windowManager.defaultDisplay.width
        val height = if (DeviceProxy.getInstance().isWPOSQT) {
            (windowManager.defaultDisplay.height * 300.0 / 800.0).toInt()
        } else {
            (windowManager.defaultDisplay.height * 342.0 / 1280.0).toInt()
        }
        this.width = width
        this.height = height
        this.isFocusable = true
        this.isOutsideTouchable = true
        /*在android4.0/5.0系统上,使用popupWindow时,点击内容外部区域无法关闭，可以调用setBackgroundDrawable，设置一张全透明的图片
        * 而在6.0以后的版本就不用调用这个方法，这里为了兼容，调用该方法
        */
        this.setBackgroundDrawable(context.getDrawable(R.drawable.comm_shape_transparent))
        this.setOnDismissListener {
            val lp = mContext.window.attributes
            lp.alpha = 1.0f
            mContext.window.attributes = lp
        }
    }

    /**
     * 展示弹窗
     */
    fun show(view: View) {
        Log.d("PopupWindow", "show")
        super.showAsDropDown(view)
        val lp = mContext.window.attributes
        lp.alpha = 1.0f
        mContext.window.attributes = lp
    }
}


