package com.wiseasy.smartpos.comm.view

import android.app.Dialog
import android.content.Context
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewbinding.ViewBinding
import com.wiseasy.smartpos.comm.R
import com.wiseasy.smartpos.comm.databinding.CommonDialogSelectRoleBinding
import com.wiseasy.smartpos.comm.view.adapter.UserInfoAdapter

/**
 * Created by Bella on 2022/1/10.
 * 添加User时，选择角色、选择门店使用的Dialog
 */
class CommonSelectRoleDialog : Dialog, View.OnClickListener, UserInfoAdapter.OnSelectListener {
    private val TAG = "CommonSelectRoleDialog"
    lateinit var mBinding: CommonDialogSelectRoleBinding
    var mContext: Context = context
    lateinit var mAdapter: UserInfoAdapter
    private var mListener: OnSelectResult? = null

    constructor(context: Context) : this(context, "", "", mutableListOf<String>(), null)

    constructor(context: Context, title: String) : this(
        context,
        title,
        "",
        mutableListOf<String>(),
        null
    )

    constructor(context: Context, title: String, description: String) : this(
        context,
        title,
        description,
        mutableListOf<String>(),
        null
    )

    constructor(
        context: Context,
        title: String,
        description: String,
        data: MutableList<String>,
        listener: OnSelectResult?
    ) : super(context, R.style.common_user_dialog_style) {
        mContext = context
        mBinding = CommonDialogSelectRoleBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        val layoutManager = LinearLayoutManager(mContext)
        mBinding.rvUserSelectInfo.layoutManager = layoutManager
        mAdapter = UserInfoAdapter(this)
        mBinding.rvUserSelectInfo.adapter = mAdapter
        mAdapter.setData(data)

        window?.setGravity(Gravity.BOTTOM)
        window?.decorView?.setPadding(0, 0, 0, 0)
        val lp = window?.attributes
        lp?.width = WindowManager.LayoutParams.MATCH_PARENT
        lp?.height = WindowManager.LayoutParams.WRAP_CONTENT
        window?.attributes = lp

        mBinding.ivDialogClose.setOnClickListener(this)

        mListener = listener

        //dialog弹出后点击屏幕或物理按键，dialog不消失
        this.setCanceledOnTouchOutside(false)
        this.setCancelable(false)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_dialog_close -> {
                dismiss()
            }
        }


    }

    interface OnSelectResult {
        fun onResult(data: String)
    }

    override fun onSelected(msg: String) {
        Log.i(TAG, "onSelected: $msg")
        mListener?.onResult(msg)
        dismiss()
    }

}