package com.wiseasy.smartpos.comm.view;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiseasy.smartpos.comm.R;
import com.wiseasy.smartpos.comm.databinding.CommonDialogSendEmailBinding;


/**
 * 发送邮箱弹窗
 */
public class CommonSendEmailDialog extends Dialog implements View.OnClickListener {

    private CommonDialogSendEmailBinding mBinding;
    private Context mContext;
    private OnClickListener mListener;
    private TextView mTvTitle;
    private Button mBtnCancel;
    private Button mBtnConfirm;
    private ImageView mIvIcon;

    private CommonSendEmailDialog(Context context) {
        this(context, "", true);
    }

    private CommonSendEmailDialog(Context context, String content) {
        this(context, content, true);

    }

    private CommonSendEmailDialog(Context context, String content, Boolean isShowCancel) {
        super(context, R.style.custom_dialog_style);
        mContext = context;

        mBinding = CommonDialogSendEmailBinding.inflate(LayoutInflater.from(context));

        setContentView(mBinding.getRoot());

//        mIvIcon = mBinding.ivIcon;

        mTvTitle = mBinding.tvTitle;
        mTvTitle.setText(content);
        mTvTitle.setMovementMethod(ScrollingMovementMethod.getInstance());

        mBtnCancel = mBinding.btnCancel;
        mBtnCancel.setOnClickListener(this);

        mBtnConfirm = mBinding.btnConfirm;
        mBtnConfirm.setOnClickListener(this);

        mBinding.ivExit.setOnClickListener(this);

        if (!isShowCancel) {
            mBtnCancel.setVisibility(View.GONE);
            mBtnConfirm.setText(mContext.getResources().getString(R.string.base_got_it));
        }

        mBinding.btnConfirm.setEnabled(false);
        mBinding.etInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mBinding.btnConfirm.setEnabled(!TextUtils.isEmpty(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_confirm) {
            if (mListener != null) mListener.onConfirm(mBinding.etInput.getText().toString());

        } else if (i == R.id.btn_cancel) {
            dismiss();
            if (mListener != null) mListener.onCancel();
        } else if (i == R.id.iv_exit) {
            dismiss();
        }
    }

    public void setDialogListener(OnClickListener listener) {
        mListener = listener;
    }

    public void setTitleText(String content) {
        mTvTitle.setText(content);
    }

    public void setButtonText(String cancel, String confirm) {
        mBtnCancel.setText(cancel);
        mBtnConfirm.setText(confirm);
    }

    /**
     * 设置错误信息
     */
    public void setErrorText(String errorText) {
        mBinding.tvErrorMsg.setVisibility(View.VISIBLE);
        mBinding.tvErrorMsg.setText(errorText);
    }

    public void clickConfirmButton() {
        mBtnConfirm.performClick();
    }

    public interface OnClickListener {
        void onConfirm(String email);

        void onCancel();
    }

    public static class Builder {

        private CommonSendEmailDialog baseDialog;

        public Builder(Context context) {
            baseDialog = new CommonSendEmailDialog(context);
        }

        public Builder setContent(String content) {
            baseDialog.mTvTitle.setText(content);
            return this;
        }

        public Builder setIcon(boolean isShow, int drawableId) {
            if (isShow) {
                baseDialog.mIvIcon.setVisibility(View.VISIBLE);
                baseDialog.mIvIcon.setImageResource(drawableId);
            }
            return this;
        }

        public Builder setCancelButton(Boolean isShow, String text) {
            if (isShow) {
                baseDialog.mBtnCancel.setText(text);
            } else {
                baseDialog.mBtnCancel.setVisibility(View.GONE);
            }
            return this;
        }

        public Builder setCancelButtonVisibility(Boolean isShow) {
            baseDialog.mBtnCancel.setVisibility(isShow ? View.VISIBLE : View.GONE);
            return this;
        }

        public Builder setCancelButtonText(String text) {
            baseDialog.mBtnCancel.setText(text);
            return this;
        }

        public Builder setConfirmButton(Boolean isShow, String text) {
            if (isShow) {
                baseDialog.mBtnConfirm.setText(text);
            } else {
                baseDialog.mBtnConfirm.setVisibility(View.GONE);
            }
            return this;
        }

        public Builder setConfirmButtonVisibility(Boolean isShow) {
            baseDialog.mBtnConfirm.setVisibility(isShow ? View.VISIBLE : View.GONE);
            return this;
        }

        public Builder setConfirmButtonText(String text) {
            baseDialog.mBtnConfirm.setText(text);
            return this;
        }

        public Builder setListener(OnClickListener listener) {
            baseDialog.mListener = listener;
            return this;
        }

        public CommonSendEmailDialog create() {
            return baseDialog;
        }

        public CommonSendEmailDialog createAndShow() {
            baseDialog.show();
            return baseDialog;
        }
    }
}
