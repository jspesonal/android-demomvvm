package com.wiseasy.smartpos.comm.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiseasy.smartpos.comm.R;
import com.wiseasy.smartpos.comm.databinding.BaseViewCustomDetailItemBinding;

public class CustomDetailItem extends RelativeLayout {

    private TextView mTvHint, mTvText;

    public CustomDetailItem(Context context) {
        this(context, null);
    }

    public CustomDetailItem(Context context, AttributeSet attrs) {
        super(context, attrs);

        BaseViewCustomDetailItemBinding mBinding = BaseViewCustomDetailItemBinding.inflate(LayoutInflater.from(context));

        mTvHint = mBinding.tvHint;
        mTvText = mBinding.tvText;

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.CustomDetailItem);
        String hint = array.getString(R.styleable.CustomDetailItem_hint);
        String text = array.getString(R.styleable.CustomDetailItem_text);

        mTvHint.setText(hint);
        mTvText.setText(text);

        addView(mBinding.getRoot());
        array.recycle();
    }

    public void setText(String text) {
        mTvText.setText(text);
    }

    public String getText() {
        return mTvText.getText().toString();
    }

}
