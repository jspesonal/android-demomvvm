package com.wiseasy.smartpos.comm.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wiseasy.smartpos.comm.R
import com.wiseasy.smartpos.comm.view.CommonSelectRoleDialog

/**
 * Created by Bella on 2022/1/10.
 */
class UserInfoAdapter(listener: OnSelectListener): RecyclerView.Adapter<UserInfoAdapter.ViewHolder>() {

    private lateinit var mData: MutableList<String>
    private var mListener: OnSelectListener = listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_user_select_info, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = mData[position]
        holder.tvUserInfoItem.text = data

        holder.itemView.setOnClickListener {
            mListener.onSelected(data)
        }
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvUserInfoItem: TextView = itemView.findViewById(R.id.tv_user_info_item)

    }

    fun setData(data: MutableList<String>) {
        mData = data
        notifyDataSetChanged()
    }

    fun clear() {
        if (mData != null) {
            mData.clear()
            notifyDataSetChanged()
        }
    }

    interface OnSelectListener {
        fun onSelected(msg: String)
    }
}