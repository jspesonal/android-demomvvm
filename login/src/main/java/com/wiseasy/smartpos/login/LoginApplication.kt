package com.wiseasy.smartpos.login

import android.util.Log
import com.google.gson.Gson
import com.wiseasy.smartpos.base.BaseApplication
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.bean.BasicAuth
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.http.wisecloud.RetrofitClientWiseCloud
import okhttp3.Headers


/**
 * Created by Belle on 2021/12/29.
 * Login模块单独运行时(作为独立的app)，会调用该Application，
 * 然后该Application调用Like(做一些初始化类操作)
 * Login作为模块运行时，会只调用Like
 */
class LoginApplication : BaseApplication() {

    private val TAG = "LoginApplication"

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate")

        //调用Like
        LoginApplicationLike().init(this)
        //初始化retrofit
        initClient()
        //初始化测试阶段的参数
        initTestParam()
    }

    private fun initClient() {
        Log.d(TAG, "initClient start")
        RetrofitClientSmartPay.init(context)
        val headers = Headers.Builder()
            .add("Content-Type", "application/json;charset=UTF-8")
            .add("X-Device-Model", "Android")
            .add("X-OS-Name", "Android")
            .add("X-OS-Version", "Android 8.0.1")
            .add("X-App-Version", "1.0.1")
            .add("Accept-Language", "en")
            .build()

        RetrofitClientSmartPay.addCommonHeaders(headers = headers)

        //WiseCloud接口初始化
        RetrofitClientWiseCloud.init(context)
        val headersAboutWiseCloud = Headers.Builder()
            .add("Content-Type", "application/json;charset=UTF-8")
            .add("Accept-Language", "zh-CN")
            .build()

        RetrofitClientWiseCloud.addCommonHeaders(headers = headersAboutWiseCloud)
        Log.d(TAG, "initClient end")
    }

    private fun initTestParam() {
        SPUtil.setSmartPayToken(context, "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI0MjU0NzY1OTM2MTk5NTYzIiwiaWF0IjoxNjQxNDU0MzY3LCJpc3MiOiJTTUFSVFBBWSJ9.jnZ1SMFA5ZA-fW6hPi30yB3W35PltIuDgINQqvDb92U")
        SPUtil.setSmartPaySpMid(context, "00256489")
        SPUtil.setSmartPaySpTid(context, "AP10000015")
        SPUtil.setSmartPayPhone(context, "0935263989")

        SPUtil.setSmartPayValidTill(context, 99999999999999)

//        val basicAuth = BasicAuth()
//        basicAuth.spMID = "00257171"
//        basicAuth.spTID = "01000001"
//        basicAuth.deviceSN = "PP35271909000079"
////        basicAuth.deviceSN = CommonUtil.getSN(context)
//        basicAuth.wid = "1000000015873"
//        basicAuth.phone = "0341112220"
//
//        val basicAuthString = Gson().toJson(basicAuth)
//        SPUtil.setSmartPayBasicAuth(context, basicAuthString)
//        //测试阶段是否第一次登录设置成true
//        SPUtil.setIsFirstLogin(context, true)
    }
}