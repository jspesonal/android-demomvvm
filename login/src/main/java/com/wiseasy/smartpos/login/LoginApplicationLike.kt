package com.wiseasy.smartpos.login

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import com.wiseasy.smartpos.login.module.serviceimpl.LoginModuleServiceImpl
import com.wiseasy.smartpos.base.IApplicationLike
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.comm.module.service.LoginModuleService
import com.wiseasy.smartpos.login.init.InitService

/**
 * Created by Bella on 2021/12/20.
 */
class LoginApplicationLike : IApplicationLike {

    private val TAG = "LoginApplicationLike"
    private lateinit var context: Context
    private val router = Router.getInstance()

    override fun init(application: Application) {
        Log.d(TAG, "init")
        context = application.applicationContext
        //做一些初始化的操作
        router.addService(LoginModuleService::class.java, LoginModuleServiceImpl())
        //启动初始化服务
        startInitService()
    }

    /**
     * 启动初始化服务
     */
    private fun startInitService() {
        Log.d(TAG, "startInitService")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // >= Android 8.0，不允许启动后台服务
            context.startForegroundService(Intent(context, InitService::class.java))
        } else {
            context.startService(Intent(context, InitService::class.java))
        }
    }

}