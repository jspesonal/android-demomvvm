package com.wiseasy.smartpos.login.firstlogin

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.core.view.isVisible
import com.wiseasy.smartpos.base.event.FinishFirstLoginEvent
import com.wiseasy.smartpos.base.event.RxBus
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.base.util.closeKeyboard
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonCustomNumberInputKeyboard
import com.wiseasy.smartpos.login.R
import com.wiseasy.smartpos.login.databinding.ActivityFirstLoginBinding
import com.wiseasy.smartpos.login.otp.VerifyOTPActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import okhttp3.Headers

/**
 * 第一次登录界面
 */
class FirstLoginActivity : CommonActivity<ActivityFirstLoginBinding>(), FirstLoginContract.IView {

    private lateinit var mContext: Context
    private lateinit var mPresenter: FirstLoginPresenter

    private val mCompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        initView()
        subscribeEvent()
    }

    override fun onDestroy() {
        mCompositeDisposable.dispose()
        super.onDestroy()
    }

    override fun getBinding(): ActivityFirstLoginBinding {
        return ActivityFirstLoginBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)

        when (v.id) {
            R.id.iv_clear_phone -> {//清除手机号（用户名）
                mBinding.keyBoard.clear()
            }

            R.id.btn_next -> {//下一步
                showLoading()
                mPresenter.firstLogin(mBinding.etPhone.text.toString())
            }

            R.id.et_phone -> {//手机号
                mBinding.keyBoard.visibility = VISIBLE
                mBinding.btnNext.visibility = GONE
                closeKeyboard(this, mBinding.etPhone)
            }

            R.id.iv_vietnam -> {//越南语
                log("选择了越南语")
                SPUtil.setSettingLanguage(mContext, 0)
                onLanguageChange()
            }

            R.id.iv_english -> {//英语
                log("选择了英语")
                SPUtil.setSettingLanguage(mContext, 1)
                onLanguageChange()
            }

        }
    }

    override fun onBackPressed() {
        //如果键盘处于显示状态，则手机键盘，否则调用系统方法
        if (mBinding.keyBoard.isVisible) {
            mBinding.keyBoard.visibility = GONE
            mBinding.btnNext.visibility = VISIBLE
        } else {
            super.onBackPressed()
        }
    }

    override fun onFirstLoginSuccess(reId: String) {
        log("onFirstLoginSuccess")
        hideLoading()
        val intent = Intent(mContext, VerifyOTPActivity::class.java)
        intent.putExtra("referenceId", reId)
        intent.putExtra("isNeedReLogin", false)
        startActivity(intent)
//        VerifyOTPActivity.actionStart(mContext, mBinding.etPhone.text.toString(), reId, false)

    }


    override fun onFirstLoginFail(msg: String?) {
        log("onFirstLoginFail|msg = $msg")
        mBinding.tvErrorMsg.visibility = VISIBLE
        mBinding.tvErrorMsg.text = msg
        hideLoading()
    }

    private fun initView() {
        setFillScreen()
        mBinding.btnNext.isEnabled = false
        //数字键盘
        mBinding.keyBoard.setMaxLength(10)
        mBinding.keyBoard.setKeyBoardClickListener(object : CommonCustomNumberInputKeyboard.KeyBoardClickListener {
            override fun onOkTouch() {
                mBinding.keyBoard.visibility = GONE
                mBinding.btnNext.visibility = VISIBLE
            }

            override fun onInputNumberTouch(inputNumber: String?) {
                mBinding.etPhone.text = Editable.Factory.getInstance().newEditable(inputNumber)
                if (TextUtils.isEmpty(inputNumber)) {
                    mBinding.ivClearPhone.visibility = GONE
                    mBinding.btnNext.isEnabled = false
                } else {
                    mBinding.ivClearPhone.visibility = VISIBLE
                    mBinding.btnNext.isEnabled = true
                }
            }

        })

        setViewByLanguage()
    }

    private fun initData() {
        mContext = this
        mPresenter = FirstLoginPresenter(mContext)
//        mBinding.etPhone.setText("0935263989")
    }

    /**
     * 订阅事件
     */
    private fun subscribeEvent() {
        log("subscribeEvent订阅事件")
        mCompositeDisposable.add(RxBus.getInstance().toObservable(FinishFirstLoginEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                log("收到销毁首次登陆界面")
                finish()
            })
    }

    /**
     * 根据选择的语言设置UI
     */
    private fun setViewByLanguage() {
        val languageId = SPUtil.getSettingLanguage(mContext)
        var languageStr = ""
        if (languageId == 0) {
            //选择的是越南语
            mBinding.tvVietnam.setTextColor(Color.parseColor("#ffffffff"))
            mBinding.ivVietnam.isSelected = true
            mBinding.tvEnglish.setTextColor(Color.parseColor("#88ffffff"))
            mBinding.ivEnglish.isSelected = false
            languageStr = "vn"
        } else {
            //选择的是英语
            mBinding.tvVietnam.setTextColor(Color.parseColor("#88ffffff"))
            mBinding.ivVietnam.isSelected = false
            mBinding.tvEnglish.setTextColor(Color.parseColor("#ffffffff"))
            mBinding.ivEnglish.isSelected = true
            languageStr = "en"
        }
        //重新设置SmartPay网络消息头语言类型
        val headers = Headers.Builder().add("Accept-Language", languageStr).build()
        RetrofitClientSmartPay.addCommonHeaders(headers)
    }

    /**
     * 语言改变时的动作
     */
    private fun onLanguageChange() {
        val intent = Intent(mContext, FirstLoginActivity::class.java)
        startActivity(intent)
        finish()

        setViewByLanguage()
    }

}