package com.wiseasy.smartpos.login.firstlogin

import com.wiseasy.smartpos.base.mvp.BaseIView

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/10
 */
interface FirstLoginContract {

    interface IView : BaseIView {
        /**
         * 第一次登录成功
         */
        fun onFirstLoginSuccess(reId: String)

        /**
         * 第一次登录失败
         */
        fun onFirstLoginFail(msg: String?)
    }

    interface IPresenter {
        /**
         * 第一次登录
         */
        fun firstLogin(number: String)
    }

}