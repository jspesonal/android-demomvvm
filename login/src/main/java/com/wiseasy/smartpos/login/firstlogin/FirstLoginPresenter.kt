package com.wiseasy.smartpos.login.firstlogin

import android.content.Context
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.kotlin.withIOContext
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.comm.bean.FirstLoginBean
import com.wiseasy.smartpos.comm.http.smartpay.ISmartPayAPI
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.util.isCorrectPhone
import com.wiseasy.smartpos.comm.util.putBasicAuthFirstLogin
import com.wiseasy.smartpos.login.R

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/10
 */
class FirstLoginPresenter(context: Context) : BasePresenter(), FirstLoginContract.IPresenter {

    private var mView: FirstLoginContract.IView

    init {
        mContext = context
        mView = context as FirstLoginContract.IView
    }

    override fun firstLogin(number: String) {
        log("firstLogin|number = $number")
        if (number.length < 10 || !isCorrectPhone(number)) {
            mView.onFirstLoginFail(mContext?.getString(R.string.login_phone_error))
            return
        }
        SPUtil.setSmartPayPhone(mContext, number)
        launchRx {
            val requestJson = JSONObject()
            val basicAuth = JSONObject()
            var resId: String? = ""
            mContext?.let { putBasicAuthFirstLogin(it, basicAuth) }
            requestJson["basicAuth"] = basicAuth
            requestJson["landingPage"] = "homepage"

            var result: FirstLoginBean? = null
            withIOContext {
                result = RetrofitClientSmartPay.createWithoutToken(ISmartPayAPI::class.java)
                    .firstLogin(RetrofitClientSmartPay.buildRequestBody(requestJson))
                    .dataConvert(FirstLoginBean::class.java)
                log("firstLogin|result = $result")
                resId = RetrofitClientSmartPay.getRequestId()
            }
            result?.let {
                SPUtil.setSmartPayOtpDuration(mContext, result!!.duration?.toLong())
                resId?.let { it1 -> mView.onFirstLoginSuccess(it1) }
            }
//            mView.onFirstLoginSuccess()
        }.await {
            it.printStackTrace()
            mView.onFirstLoginFail(it.message)
        }

    }

}