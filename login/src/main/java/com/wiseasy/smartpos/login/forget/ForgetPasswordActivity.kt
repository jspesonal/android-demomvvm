package com.wiseasy.smartpos.login.forget

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.View
import androidx.core.view.isVisible
import com.wiseasy.smartpos.base.util.closeKeyboard
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonCustomNumberInputKeyboard
import com.wiseasy.smartpos.login.R
import com.wiseasy.smartpos.login.databinding.ActivityForgetPasswordBinding
import com.wiseasy.smartpos.login.otp.VerifyOTPActivity

/**
 * 忘记密码界面
 */
class ForgetPasswordActivity : CommonActivity<ActivityForgetPasswordBinding>(), ForgetPasswordContract.IView {

    private lateinit var mContext: Context
    private lateinit var mPresenter: ForgetPasswordPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initDate()
        initView()
    }

    override fun getBinding(): ActivityForgetPasswordBinding {
        return ActivityForgetPasswordBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btn_next -> {//下一步
                mPresenter.forgotPassword(mBinding.etPhone.text.toString())
            }

            R.id.iv_back_password -> {//返回图标
                finish()
            }

            R.id.iv_clear_phone -> {//清除手机号（用户名）
                mBinding.keyBoard.clear()
            }

            R.id.et_phone -> {//手机号
                mBinding.keyBoard.visibility = View.VISIBLE
                mBinding.btnNext.visibility = View.GONE
                closeKeyboard(this, mBinding.etPhone)
            }
        }
    }

    override fun onBackPressed() {
        //如果键盘处于显示状态，则手机键盘，否则调用系统方法
        if (mBinding.keyBoard.isVisible) {
            mBinding.keyBoard.visibility = View.GONE
            mBinding.btnNext.visibility = View.VISIBLE
        } else {
            super.onBackPressed()
        }
    }

    override fun onForgetPasswordSuccess(refId: String) {
        log("onForgetPasswordSuccess")
        hideLoading()
        val intent = Intent(mContext, VerifyOTPActivity::class.java)
        intent.putExtra("referenceId", refId)
        intent.putExtra("isNeedReLogin", true)
        startActivity(intent)
    }

    override fun onForgotPasswordFail(msg: String?) {
        log("onForgotPasswordFail|mag: $msg")
        hideLoading()
        mBinding.tvErrorMsg.visibility = View.VISIBLE
        mBinding.tvErrorMsg.text = msg
    }

    private fun initDate() {
        mContext = this
        mPresenter = ForgetPasswordPresenter(mContext)
    }

    private fun initView() {
        mBinding.btnNext.isEnabled = false
        //数字键盘
        mBinding.keyBoard.setMaxLength(10)
        mBinding.keyBoard.setKeyBoardClickListener(object : CommonCustomNumberInputKeyboard.KeyBoardClickListener {
            override fun onOkTouch() {
                mBinding.keyBoard.visibility = View.GONE
                mBinding.btnNext.visibility = View.VISIBLE
            }

            override fun onInputNumberTouch(inputNumber: String?) {
                mBinding.etPhone.text = Editable.Factory.getInstance().newEditable(inputNumber)
                if (TextUtils.isEmpty(inputNumber)) {
                    mBinding.ivClearPhone.visibility = View.GONE
                    mBinding.btnNext.isEnabled = false
                } else {
                    mBinding.ivClearPhone.visibility = View.VISIBLE
                    mBinding.btnNext.isEnabled = true
                }
            }

        })
    }

}