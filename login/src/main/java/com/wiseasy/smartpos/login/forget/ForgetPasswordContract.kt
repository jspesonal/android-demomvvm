package com.wiseasy.smartpos.login.forget

import com.wiseasy.smartpos.base.mvp.BaseIView

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/10
 */
interface ForgetPasswordContract {

    interface IView : BaseIView {
        /**
         * 忘记密码成功
         */
        fun onForgetPasswordSuccess(refId: String)

        /**
         * 忘记密码失败
         */
        fun onForgotPasswordFail(msg: String?)
    }

    interface IPresenter {
        /**
         * 忘记密码
         */
        fun forgotPassword(number: String)
    }

}