package com.wiseasy.smartpos.login.forget

import android.content.Context
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.base.util.withIOContext
import com.wiseasy.smartpos.comm.bean.FirstLoginBean
import com.wiseasy.smartpos.comm.http.smartpay.ISmartPayAPI
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay.getRequestId
import com.wiseasy.smartpos.comm.util.isCorrectPhone
import com.wiseasy.smartpos.comm.util.putBasicAuth
import com.wiseasy.smartpos.login.R

/**
 * @author Belle(Baiyunyan)
 * @date 2022/02/16
 */
class ForgetPasswordPresenter(context: Context) : BasePresenter(), ForgetPasswordContract.IPresenter {

    private var mView: ForgetPasswordContract.IView

    init {
        mContext = context
        mView = context as ForgetPasswordContract.IView
    }

    override fun forgotPassword(number: String) {
        log("forgotPassword|number = $number")
        mView.showLoading()
        if (number.length < 10 || !isCorrectPhone(number)) {
            mView.onForgotPasswordFail(mContext?.getString(R.string.login_phone_error))
            return
        }
        SPUtil.setSmartPayPhone(mContext, number)
        launchRx {
            val requestJson = JSONObject()
            val basicAuth = JSONObject()
            var referId: String = ""
            mContext?.let {
                putBasicAuth(mContext!!, basicAuth, false)
            }
            var result: FirstLoginBean? = null
            requestJson["basicAuth"] = basicAuth
            requestJson["landingPage"] = "homepage"
            withIOContext {
                result = RetrofitClientSmartPay.createWithoutToken(ISmartPayAPI::class.java)
                    .forgetPassword(RetrofitClientSmartPay.buildRequestBody(requestJson)).dataConvert(
                        FirstLoginBean::class.java
                    )
                log("login: result = $result")
                referId = getRequestId()
            }
            result?.let {
                SPUtil.setSmartPayOtpDuration(mContext, result!!.duration?.toLong())
                mView.onForgetPasswordSuccess(referId)
            }

        }.await {
            it.printStackTrace()
            mView.onForgotPasswordFail(it.message)
        }
    }

}