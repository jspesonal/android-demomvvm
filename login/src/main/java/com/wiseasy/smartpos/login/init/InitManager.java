package com.wiseasy.smartpos.login.init;

public class InitManager {

    public static int INIT_STATUS_UNSTART = 0;
    public static int INIT_STATUS_SUCCESS = 1;
    public static int INIT_STATUS_FAIL = 2;
    public static int initStatus = INIT_STATUS_UNSTART;
    public static OnInitListener listener;

    public static void initSuccess() {
        initStatus = INIT_STATUS_SUCCESS;
        if (listener != null) listener.onSuccess();
    }

    public static void initFail(String msg) {
        initStatus = INIT_STATUS_FAIL;
        if (listener != null) listener.onFail(msg);
    }

    public interface OnInitListener {

        void onSuccess();

        void onFail(String msg);
    }

}
