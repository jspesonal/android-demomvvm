package com.wiseasy.smartpos.login.init

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.text.TextUtils
import android.util.Log
import kotlin.Throws
import io.reactivex.schedulers.Schedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import com.wiseasy.smartpos.base.util.DateUtil
import com.wiseasy.emvprocess.SDKInstance
import com.wiseasy.emvprocess.LibInit
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.http.wisecloud.RetrofitClientWiseCloud
import com.wiseasy.smartpos.base.printer.PrinterConfig
import com.wiseasy.smartpos.base.device.DeviceProxy
import com.wiseasy.smartpos.base.printer.printer.AbsPrinter
import com.alibaba.fastjson.JSON
import com.wiseasy.smartpos.login.R
import io.reactivex.Single
import okhttp3.Headers
import wangpos.sdk4.libbasebinder.Printer
import java.lang.Exception

class InitService : Service() {

    private val TAG = "InitService"
    var isInit = false

    override fun onCreate() {
        super.onCreate()
        log("onCreate")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            // 创建Channel，会显示在该通知顶部
            val mChannel = NotificationChannel("wise-cashier", getString(R.string.app_name), NotificationManager.IMPORTANCE_LOW)
            notificationManager.createNotificationChannel(mChannel)
            // 创建Notification
            val notification = Notification.Builder(this, "wise-cashier")
                .setContentTitle(getString(R.string.app_name)) // 标题
                .setContentText(getString(R.string.login_init_running)) // 内容
                .setSmallIcon(R.mipmap.ic_launcher_transparent) // 通知顶部小图标，Channel名称的左侧----5.0后Android官方建议不要为通知的图标添加任何额外的透明度，渐变色，不要企图用颜色将通知图标与其他应用，比如系统应用，应用的通知图标只能是在透明的背景上有白色的图案。
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher)) // 右侧大图标
                .build()
            startForeground(1, notification)
        }
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        log("onStartCommand")
        if (!isInit) {
            isInit = true
            init()
        }
        // 非粘性的：如果在执行完onStartCommand后，服务被异常kill掉，系统不会自动重启该服务。
        return START_NOT_STICKY
    }

    private fun init() {
        Single.create<Int> { singleEmitter ->
            //初始化Pos参数
            initPOSParams()
            //初始化网络
            initClient()
            //初始打印机
            initPrinter()

            singleEmitter.onSuccess(0)

        }.subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<Int> {

                override fun onSubscribe(disposable: Disposable) {

                }

                override fun onSuccess(o: Int) {
                    loge("Init Sdk Success")
                    InitManager.initSuccess()
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    InitManager.initFail(e.toString())
                    loge("Init Sdk Fail:$e")
                }
            })
        printLog()
    }

    override fun onBind(intent: Intent): IBinder? {
        return Binder()
    }

    @Throws(Exception::class)
    private fun initWiseasySDK() {
        log("initWiseasySDK")
        val str = DateUtil.date2Str(DateUtil.str2Date(DateUtil.currentDatetime()), "yyyyMMddHHmmss") // 1971 < year < 2099
        if (str.substring(0, 4).toInt() < 1970) {
            throw Exception("System time error, please connect the network to get the time automatically!")
        }

        /*
            1.SDK4实例对象初始化
         */log("initWiseasySDK|SDKInstance initSDK")
        SDKInstance.initSDK(applicationContext)

        /*
            2.应用启动后进行库初始化
         */log("initWiseasySDK|LibInit initBankCard")
        LibInit.init(true)
    }

    private fun printLog() {
        log("System Version=" + Build.DISPLAY)
        log("Product Name=" + Build.MODEL)
        log("SN=" + CommonUtil.getSN(this))
    }

    private fun initPOSParams() {
        SPUtil.setIsChangeLanguage(this, false)
        SPUtil.setIsScreenBan(this, false)
//        SPUtil.setSmartPayToken(this, "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI1ODEwMDc3NzQ4Nzk3NTA1IiwiaWF0IjoxNjQzMDA5Njc5LCJpc3MiOiJTTUFSVFBBWSJ9.kCJmaiC2-AKEsPPUmH-s2qoPqD3TBJM6u9-I4ESVtuI")
//        SPUtil.setSmartPaySpMid(this, "00257110")
//        SPUtil.setSmartPaySpTid(this, "01000002")
//        SPUtil.setSmartPayPhone(this, "0988303036")
    }

    private fun initClient() {
        log("initClient start")
        RetrofitClientSmartPay.init(this)
        val languageStr = if (SPUtil.getSettingLanguage(this) == 0) {
            "vn"
        } else {
            "en"
        }
        var headers = Headers.Builder()
            .add("Content-Type", "application/json;charset=UTF-8")
            .add("X-Device-Model", "Android")
            .add("X-OS-Name", "Android")
            .add("X-OS-Version", CommonUtil.getSystemVersion())//"8.0.1"
            .add("X-App-Version", CommonUtil.getVersionName(this))//"1.0.1"
            .add("Accept-Language", languageStr)//en/vn
            .build()
        RetrofitClientSmartPay.addCommonHeaders(headers.also { headers = it })

        //WiseCloud接口初始化
        RetrofitClientWiseCloud.init(this)
        val headersAboutWiseCloud = Headers.Builder()
            .add("Content-Type", "application/json;charset=UTF-8")
            .add("Accept-Language", "en-US")
            .build()
        RetrofitClientWiseCloud.addCommonHeaders(headersAboutWiseCloud.also { headers = it })
        Log.d(TAG, "initClient end")
    }

    private fun initPrinter() {
        log("initPrinter start")
        SDKInstance.mPrinter = Printer(applicationContext)
        val settingPrinter = SPUtil.getSettingPrinter(this)
        if (TextUtils.isEmpty(settingPrinter)) {
            log("首次初始化打印机配置")
            val printerConfig = PrinterConfig()
            if (DeviceProxy.getInstance().device.isSupportPrint) {
                log("设备自带打印机，初始化为使用本机打印")
                printerConfig.currentPrinterType = AbsPrinter.PRINTER_TYPE_LOCAL
            } else {
//                log("设备不自带打印机，初始化为未配置任何打印机，即不启用打印机")
//                printerConfig.currentPrinterType = AbsPrinter.PRINTER_TYPE_NONE
                log("设备不自带打印机，初始化为蓝牙打印机")
                printerConfig.currentPrinterType = AbsPrinter.PRINTER_TYPE_BLUETOOTH
            }
            SPUtil.setSettingPrinter(this, JSON.toJSONString(printerConfig))
        } else {
            log("非首次初始化打印机配置")
        }
        log("initPrinter end")
    }

    private fun log(msg: String) {
        Log.d(TAG, msg)
    }

    private fun loge(msg: String) {
        Log.d(TAG, msg)
    }

}