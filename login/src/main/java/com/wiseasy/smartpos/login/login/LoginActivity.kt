package com.wiseasy.smartpos.login.login

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.core.view.isVisible
import com.wiseasy.smartpos.login.main.HomeActivity
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.view.ActivityCollector
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonCustomNumberInputKeyboard
import com.wiseasy.smartpos.login.R
import com.wiseasy.smartpos.login.databinding.ActivityLoginBinding
import com.wiseasy.smartpos.login.forget.ForgetPasswordActivity
import com.wiseasy.smartpos.login.otp.VerifyOTPActivity
import com.wiseasy.smartpos.login.setpassword.SetPasswordActivity
import okhttp3.Headers

class LoginActivity : CommonActivity<ActivityLoginBinding>(), LoginContract.IView {

    private lateinit var mContext: Context
    private lateinit var mPresenter: LoginPresenter

    private var mIsFocusPhone = false//输入手机号是否获取焦点
    private var mIsFocusPassword = false//输入密码是否获取焦点

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
        initView()

        ActivityCollector.finishAllWithoutCurrent(this)
    }

    override fun getBinding(): ActivityLoginBinding {
        return ActivityLoginBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)

        when (v.id) {
            R.id.iv_clear_phone -> {//清除手机号（用户名）
                mBinding.etPhone.text = Editable.Factory.getInstance().newEditable("")
            }

            R.id.tv_forgot_password -> {//忘记密码
//                val intent = Intent(mContext, VerifyOTPActivity::class.java)
//                intent.putExtra("isNeedReLogin", true)
//                startActivity(intent)
//                mPresenter.forgotPassword(mBinding.etPhone.text.toString())
//                VerifyOTPActivity.actionStart(mContext, mBinding.etPhone.text.toString(), "", true)
                startActivity(Intent(mContext, ForgetPasswordActivity::class.java))
            }

            R.id.btn_sign_in -> {//登录
                mPresenter.login(mBinding.etPhone.text.toString(), mBinding.etPassword.text.toString())
            }

            R.id.et_phone -> {//输入手机号
                log("输入手机号获取焦点")
                mIsFocusPhone = true
                mIsFocusPassword = false
                mBinding.etPhone.setBackgroundResource(R.drawable.login_shape_input_focus_bg)
                mBinding.etPassword.setBackgroundResource(R.drawable.login_shape_input_bg)
                mBinding.btnSignIn.visibility = View.GONE
                mBinding.keyBoard.visibility = View.VISIBLE
                mBinding.keyBoard.setMaxLength(10)
                val phone = mBinding.etPhone.text.toString()
                mBinding.keyBoard.clear()
                mBinding.keyBoard.input(phone)
            }
            R.id.et_password -> {//输入密码
                log("输入密码获取焦点")
                setClickPasswordView()
            }

            R.id.iv_vietnam -> {//越南语
                log("选择了越南语")
                SPUtil.setSettingLanguage(mContext, 0)
                onLanguageChange()
            }

            R.id.iv_english -> {//英语
                log("选择了英语")
                SPUtil.setSettingLanguage(mContext, 1)
                onLanguageChange()
            }

        }
    }

    override fun onBackPressed() {
        //如果键盘处于显示状态，则手机键盘，否则调用系统方法
        if (mBinding.keyBoard.isVisible) {
            mBinding.keyBoard.visibility = View.GONE
            mBinding.btnSignIn.visibility = View.VISIBLE
        } else {
            super.onBackPressed()
        }
    }

    override fun onLoginSuccess() {
        log("onLoginSuccess")
        hideLoading()
        SPUtil.setIsFirstLogin(mContext, false)
        if (SPUtil.getIsFirstLoginSmartPay(mContext)) {
            startActivity(Intent(mContext, SetPasswordActivity::class.java))
        } else {
            startActivity(Intent(mContext, HomeActivity::class.java))
            finish()
        }

    }

    override fun onForgetPasswordSuccess(refId: String) {
        Log.i(TAG, "onForgetPasswordSuccess: ")
        hideLoading()
        val intent = Intent(mContext, VerifyOTPActivity::class.java)
        intent.putExtra("referenceId", refId)
        intent.putExtra("isNeedReLogin", true)
        startActivity(intent)
//        VerifyOTPActivity.actionStart(mContext, mBinding.etPhone.text.toString(), refId, true)
    }

    override fun onLoginOrForgotPasswordFail(msg: String?) {
        log("onLoginOrForgotPasswordFail|mag: $msg")
        hideLoading()
        mBinding.tvErrorMsg.visibility = View.VISIBLE
        mBinding.tvErrorMsg.text = msg
    }

    private fun initView() {
        setFillScreen()
        mBinding.btnSignIn.isEnabled = false
        //数字键盘
        mBinding.keyBoard.setKeyBoardClickListener(object : CommonCustomNumberInputKeyboard.KeyBoardClickListener {
            override fun onOkTouch() {
                //如果是当前焦点在输入手机号，则点击Next后，光标移动到下一个输入框
                if (mIsFocusPhone) {
                    setClickPasswordView()
                    return
                }
                //如果当前焦点在输入密码，则点击Next后，收起软键盘
                if (mIsFocusPassword) {
                    mBinding.keyBoard.visibility = View.GONE
                    mBinding.btnSignIn.visibility = View.VISIBLE
                }
            }

            override fun onInputNumberTouch(inputNumber: String?) {
                if (mIsFocusPhone) {
//                    log("输入手机号获取焦点，inputNumber = $inputNumber")
                    mBinding.etPhone.text = Editable.Factory.getInstance().newEditable(inputNumber)
                }
                if (mIsFocusPassword) {
//                    log("输入密码获取焦点，inputNumber = $inputNumber")
                    mBinding.etPassword.text = Editable.Factory.getInstance().newEditable(inputNumber)
                }
                mBinding.btnSignIn.isEnabled = !TextUtils.isEmpty(mBinding.etPhone.text.toString()) && !TextUtils.isEmpty(mBinding.etPassword.text.toString())
            }

        })

        //设置两个输入框不可获取焦点
        mBinding.etPhone.isFocusable = false
        mBinding.etPassword.isFocusable = false

        setViewByLanguage()

    }

    private fun initData() {
        mContext = this
        mPresenter = LoginPresenter(mContext)

//        mBinding.etPhone.text = Editable.Factory.getInstance().newEditable("0988303038")
//        mBinding.etPassword.text = Editable.Factory.getInstance().newEditable("303038")
    }

    /**
     * 设置点击 密码 后的view
     */
    private fun setClickPasswordView() {
        mIsFocusPassword = true
        mIsFocusPhone = false
        mBinding.etPhone.setBackgroundResource(R.drawable.login_shape_input_bg)
        mBinding.etPassword.setBackgroundResource(R.drawable.login_shape_input_focus_bg)
        mBinding.btnSignIn.visibility = View.GONE
        mBinding.keyBoard.visibility = View.VISIBLE
        mBinding.keyBoard.setMaxLength(6)
        val password = mBinding.etPassword.text.toString()
//        log("password = $password")
        mBinding.keyBoard.clear()
        mBinding.keyBoard.input(password)
    }

    /**
     * 根据选择的语言设置UI
     */
    private fun setViewByLanguage() {
        val languageId = SPUtil.getSettingLanguage(mContext)
        var languageStr = ""
        if (languageId == 0) {
            //选择的是越南语
            mBinding.tvVietnam.setTextColor(Color.parseColor("#ffffffff"))
            mBinding.ivVietnam.isSelected = true
            mBinding.tvEnglish.setTextColor(Color.parseColor("#88ffffff"))
            mBinding.ivEnglish.isSelected = false
            languageStr = "vn"
        } else {
            //选择的是英语
            mBinding.tvVietnam.setTextColor(Color.parseColor("#88ffffff"))
            mBinding.ivVietnam.isSelected = false
            mBinding.tvEnglish.setTextColor(Color.parseColor("#ffffffff"))
            mBinding.ivEnglish.isSelected = true
            languageStr = "en"
        }
        //重新设置SmartPay网络消息头语言类型
        val headers = Headers.Builder().add("Accept-Language", languageStr).build()
        RetrofitClientSmartPay.addCommonHeaders(headers)
    }

    /**
     * 语言改变时的动作
     */
    private fun onLanguageChange() {
        val intent = Intent(mContext, LoginActivity::class.java)
        startActivity(intent)
        finish()

        setViewByLanguage()
    }


}