package com.wiseasy.smartpos.login.login

import com.wiseasy.smartpos.base.mvp.BaseIView

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/10
 */
interface LoginContract {

    interface IView : BaseIView {
        /**
         * 登录成功
         */
        fun onLoginSuccess()

        /**
         * 忘记密码成功
         */
        fun onForgetPasswordSuccess(refId: String)

        /**
         * 登录/忘记密码失败
         */
        fun onLoginOrForgotPasswordFail(msg: String?)
    }

    interface IPresenter {
        /**
         * 登录
         */
        fun login(number: String, password: String)

        /**
         * 忘记密码
         */
        fun forgotPassword(number: String)
    }

}