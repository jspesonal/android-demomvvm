package com.wiseasy.smartpos.login.login

import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.alibaba.fastjson.JSONObject
import com.google.gson.Gson
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.base.util.withIOContext
import com.wiseasy.smartpos.comm.bean.FirstLoginBean
import com.wiseasy.smartpos.comm.bean.LoginBean
import com.wiseasy.smartpos.comm.bean.MerchantMappingInfo
import com.wiseasy.smartpos.comm.http.smartpay.ISmartPayAPI
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay.getRequestId
import com.wiseasy.smartpos.comm.http.wisecloud.IWiseCloudAPI
import com.wiseasy.smartpos.comm.http.wisecloud.RetrofitClientWiseCloud
import com.wiseasy.smartpos.comm.util.isCorrectPhone
import com.wiseasy.smartpos.comm.util.putBasicAuth
import com.wiseasy.smartpos.comm.util.putBasicAuthLogin
import com.wiseasy.smartpos.login.R

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/10
 */
class LoginPresenter(context: Context) : BasePresenter(), LoginContract.IPresenter {

    private var mView: LoginContract.IView

    init {
        mContext = context
        mView = context as LoginContract.IView
    }

    override fun login(number: String, password: String) {
        log("login|number = $number, password = $password")
        mView.showLoading()
        if (number.length < 10 || !isCorrectPhone(number)) {
            mView.onLoginOrForgotPasswordFail(mContext?.getString(R.string.login_phone_error))
            return
        }
        if (password.length < 6) {
            mView.onLoginOrForgotPasswordFail(mContext?.getString(R.string.login_password_digit))
            return
        }
        SPUtil.setSmartPayPhone(mContext, number)
        launchRx {
            val requestJson = JSONObject()
            val basicAuth = JSONObject()
            mContext?.let {
                putBasicAuthLogin(mContext!!, password, basicAuth)
            }
            var result: LoginBean? = null
            requestJson["basicAuth"] = basicAuth
            withIOContext {
                result = RetrofitClientSmartPay.createWithoutToken(ISmartPayAPI::class.java)
                    .login(RetrofitClientSmartPay.buildRequestBody(requestJson)).dataConvert(LoginBean::class.java)
                Log.i(TAG, "login: result = $result")
            }
            result?.let {
                SPUtil.setSmartPayToken(mContext, result?.accessToken)
                //后台返回时间的时区为GMT+7
                val dateA: String? = result?.validTill?.replace("-", "")?.replace(":", "")?.replace(" ", "")
                Log.i(TAG, "verifyOTP: dataA : ${dateA?.toLong()}")
                SPUtil.setSmartPayValidTill(mContext, dateA?.toLong())
                SPUtil.setSmartPayWid(mContext, result?.wid)
                if (result!!.firstLogin == 1) {
                    SPUtil.setIsFirstLoginSmartPay(mContext, true)
                } else {
                    SPUtil.setIsFirstLoginSmartPay(mContext, false)
                }
                SPUtil.setSmartPayUserName(mContext, result!!.staffName)
                SPUtil.setSmartPayUserPosition(mContext, result!!.position)
                //如果是SP的数据为空（也就是第一次调用登录接口）或更新了版本号需要重新拉取商户信息
                val currentVersion = CommonUtil.getVersionName(mContext)
                val spVersion = SPUtil.getAppVersion(mContext)
                log("spVersion = $spVersion, currentVersion = $currentVersion")
                if (TextUtils.isEmpty(spVersion) || spVersion == currentVersion) {
                    log("版本无更新，不需要重新拉取配置")
                    mView.onLoginSuccess()
                } else {
                    //重新拉取商户信息
                    log("需要重新拉取商户信息")
                    getMerchantInfo()
                }
            }

        }.await {
            it.printStackTrace()
            mView.onLoginOrForgotPasswordFail(it.message)
        }
    }

    /**
     * 获取商户信息
     */
    private fun getMerchantInfo() {
        log("getMerchantInfo")
        launchRx {
            val json = JSONObject()
            json["sn"] = CommonUtil.getSN(mContext)
            var result: MerchantMappingInfo? = null
            withIOContext {
                result = RetrofitClientWiseCloud.create(IWiseCloudAPI::class.java)
                    .getMerchantInfo(RetrofitClientWiseCloud.buildRequestBody(json))
                    .dataConvert(MerchantMappingInfo::class.java)
            }
            result?.let {
                if (TextUtils.isEmpty(it.thirdMid)) {
                    mView.onLoginOrForgotPasswordFail(mContext?.getString(R.string.login_first_third_mid_empty))
                    return@let
                }
                if (TextUtils.isEmpty(it.thirdTid)) {
                    mView.onLoginOrForgotPasswordFail(mContext?.getString(R.string.login_first_third_tid_empty))
                    return@let
                }
                SPUtil.setSmartPaySpMid(mContext, result?.thirdMid)
                SPUtil.setSmartPaySpTid(mContext, result?.thirdTid)
                SPUtil.setWiseCloudMerchantInfo(mContext, Gson().toJson(result))
                mView.onLoginSuccess()
            }

        }.await {
            it.printStackTrace()
            mView.onLoginOrForgotPasswordFail(it.message)
        }
    }

    override fun forgotPassword(number: String) {
        log("forgotPassword|number = $number")
        mView.showLoading()
        if (number.length < 10) {
            mView.onLoginOrForgotPasswordFail(mContext?.getString(R.string.login_phone_error))
            return
        }
        SPUtil.setSmartPayPhone(mContext, number)
        launchRx {
            val requestJson = JSONObject()
            val basicAuth = JSONObject()
            var referId: String = ""
            mContext?.let {
                putBasicAuth(mContext!!, basicAuth, false)
            }
            var result: FirstLoginBean? = null
            requestJson["basicAuth"] = basicAuth
            requestJson["landingPage"] = "homepage"
            withIOContext {
                result = RetrofitClientSmartPay.createWithoutToken(ISmartPayAPI::class.java)
                    .forgetPassword(RetrofitClientSmartPay.buildRequestBody(requestJson)).dataConvert(
                        FirstLoginBean::class.java
                    )
                Log.i(TAG, "login: result = $result")
                referId = getRequestId()
            }
            result?.let {
                SPUtil.setSmartPayOtpDuration(mContext, result!!.duration?.toLong())
                mView.onForgetPasswordSuccess(referId)
            }

        }.await {
            it.printStackTrace()
            mView.onLoginOrForgotPasswordFail(it.message)
        }
    }

}