package com.wiseasy.smartpos.login.main

import android.content.Context
import android.os.Bundle
import android.view.View
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.module.service.MobileModuleService
import com.wiseasy.smartpos.comm.module.service.OtherModuleService
import com.wiseasy.smartpos.comm.module.service.TopUpModuleService
import com.wiseasy.smartpos.comm.view.ActivityCollector
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.login.R
import com.wiseasy.smartpos.login.databinding.ActivityHomeBinding

class HomeActivity : CommonActivity<ActivityHomeBinding>() {

    private lateinit var mContext: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        initView()

        ActivityCollector.finishAllWithoutCurrent(this)
    }

    override fun getBinding(): ActivityHomeBinding {
        return ActivityHomeBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.ll_smartpay -> {
                //Router.getInstance().getService(MobileModuleService::class.java)
                 //   ?.goToSmartPayActivity(mContext)
                Router.getInstance().getService(TopUpModuleService::class.java)?.goToTopUpActivity(mContext)
            }
            R.id.ll_other -> {
                Router.getInstance().getService(OtherModuleService::class.java)
                    ?.goToOtherActivity(mContext)
            }
        }
    }

    private fun initData() {
        mContext = this
        SPUtil.setAppVersion(mContext, CommonUtil.getVersionName(mContext))
    }

    private fun initView() {

    }


    override fun onBackPressed() {
        mDialog = CommonDialog.Builder(this).setTitle(getString(R.string.login_exit_app))
            .setButton(getString(R.string.common_ok), getString(R.string.common_no))
            .setListener(object : CommonDialog.OnClickListener {
                override fun onConfirm() {
                    mDialog?.dismiss()
                    ActivityCollector.finishAll()
                }

                override fun onCancel() {
                    mDialog?.dismiss()
                }

            })
            .createAndShow()
    }

}