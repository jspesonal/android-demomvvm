package com.wiseasy.smartpos.login.module.serviceimpl

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import com.google.gson.Gson
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.bean.MerchantMappingInfo
import com.wiseasy.smartpos.login.login.LoginActivity
import com.wiseasy.smartpos.login.main.HomeActivity
import com.wiseasy.smartpos.comm.module.service.LoginModuleService

/**
 * Created by Belle on 2021/12/29.
 */
class LoginModuleServiceImpl : LoginModuleService {

    override fun goToHomeActivity(context: Context) {
        context.startActivity(Intent(context, HomeActivity::class.java))
    }

    override fun goToLoginActivity(context: Context) {
        context.startActivity(Intent(context, LoginActivity::class.java))
    }

    override fun getMerchantInfo(context: Context): MerchantMappingInfo {
        val merchantMappingInfoJson = SPUtil.getWiseCloudMerchantInfo(context)
        if (TextUtils.isEmpty(merchantMappingInfoJson)) {
            return MerchantMappingInfo()
        }
        return Gson().fromJson(merchantMappingInfoJson, MerchantMappingInfo::class.java)
    }

}