package com.wiseasy.smartpos.login.otp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextUtils
import android.view.View
import androidx.core.view.isVisible
import com.wiseasy.smartpos.base.event.FinishFirstLoginEvent
import com.wiseasy.smartpos.base.event.RxBus
import com.wiseasy.smartpos.base.event.ScreenCanUseEvent
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.base.util.closeKeyboard
import com.wiseasy.smartpos.comm.Constant.SCREEN_TIME
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonCustomNumberInputKeyboard
import com.wiseasy.smartpos.login.R
import com.wiseasy.smartpos.login.databinding.ActivityVerifyOtpBinding
import com.wiseasy.smartpos.login.receiver.AlarmUtil
import com.wiseasy.smartpos.login.setpassword.SetPasswordActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

/**
 * 验证OTP界面
 */
class VerifyOTPActivity : CommonActivity<ActivityVerifyOtpBinding>(), VerifyOTPContract.IView {

    private lateinit var mContext: Context
    private lateinit var mPresenter: VerifyOTPPresenter
    private var mReferenceId: String? = ""
    private var mPhone: String? = ""

    private var mCountDownTimer: CountDownTimer? = null
    private var COUNT_DOWN = 180L //倒计时
    private var mReSendCount = 0//点击重试发送按钮的次数

    private val mCompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        initView()
        subscribeEvent()
    }
//
//    companion object {
//        fun actionStart(context: Context, phone: String, refId: String, isNeedReLogin: Boolean) {
//            val intent = Intent(context, VerifyOTPActivity::class.java)
//            intent.putExtra("referenceId", refId)
//            intent.putExtra("phone", phone)
//            intent.putExtra("isNeedReLogin", isNeedReLogin)
//            context.startActivity(intent)
//        }
//    }

    override fun onDestroy() {
        mCountDownTimer.let {
            it?.cancel()
            it == null
        }
        mCompositeDisposable.dispose()
        super.onDestroy()
    }

    override fun getBinding(): ActivityVerifyOtpBinding {
        return ActivityVerifyOtpBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.iv_back_password -> {//返回按钮
                finish()
            }

            R.id.ll_resend_otp -> {//重新发送验证码
                mReSendCount++
                mBinding.tvErrorMsg.visibility = View.INVISIBLE
                timer(SPUtil.getSmartPayOtpDuration(this))
                mBinding.tvTime.visibility = View.VISIBLE
                mPresenter.resendOTP(mPhone!!)
            }

            R.id.btn_sign_in -> {//登录
                showLoading()
                mPresenter.verifyOTP(mPhone!!, mBinding.etOtp.text.toString())
            }

            R.id.et_otp -> {//OTP
                mBinding.keyBoard.visibility = View.VISIBLE
                mBinding.btnSignIn.visibility = View.GONE
                closeKeyboard(this, mBinding.etOtp)
            }
        }
    }

    override fun onBackPressed() {
        //如果键盘处于显示状态，则手机键盘，否则调用系统方法
        if (mBinding.keyBoard.isVisible) {
            mBinding.keyBoard.visibility = View.GONE
            mBinding.btnSignIn.visibility = View.VISIBLE
        } else {
            super.onBackPressed()
        }
    }

    override fun onVerifySuccess() {
        log("onVerifySuccess")
        RxBus.getInstance().post(FinishFirstLoginEvent())
        mCountDownTimer?.cancel()
        hideLoading()

        val isNeedReLogin = intent.getBooleanExtra("isNeedReLogin", false)
        val intent = Intent(mContext, SetPasswordActivity::class.java)
        intent.putExtra("isNeedReLogin", isNeedReLogin)
        startActivity(intent)

        finish()
    }

    override fun onVerifyOrResendFail(msg: String?) {
        log("onVerifyFail|msg = $msg")
        mBinding.tvErrorMsg.visibility = View.VISIBLE
        mBinding.tvErrorMsg.text = msg
        hideLoading()
    }

    private fun initData() {
        mContext = this
        COUNT_DOWN = SPUtil.getSmartPayOtpDuration(mContext)
        mReferenceId = intent.getStringExtra("referenceId")
        mPhone = SPUtil.getSmartPayPhone(mContext)
        mPresenter = VerifyOTPPresenter(mContext, mReferenceId!!)
//        mBinding.etOtp.setText("111111")

    }

    private fun initView() {
        setFillScreen()
        mBinding.btnSignIn.isEnabled = false
        //数字键盘
        mBinding.keyBoard.setMaxLength(6)
        mBinding.keyBoard.setKeyBoardClickListener(object : CommonCustomNumberInputKeyboard.KeyBoardClickListener {
            override fun onOkTouch() {
                mBinding.keyBoard.visibility = View.GONE
                mBinding.btnSignIn.visibility = View.VISIBLE
            }

            override fun onInputNumberTouch(inputNumber: String?) {
                mBinding.etOtp.text = Editable.Factory.getInstance().newEditable(inputNumber)
                mBinding.btnSignIn.isEnabled = !TextUtils.isEmpty(inputNumber)
            }

        })

        //如果现在处于禁屏状态，则
        // 1. 不调用数字键盘
        // 2. 重新发送不可点击
        // 3. 不开启倒计时
        if (SPUtil.getIsScreenBan(mContext)) {
            log("initView|处于禁屏状态")
            mBinding.etOtp.isEnabled = false
            mBinding.llResendOtp.isEnabled = false
        } else {
            log("initView|不处于禁屏状态，开启倒计时")
            timer(SPUtil.getSmartPayOtpDuration(mContext))
        }
    }

    /**
     * 订阅事件
     */
    private fun subscribeEvent() {
        log("subscribeEvent订阅事件")
        mCompositeDisposable.add(RxBus.getInstance().toObservable(ScreenCanUseEvent::class.java)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                log("收到屏幕禁屏结束通知")
                //接到禁屏通知后
                // 1. 可以调用数字键盘
                // 2. 重新发送可点击
                // 3. 倒计时可开启
                mReSendCount = 0
                mBinding.etOtp.isEnabled = true
                mBinding.llResendOtp.isEnabled = true
                if (!TextUtils.isEmpty(mBinding.etOtp.text.toString())) {
                    mBinding.btnSignIn.isEnabled = true
                }
            })
    }

    private fun timer(time: Long) {
        mCountDownTimer = object : CountDownTimer((time + 1) * 1000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                mBinding.tvTime.text = "${millisUntilFinished / 1000} s ${getString(R.string.login_otp_left)}"
                log("倒计时：$millisUntilFinished 毫秒")
                mBinding.llResendOtp.isEnabled = false
            }

            override fun onFinish() {
                log("计时器完成|mReSendCount = $mReSendCount")
                //当重试三次后，都禁用，否则发定时闹钟（5分钟后进行解禁）
                if (mReSendCount < 3) {
                    log("可重新发送OTP")
                    mBinding.tvTime.visibility = View.INVISIBLE
                    mBinding.llResendOtp.isEnabled = true
                } else {
                    val screenTime = SCREEN_TIME * 60 * 1000L
                    log("发送闹钟广播|$SCREEN_TIME 分钟后响")
                    AlarmUtil.setAlarm(mContext, screenTime)
                    SPUtil.setIsScreenBan(mContext, true)
                    mBinding.tvTime.text = ""
                    mBinding.etOtp.isEnabled = false
                    mBinding.btnSignIn.isEnabled = false
                }
            }

        }
        mCountDownTimer?.start()
    }

}