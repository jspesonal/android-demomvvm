package com.wiseasy.smartpos.login.otp

import com.wiseasy.smartpos.base.mvp.BaseIView

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/10
 */
interface VerifyOTPContract {

    interface IView : BaseIView {
        /**
         * 验证成功
         */
        fun onVerifySuccess()

        /**
         * 验证或重新获取OTP失败
         */
        fun onVerifyOrResendFail(msg: String?)


    }

    interface IPresenter {
        /**
         * 重新获取OTP
         */
        fun resendOTP(phone: String)

        /**
         * 验证OTP
         */
        fun verifyOTP(phone: String, otp: String)
    }

}