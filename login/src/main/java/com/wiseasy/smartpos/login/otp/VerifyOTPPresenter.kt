package com.wiseasy.smartpos.login.otp

import android.content.Context
import android.util.Log
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.kotlin.withIOContext
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.comm.bean.FirstLoginBean
import com.wiseasy.smartpos.comm.bean.LoginBean
import com.wiseasy.smartpos.comm.http.smartpay.ISmartPayAPI
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.util.putBasicAuthFirstLogin
import com.wiseasy.smartpos.comm.util.putBasicAuthVerifyOtp
import com.wiseasy.smartpos.login.R

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/10
 */
class VerifyOTPPresenter(context: Context, referenceId: String) : BasePresenter(), VerifyOTPContract.IPresenter {

    private var mView: VerifyOTPContract.IView
    private var mReferenceId: String

    init {
        mContext = context
        mReferenceId = referenceId
        mView = context as VerifyOTPContract.IView
    }

    override fun resendOTP(phone: String) {
        log("resendOTP")
        launchRx {
            val requestJson = JSONObject()
            val basicAuth = JSONObject()
            var resId: String? = ""
            mContext?.let { putBasicAuthFirstLogin(it, basicAuth) }
            requestJson["basicAuth"] = basicAuth
            requestJson["landingPage"] = "fisrstlogin"

            var result: FirstLoginBean? = null
            withIOContext {
                result = RetrofitClientSmartPay.createWithoutToken(ISmartPayAPI::class.java)
                    .firstLogin(RetrofitClientSmartPay.buildRequestBody(requestJson))
                    .dataConvert(FirstLoginBean::class.java)
                log("resendOTP|result = $result")
                resId = RetrofitClientSmartPay.getRequestId()
                result?.duration.let {
                    SPUtil.setSmartPayOtpDuration(mContext, result!!.duration!!.toLong())
                }
            }
        }.await {
            it.printStackTrace()
            mView.onVerifyOrResendFail(it.message)
        }
    }

    override fun verifyOTP(phone: String, otp: String) {
        log("verifyOTP|otp = $otp")
        if (otp.length < 6) {
            mView.onVerifyOrResendFail(mContext?.getString(R.string.login_otp_error))
            return
        }
        launchRx {
            val requestJson = JSONObject()
            val basicAuth = JSONObject()
            mContext?.let {
                putBasicAuthVerifyOtp(it, otp, basicAuth)
            }

            requestJson["basicAuth"] = basicAuth
            requestJson["referenceId"] = mReferenceId

            var result: LoginBean? = null
            withIOContext {
                result = RetrofitClientSmartPay.createWithoutToken(ISmartPayAPI::class.java)
                    .verifyOtp(RetrofitClientSmartPay.buildRequestBody(requestJson))
                    .dataConvert(LoginBean::class.java)
                log("verifyOTP|result = $result")
            }
            result?.let {
                SPUtil.setSmartPayToken(mContext, it.accessToken)
                //后台返回时间的时区为GMT+7
                val dateA: String? = result?.validTill?.replace("-", "")?.replace(":", "")?.replace(" ", "")
                Log.i(TAG, "verifyOTP: dataA : ${dateA?.toLong()}")
                SPUtil.setSmartPayValidTill(mContext, dateA?.toLong())
                //获取东七区当前时间
//                val dateB: String = DateUtil.getCurDateStrByTimeZone("yyyyMMddHHmmss", "GMT+0:00")
                mView.onVerifySuccess()
            }
//            mView.onVerifySuccess()
        }.await {
            it.printStackTrace()
            mView.onVerifyOrResendFail(it.message)
        }
    }


}
