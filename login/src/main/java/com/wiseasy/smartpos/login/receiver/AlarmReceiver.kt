package com.wiseasy.smartpos.login.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.wiseasy.smartpos.base.event.RxBus
import com.wiseasy.smartpos.base.event.ScreenCanUseEvent
import com.wiseasy.smartpos.base.util.SPUtil

/**
 * 定时广播接收器
 */
class AlarmReceiver : BroadcastReceiver() {

    private val TAG = "AlarmReceiver -> "

    override fun onReceive(context: Context, intent: Intent?) {
        Log.d(TAG, "onReceive|收到闹钟广播")
        //设置SP里的禁屏时间为0（主要针对于杀掉进行重新进情况的处理）
        SPUtil.setIsScreenBan(context, false)
        //发送解禁屏时间（主要针对于当前界面就是OTP等与禁屏有关的界面情况的处理）
        RxBus.getInstance().post(ScreenCanUseEvent())

    }
}