package com.wiseasy.smartpos.login.receiver

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log

/**
 * @author Belle(Baiyunyan)
 * @date 2022/02/17
 */
object AlarmUtil {

    private val TAG = "AlarmUtil ->"
    private var mAlarmManager: AlarmManager? = null
    private var mPendingIntent: PendingIntent? = null

    /**
     * 开启闹钟
     * @param context Context
     * @param alarmTime Long XX毫秒后发送广播
     */
    fun setAlarm(context: Context, alarmTime: Long) {
        Log.d(TAG, "$alarmTime 毫秒后发送广播")
        if (mAlarmManager != null && mPendingIntent != null) {
            Log.d(TAG, "取消之前的闹铃")
            mAlarmManager?.cancel(mPendingIntent)
        }

        mAlarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val trigerAtTime = System.currentTimeMillis() + alarmTime
        val intentReceiver = Intent(context, AlarmReceiver::class.java)
        mPendingIntent = PendingIntent.getBroadcast(context, 0, intentReceiver, 0)

        Log.d(TAG, "Build.VERSION.SDK_INT = ${Build.VERSION.SDK_INT}")
        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
                Log.d(TAG, "android 6.0 及以上")
                mAlarmManager?.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, trigerAtTime, mPendingIntent)
            }
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT -> {
                Log.d(TAG, "android 4.4 到 6.0")
                mAlarmManager?.setExact(AlarmManager.RTC_WAKEUP, trigerAtTime, mPendingIntent)
            }
            else -> {
                Log.d(TAG, "小于android 4.4")
                mAlarmManager!![AlarmManager.RTC_WAKEUP, trigerAtTime] = mPendingIntent //java中的AlarmManager.set()方法
            }
        }

    }

}