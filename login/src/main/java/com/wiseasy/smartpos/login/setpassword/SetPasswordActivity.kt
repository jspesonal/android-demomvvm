package com.wiseasy.smartpos.login.setpassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.View
import android.view.View.*
import androidx.core.view.isVisible
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonCustomNumberInputKeyboard
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.login.R
import com.wiseasy.smartpos.login.databinding.ActivitySetPasswordBinding
import com.wiseasy.smartpos.login.login.LoginActivity
import com.wiseasy.smartpos.login.main.HomeActivity

/**
 * 设置密码界面
 */
class SetPasswordActivity : CommonActivity<ActivitySetPasswordBinding>(), SetPasswordContract.IView {

    private lateinit var mContext: Context
    private lateinit var mPresenter: SetPasswordPresenter
    private var mIsNeedReLogin: Boolean = false//是否需要重新登录
    private var mIsFocusNewPassword = false//输入新密码是否获取焦点
    private var mIsFocusNewPasswordReenter = false//重新输入新密码是否获取焦点

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        initView()
    }

    override fun getBinding(): ActivitySetPasswordBinding {
        return ActivitySetPasswordBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btn_confirm -> {//确定按钮
                mPresenter.setPassword(mBinding.etNewPassword.text.toString(), mBinding.etNewPasswordReenter.text.toString())
            }
            R.id.et_new_password -> {//输入新密码
                log("输入新密码获取焦点")
                mIsFocusNewPassword = true
                mIsFocusNewPasswordReenter = false
                mBinding.etNewPassword.setBackgroundResource(R.drawable.login_shape_input_focus_bg)
                mBinding.etNewPasswordReenter.setBackgroundResource(R.drawable.login_shape_input_bg)
                mBinding.btnConfirm.visibility = GONE
                mBinding.keyBoard.visibility = VISIBLE
                val password = mBinding.etNewPassword.text.toString()
                mBinding.keyBoard.clear()
                mBinding.keyBoard.input(password)
            }
            R.id.et_new_password_reenter -> {//重新输入新密码
                log("重新输入新密码获取焦点")
                setClickNewPasswordView()
            }
        }
    }

    override fun onBackPressed() {
        //如果键盘处于显示状态，则手机键盘，否则调用系统方法
        if (mBinding.keyBoard.isVisible) {
            mBinding.keyBoard.visibility = GONE
            mBinding.btnConfirm.visibility = VISIBLE
        } else {
            super.onBackPressed()
        }
    }

    override fun onSetSuccess() {
        log("onSetSuccess")
        hideLoading()
        if (mIsNeedReLogin || SPUtil.getIsFirstLoginSmartPay(mContext)) {
            mDialog = CommonDialog.Builder(this@SetPasswordActivity)
                .setTitle(getString(R.string.login_update_pwd_success_tip))
                .setButton(getString(R.string.common_close))
                .setListener(object : CommonDialog.OnClickListener {
                    override fun onConfirm() {
                        mDialog.dismiss()
                        startActivity(Intent(mContext, LoginActivity::class.java))
                        finish()
                    }

                    override fun onCancel() {
                    }

                })
                .createAndShow()

        } else {
            startActivity(Intent(mContext, HomeActivity::class.java))
            finish()
        }
    }

    override fun onSetFail(msg: String?) {
        log("onSetFail|msg = $msg")
        hideLoading()
        mBinding.tvErrorMsg.visibility = View.VISIBLE
        mBinding.tvErrorMsg.text = msg
    }

    private fun initView() {
        setFillScreen()

        //数字键盘
        mBinding.keyBoard.setMaxLength(6)
        mBinding.keyBoard.setKeyBoardClickListener(object : CommonCustomNumberInputKeyboard.KeyBoardClickListener {
            override fun onOkTouch() {
                //如果是当前焦点在输入密码，则点击Next后，光标移动到下一个输入框
                if (mIsFocusNewPassword) {
                    setClickNewPasswordView()
                    return
                }
                //如果当前焦点在重新输入密码，则点击Next后，收起软键盘
                if (mIsFocusNewPasswordReenter) {
                    mBinding.keyBoard.visibility = GONE
                    mBinding.btnConfirm.visibility = VISIBLE
                }
            }

            override fun onInputNumberTouch(inputNumber: String?) {
                if (mIsFocusNewPassword) {
//                    log("输入密码获取焦点，inputNumber = $inputNumber")
                    mBinding.etNewPassword.text = Editable.Factory.getInstance().newEditable(inputNumber)
                }
                if (mIsFocusNewPasswordReenter) {
//                    log("重新输入密码获取焦点，inputNumber = $inputNumber")
                    mBinding.etNewPasswordReenter.text = Editable.Factory.getInstance().newEditable(inputNumber)
                }
                mBinding.btnConfirm.isEnabled = !TextUtils.isEmpty(mBinding.etNewPassword.text.toString()) && !TextUtils.isEmpty(mBinding.etNewPasswordReenter.text.toString())
            }

        })

        //设置两个输入框不可获取焦点
        mBinding.etNewPassword.isFocusable = false
        mBinding.etNewPasswordReenter.isFocusable = false

        mBinding.btnConfirm.isEnabled = !TextUtils.isEmpty(mBinding.etNewPassword.text.toString()) && !TextUtils.isEmpty(mBinding.etNewPasswordReenter.text.toString())
    }

    private fun initData() {
        mContext = this
        mPresenter = SetPasswordPresenter(mContext)
        mIsNeedReLogin = intent.getBooleanExtra("isNeedReLogin", false)
//        mBinding.etNewPassword.setText("303035")
//        mBinding.etNewPasswordReenter.setText("303035")
    }

    /**
     * 设置点击 重新输入密码 后的view
     */
    private fun setClickNewPasswordView() {
        mIsFocusNewPasswordReenter = true
        mIsFocusNewPassword = false
        mBinding.etNewPassword.setBackgroundResource(R.drawable.login_shape_input_bg)
        mBinding.etNewPasswordReenter.setBackgroundResource(R.drawable.login_shape_input_focus_bg)
        mBinding.btnConfirm.visibility = GONE
        mBinding.keyBoard.visibility = VISIBLE
        val reEnter = mBinding.etNewPasswordReenter.text.toString()
//        log("reEnter = $reEnter")
        mBinding.keyBoard.clear()
        mBinding.keyBoard.input(reEnter)
    }

}