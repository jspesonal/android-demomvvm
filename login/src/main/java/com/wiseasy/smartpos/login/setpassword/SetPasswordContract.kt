package com.wiseasy.smartpos.login.setpassword

import com.wiseasy.smartpos.base.mvp.BaseIView

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/10
 */
interface SetPasswordContract {

    interface IView : BaseIView {
        /**
         * 保存成功
         */
        fun onSetSuccess()

        /**
         * 设置失败
         */
        fun onSetFail(msg: String?)
    }

    interface IPresenter {
        /**
         * 设置密码
         */
        fun setPassword(newPassword: String, reEnterNewPassword: String)

        /**
         * 获取商户信息
         */
        fun getMerchantInfo()
    }

}