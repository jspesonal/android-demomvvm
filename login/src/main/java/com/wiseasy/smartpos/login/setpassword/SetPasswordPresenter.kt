package com.wiseasy.smartpos.login.setpassword

import android.content.Context
import android.text.TextUtils
import com.alibaba.fastjson.JSONObject
import com.google.gson.Gson
import com.wiseasy.smartpos.base.kotlin.withIOContext
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.comm.bean.MerchantMappingInfo
import com.wiseasy.smartpos.comm.http.smartpay.ISmartPayAPI
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.http.wisecloud.IWiseCloudAPI
import com.wiseasy.smartpos.comm.http.wisecloud.RetrofitClientWiseCloud
import com.wiseasy.smartpos.comm.util.putBasicAuthSavePassword
import com.wiseasy.smartpos.login.R
import okhttp3.Response

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/10
 */
class SetPasswordPresenter(context: Context) : BasePresenter(), SetPasswordContract.IPresenter {

    private var mView: SetPasswordContract.IView

    init {
        mContext = context
        mView = context as SetPasswordContract.IView
    }

    override fun setPassword(newPassword: String, reEnterNewPassword: String) {
        log("setPassword|newPassword = $newPassword, reEnterNewPassword = $reEnterNewPassword")
        mView.showLoading()
        if (newPassword != reEnterNewPassword) {
            mView.onSetFail(mContext?.getString(R.string.login_set_password_error))
            return
        }
        if (newPassword.length < 6 || reEnterNewPassword.length < 6) {
            mView.onSetFail(mContext?.getString(R.string.login_password_digit))
            return
        }
        launchRx {
            val requestJson = JSONObject()
            val basicAuth = JSONObject()
            mContext?.let {
                putBasicAuthSavePassword(it, newPassword, basicAuth)
            }

            requestJson["basicAuth"] = basicAuth

            var result: Response? = null
            withIOContext {
                result = RetrofitClientSmartPay.create(ISmartPayAPI::class.java)
                    .savePassword(RetrofitClientSmartPay.buildRequestBody(requestJson))
                    .dataConvert(Response::class.java)
            }
            getMerchantInfo()
//            mView.onSetSuccess()
        }.await {
            it.printStackTrace()
            mView.onSetFail(it.message)
        }
    }

    override fun getMerchantInfo() {
        log("getMerchantInfo")
        launchRx {
            val json = JSONObject()
            json["sn"] = CommonUtil.getSN(mContext)
            var result: MerchantMappingInfo? = null
            withIOContext {
                result = RetrofitClientWiseCloud.create(IWiseCloudAPI::class.java)
                    .getMerchantInfo(RetrofitClientWiseCloud.buildRequestBody(json))
                    .dataConvert(MerchantMappingInfo::class.java)
            }
            result?.let {
                if (TextUtils.isEmpty(it.thirdMid)) {
                    mView.onSetFail(mContext?.getString(R.string.login_first_third_mid_empty))
                    return@let
                }
                if (TextUtils.isEmpty(it.thirdTid)) {
                    mView.onSetFail(mContext?.getString(R.string.login_first_third_tid_empty))
                    return@let
                }
                if (TextUtils.isEmpty(it.merchantNo)) {
                    mView.onSetFail(mContext?.getString(R.string.login_first_mid_empty))
                    return@let
                }
                if (TextUtils.isEmpty(it.merchantName)) {
                    mView.onSetFail(mContext?.getString(R.string.login_first_mname_empty))
                    return@let
                }
                if (TextUtils.isEmpty(it.storeNo)) {
                    mView.onSetFail(mContext?.getString(R.string.login_first_tid_empty))
                    return@let
                }
                SPUtil.setSmartPaySpMid(mContext, result?.thirdMid)
                SPUtil.setSmartPaySpTid(mContext, result?.thirdTid)
                SPUtil.setWiseCloudMerchantInfo(mContext, Gson().toJson(result))
                SPUtil.setIsFirstLogin(mContext, false)
                mView.onSetSuccess()
            }

        }.await {
            it.printStackTrace()
            mView.onSetFail(it.message)
        }
    }

}