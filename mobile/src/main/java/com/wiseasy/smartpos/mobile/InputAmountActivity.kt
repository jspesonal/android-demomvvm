package com.wiseasy.smartpos.mobile

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.View.*
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.tbruyelle.rxpermissions2.RxPermissions
import com.wiseasy.android.utils.decimalFormat
import com.wiseasy.smartpos.mobile.databinding.ActivityInputAmountBinding
import com.wiseasy.smartpos.base.util.closeKeyboard
import com.wiseasy.smartpos.comm.Constant.PAYMENT_METHOD_QR_CODE
import com.wiseasy.smartpos.comm.Constant.PAYMENT_METHOD_SCAN_TO_PAY
import com.wiseasy.smartpos.comm.Constant.UNIT
import com.wiseasy.smartpos.comm.bean.TransDetailBean
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonCustomNumberInputKeyboard
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.comm.view.CommonPopupWindow
import com.wiseasy.smartpos.mobile.qrpay.QRPayActivity
import com.wiseasy.smartpos.mobile.scan.ScanPayActivity
import com.wiseasy.smartpos.mobile.view.QRPayPopupWindow
import com.wiseasy.smartpos.mobile.view.ScanToPayPopupWindow

/**
 * 输入金额界界面
 */
class InputAmountActivity : CommonActivity<ActivityInputAmountBinding>() {

    private lateinit var mContext: Context
    private lateinit var mTransType: String

    private var mPopupWindow: CommonPopupWindow? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        initView()
    }

    override fun onDestroy() {
        mPopupWindow?.dismiss()
        super.onDestroy()
    }

    override fun getBinding(): ActivityInputAmountBinding {
        return ActivityInputAmountBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.iv_clear -> {//清除金额
//                mBinding.etAmount.text = Editable.Factory.getInstance().newEditable("")
                mBinding.keyBoard.clear()
            }
            R.id.btn_next -> {//下一步
                if (TextUtils.isEmpty(mBinding.tvAmount.text)) {
                    toast(getString(R.string.mobile_input_amount))
                    return
                }
                if (TextUtils.isEmpty(mBinding.etNote.text.toString().trim())) {
                    toast(getString(R.string.mobile_input_note))
                    return
                }
                val amount = mBinding.tvAmount.text.toString().replace(UNIT, "").replace(",", "").toInt()
                if (amount < 1000 || amount > 50000000) {
                    toast(getString(R.string.mobile_scan_amount))
                    return
                }
                when (mTransType) {
                    PAYMENT_METHOD_SCAN_TO_PAY -> {
                        //先检测是否打开相机权限,如果没有打开，则弹窗提示
                        checkPermissions()
                    }
                    PAYMENT_METHOD_QR_CODE -> {
                        val intent = Intent(this, QRPayActivity::class.java)
                        goToNextPage(intent)
                    }
                    else -> {
                        Intent(this, QRPayActivity::class.java)
                    }
                }

            }
            R.id.tv_amount -> {
                closeKeyboard(this, mBinding.etNote)
                mBinding.keyBoard.visibility = VISIBLE
                mBinding.btnNext.visibility = INVISIBLE
            }
            R.id.et_note -> {
                mBinding.keyBoard.visibility = GONE
                mBinding.btnNext.visibility = VISIBLE
            }
        }
    }

    override fun onBackPressed() {
        //如果键盘处于显示状态，则手机键盘，否则调用系统方法
        if (mBinding.keyBoard.isVisible) {
            mBinding.keyBoard.visibility = View.GONE
            mBinding.btnNext.visibility = View.VISIBLE
        } else {
            super.onBackPressed()
        }
    }

    override fun onActionBarRight() {

        when (mTransType) {
            PAYMENT_METHOD_SCAN_TO_PAY -> {
                mPopupWindow = ScanToPayPopupWindow(mContext)
            }
            PAYMENT_METHOD_QR_CODE -> {
                mPopupWindow = QRPayPopupWindow(mContext)
            }
        }
        mPopupWindow?.show(mBinding.actionBarWhite)
    }

    private fun initData() {
        mContext = this
        mTransType = intent.getStringExtra("trans_type")!!
    }

    private fun initView() {
        var actionBarTitle = ""
        when (mTransType) {
            PAYMENT_METHOD_SCAN_TO_PAY -> {
                actionBarTitle = getString(R.string.mobile_scan_to_pay)
                Glide.with(mContext).load(R.mipmap.mobile_gif_scan_to_pay).into(mBinding.ivBackground)
            }
            PAYMENT_METHOD_QR_CODE -> {
                actionBarTitle = getString(R.string.mobile_qr_pay)
                Glide.with(mContext).load(R.mipmap.mobile_gif_qr_pay).into(mBinding.ivBackground)
            }
        }
        mBinding.actionBarWhite.setTitle(actionBarTitle)

        //数字键盘
        mBinding.keyBoard.setMaxLengthCanNotZero(9)
        mBinding.keyBoard.setKeyBoardClickListener(object : CommonCustomNumberInputKeyboard.KeyBoardClickListener {
            override fun onOkTouch() {
                mBinding.btnNext.visibility = VISIBLE
                mBinding.keyBoard.visibility = GONE
            }

            override fun onInputNumberTouch(inputNumber: String?) {
                log("onInputNumberTouch|inputNumber = $inputNumber")
                if (TextUtils.isEmpty(inputNumber)) {
                    mBinding.tvAmount.text = ""
                    mBinding.ivClear.visibility = INVISIBLE
                } else {
                    mBinding.tvAmount.text = decimalFormat(inputNumber!!) + UNIT
                    mBinding.ivClear.visibility = VISIBLE
                }
            }

        })

        //输入备注输入框是否获取焦点
        mBinding.etNote.onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
            log("hasFocus = $hasFocus")
            if (hasFocus) {
                mBinding.keyBoard.visibility = GONE
            }
        }

    }

    /**
     * 跳转到下一个界面
     */
    private fun goToNextPage(intent: Intent) {
        val trans = TransDetailBean()
        trans.amount = mBinding.tvAmount.text.toString().replace(UNIT, "").replace(",", "").toInt()
        trans.desc = mBinding.etNote.text.toString()
        intent.putExtra("trans", trans)
        startActivity(intent)
        finish()
    }

    /**
     * 申请权限
     * 检测是否打开了相机权限
     */
    @SuppressLint("CheckResult")
    private fun checkPermissions() {
        log("checkPermissions")
        val rxPermissions = RxPermissions(this)
        rxPermissions.request(
            Manifest.permission.CAMERA,
        )
            .subscribe { granted ->
                log("权限申请结果：$granted")
                if (!granted) {
                    mDialog = CommonDialog.Builder(this)
                        .setTitle(getString(R.string.mobile_scan_camera_permission_not_open))
                        .setButton(getString(R.string.common_ok))
                        .createAndShow()
                } else {
                    //跳转到扫码界面
                    val intent = Intent(this, ScanPayActivity::class.java)
                    goToNextPage(intent)
                }
            }
    }

}