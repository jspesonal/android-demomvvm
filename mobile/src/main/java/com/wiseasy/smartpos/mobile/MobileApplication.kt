package com.wiseasy.smartpos.mobile

import android.util.Log
import com.google.gson.Gson
import com.wiseasy.smartpos.base.BaseApplication
import com.wiseasy.smartpos.base.speech.SpeakManager
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.bean.BasicAuth
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import okhttp3.Headers

/**
 * Created by Bella on 2021/12/20.
 * Mobile模块单独运行时(作为独立的app)，会调用该Application，
 * 然后该Application调用Like(做一些初始化类操作)
 * Mobile作为模块运行时，会只调用Like
 */
class MobileApplication : BaseApplication() {

    private val TAG = "MobileApplication"

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate")
        //调用Like
        MobileApplicationLike().init(this)
        //初始化retrofit
        initClient()
        //初始化测试阶段的参数
        initTestParam()
        //语音初始化
        SpeakManager.getInstance().init(this)
    }

    private fun initClient() {
        Log.d(TAG, "initClient start")
        RetrofitClientSmartPay.init(context)
        val headers = Headers.Builder()
            .add("Content-Type", "application/json;charset=UTF-8")
            .add("X-Device-Model", "Android")
            .add("X-OS-Name", "Android")
            .add("X-OS-Version", "Android 8.0.1")
            .add("X-App-Version", "1.0.1")
            .add("Accept-Language", "en")
            .build()

        RetrofitClientSmartPay.addCommonHeaders(headers = headers)
        Log.d(TAG, "initClient end")
    }

    private fun initTestParam() {
        SPUtil.setSmartPayToken(context, "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI1ODEwMDc3NzQ4Nzk3NTA1IiwiaWF0IjoxNjQzMDA5Njc5LCJpc3MiOiJTTUFSVFBBWSJ9.kCJmaiC2-AKEsPPUmH-s2qoPqD3TBJM6u9-I4ESVtuI")
        SPUtil.setSmartPaySpMid(context, "00257110")
        SPUtil.setSmartPaySpTid(context, "01000002")
        SPUtil.setSmartPayPhone(context, "0988303036")

        SPUtil.setSmartPayValidTill(context, 99999999999999)

//        val basicAuth = BasicAuth()
//        basicAuth.spMID = "00257171"
//        basicAuth.spTID = "01000001"
//        basicAuth.deviceSN = CommonUtil.getSN(context)
//        basicAuth.wid = "1000000015873"
//        basicAuth.phone = "0341112220"
//
//        val basicAuthString = Gson().toJson(basicAuth)
//        SPUtil.setSmartPayBasicAuth(context, basicAuthString)
    }
}