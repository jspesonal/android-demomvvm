package com.wiseasy.smartpos.mobile

import android.app.Application
import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.wiseasy.smartpos.base.IApplicationLike
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.bean.BasicAuth
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.module.service.MobileModuleService
import com.wiseasy.smartpos.mobile.module.serviceimpl.MobileModuleServiceImpl
import okhttp3.Headers


/**
 * Created by Bella on 2021/12/20.
 */
class MobileApplicationLike : IApplicationLike {

    private val TAG = "MobileApplicationLike"
    private val router = Router.getInstance()
    private lateinit var context: Context

    override fun init(application: Application) {
        Log.d(TAG, "init")
        context = application.applicationContext
        //做一些初始化的操作
        router.addService(MobileModuleService::class.java, MobileModuleServiceImpl())
    }

}