package com.wiseasy.smartpos.mobile

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.wiseasy.smartpos.comm.Constant
import com.wiseasy.smartpos.comm.bean.CustomerInfoBean
import com.wiseasy.smartpos.comm.bean.TransDetailBean
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.mobile.databinding.ActivitySmartPayBinding
import com.wiseasy.smartpos.mobile.result.PayResultActivity

/**
 * 码付入口界面
 */
class SmartPayActivity : CommonActivity<ActivitySmartPayBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
    }

    override fun getBinding(): ActivitySmartPayBinding {
        return ActivitySmartPayBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.ll_tap_to_pay -> {
                val trans = TransDetailBean()
                trans.amount = 10000
                trans.desc = "this is a big customer"
                trans.qrcode = "123456"
                trans.orderNo = "d22a0772e6164c7fa0fa7e009"
                trans.transId = "2022010900000040"
                trans.status = "PAYED"//PAYED   PROCESSING   PAY_ERROR
                trans.created = "2022-01-07T09:59:22"
                trans.paymentTime = "2022-01-07T09:59:33"
                trans.receiptNo = "20220126-MM-00257110-001"

                val customerInfo = CustomerInfoBean()
                customerInfo.name = "Huỳnh Văn Hoạt"
                customerInfo.phone = "090****001"
                customerInfo.logo = "https://dev-s3.paysmart.com.vn/dev-staticweb/avatar/1000000659336_9080000012g0gipe.ico"

                trans.customerInfo = customerInfo
                val intent = Intent(this, PayResultActivity::class.java)
//                val intent = Intent(this, QRPayActivity::class.java)
                intent.putExtra("trans", trans)
//                startActivity(intent)

//                if (PrinterManager.getInstance().isNeedPrint(this)) {
//                    log("print|开始打印")
//                    PrinterManager.getInstance().print(this, trans, TransactionAdapter(this, false)) { e ->
//                        log("print|event")
//                        if (e != null) {
//                            log("打印失败：${e.msg}")
//                        } else {
//                            log("打印成功")
//                        }
//                    }
//                } else {
//                    log("print|未配置打印机")
//                }
            }

            R.id.ll_scan_to_pay -> {
                val intent = Intent(this, InputAmountActivity::class.java)
                intent.putExtra("trans_type", Constant.PAYMENT_METHOD_SCAN_TO_PAY)
                startActivity(intent)
                finish()
            }

            R.id.ll_qr_pay -> {
                val intent = Intent(this, InputAmountActivity::class.java)
                intent.putExtra("trans_type", Constant.PAYMENT_METHOD_QR_CODE)
                startActivity(intent)
                finish()
            }
        }
    }

    private fun initView() {

    }

}