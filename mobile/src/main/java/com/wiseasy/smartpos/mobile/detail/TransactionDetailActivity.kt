package com.wiseasy.smartpos.mobile.detail

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bumptech.glide.Glide
import com.wiseasy.android.utils.decimalFormat
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.comm.Constant
import com.wiseasy.smartpos.comm.Constant.UNIT
import com.wiseasy.smartpos.comm.bean.TransDetailOnDetailPageBean
import com.wiseasy.smartpos.comm.module.service.LoginModuleService
import com.wiseasy.smartpos.comm.util.checkEmail
import com.wiseasy.smartpos.comm.util.getIconByStatusOnDetailPage
import com.wiseasy.smartpos.comm.util.getPaymentMethodNameByPaymentMethod
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.comm.view.CommonSendEmailDialog
import com.wiseasy.smartpos.mobile.R
import com.wiseasy.smartpos.mobile.databinding.ActivityTransactionDetailBinding

/**
 * 码付交易详情界面
 */
class TransactionDetailActivity : CommonActivity<ActivityTransactionDetailBinding>(), TransactionDetailContract.IView {

    private lateinit var mContext: Context
    private lateinit var mPresenter: TransactionDetailPresenter
    private lateinit var mOrderNo: String

    private var mSendEmailDialog: CommonSendEmailDialog? = null
    private var mEmail = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
    }

    override fun getBinding(): ActivityTransactionDetailBinding {
        return ActivityTransactionDetailBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btn_back -> {//返回主界面
                Router.getInstance().getService(LoginModuleService::class.java)?.goToHomeActivity(mContext)
                finish()
            }
            R.id.tv_send_email -> {//发送短信
                mSendEmailDialog = CommonSendEmailDialog.Builder(this)
                    .setContent(getString(R.string.mobile_send_email))
                    .setListener(object : CommonSendEmailDialog.OnClickListener {
                        override fun onConfirm(email: String) {
                            if (checkEmail(email)) {
                                mSendEmailDialog?.dismiss()
                                mEmail = email
                                mPresenter.sendEmail(email, mOrderNo)
                            } else {
                                mSendEmailDialog?.setErrorText(getString(R.string.mobile_email_error))
                            }

                        }

                        override fun onCancel() {
                        }

                    })
                    .createAndShow()
            }
        }
    }

    override fun onQuerySuccess(result: TransDetailOnDetailPageBean) {
        log("onQuerySuccess")
        hideLoading()
        mBinding.ivStatus.setImageDrawable(getIconByStatusOnDetailPage(mContext, result.status!!))
        mBinding.tvAmount.text = decimalFormat(result.amount.toString()) + UNIT

        Glide.with(mContext).load(result.customerAvatar).into(mBinding.ivCustomerIcon)
        mBinding.tvCustomerName.text = result.customerName
        mBinding.tvCustomerPhone.text = result.customerPhone

        mBinding.viewTransId.text = result.transId
        mBinding.viewTime.text = result.date
        mBinding.viewPayMethod.text = getPaymentMethodNameByPaymentMethod(mContext, result.typeTrans)
        mBinding.viewTransFee.text = decimalFormat(result.fee.toString()) + UNIT
        mBinding.viewReceivedAmount.text = decimalFormat(result.paidAmount.toString()) + UNIT

        //只有交易结果是成功或已结算状态，才显示发送邮件的按钮，否则不显示
        when (result.status) {
            Constant.TRANS_STATUS_DETAIL_SETTLEMENT, Constant.TRANS_STATUS_DETAIL_SUCCESS -> {
                mBinding.tvSendEmail.visibility = VISIBLE
            }
            else -> {
                mBinding.tvSendEmail.visibility = GONE
            }
        }
    }

    override fun onQueryFail(msg: String) {
        log("onQueryFail")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setTitle(msg)
            .setButton(getString(R.string.common_close))
            .createAndShow()
    }

    override fun onSendSuccess() {
        log("onSendSuccess")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setLittleIcon(R.mipmap.mobile_result_success)
            .setTitle(getString(R.string.mobile_send_email_success))
            .setDescription("${getString(R.string.mobile_email_to)} \n $mEmail")
            .setButton(getString(R.string.common_ok))
            .createAndShow()
    }

    override fun onSendFail(msg: String) {
        log("onSendFail|msg = $msg")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setLittleIcon(R.mipmap.mobile_result_fail)
            .setTitle(getString(R.string.mobile_send_email_fail))
            .setDescription("${getString(R.string.mobile_email_to_error)} \n $mEmail \n \n ${getString(R.string.mobile_email_to_try_again)}")
            .setButton(getString(R.string.common_retry))
            .setExitIsShow(true)
            .setListener(object : CommonDialog.OnClickListener {
                override fun onConfirm() {
                    mPresenter.sendEmail(mEmail, mOrderNo)
                }

                override fun onCancel() {
                }

            })
            .createAndShow()
    }

    private fun initData() {
        mContext = this
        mPresenter = TransactionDetailPresenter(mContext)

        mOrderNo = intent.getStringExtra("orderNo")!!
        mPresenter.query(mOrderNo)
    }

}