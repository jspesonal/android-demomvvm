package com.wiseasy.smartpos.mobile.detail

import com.wiseasy.smartpos.base.mvp.BaseIView
import com.wiseasy.smartpos.comm.bean.TransDetailOnDetailPageBean

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/07
 */
interface TransactionDetailContract {

    interface IView : BaseIView {

        /**
         * 查询成功
         */
        fun onQuerySuccess(result: TransDetailOnDetailPageBean)

        /**
         * 查询失败
         */
        fun onQueryFail(msg: String)

        /**
         * 发送短信成功
         */
        fun onSendSuccess()

        /**
         * 发送短信失败
         */
        fun onSendFail(msg: String)
    }

    interface IPresenter {
        /**
         * 查询交易
         */
        fun query(orderNo: String)

        /**
         * 发送短信
         */
        fun sendEmail(email: String, orderNo: String)
    }

}