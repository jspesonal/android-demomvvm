package com.wiseasy.smartpos.mobile.detail

import android.content.Context
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.kotlin.withIOContext
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.comm.bean.SendEmailBean
import com.wiseasy.smartpos.comm.bean.TransDetailOnDetailPageBean
import com.wiseasy.smartpos.comm.http.smartpay.ISmartPayAPI
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.util.putBasicAuth

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/07
 */
class TransactionDetailPresenter(context: Context) : BasePresenter(), TransactionDetailContract.IPresenter {

    private var mView: TransactionDetailContract.IView

    init {
        mContext = context
        mView = context as TransactionDetailContract.IView
    }

    override fun query(orderNo: String) {
        log("queryOrder|orderNo = $orderNo")
        mView.showLoading()
        launchRx {
            val basicAuth = JSONObject()
            mContext?.let { putBasicAuth(it, basicAuth) }

            val json = JSONObject()
            json["basicAuth"] = basicAuth
//            json["channel"] = "WSP"
            json["refNo"] = orderNo
//            json["createTime"] = DateUtil.getCurDateStrByTimeZone("yyyy-MM-dd'T'HH:mm:ss", Constant.TOMEZONE)

            var result: TransDetailOnDetailPageBean? = null
            withIOContext() {
                result = RetrofitClientSmartPay.create(ISmartPayAPI::class.java).getTransDetail(RetrofitClientSmartPay.buildRequestBody(json)).dataConvert(TransDetailOnDetailPageBean::class.java)
            }
            mView.onQuerySuccess(result!!)
        }.await {
            it.printStackTrace()
            mView.onQueryFail(it.message!!)
        }
    }

    override fun sendEmail(email: String, orderNo: String) {
        log("sendEmail")
        mView.showLoading()
        launchRx {
            val basicAuth = JSONObject()
            putBasicAuth(mContext!!, basicAuth)

            val json = JSONObject()
            json["basicAuth"] = basicAuth
            json["email"] = email
            json["referenceId"] = orderNo

            com.wiseasy.smartpos.base.util.withIOContext {
                RetrofitClientSmartPay.create(ISmartPayAPI::class.java).sendEmail(RetrofitClientSmartPay.buildRequestBody(json)).dataConvert(SendEmailBean::class.java)
            }

            mView.onSendSuccess()

        }.await {
            it.printStackTrace()
            mView.onSendFail(it.message!!)
        }
    }

}