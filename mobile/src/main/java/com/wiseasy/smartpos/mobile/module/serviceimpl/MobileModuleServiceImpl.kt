package com.wiseasy.smartpos.mobile.module.serviceimpl

import android.content.Context
import android.content.Intent
import com.wiseasy.smartpos.comm.module.service.MobileModuleService
import com.wiseasy.smartpos.mobile.detail.TransactionDetailActivity
import com.wiseasy.smartpos.mobile.SmartPayActivity

/**
 * Created by Bella on 2021/12/20.
 */
class MobileModuleServiceImpl : MobileModuleService {

    override fun goToSmartPayActivity(context: Context) {
        context.startActivity(Intent(context, SmartPayActivity::class.java))
    }

    /**
     * 跳转到详情界面
     */
    override fun goToCodePayDetailActivity(context: Context, orderNo: String) {
        val intent = Intent(context, TransactionDetailActivity::class.java)
        intent.putExtra("orderNo", orderNo)
        context.startActivity(intent)
    }
}