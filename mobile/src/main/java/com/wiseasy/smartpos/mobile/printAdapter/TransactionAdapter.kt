package com.wiseasy.smartpos.mobile.printAdapter

import android.content.Context
import android.text.TextUtils
import com.wiseasy.android.utils.decimalFormat
import com.wiseasy.smartpos.base.printer.print.BaseAdapter
import com.wiseasy.smartpos.comm.bean.TransDetailBean
import com.wiseasy.smartpos.base.printer.print.PrintPage
import com.wiseasy.smartpos.base.printer.print.Row
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.base.util.DateUtil
import com.wiseasy.smartpos.comm.Constant
import com.wiseasy.smartpos.comm.module.service.LoginModuleService
import com.wiseasy.smartpos.comm.util.getPaymentMethodNameByPaymentMethod
import com.wiseasy.smartpos.comm.util.getResultByStatus
import com.wiseasy.smartpos.mobile.R
import java.util.ArrayList

/**
 * 打印交易
 */
class TransactionAdapter(context: Context, isRePrint: Boolean) : BaseAdapter<TransDetailBean?>() {
    private val TAG = "TransactionAdapter -> "
    private var mContext: Context = context
    private val mPageCount = 1
    private var mIsRePrint = isRePrint //是否是重打印

    private val PRINT_TYPE_ONE = 1
    private val PRINT_TYPE_TWO = 2
    private val PRINT_TYPE_REPRINT = 3

    override fun getPrintPage(transaction: TransDetailBean?): ArrayList<PrintPage> {
        val printPages = ArrayList<PrintPage>()
        when (mPageCount) {
            PRINT_TYPE_ONE -> printPages.add(createPage(transaction, 3))
            PRINT_TYPE_TWO -> {
                printPages.add(createPage(transaction, 1))
                printPages.add(createPage(transaction, 2))
            }
        }
        return printPages
    }

    override fun createPage(transaction: TransDetailBean?, printIndex: Int): PrintPage {
        val printPage = PrintPage()
        buildHead(printPage.rows)
        buildBody(printPage.rows, transaction!!)
        buildFoot(printPage.rows, printIndex)
        return printPage
    }

    /**
     * 打印头部
     */
    private fun buildHead(rows: ArrayList<Row>) {
        rows.add(Row.imageRow(R.mipmap.mobile_print_logo.toString(), Row.IMAGETYPE_LOGO))//图标
//        //地址
//        val address = Router.getInstance().getService(LoginModuleService::class.java)?.getMerchantInfo(mContext)?.storeAddress
//        rows.add(Row.textMediumNormalCenterChangeLineRow(address))
    }

    /**
     * 打印主要信息
     */
    private fun buildBody(rows: ArrayList<Row>, trans: TransDetailBean) {
        //小票号
        rows.add(Row.textMediumNormalLeftRow(getString(R.string.mobile_print_receipt_no)))
        rows.add(Row.textMediumBoldRightRow(trans.receiptNo))

        //Pay to (商户名称 + 商户地址)
        val merchantMappingInfo = Router.getInstance().getService(LoginModuleService::class.java)?.getMerchantInfo(mContext)
        val merchantName = merchantMappingInfo?.merchantName
        val merchantAddress = merchantMappingInfo?.merchantAddress
        rows.add(Row.textMediumNormalLeftRow(getString(R.string.mobile_print_pay_to)))
        rows.add(Row.textMediumBoldRightChangeLineRow("$merchantName  $merchantAddress"))

        //交易时间
        rows.add(Row.textMediumNormalLeftRow(getString(R.string.mobile_print_time)))
        rows.add(Row.textMediumBoldRightRow(trans.paymentTime))

        //备注
        rows.add(Row.textMediumNormalLeftRow(getString(R.string.mobile_print_note)))
        rows.add(Row.textMediumBoldRightRow(trans.desc))

        //交易号
        rows.add(Row.textMediumNormalLeftRow(getString(R.string.mobile_print_trans_id)))
        if (!TextUtils.isEmpty(trans.transId)) {
            rows.add(Row.textMediumBoldRightRow(trans.transId))
        }
        //交易方式
        rows.add(Row.textMediumNormalLeftRow(getString(R.string.mobile_print_payment_method)))
        rows.add(Row.textMediumBoldRightRow(trans.requestType))

        //订单号
        rows.add(Row.textMediumNormalLeftRow(getString(R.string.mobile_print_order_number)))
        rows.add(Row.textMediumBoldRightRow(trans.orderNo))

        //订单金额
        rows.add(Row.textMediumNormalLeftRow(getString(R.string.mobile_print_order_amount)))
        rows.add(Row.textMediumBoldRightRow(getAmount(trans.amount)))
        //小费
        rows.add(Row.textMediumNormalLeftRow(getString(R.string.mobile_print_trans_fee)))
        rows.add(Row.textMediumBoldRightRow(getAmount(trans.fee)))
        //优惠金额
        rows.add(Row.textMediumNormalLeftRow(getString(R.string.mobile_print_voucher)))
        rows.add(Row.textMediumBoldRightRow(getAmount(trans.voucherAmount)))
        //实付金额
        rows.add(Row.textMediumNormalLeftRow(getString(R.string.mobile_print_paid_amount)))
        rows.add(Row.textMediumBoldRightRow(getAmount(trans.paidAmount)))
        //交易状态
        rows.add(Row.textMediumNormalBothAligned(getString(R.string.mobile_print_status), getResultByStatus(mContext, trans.status)))
    }

    /**
     * 打印底部
     */
    private fun buildFoot(rows: ArrayList<Row>, pageCount: Int) {
//        rows.add(Row.repeatContentLine("-"))

        // 如果是重打印则再多打印一行“重打印”字样
        if (mIsRePrint) {
            rows.add(Row.textSmallNormalCenterRow(DateUtil.getZoneId() + " " + DateUtil.getCurDateStrByTimeZone("yyyy-MM-dd'T'HH:mm:ss", Constant.TOMEZONE)))
        }

        // 打印底部留白
        buildBottomWhiteSpace(rows, mContext)
    }

    private fun getString(stringId: Int): String {
        return mContext.getString(stringId) + ": "
    }

    private fun getAmount(amount: Int): String {
        return decimalFormat(amount.toString()) + Constant.UNIT
    }

}