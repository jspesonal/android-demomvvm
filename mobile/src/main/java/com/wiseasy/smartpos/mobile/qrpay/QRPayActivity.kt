package com.wiseasy.smartpos.mobile.qrpay

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.wiseasy.android.utils.decimalFormat
import com.wiseasy.smartpos.mobile.databinding.ActivityQrpayBinding
import com.wiseasy.smartpos.base.util.QRUtils
import com.wiseasy.smartpos.comm.Constant.UNIT
import com.wiseasy.smartpos.comm.bean.TransDetailBean
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.comm.view.CommonPopupWindow
import com.wiseasy.smartpos.mobile.R
import com.wiseasy.smartpos.mobile.result.PayResultActivity
import com.wiseasy.smartpos.mobile.view.QRPayPopupWindow

/**
 * 生成二维码界面
 */
class QRPayActivity : CommonActivity<ActivityQrpayBinding>(), QRPayContact.IView {

    private lateinit var mContext: Context
    private lateinit var mPresenter: QRPayPresenter
    private lateinit var mTrans: TransDetailBean
    private lateinit var mAmount: String
    private var mPopupWindow: CommonPopupWindow? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        initView()
    }

    override fun onDestroy() {
        mPopupWindow?.dismiss()
        mPresenter.onDestroy()
        super.onDestroy()
    }

    override fun getBinding(): ActivityQrpayBinding {
        return ActivityQrpayBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btn_cancel -> {
                finish()
            }
        }
    }

    override fun onActionBarRight() {
        mPopupWindow = QRPayPopupWindow(mContext)
        mPopupWindow?.show(mBinding.actionBarWhite)
    }

    override fun onQRCodeSuccess(qrCode: String) {
        log("onQRCodeSuccess|qrCode = $qrCode")
        hideLoading()
        val bitmap = QRUtils.createQrcode(qrCode, 370, 370)
        mBinding.ivQr.setImageBitmap(bitmap)
    }

    override fun onQRCodeFail(msg: String?) {
        log("onQRCodeFail|msg = $msg")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setTitle(msg)
            .setButton(getString(R.string.common_retry), getString(R.string.common_cancel))
            .setListener(object : CommonDialog.OnClickListener {
                override fun onConfirm() {
                    log("重试")
                    mPresenter.getQRCode(mTrans)
                }

                override fun onCancel() {
                    finish()
                }

            })
            .createAndShow()
    }

    override fun onQuerySuccess(trans: TransDetailBean) {
        log("onQuerySuccess")
        hideLoading()
        val intent = Intent(mContext, PayResultActivity::class.java)
        intent.putExtra("trans", trans)
        startActivity(intent)
        finish()
    }

    private fun initData() {
        mContext = this
        mPresenter = QRPayPresenter(mContext)
        mTrans = intent.getSerializableExtra("trans") as TransDetailBean
        mAmount = mTrans.amount.toString()
        mPresenter.getQRCode(mTrans)
    }

    private fun initView() {
        mBinding.tvAmount.text = "${decimalFormat(mAmount!!)}$UNIT"
    }

}