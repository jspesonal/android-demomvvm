package com.wiseasy.smartpos.mobile.qrpay

import com.wiseasy.smartpos.base.mvp.BaseIView
import com.wiseasy.smartpos.comm.bean.TransDetailBean

/**
 * @author Belle(Baiyunyan)
 * @date 2021/12/29
 */
interface QRPayContact {

    interface IView : BaseIView {

        /**
         * 获取二维码成功
         */
        fun onQRCodeSuccess(qrCode: String)

        /**
         * 获取二维码失败
         */
        fun onQRCodeFail(msg: String?)

        /**
         * 查询交易成功
         */
        fun onQuerySuccess(trans: TransDetailBean)

    }

    interface IPresenter {
        /**
         * 获取二维码
         */
        fun getQRCode(trans: TransDetailBean)

        /**
         * 查询交易详情
         */
        fun queryOrder(orderNo: String)
    }

}