package com.wiseasy.smartpos.mobile.qrpay

import android.content.Context
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.kotlin.withIOContext
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.*
import com.wiseasy.smartpos.comm.Constant
import com.wiseasy.smartpos.comm.Constant.TOMEZONE
import com.wiseasy.smartpos.comm.Constant.TRANS_STATUS_OPEN
import com.wiseasy.smartpos.comm.Constant.TRANS_STATUS_PAYED
import com.wiseasy.smartpos.comm.Constant.TRANS_STATUS_PAY_ERROR
import com.wiseasy.smartpos.comm.Constant.TRANS_STATUS_PROCESSING
import com.wiseasy.smartpos.comm.bean.QRPayBean
import com.wiseasy.smartpos.comm.bean.TransDetailBean
import com.wiseasy.smartpos.comm.http.smartpay.ISmartPayAPI
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.util.putBasicAuth
import com.wiseasy.smartpos.mobile.R

/**
 * @author Belle(Baiyunyan)
 * @date 2021/12/29
 */
class QRPayPresenter(context: Context) : BasePresenter(), QRPayContact.IPresenter {

    private var mView: QRPayContact.IView
    private var mRequestId: String = ""
    private var RETRY_TIME = 60//重试次数

    init {
        mContext = context
        mView = context as QRPayContact.IView
    }

    override fun getQRCode(trans: TransDetailBean) {
        log("getQRCode")
        RETRY_TIME = 60
        mView.showLoading()
        launchRx {
            val requestJson = JSONObject()
            val basicAuth = JSONObject()
            mContext?.let { putBasicAuth(it, basicAuth) }
            requestJson["basicAuth"] = basicAuth
            requestJson["channel"] = "WSP"
            requestJson["amount"] = trans.amount
            requestJson["requestType"] = "qrcode"
            requestJson["created"] = DateUtil.getCurDateStrByTimeZone("yyyy-MM-dd'T'HH:mm:ss", TOMEZONE)
//            requestJson["branchCode"] = "branchCode1"
            requestJson["desc"] = trans.desc

            var result: QRPayBean? = null

            withIOContext {
                result = RetrofitClientSmartPay.create(ISmartPayAPI::class.java).getQrCode(RetrofitClientSmartPay.buildRequestBody(requestJson)).dataConvert(QRPayBean::class.java)
                log("getQRCode|result = $result")
                mRequestId = RetrofitClientSmartPay.getRequestId()
                log("getQRCode|requestId = $mRequestId")
            }
            result?.payload?.let { mView.onQRCodeSuccess(it) }
            queryOrder(mRequestId)
        }.await {
            it.printStackTrace()
            log("生成二维码失败")
            mView.onQRCodeFail(it.message)
        }

    }

    /**
     * 查询交易详情
     */
    override fun queryOrder(orderNo: String) {
        log("queryOrder|orderNo = $orderNo")
        launchRx {
            val basicAuth = JSONObject()
            mContext?.let { putBasicAuth(it, basicAuth) }

            val json = JSONObject()
            json["basicAuth"] = basicAuth
            json["channel"] = "WSP"
            json["referenceId"] = orderNo
            json["createTime"] = DateUtil.getCurDateStrByTimeZone("yyyy-MM-dd'T'HH:mm:ss", Constant.TOMEZONE)

            var result: TransDetailBean? = null
            withIOContext(RETRY_TIME, 5000) {
                result = RetrofitClientSmartPay.create(ISmartPayAPI::class.java).queryTransStatus(RetrofitClientSmartPay.buildRequestBody(json)).dataConvert(TransDetailBean::class.java)
                log("还剩 ${--RETRY_TIME} 次机会重试")
                //如果查询到的结果是OPEN状态，且重试次数大于0，则触发重试机制
                if (result?.status == TRANS_STATUS_OPEN && RETRY_TIME > 0) {
                    throw Exception("")
                }
            }
            mView.onQuerySuccess(result!!)
        }.await {
            it.printStackTrace()
            mView.onQRCodeFail("${mContext?.getString(R.string.mobile_scan_query_fail)} \n ${it.message} \n (${mContext?.getString(R.string.mobile_scan_go_to_trans_list)})")
        }
    }

}