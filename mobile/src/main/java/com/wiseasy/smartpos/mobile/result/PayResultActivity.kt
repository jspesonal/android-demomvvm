package com.wiseasy.smartpos.mobile.result

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.bumptech.glide.Glide
import com.wiseasy.android.utils.decimalFormat
import com.wiseasy.smartpos.comm.view.CommonSendEmailDialog
import com.wiseasy.smartpos.mobile.databinding.ActivityPayResultBinding
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.base.speech.SpeakManager
import com.wiseasy.smartpos.comm.Constant
import com.wiseasy.smartpos.comm.Constant.UNIT
import com.wiseasy.smartpos.comm.bean.TransDetailBean
import com.wiseasy.smartpos.comm.module.service.LoginModuleService
import com.wiseasy.smartpos.comm.util.checkEmail
import com.wiseasy.smartpos.comm.util.getIconByStatusOnResultPage
import com.wiseasy.smartpos.comm.util.getResultByStatus
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.mobile.detail.TransactionDetailActivity
import com.wiseasy.smartpos.mobile.R

/**
 * 码付交易结果界面
 */
class PayResultActivity : CommonActivity<ActivityPayResultBinding>(), PayResultContract.IView {

    private lateinit var mContext: Context
    private var mSendEmailDialog: CommonSendEmailDialog? = null
    private lateinit var mPresenter: PayResultPresenter
    private lateinit var mTrans: TransDetailBean
    private var mEmail = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        initView()
    }

    override fun onDestroy() {
        mSendEmailDialog?.dismiss()
        super.onDestroy()
    }

    override fun getBinding(): ActivityPayResultBinding {
        return ActivityPayResultBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btn_detail, R.id.tv_detail -> {
                val intent = Intent(mContext, TransactionDetailActivity::class.java)
                intent.putExtra("orderNo", mTrans.orderNo)
                startActivity(intent)
                finish()
            }
            R.id.btn_email -> {
                mSendEmailDialog = CommonSendEmailDialog.Builder(this)
                    .setContent(getString(R.string.mobile_send_email))
                    .setListener(object : CommonSendEmailDialog.OnClickListener {
                        override fun onConfirm(email: String) {
                            if (checkEmail(email)) {
                                mSendEmailDialog?.dismiss()
                                mEmail = email
                                mPresenter.sendEmail(email, mTrans.orderNo!!)
                            } else {
                                mSendEmailDialog?.setErrorText(getString(R.string.mobile_email_error))
                            }

                        }

                        override fun onCancel() {
                        }

                    })
                    .createAndShow()
            }
            R.id.btn_back -> {
                Router.getInstance().getService(LoginModuleService::class.java)?.goToHomeActivity(mContext)
                finish()
            }
        }
    }

    override fun onSendSuccess() {
        log("onSendSuccess")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setLittleIcon(R.mipmap.mobile_result_success)
            .setTitle(getString(R.string.mobile_send_email_success))
            .setDescription("${getString(R.string.mobile_email_to)} \n $mEmail")
            .setButton(getString(R.string.common_ok))
            .createAndShow()
    }

    override fun onSendFail(msg: String) {
        log("onSendFail|msg = $msg")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setLittleIcon(R.mipmap.mobile_result_fail)
            .setTitle(getString(R.string.mobile_send_email_fail))
            .setDescription("${getString(R.string.mobile_email_to_error)} \n $mEmail \n \n ${getString(R.string.mobile_email_to_try_again)}")
            .setButton(getString(R.string.common_retry))
            .setExitIsShow(true)
            .setListener(object : CommonDialog.OnClickListener {
                override fun onConfirm() {
                    mPresenter.sendEmail(mEmail, mTrans.orderNo!!)
                }

                override fun onCancel() {
                }

            })
            .createAndShow()
    }

    override fun printSuccess() {
        log("printSuccess")
        hideLoading()
    }

    override fun printFail(msg: String) {
        log("printFail|msg = $msg")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setTitle(getString(R.string.print_fail))
            .setDescription(msg)
            .setButton(getString(R.string.common_retry), getString(R.string.common_cancel))
            .setListener(object : CommonDialog.OnClickListener {
                override fun onConfirm() {
                    mPresenter.print(mTrans)
                }

                override fun onCancel() {
                }

            })
            .createAndShow()
    }

    private fun initData() {
        mContext = this
        mPresenter = PayResultPresenter(mContext)
        mTrans = intent.getSerializableExtra("trans") as TransDetailBean
    }

    private fun initView() {
        mSystemBar.setStatusBarColor(0x00000000)
        //全屏
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        showPayResult()
    }

    private fun showPayResult() {
        mBinding.ivResult.setImageDrawable(getIconByStatusOnResultPage(mContext, mTrans.status!!))
        mBinding.tvResult.text = getResultByStatus(mContext, mTrans.status)
        Glide.with(mContext).load(mTrans.customerInfo?.logo).into(mBinding.ivCustomerIcon)
        mBinding.tvAmount.text = decimalFormat(mTrans.amount.toString()) + UNIT
        mBinding.tvCustomerName.text = mTrans.customerInfo?.name
        mBinding.tvCustomerPhone.text = mTrans.customerInfo?.phone

        mBinding.tvTime.text = mTrans.paymentTime

        when (mTrans.status) {
            Constant.TRANS_STATUS_PROCESSING, Constant.TRANS_STATUS_PAY_ERROR, Constant.TRANS_STATUS_OPEN -> {
                mBinding.llDetail.visibility = View.GONE
                mBinding.tvDetail.visibility = View.VISIBLE
            }
            Constant.TRANS_STATUS_PAYED -> {
                mBinding.llDetail.visibility = View.VISIBLE
                mBinding.tvDetail.visibility = View.GONE
                mPresenter.print(mTrans)
                speak()
            }
        }
    }

    /**
     * 语音播报
     */
    private fun speak() {
        log("speak")
        val speakContent = getString(R.string.mobile_pay_speak_smart_pay) + String.format(getString(R.string.mobile_pay_speak_collection), "${mTrans.amount} $UNIT")
        SpeakManager.getInstance().speak(speakContent)
    }

}