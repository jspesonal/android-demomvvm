package com.wiseasy.smartpos.mobile.result

import com.wiseasy.smartpos.base.mvp.BaseIView
import com.wiseasy.smartpos.comm.bean.TransDetailBean

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/04
 */
interface PayResultContract {

    interface IView : BaseIView {

        /**
         * 发送短信成功
         */
        fun onSendSuccess()

        /**
         * 发送短信失败
         */
        fun onSendFail(msg: String)

        /**
         * 打印成功
         */

        fun printSuccess()

        /**
         * 打印失败
         */
        fun printFail(msg: String)

    }

    interface IPresenter {
        /**
         * 发送短信
         */
        fun sendEmail(email: String, orderNo: String)

        /**
         * 打印
         */
        fun print(trans: TransDetailBean)
    }

}