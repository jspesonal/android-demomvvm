package com.wiseasy.smartpos.mobile.result

import android.content.Context
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.printer.PrinterManager
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.base.util.withIOContext
import com.wiseasy.smartpos.comm.bean.SendEmailBean
import com.wiseasy.smartpos.comm.bean.TransDetailBean
import com.wiseasy.smartpos.comm.http.smartpay.ISmartPayAPI
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.util.putBasicAuth
import com.wiseasy.smartpos.mobile.R
import com.wiseasy.smartpos.mobile.printAdapter.TransactionAdapter

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/04
 */
class PayResultPresenter(context: Context) : BasePresenter(), PayResultContract.IPresenter {

    private var mView: PayResultContract.IView
    private var mRequestId: String? = null

    init {
        mContext = context
        mView = context as PayResultContract.IView
    }

    override fun sendEmail(email: String, orderNo: String) {
        log("sendEmail")
        mView.showLoading()
        launchRx {
            val basicAuth = JSONObject()
            putBasicAuth(mContext!!, basicAuth)

            val json = JSONObject()
            json["basicAuth"] = basicAuth
            json["email"] = email
            json["referenceId"] = orderNo

            withIOContext {
                RetrofitClientSmartPay.create(ISmartPayAPI::class.java).sendEmail(RetrofitClientSmartPay.buildRequestBody(json)).dataConvert(SendEmailBean::class.java)
            }

            mView.onSendSuccess()

        }.await {
            it.printStackTrace()
            mView.onSendFail(it.message!!)
        }
    }

    override fun print(trans: TransDetailBean) {
        log("print")
        if (PrinterManager.getInstance().isNeedPrint(mContext)) {
            log("print|开始打印")
            mView.showLoading(mContext?.getString(R.string.print_ing), false)
            PrinterManager.getInstance().print(mContext, trans, TransactionAdapter(mContext!!, false)) { e ->
                log("print|event")
                if (e != null) {
                    mView.printFail(e.msg)
                } else {
                    mView.printSuccess()
                }
            }
        } else {
            log("print|未配置打印机")
        }
    }

}