package com.wiseasy.smartpos.mobile.scan

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.MediaPlayer.OnCompletionListener
import android.os.Build
import android.os.Bundle
import android.os.Vibrator
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.wiseasy.smartpos.base.scanner.DecodeListener
import com.wiseasy.smartpos.base.scanner.ScannerComponent
import com.wiseasy.smartpos.mobile.databinding.ActivityScanPayBinding
import com.wiseasy.smartpos.base.view.BaseDialog
import com.wiseasy.smartpos.comm.bean.TransDetailBean
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.mobile.result.PayResultActivity
import com.wiseasy.smartpos.mobile.R
import java.io.IOException

/**
 * 扫码界面
 */
class ScanPayActivity : CommonActivity<ActivityScanPayBinding>(), DecodeListener, ScanPayContract.IView {

    private val BEEP_VOLUME = 0.50f //音量
    private val VIBRATE_DURATION = 200L //震动时间

    private var scannerComponent: ScannerComponent? = null//扫码组件
    private var mediaPlayer: MediaPlayer? = null//媒体播放器
    private var playBeep = false //是否蜂鸣
    private var vibrate = false //是否震动

    private lateinit var mContext: Context
    private lateinit var mPresenter: ScanPayPresenter
    private lateinit var mTrans: TransDetailBean

    private val beepListener = OnCompletionListener { mediaPlayer!!.seekTo(0) } //蜂鸣监听

    //view
    private val dialog: BaseDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        initView()
        initScanner()
    }

    override fun getBinding(): ActivityScanPayBinding {
        return ActivityScanPayBinding.inflate(layoutInflater)
    }

    override fun onResume() {
        super.onResume()
        if (scannerComponent != null) {
            if (mBinding.svScan.visibility == View.GONE) {
                mBinding.svScan.visibility = View.VISIBLE
            }
            scannerComponent?.initCamera()
        }
    }

    override fun onStart() {
        super.onStart()
        scannerComponent!!.onStart(this)
        scannerComponent!!.startDecode()
    }

    override fun onPause() {
        super.onPause()
        if (scannerComponent != null) {
            if (mBinding.svScan.visibility == View.VISIBLE) {
                mBinding.svScan.visibility = View.GONE
            }
            scannerComponent?.releaseCamera()
        }
    }

    override fun onStop() {
        super.onStop()
        scannerComponent!!.onStop(this)
    }

    override fun onDestroy() {
        dialog?.dismiss()
        scannerComponent!!.onDestroy(this)
        mPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.iv_back -> {
                finish()
            }
            R.id.iv_scan_small -> {
//                startActivity(Intent(this, PayResultActivity::class.java))
            }
        }
    }

    override fun onDecode(result: String) {
        var str = result
        if (str.contains(",")) {
            val resultArray = result.split(",").toTypedArray()
            str = resultArray[0]
        }
        playBeepSoundAndVibrate()
        mBinding.scannerAnimView.lineAnimationEnabled = false
        //如果是空，重新解码，如果是非空，解码成功，向后传递
        if (TextUtils.isEmpty(str)) {
            scannerComponent!!.startDecode()
        } else {
            Log.i(TAG, "扫到的二维码为：$str")
            mTrans.qrcode = str
            mPresenter.scanPay(mTrans)
        }
    }

    /**
     * 支付成功
     */
    override fun onQuerySuccess(result: TransDetailBean) {
        log("onPaySuccess")
        hideLoading()
        val intent = Intent(mContext, PayResultActivity::class.java)
        intent.putExtra("trans", result)
        startActivity(intent)
        finish()
    }

    /**
     * 支付失败
     */
    override fun onPayError(msg: String?) {
        log("onPayFail|msg = $msg")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setTitle(msg)
            .setButton(getString(R.string.common_retry), getString(R.string.common_cancel))
            .setListener(object : CommonDialog.OnClickListener {
                override fun onConfirm() {
                    log("重试")
                    scannerComponent!!.onStart(mContext)
                    scannerComponent!!.startDecode()
                }

                override fun onCancel() {
                    finish()
                }

            })
            .createAndShow()
    }

    private fun initData() {
        mContext = this
        mPresenter = ScanPayPresenter(mContext)
        mTrans = intent.getSerializableExtra("trans") as TransDetailBean
    }

    private fun initView() {
        //状态栏字体颜色给为浅色
        val decorViewGroup = window.decorView as ViewGroup
        val option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        decorViewGroup.systemUiVisibility = option
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.TRANSPARENT
        }
    }

    private fun initScanner() {
        mBinding.svScan.visibility = View.VISIBLE
        scannerComponent = ScannerComponent(mBinding.svScan, this)
        setScreenBrightness()
        initMedia()
    }

    /**
     * 初始化系统媒体方面的设置
     */
    private fun initMedia() {
        playBeep = true
        val audioService = getSystemService(AUDIO_SERVICE) as AudioManager
        if (audioService.ringerMode != AudioManager.RINGER_MODE_NORMAL) {
            playBeep = false
        }
        initBeepSound()
        vibrate = true
    }

    /**
     * 初始化蜂鸣器
     */
    private fun initBeepSound() {
        if (playBeep && mediaPlayer == null) {
            volumeControlStream = AudioManager.STREAM_MUSIC
            mediaPlayer = MediaPlayer()
            mediaPlayer?.setAudioStreamType(AudioManager.STREAM_MUSIC)
            mediaPlayer?.setOnCompletionListener(beepListener)
            val file = resources.openRawResourceFd(R.raw.beep)
            try {
                mediaPlayer?.setDataSource(file.fileDescriptor, file.startOffset, file.length)
                file.close()
                mediaPlayer?.setVolume(BEEP_VOLUME, BEEP_VOLUME)
                mediaPlayer?.prepare()
            } catch (e: IOException) {
                mediaPlayer = null
            }
        }
    }

    /**
     * 屏幕亮度
     */
    private fun setScreenBrightness() {
        val lp = window.attributes
        lp.screenBrightness = 1.0f
        window.attributes = lp
    }

    /**
     * 响蜂鸣器
     */
    private fun playBeepSoundAndVibrate() {
        if (playBeep && mediaPlayer != null) {
            mediaPlayer?.start()
        }
        if (vibrate) {
            val vibrator = getSystemService(VIBRATOR_SERVICE) as Vibrator
            vibrator.vibrate(VIBRATE_DURATION)
        }
    }

}