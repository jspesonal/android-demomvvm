package com.wiseasy.smartpos.mobile.scan

import com.wiseasy.smartpos.base.mvp.BaseIView
import com.wiseasy.smartpos.comm.bean.TransDetailBean

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/06
 */
interface ScanPayContract {

    interface IView : BaseIView {
        /**
         * 查询交易成功
         */
        fun onQuerySuccess(result: TransDetailBean)

        /**
         * 交易异常
         */
        fun onPayError(msg: String?)

    }

    interface IPresenter {
        /**
         * 扫码接口
         */
        fun scanPay(trans: TransDetailBean)

        /**
         * 查询交易详情
         */
        fun queryOrder(orderNo: String)
    }

}