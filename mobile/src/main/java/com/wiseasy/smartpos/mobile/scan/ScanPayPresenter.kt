package com.wiseasy.smartpos.mobile.scan

import android.content.Context
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.DateUtil
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.base.util.withIOContext
import com.wiseasy.smartpos.comm.Constant
import com.wiseasy.smartpos.comm.bean.ScanPayBean
import com.wiseasy.smartpos.comm.bean.TransDetailBean
import com.wiseasy.smartpos.comm.http.smartpay.ISmartPayAPI
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.util.putBasicAuth
import com.wiseasy.smartpos.mobile.R

/**
 * @author Belle(Baiyunyan)
 * @date 2022/01/06
 */
class ScanPayPresenter(context: Context) : BasePresenter(), ScanPayContract.IPresenter {

    private var mView: ScanPayContract.IView
    private var mRequestId: String? = null
    private var RETRY_TIME = 3//重试次数

    init {
        mContext = context
        mView = context as ScanPayContract.IView
    }

    override fun scanPay(trans: TransDetailBean) {
        log("quickPay")
        mView.showLoading()
        RETRY_TIME = 3
        launchRx {
            val basicAuth = JSONObject()
            mContext?.let { putBasicAuth(it, basicAuth) }

            val json = JSONObject()
            json["basicAuth"] = basicAuth
            json["channel"] = "WSP"
            json["authCode"] = trans.qrcode
            json["amount"] = trans.amount
            json["requestType"] = "quickpay"
            json["desc"] = trans.desc
            json["created"] = DateUtil.getCurDateStrByTimeZone("yyyy-MM-dd'T'HH:mm:ss", Constant.TOMEZONE)

            withIOContext {
                RetrofitClientSmartPay.create(ISmartPayAPI::class.java).quickPay(RetrofitClientSmartPay.buildRequestBody(json)).dataConvert(ScanPayBean::class.java)
            }
            mRequestId = RetrofitClientSmartPay.getRequestId()
            queryOrder(mRequestId!!)
        }.await {
            it.printStackTrace()
            mView.onPayError(it.message)
        }
    }

    override fun queryOrder(orderNo: String) {
        log("queryOrder|orderNo = $orderNo")
        mView.showLoading()
        launchRx {
            val basicAuth = JSONObject()
            mContext?.let { putBasicAuth(it, basicAuth) }

            val json = JSONObject()
            json["basicAuth"] = basicAuth
            json["channel"] = "WSP"
            json["referenceId"] = orderNo
            json["createTime"] = DateUtil.getCurDateStrByTimeZone("yyyy-MM-dd'T'HH:mm:ss", Constant.TOMEZONE)

            var result: TransDetailBean? = null
            withIOContext(RETRY_TIME, 3000) {
                result = RetrofitClientSmartPay.create(ISmartPayAPI::class.java).queryTransStatus(RetrofitClientSmartPay.buildRequestBody(json)).dataConvert(TransDetailBean::class.java)
                log("还剩 ${--RETRY_TIME} 次机会重试")
                //如果查询到的结果是OPEN状态，且重试次数大于0，则触发重试机制
                if (result?.status == Constant.TRANS_STATUS_OPEN && RETRY_TIME > 0) {
                    throw Exception("")
                }
            }
            mView.onQuerySuccess(result!!)
        }.await {
            it.printStackTrace()
            mView.onPayError("${mContext?.getString(R.string.mobile_scan_query_fail)} \n ${it.message} \n (${mContext?.getString(R.string.mobile_scan_go_to_trans_list)})")
        }
    }

}