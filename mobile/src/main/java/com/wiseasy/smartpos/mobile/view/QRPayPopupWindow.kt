package com.wiseasy.smartpos.mobile.view

import android.content.Context
import android.view.LayoutInflater
import com.wiseasy.smartpos.comm.view.CommonPopupWindow
import com.wiseasy.smartpos.mobile.databinding.PopupWindowQrPayBinding

/**
 * 二维码支付输入金额界面流程弹窗
 * @author Belle(Baiyunyan)
 * @date 2022/01/11
 */
class QRPayPopupWindow(context: Context) : CommonPopupWindow(context) {

    init {
        val mBinding: PopupWindowQrPayBinding = PopupWindowQrPayBinding.inflate(LayoutInflater.from(context))
        this.contentView = mBinding.root
    }

}