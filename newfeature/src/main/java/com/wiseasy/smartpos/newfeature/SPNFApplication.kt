package com.wiseasy.smartpos.newfeature

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import com.wiseasy.smartpos.newfeature.di.DaggerSPNFComponent
import com.wiseasy.smartpos.newfeature.module.service.TopUpServiceImpl
import com.wiseasy.smartpos.base.BaseApplication
import com.wiseasy.smartpos.base.IApplicationLike
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.comm.module.service.TopUpModuleService
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class SPNFApplication(application: Application): BaseApplication(), HasAndroidInjector {


    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun androidInjector(): AndroidInjector<Any>? = dispatchingAndroidInjector as AndroidInjector<Any>

    private var mContext: Context = application.applicationContext
    private var mApplication: Application = application

    companion object {
        @SuppressLint("StaticFieldLeak")
        private lateinit var instance: SPNFApplication
        fun create(application: Application) {
            instance = SPNFApplication(application)
        }
        fun getInstance():SPNFApplication = instance
    }

    init {
        initDaggerComponent()
    }

    fun getMainApplication(): Application {
        return mApplication
    }

    fun getContext(): Context {
        return mContext
    }

    private fun initDaggerComponent() {
        DaggerSPNFComponent.factory().create(this).inject(this)

    }
}