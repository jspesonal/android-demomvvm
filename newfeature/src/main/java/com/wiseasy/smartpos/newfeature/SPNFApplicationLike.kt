package com.wiseasy.smartpos.newfeature

import android.app.Application
import com.wiseasy.smartpos.base.IApplicationLike
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.comm.module.service.TopUpModuleService
import com.wiseasy.smartpos.newfeature.module.service.TopUpServiceImpl

class SPNFApplicationLike: IApplicationLike {

    private val router = Router.getInstance()

    override fun init(application: Application) {
        router.addService(TopUpModuleService::class.java, TopUpServiceImpl(application))
    }
}