package com.wiseasy.smartpos.newfeature.binding

import android.view.View
import androidx.databinding.BindingAdapter

/**
 * Simplification to check and setup view as visible.
 */
@set:BindingAdapter("visible")
var View.visible
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

/**
 * Simplification to check and setup view as gone.
 */
@set:BindingAdapter("gone")
var View.gone
    get() = visibility == View.GONE
    set(value) {
        visibility = if (value) View.GONE else View.VISIBLE
    }

/**
 * Simplification to check and setup view as invisible.
 */
@set:BindingAdapter("invisible")
var View.invisible
    get() = visibility == View.INVISIBLE
    set(value) {
        visibility = if (value) View.INVISIBLE else View.VISIBLE
    }

@BindingAdapter("custom_width")
fun setLayoutWidth(view: View, width: Float) {
    view.layoutParams = view.layoutParams.apply { this.width = width.toInt() }
}

@BindingAdapter("android:onClick")
fun View.setOnClick(clickListener: View.OnClickListener?) {
    setOnClickListener {
        isClickable = false
        postDelayed({ isClickable = true }, 1000)
        clickListener?.onClick(this)
    }
}