package com.wiseasy.smartpos.newfeature.binding

import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.databinding.BindingAdapter
import com.wiseasy.smartpos.newfeature.extentions.getTextInput
import com.wiseasy.smartpos.newfeature.extentions.hideKeyboard

@BindingAdapter("onEditorEnterAction") // I like it to match the listener method name
fun EditText.onEditorEnterAction(function: Function1<String, Unit>) {
    setOnEditorActionListener { _, actionId, _ ->
        when (actionId) {
            EditorInfo.IME_ACTION_DONE,
            EditorInfo.IME_ACTION_SEND,
            EditorInfo.IME_ACTION_SEARCH,
            EditorInfo.IME_ACTION_GO -> function(this.getTextInput())
            else -> Unit
        }
        true
    }
}

@BindingAdapter("hideKeyboardWhenEnterAction") // I like it to match the listener method name
fun EditText.hideKeyboardWhenEnterAction(isHide: Boolean) {
    if (isHide) {
        onEditorEnterAction {
            clearFocus()
            hideKeyboard()
        }
    }
}