/*
 * Copyright 2019 vmadalin.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wiseasy.smartpos.newfeature.binding

import android.graphics.drawable.Drawable
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wiseasy.smartpos.newfeature.ui.widget.CustomDividerItemDecoration
import com.wiseasy.smartpos.newfeature.ui.widget.RecyclerViewItemDecoration

/**
 * Add an [RecyclerViewItemDecoration] to this RecyclerView. Item decorations can
 * affect both measurement and drawing of individual item views.
 *
 * @param spacingPx Spacing in pixels to set as a item margin.
 * @see RecyclerView.addItemDecoration
 */
@BindingAdapter("itemDecorationSpacing")
fun RecyclerView.setItemDecorationSpacing(spacingPx: Float) {
    addItemDecoration(
        RecyclerViewItemDecoration(spacingPx.toInt())
    )
}


@BindingAdapter("dividerDrawable", "vertical", "hideWithLastItem", requireAll = true)
fun RecyclerView.setDividerDrawable(
    drawable: Drawable,
    vertical: Boolean,
    hideWithLastItem: Boolean
) {
    val orientation = if (vertical) {
        LinearLayoutManager.VERTICAL
    } else {
        LinearLayoutManager.HORIZONTAL
    }
    val dividerItemDecoration = CustomDividerItemDecoration(
        orientation,
        hideWithLastItem
    ).apply { divider = drawable }
    addItemDecoration(dividerItemDecoration)
}