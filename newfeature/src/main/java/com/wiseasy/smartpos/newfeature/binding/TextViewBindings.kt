package com.wiseasy.smartpos.newfeature.binding

import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.databinding.BindingAdapter

@BindingAdapter("textHtml")
fun TextView.showTextHtml(text: String?) {
    setText(HtmlCompat.fromHtml(text?: "", 0))
}