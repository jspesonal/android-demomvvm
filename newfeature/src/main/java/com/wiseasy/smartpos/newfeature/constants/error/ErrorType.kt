package com.wiseasy.smartpos.newfeature.constants.error

object ErrorType {
    const val ERROR_DEFAULT = "error.default"
    const val ERROR_EXCEPTION = "error.exception"
    const val ERROR_NETWORK = "error.network"
    const val ERROR_UNKNOWN = "error.unknown"
    const val ERROR_NOT_FOUND = "error.not.found"
    const val ERROR_UNAUTHORIZED = "error.unauthorized"
}
