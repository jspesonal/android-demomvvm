package com.wiseasy.smartpos.newfeature.constants.http

object HttpHeader {
    const val HEADER_AUTHORIZATION = "Authorization"
    const val HEADER_ACCEPT_LANGUAGE = "Accept-Language"
    const val HEADER_ETAG = "ETag"
}