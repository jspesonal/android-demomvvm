package com.wiseasy.smartpos.newfeature.constants.http

object HttpStatusCode {
    const val NOT_MODIFIED = 304
    const val NOT_FOUND = 404
    const val UNAUTHORIZED = 401
}