package com.wiseasy.smartpos.newfeature.data.model

import com.google.gson.annotations.SerializedName
import com.wiseasy.smartpos.newfeature.constants.error.ErrorType

data class ErrorResponse (
    val type: String? = ErrorType.ERROR_DEFAULT,
    @SerializedName("status")
    val code: Int? = null, // code of http or status of server
    val error: String)
