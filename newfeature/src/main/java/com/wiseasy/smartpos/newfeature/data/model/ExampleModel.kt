package com.wiseasy.smartpos.newfeature.data.model

data class ExampleModel(
    val list: List<Exhibit>
)

data class Exhibit(

    val images: List<String>,
    val title: String
)