package com.wiseasy.smartpos.newfeature.data.reposity

import com.wiseasy.smartpos.newfeature.data.model.ExampleModel


interface ExampleRepository {

    suspend fun callApiExample(): ExampleModel
}