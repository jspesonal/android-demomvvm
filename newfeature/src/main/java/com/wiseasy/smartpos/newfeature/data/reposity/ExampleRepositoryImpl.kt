package com.wiseasy.smartpos.newfeature.data.reposity

import com.wiseasy.smartpos.newfeature.data.model.ExampleModel
import javax.inject.Inject

class ExampleRepositoryImpl @Inject constructor(private val service: ExampleService): ExampleRepository{

    override suspend fun callApiExample(): ExampleModel {
        return service.callApiExample()
    }
}