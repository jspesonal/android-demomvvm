package com.wiseasy.smartpos.newfeature.data.reposity

import com.wiseasy.smartpos.newfeature.data.model.ExampleModel
import retrofit2.http.GET

interface ExampleService {
    @GET("movielist.json")
    suspend fun callApiExample(): ExampleModel
}