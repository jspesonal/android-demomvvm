package com.wiseasy.smartpos.newfeature.di

import android.app.Application
import android.content.Context
import com.wiseasy.smartpos.newfeature.SPNFApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule() {

    @Provides
    @Singleton
    fun provideApplication(spApp: SPNFApplication): Application {
        return spApp.getMainApplication()
    }

    @Provides
    @Singleton
    fun provideApplicationContext(app: SPNFApplication): Context {
        return app.getContext()
    }
}