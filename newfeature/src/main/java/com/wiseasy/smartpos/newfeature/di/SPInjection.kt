package com.wiseasy.smartpos.newfeature.di

import android.app.Activity
import androidx.fragment.app.Fragment
import com.wiseasy.smartpos.newfeature.SPNFApplication
import dagger.android.HasAndroidInjector
import dagger.internal.Preconditions

class SPInjection {

    companion object {
        fun inject(activity: Activity) {
            Preconditions.checkNotNull(activity, "activity")
            inject(activity, SPNFApplication.getInstance())
        }

        fun inject(fragment: Fragment) {
            Preconditions.checkNotNull(fragment, "fragment")
            val hasAndroidInjector = findHasAndroidInjectorForFragment(fragment)
            inject(fragment, hasAndroidInjector)
        }


        private fun findHasAndroidInjectorForFragment(fragment: Fragment?): HasAndroidInjector {
            var parentFragment = fragment
            while (parentFragment?.parentFragment.also { parentFragment = it } != null) {
                if (parentFragment is HasAndroidInjector) {
                    return parentFragment as HasAndroidInjector
                }
            }
            val activity: Activity? = fragment?.activity
            return if (activity is HasAndroidInjector) {
                activity
            } else SPNFApplication.getInstance()
        }

        private fun inject(target: Any, hasAndroidInjector: HasAndroidInjector) {
            val androidInjector = hasAndroidInjector.androidInjector()
            Preconditions.checkNotNull(
                androidInjector, "%s.androidInjector() returned null", hasAndroidInjector.javaClass
            )
            androidInjector.inject(target)
        }
    }
}