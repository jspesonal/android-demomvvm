package com.wiseasy.smartpos.newfeature.di

import com.wiseasy.smartpos.newfeature.SPNFApplication
import com.wiseasy.smartpos.newfeature.di.module.ActivityBindingModule
import com.wiseasy.smartpos.newfeature.di.network.modules.APIModule
import com.wiseasy.smartpos.newfeature.di.network.modules.DataModule
import com.wiseasy.smartpos.newfeature.di.network.modules.NetworkModule
import com.wiseasy.smartpos.newfeature.di.viewmodel.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(modules = [
    AndroidSupportInjectionModule::class,
    ViewModelModule::class,
    AppModule::class,
    APIModule::class,
    DataModule::class,
    NetworkModule::class,
    ActivityBindingModule::class,
])
@Singleton
interface SPNFComponent: AndroidInjector<SPNFApplication> {

    @Component.Factory
    abstract class SPFactory : AndroidInjector.Factory<SPNFApplication>

}

