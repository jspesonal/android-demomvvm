package com.wiseasy.smartpos.newfeature.di.module

import com.wiseasy.smartpos.newfeature.ui.screen.topup.TopUpActivity
import com.wiseasy.smartpos.newfeature.di.scope.ActivityScope
import com.wiseasy.smartpos.newfeature.ui.screen.topup.TopUpModule
import com.wiseasy.smartpos.newfeature.ui.screen.topup.landingpage.BillPaymentServiceModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [
        TopUpModule::class,
        BillPaymentServiceModule::class
    ])
    abstract fun providerTopUpActivity(): TopUpActivity
}