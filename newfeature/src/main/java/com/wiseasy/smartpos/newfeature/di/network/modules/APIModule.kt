package com.wiseasy.smartpos.newfeature.di.network.modules

import com.wiseasy.smartpos.newfeature.di.Qualifiers
import com.wiseasy.smartpos.newfeature.retrofit.AuthenticatorInterceptor
import com.wiseasy.smartpos.newfeature.retrofit.NullOrEmptyConverterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class APIModule() {

    @Singleton
    @Provides
    @Named(Qualifiers.SP_SERVICE)
    fun provideOKHttpClient(authenticatorInterceptor: AuthenticatorInterceptor, loggingInterceptor: HttpLoggingInterceptor): OkHttpClient{
        return  OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(authenticatorInterceptor)
            .readTimeout(1200,TimeUnit.SECONDS)
            .connectTimeout(1200, TimeUnit.SECONDS)
            .build()
    }

    @Singleton
    @Provides
    fun provideGSON(): GsonConverterFactory {
        return  GsonConverterFactory.create()
    }

    @Singleton
    @Provides
    fun provideLogging(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

    @Singleton
    @Provides
    fun provideAuthenticator(): AuthenticatorInterceptor {
        return AuthenticatorInterceptor()
    }

    @Singleton
    @Provides
    @Named(Qualifiers.SP_SERVICE)
    fun provideRetrofit(gsonConverterFactory: GsonConverterFactory, @Named(Qualifiers.SP_SERVICE) okHttpClient: OkHttpClient): Retrofit{
        return Retrofit.Builder()
            .baseUrl("https://howtodoandroid.com/")
            .addConverterFactory(NullOrEmptyConverterFactory().converterFactory())
            .addConverterFactory(gsonConverterFactory)
            .client(okHttpClient)
            .build()
    }

}