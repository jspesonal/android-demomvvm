package com.wiseasy.smartpos.newfeature.di.network.modules

import com.wiseasy.smartpos.newfeature.data.reposity.ExampleRepository
import com.wiseasy.smartpos.newfeature.data.reposity.ExampleRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module(includes = [APIModule::class])
abstract class DataModule {

    @Binds
    @Singleton
    abstract fun bindExampleRepository(repo: ExampleRepositoryImpl): ExampleRepository

}