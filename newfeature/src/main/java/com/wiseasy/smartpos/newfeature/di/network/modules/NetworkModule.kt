package com.wiseasy.smartpos.newfeature.di.network.modules

import com.wiseasy.smartpos.newfeature.data.reposity.ExampleService
import com.wiseasy.smartpos.newfeature.di.Qualifiers
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideExampleApiService(@Named(Qualifiers.SP_SERVICE) retrofit: Retrofit): ExampleService {
        return retrofit.create(ExampleService::class.java)
    }

}