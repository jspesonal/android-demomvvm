package com.wiseasy.smartpos.newfeature.di.scope

import java.lang.annotation.RetentionPolicy
import javax.inject.Scope
import java.lang.annotation.Documented
import java.lang.annotation.Retention

@Scope
@Documented
@Retention(value = RetentionPolicy.RUNTIME)
annotation class ChildFragmentScope
