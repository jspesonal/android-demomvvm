package com.wiseasy.smartpos.newfeature.domain

import com.wiseasy.smartpos.newfeature.data.model.ExampleModel
import com.wiseasy.smartpos.newfeature.data.reposity.ExampleRepository
import javax.inject.Inject

class ExampleUseCase @Inject constructor(private val repo: ExampleRepository) :
    SingleUseCase<ExampleUseCase.Params, ExampleModel>() {



     class Params(private val number: Int) {

    }

    override suspend fun execute(param: Params): ExampleModel {
        return repo.callApiExample()
    }


}