package com.wiseasy.smartpos.newfeature.domain

abstract class SingleUseCase<P, R> {
    abstract suspend fun execute(param: P): R
}