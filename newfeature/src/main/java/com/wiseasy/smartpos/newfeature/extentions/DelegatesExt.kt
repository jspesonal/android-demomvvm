package com.wiseasy.smartpos.newfeature.extentions

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * @author Hugo
 * @date 2021/10/4
 */
object DelegatesExt {
    fun <T> notNullSingValue() = NotNullSingleValueVar<T>()
}

class NotNullSingleValueVar<T> : ReadWriteProperty<Any?, T> {
    override fun getValue(thisRef: Any?, property: KProperty<*>): T =
        value ?: throw IllegalStateException("${property.name} not initialized")

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this.value = if (this.value == null) value
        else throw IllegalStateException("${property.name} already initialized")
    }

    private var value: T? = null
}
