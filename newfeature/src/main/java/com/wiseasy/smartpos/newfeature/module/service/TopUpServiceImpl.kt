package com.wiseasy.smartpos.newfeature.module.service

import android.app.Application
import android.content.Context
import android.content.Intent
import com.wiseasy.smartpos.newfeature.ui.screen.topup.TopUpActivity
import com.wiseasy.smartpos.comm.module.service.TopUpModuleService
import com.wiseasy.smartpos.newfeature.SPNFApplication

class TopUpServiceImpl(application: Application): TopUpModuleService {

    init {
        SPNFApplication.create(application)
    }

    override fun goToTopUpActivity(context: Context) {
        context.startActivity(Intent(context, TopUpActivity::class.java))
    }
}