package com.wiseasy.smartpos.newfeature.retrofit

import okhttp3.Interceptor
import okhttp3.Response

class AuthenticatorInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val url = original.url().newBuilder().build()

        val requestBuilder = original.newBuilder().url(url)
        val token = ""
        if(!token.isNullOrEmpty()) {
            requestBuilder.addHeader("Authorization", token)
        }

        val language = ""
        if(language.isNotEmpty()) {
            requestBuilder.addHeader("Accept-Language", language)
        }
        requestBuilder.addHeader("Content-Type", "application/json")
        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}