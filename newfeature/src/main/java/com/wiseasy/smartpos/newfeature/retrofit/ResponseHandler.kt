package com.wiseasy.smartpos.newfeature.retrofit

import android.content.Context
import com.google.gson.Gson
import com.wiseasy.smartpos.newfeature.constants.error.ErrorType
import com.wiseasy.smartpos.newfeature.constants.http.HttpStatusCode
import com.wiseasy.smartpos.newfeature.data.model.ErrorResponse
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException

object ResponseHandler {

    fun getError(context: Context, throwable: Throwable): ErrorResponse {
        //TODO
        val defaultError = ""
        return when(throwable) {
            is HttpException -> {
                when (throwable.code()) {
                    HttpStatusCode.UNAUTHORIZED -> {
                        ErrorResponse(
                            ErrorType.ERROR_UNAUTHORIZED,
                            throwable.code(),
                            getResponseMessage(throwable) ?: defaultError
                        )
                    }
                    else -> {
                        var errorResponse = ErrorResponse(
                            ErrorType.ERROR_DEFAULT,
                            throwable.code(),
                            getResponseMessage(throwable) ?: defaultError
                        )
                        val body = throwable.response()?.errorBody()
                        body?.charStream()?.let {
                            errorResponse = Gson().fromJson(it, ErrorResponse::class.java)
                        }
                        errorResponse
                    }

                }
            } is IOException, is NetworkException -> {
                ErrorResponse(
                    type = ErrorType.ERROR_NETWORK,
                    //TODO
                    error = "internet_not_working"
                )
            } else -> {
                ErrorResponse(ErrorType.ERROR_UNKNOWN, error = throwable.message ?: defaultError)
            }
        }
    }

    private fun getResponseMessage(exception: HttpException): String? {
        val message = exception.message()
        return if (!message.isNullOrEmpty()) {
            message
        } else {
            val jsonObject = exception.response()?.errorBody()?.string()?.trim()?.let {
                try {
                    JSONObject(it)
                } catch (e: Exception) {
                    null
                }
            }
            jsonObject?.run {
                if (has("error") && !isNull("error")) {
                    getString("error")
                } else {
                    ""
                }
            }
        }
    }
}