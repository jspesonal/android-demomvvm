package com.wiseasy.smartpos.newfeature.ui.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.wiseasy.smartpos.newfeature.di.SPInjection
import com.wiseasy.smartpos.newfeature.extentions.hideSoftKeyboard
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

abstract class SPBaseActivity<B : ViewDataBinding>: AppCompatActivity(), HasAndroidInjector {

    @get:LayoutRes
    protected abstract val layoutId: Int
    lateinit var viewBinding: B

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    abstract fun onCreatedView(savedInstanceState: Bundle?)

    override fun androidInjector(): AndroidInjector<Any> =
        dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        SPInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewBinding = DataBindingUtil.setContentView(this, layoutId)
        viewBinding.lifecycleOwner = this
        onCreatedView(savedInstanceState)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onStop() {
        super.onStop()
        hideSoftKeyboard()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    protected fun <T, LD : LiveData<T>> observe(liveData: LD, onChanged: (T) -> Unit) {
        liveData.observe(this, Observer {
            it?.let(onChanged)
        })
    }

}