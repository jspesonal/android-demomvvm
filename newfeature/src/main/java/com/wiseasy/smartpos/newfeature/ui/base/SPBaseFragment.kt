package com.wiseasy.smartpos.newfeature.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.wiseasy.smartpos.newfeature.di.SPInjection
import com.wiseasy.smartpos.newfeature.extentions.hideSoftKeyboard

abstract class SPBaseFragment<B : ViewDataBinding> (
    @LayoutRes
    private val layoutId: Int
) : Fragment() {

    lateinit var viewBinding: B

    private var isNeededUpdated: Boolean = false

    /**
     * Called to Initialize view data binding variables when fragment view is created.
     */
    abstract fun onInitDataBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        SPInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (::viewBinding.isInitialized.not()) {
            viewBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
            onInitDataBinding()
        }
        viewBinding.apply {
            lifecycleOwner = viewLifecycleOwner
        }
        return viewBinding.root
    }

    protected fun <T, LD : LiveData<T>> observe(liveData: LD, onChanged: (T) -> Unit) {
        liveData.observe(this, Observer {
            it?.let(onChanged)
        })
    }

    override fun onStop() {
        super.onStop()
        activity?.hideSoftKeyboard()
    }
}