package com.wiseasy.smartpos.newfeature.ui.base

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.wiseasy.smartpos.newfeature.SPNFApplication
import com.wiseasy.smartpos.newfeature.data.model.ErrorResponse
import com.wiseasy.smartpos.newfeature.retrofit.ResponseHandler
import com.wiseasy.smartpos.newfeature.utils.SingleLiveEvent
import kotlinx.coroutines.CoroutineExceptionHandler

abstract class SPBaseViewModel(protected val app: SPNFApplication): ViewModel() {

    private val _loading: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val loading: LiveData<Boolean>
        get() = _loading

    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.d("Exception handled", throwable.localizedMessage as String)
        handleError(ResponseHandler.getError(app.getContext(), throwable))
    }

    abstract fun handleError(error: ErrorResponse)
}