package com.wiseasy.smartpos.newfeature.ui.screen.topup

import android.os.Bundle
import android.util.Log
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.wiseasy.smartpos.newfeature.R
import com.wiseasy.smartpos.newfeature.databinding.ActivityTopUpBinding
import com.wiseasy.smartpos.newfeature.domain.ExampleUseCase
import com.wiseasy.smartpos.newfeature.ui.base.SPBaseActivity
import kotlinx.coroutines.*
import javax.inject.Inject

class TopUpActivity() : SPBaseActivity<ActivityTopUpBinding>() {

    override val layoutId: Int = R.layout.activity_top_up

    private lateinit var appBarConfiguration: AppBarConfiguration

    @Inject
    lateinit var exampleUseCase: ExampleUseCase

    var job: Job? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreatedView(savedInstanceState: Bundle?) {
//        val navController = findNavController(R.id.spnf_nav_host_fragment_content_top_up)
//        appBarConfiguration = AppBarConfiguration(navController.graph)
//        setupActionBarWithNavController(navController, appBarConfiguration)

        viewBinding.fab.setOnClickListener { view ->
            callApi()
        }
    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.d("Exception handled", throwable.localizedMessage)
    }
    private fun callApi() {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = exampleUseCase.execute(ExampleUseCase.Params(1))
            withContext(Dispatchers.Main) {
                Log.d("callApiExample", response.toString())
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.spnf_nav_host_fragment_content_top_up)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

}