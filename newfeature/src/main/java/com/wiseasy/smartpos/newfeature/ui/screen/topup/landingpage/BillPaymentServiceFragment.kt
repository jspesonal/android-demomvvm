package com.wiseasy.smartpos.newfeature.ui.screen.topup.landingpage

import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.wiseasy.smartpos.newfeature.R
import com.wiseasy.smartpos.newfeature.databinding.FragmentBillPaymentServiceBinding
import com.wiseasy.smartpos.newfeature.ui.base.SPBaseFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class BillPaymentServiceFragment : SPBaseFragment<FragmentBillPaymentServiceBinding>(
    layoutId = R.layout.fragment_bill_payment_service
), HasAndroidInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    private val viewModel: BillPaymentServiceViewModel by viewModels { viewModelFactory }

    override fun androidInjector(): AndroidInjector<Any?>? {
        return androidInjector
    }

    override fun onInitDataBinding() {
        viewBinding.viewModel = viewModel
    }


}