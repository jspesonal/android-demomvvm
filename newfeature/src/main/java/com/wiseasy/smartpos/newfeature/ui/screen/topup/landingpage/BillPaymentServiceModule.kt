package com.wiseasy.smartpos.newfeature.ui.screen.topup.landingpage

import androidx.lifecycle.ViewModel
import com.wiseasy.smartpos.newfeature.di.scope.FragmentScope
import com.wiseasy.smartpos.newfeature.di.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap


@Module
abstract class BillPaymentServiceModule {

    @Binds
    @IntoMap
    @ViewModelKey(BillPaymentServiceViewModel::class)
    abstract fun providerBillPaymentServiceViewModel(viewModel: BillPaymentServiceViewModel): ViewModel

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun providerBillPaymentServiceFragment(): BillPaymentServiceFragment
}