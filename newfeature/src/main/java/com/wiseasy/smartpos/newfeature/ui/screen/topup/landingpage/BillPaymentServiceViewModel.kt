package com.wiseasy.smartpos.newfeature.ui.screen.topup.landingpage

import com.wiseasy.smartpos.newfeature.SPNFApplication
import com.wiseasy.smartpos.newfeature.data.model.ErrorResponse
import com.wiseasy.smartpos.newfeature.ui.base.SPBaseViewModel
import com.wiseasy.smartpos.newfeature.utils.SingleLiveEvent
import javax.inject.Inject

class BillPaymentServiceViewModel @Inject constructor(app: SPNFApplication) : SPBaseViewModel(app) {

    private val _state = SingleLiveEvent<BillPaymentServiceViewState>()
    val viewState: SingleLiveEvent<BillPaymentServiceViewState>
        get() = _state

    private val _viewEvent = SingleLiveEvent<BillPaymentServiceViewEvent>()
    val viewEvent: SingleLiveEvent<BillPaymentServiceViewEvent>
        get() = _viewEvent

    val textExample: String = "BillPaymentServiceViewModel injected"

    override fun handleError(error: ErrorResponse) {
    }
}