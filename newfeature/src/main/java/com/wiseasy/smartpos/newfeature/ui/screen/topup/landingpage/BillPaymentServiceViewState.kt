package com.wiseasy.smartpos.newfeature.ui.screen.topup.landingpage

import com.wiseasy.smartpos.newfeature.ui.base.SPBaseViewState

sealed class BillPaymentServiceViewState: SPBaseViewState {
    data class Loading(val showIndicator : Boolean) : BillPaymentServiceViewState()
    data class Error(val message: String) : BillPaymentServiceViewState()
}