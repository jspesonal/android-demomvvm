package com.wiseasy.smartpos.newfeature.ui.widget

import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.LifecycleObserver
import kotlinx.coroutines.*

class DebouncingQueryTextListener(
    private val onDebouncingQueryTextChange: (String?) -> Unit
): TextWatcher, LifecycleObserver {

    var debouncePeriod: Long = 500

    private val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.Main)

    private var searchJob: Job? = null

    override fun afterTextChanged(pTextEditable: Editable?) {
        searchJob = coroutineScope.launch {
            pTextEditable?.let {
                delay(debouncePeriod)
                onDebouncingQueryTextChange(pTextEditable.toString())
            }
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        searchJob?.cancel()
    }
}