package com.wiseasy.smartpos.newfeature.utils


import org.json.JSONObject

fun JSONObject.valueString(key : String): String {
    return if (has(key)) {
        getString(key)?: ""
    } else {
        ""
    }
}

fun JSONObject.valueInt(key : String): Int {
    return if (has(key)) {
        getInt(key)
    } else {
        0
    }
}


fun JSONObject.valueLong(key : String): Long {
    return if (has(key)) {
        getLong(key)
    } else {
        0
    }
}

