package com.wiseasy.smartpos.other

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.other.transaction.TransactionListActivity
import com.wiseasy.smartpos.other.databinding.ActivityOthersBinding
import com.wiseasy.smartpos.other.setting.SettingActivity
import com.wiseasy.smartpos.other.user.list.UserListActivity

/**
 * Created by Bella on 2021/12/28.
 */
class OtherActivity : CommonActivity<ActivityOthersBinding>() {

    override fun getBinding(): ActivityOthersBinding {
        return ActivityOthersBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        if (SPUtil.getSmartPayUserPosition(this) == 0 || SPUtil.getSmartPayUserPosition(this) == 1) {
            //0-超管， 1-店长
            mBinding.llUserManagement.visibility = View.VISIBLE
        } else {
            mBinding.llUserManagement.visibility = View.INVISIBLE
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.ll_user_management -> {
                startActivity(Intent(this, UserListActivity::class.java))
            }
            R.id.ll_trans_management -> {
                startActivity(Intent(this, TransactionListActivity::class.java))
            }
            R.id.ll_setting -> {
                startActivity(Intent(this, SettingActivity::class.java))
            }

        }
    }
}