package com.wiseasy.smartpos.other

import android.util.Log
import com.google.gson.Gson
import com.wiseasy.smartpos.base.BaseApplication
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.bean.BasicAuth
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.http.wisecloud.RetrofitClientWiseCloud
import okhttp3.Headers


/**
 * Created by Bella on 2021/12/20.
 * Setting模块单独运行时(作为独立的app)，会调用该Application，
 * 然后该Application调用Like(做一些初始化类操作)
 * Setting作为模块运行时，会只调用Like
 */
class OtherApplication : BaseApplication() {
    private val TAG = "OtherApplication"

    override fun onCreate() {
        super.onCreate()

        //调用Like
        OtherApplicationLike().init(this)

        //初始化retrofit
        initClient()
        //初始化测试阶段的参数
        initTestParam()
    }

    private fun initClient() {
        Log.d(TAG, "initClient start")
        RetrofitClientSmartPay.init(context)
        val headers = Headers.Builder()
            .add("Content-Type", "application/json;charset=UTF-8")
            .add("X-Device-Model", "Android")
            .add("X-OS-Name", "Android")
            .add("X-OS-Version", "Android 8.0.1")
            .add("X-App-Version", "1.0.1")
            .add("Accept-Language", "en")
            .build()

        RetrofitClientSmartPay.addCommonHeaders(headers = headers)

        //WiseCloud接口初始化
        RetrofitClientWiseCloud.init(context)
        val headersAboutWiseCloud = Headers.Builder()
            .add("Content-Type", "application/json;charset=UTF-8")
            .add("Accept-Language", "zh-CN")
            .build()

        RetrofitClientWiseCloud.addCommonHeaders(headers = headersAboutWiseCloud)
        Log.d(TAG, "initClient end")
    }

    private fun initTestParam() {
        SPUtil.setSmartPayToken(context, "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI1ODEwMDc3NzQ4Nzk3NTA1IiwiaWF0IjoxNjQzMDA5Njc5LCJpc3MiOiJTTUFSVFBBWSJ9.kCJmaiC2-AKEsPPUmH-s2qoPqD3TBJM6u9-I4ESVtuI")
        SPUtil.setSmartPaySpMid(context, "00257110")
        SPUtil.setSmartPaySpTid(context, "01000002")
        SPUtil.setSmartPayPhone(context, "0988303036")

        SPUtil.setSmartPayValidTill(context, 99999999999999)
    }
}