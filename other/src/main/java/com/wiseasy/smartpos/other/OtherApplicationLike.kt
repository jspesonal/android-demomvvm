package com.wiseasy.smartpos.other

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import com.google.gson.Gson
import com.wiseasy.smartpos.base.IApplicationLike
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.bean.BasicAuth
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.module.service.OtherModuleService
import com.wiseasy.smartpos.other.module.serviceimpl.OtherModuleServiceImpl
import okhttp3.Headers

/**
 * Created by Bella on 2021/12/20.
 */
class OtherApplicationLike : IApplicationLike {

    private val TAG = "OtherApplicationLike"
    private lateinit var context: Context
    private val router = Router.getInstance()

    override fun init(application: Application) {
        context = application.applicationContext
        //做一些初始化的操作
        router.addService(OtherModuleService::class.java, OtherModuleServiceImpl())

    }

}