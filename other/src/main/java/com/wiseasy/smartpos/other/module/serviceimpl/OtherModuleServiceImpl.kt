package com.wiseasy.smartpos.other.module.serviceimpl

import android.content.Context
import android.content.Intent
import com.wiseasy.smartpos.comm.module.service.OtherModuleService
import com.wiseasy.smartpos.other.OtherActivity

/**
 * Created by Bella on 2021/12/20.
 */
class OtherModuleServiceImpl : OtherModuleService {

    /**
     * 跳转到Other界面
     */
    override fun goToOtherActivity(context: Context) {
        context.startActivity(Intent(context, OtherActivity::class.java))
    }

}