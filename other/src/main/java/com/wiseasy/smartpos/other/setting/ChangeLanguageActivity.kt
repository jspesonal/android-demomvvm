package com.wiseasy.smartpos.other.setting

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.other.R
import com.wiseasy.smartpos.other.databinding.ActivityChangeLanguageBinding
import okhttp3.Headers

/**
 * 修改语言
 * Created by Bella on 2021/12/28.
 */
class ChangeLanguageActivity : CommonActivity<ActivityChangeLanguageBinding>() {

    private lateinit var mContext: Context
    private var languageId = 0 //0或1
    private var languageStr = "" //vn或en

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        initView()
    }

    override fun getBinding(): ActivityChangeLanguageBinding {
        return ActivityChangeLanguageBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.ll_language_vietnamese -> {
                log("选择了越南语")
//                SPUtil.setSettingLanguage(mContext, 0)

                languageId = 0
                languageStr = "vn"
                onLanguageChange()
            }

            R.id.ll_language_english -> {
                log("选择了英语")
//                SPUtil.setSettingLanguage(mContext, 1)

                languageId = 1
                languageStr = "en"
                onLanguageChange()
            }

            R.id.btn_ok -> {
                //重新设置SmartPay网络消息头语言类型
                val headers = Headers.Builder().add("Accept-Language", languageStr).build()
                RetrofitClientSmartPay.addCommonHeaders(headers)

                SPUtil.setSettingLanguage(mContext, languageId)
                SPUtil.setIsChangeLanguage(mContext, true)
                finish()
            }
        }
    }

    private fun initData() {
        mContext = this
        languageId = SPUtil.getSettingLanguage(mContext)
        languageStr = if (languageId == 0) {
            "vn"
        } else {
            "en"
        }
    }

    private fun initView() {
        setViewByLanguage()
    }

    /**
     * 根据选择的语言设置UI
     */
    private fun setViewByLanguage() {
        if (languageId == 0) {
            //选择的是越南语
            mBinding.ivLanguageVie.background = resources.getDrawable(R.drawable.other_change_language_selected_bg)
            mBinding.ivLanguageEng.background = resources.getDrawable(R.drawable.other_change_language_unselected_bg)
        } else {
            //选择的是英语
            mBinding.ivLanguageEng.background = resources.getDrawable(R.drawable.other_change_language_selected_bg)
            mBinding.ivLanguageVie.background = resources.getDrawable(R.drawable.other_change_language_unselected_bg)
        }
    }

    /**
     * 语言改变时的动作
     */
    private fun onLanguageChange() {
//        val intent = Intent(mContext, ChangeLanguageActivity::class.java)
//        startActivity(intent)
//        finish()

        setViewByLanguage()
//        SPUtil.setIsChangeLanguage(mContext, true)
    }

}