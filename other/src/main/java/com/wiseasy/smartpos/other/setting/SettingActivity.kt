package com.wiseasy.smartpos.other.setting

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.base.util.DeviceUtil
import com.wiseasy.smartpos.comm.bean.UpdateAppDetail
import com.wiseasy.smartpos.comm.module.service.LoginModuleService
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.other.R
import com.wiseasy.smartpos.other.databinding.ActivitySettingsBinding
import com.wiseasy.smartpos.other.setting.password.current.VerifyCurrentPasswordActivity

/**
 * 设置界面
 * Created by Bella on 2021/12/27.
 */
class SettingActivity : CommonActivity<ActivitySettingsBinding>(), SettingContract.IView {

    lateinit var mContext: Context
    lateinit var mPresenter: SettingPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
    }

    override fun getBinding(): ActivitySettingsBinding {
        return ActivitySettingsBinding.inflate(layoutInflater)
    }

    private fun initData() {
        mContext = this
        mPresenter = SettingPresenter(mContext)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.rl_change_language -> {
                startActivity(Intent(this, ChangeLanguageActivity::class.java))
            }
            R.id.rl_sign_out -> {
                mDialog = CommonDialog.Builder(this)
                    .setTitle(getString(R.string.other_logout))
                    .setButton(getString(R.string.common_ok), getString(R.string.common_no))
                    .setListener(object : CommonDialog.OnClickListener {
                        override fun onConfirm() {
                            //调用登出接口，并返回到登录界面
                            mPresenter.logout()
                        }

                        override fun onCancel() {
                            //取消登出，关闭弹窗
                            mDialog.dismiss()
                        }

                    })
                    .createAndShow()
            }
            R.id.rl_change_pwd -> {
                startActivity(Intent(this, VerifyCurrentPasswordActivity::class.java))
            }
            R.id.rl_app_update -> {
                mPresenter.updateApp()
            }
        }
    }

    override fun onLogoutSuccess() {
        log("onLogoutSuccess")
        hideLoading()
        Router.getInstance().getService(LoginModuleService::class.java)?.goToLoginActivity(mContext)
    }

    override fun onLogoutFail(msg: String?) {
        log("onLogoutFail|msg = $msg")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setTitle(msg)
            .setButton(getString(R.string.common_ok))
            .setListener(object : CommonDialog.OnClickListener {
                override fun onConfirm() {
                    mDialog?.dismiss()
                }

                override fun onCancel() {

                }

            })
            .createAndShow()
    }

    override fun onUpdateSuccess(appDetail: UpdateAppDetail) {
        log("onUpdateSuccess")
        hideLoading()
        if (DeviceUtil.isInstall(mContext, "wiseasy.com.market")) {
            log("onUpdateSuccess|即将跳转到应用市场")
            //如果安装了应用市场，则跳转到应用市场
            val intent = Intent()
            val component = ComponentName("wiseasy.com.market", "wiseasy.com.market.main.MainActivity")
            intent.component = component
            intent.putExtra("marketId", appDetail.marketId)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        } else {
            log("onUpdateSuccess|未安装应用市场")
            mDialog = CommonDialog.Builder(mContext)
                .setTitle(getString(R.string.other_app_not_install))
                .setButton(getString(R.string.common_close))
                .createAndShow()
        }

    }

    override fun onUpdateFailOrNotNeedUpdate(msg: String?) {
        log("onUpdateFail|msg = $msg")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setTitle(msg)
            .setButton(getString(R.string.common_close))
            .createAndShow()
    }
}