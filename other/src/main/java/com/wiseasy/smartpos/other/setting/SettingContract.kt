package com.wiseasy.smartpos.other.setting

import com.wiseasy.smartpos.base.mvp.BaseIView
import com.wiseasy.smartpos.comm.bean.UpdateAppDetail

/**
 * Created by Bella on 2022/1/19.
 */
interface SettingContract {

    interface IView : BaseIView {
        /**
         * 登出成功
         */
        fun onLogoutSuccess()

        /**
         * 登出失败
         */
        fun onLogoutFail(msg: String?)

        /**
         * 更新成功
         */
        fun onUpdateSuccess(appDetail: UpdateAppDetail)

        /**
         * 更新失败或无需更新
         */
        fun onUpdateFailOrNotNeedUpdate(msg: String?)

    }

    interface IPresenter {
        /**
         * 登出
         */
        fun logout()

        /**
         * 升级APP
         */
        fun updateApp()
    }
}