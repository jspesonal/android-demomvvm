package com.wiseasy.smartpos.other.setting

import android.content.Context
import android.util.Log
import com.alibaba.fastjson.JSONArray
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.base.util.withIOContext
import com.wiseasy.smartpos.comm.bean.UpdateApp
import com.wiseasy.smartpos.comm.http.smartpay.BaseResult

import com.wiseasy.smartpos.comm.http.smartpay.ISmartPayAPI
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.http.wisecloud.IWiseCloudAPI
import com.wiseasy.smartpos.comm.http.wisecloud.RetrofitClientWiseCloud
import com.wiseasy.smartpos.comm.util.putBasicAuth
import com.wiseasy.smartpos.other.R
import com.wiseasy.smartpos.other.setting.update.VersionMD5Util
import okhttp3.Response

/**
 * Created by Bella on 2022/1/19.
 */
class SettingPresenter(context: Context) : BasePresenter(), SettingContract.IPresenter {
    private var mView: SettingContract.IView

    init {
        mContext = context
        mView = context as SettingContract.IView
    }

    override fun logout() {
        log("logout")
        mView.showLoading()
        launchRx {
            val requestJson = JSONObject()
            val basicAuth = JSONObject()
            mContext?.let { putBasicAuth(it, basicAuth, false) }
            requestJson["basicAuth"] = basicAuth
            var result: BaseResult<Response>? = null
            withIOContext {
                result = RetrofitClientSmartPay.create(ISmartPayAPI::class.java)
                    .logout(RetrofitClientSmartPay.buildRequestBody(requestJson))
                Log.i(TAG, "logout|result = $result")
            }
            mView.onLogoutSuccess()
        }.await {
            it.printStackTrace()
            mView.onLogoutFail(it.message)
        }

    }

    override fun updateApp() {
        log("updateApp")
        mView.showLoading()
        launchRx {
            val appJson = JSONObject()
            appJson["appVersionNumber"] = CommonUtil.getVersionCode(mContext)
            val packageName = CommonUtil.getPackageName(mContext)
            log("updateApp|package = $packageName")
            appJson["versionMD5"] = VersionMD5Util.getVersionMd5(mContext, packageName)

            val jsonArray = JSONArray()
            jsonArray.add(appJson)

            val json = JSONObject()
            json["appList"] = jsonArray
            json["sn"] = CommonUtil.getSN(mContext)

            var result: UpdateApp? = null
            withIOContext {
                result = RetrofitClientWiseCloud.create(IWiseCloudAPI::class.java)
                    .updateApp(RetrofitClientWiseCloud.buildRequestBody(json)).dataConvert(UpdateApp::class.java)
            }
            if (result != null && result?.appList?.size != 0) {
                result?.appList?.get(0)?.let { mView.onUpdateSuccess(it) }
            } else {
                mView.onUpdateFailOrNotNeedUpdate(mContext?.getString(R.string.other_version_last))
            }

        }.await {
            it.printStackTrace()
            mView.onUpdateFailOrNotNeedUpdate(it.message)
        }
    }
}