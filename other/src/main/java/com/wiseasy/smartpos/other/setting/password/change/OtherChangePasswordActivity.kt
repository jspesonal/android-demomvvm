package com.wiseasy.smartpos.other.setting.password.change

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.View
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.comm.module.service.LoginModuleService
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonCustomNumberInputKeyboard
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.other.R
import com.wiseasy.smartpos.other.databinding.ActivityOtherChangePasswordBinding
import com.wiseasy.smartpos.other.view.PasswordCharSequenceStyle

/**
 * 修改密码
 * Created by Bella on 2022/1/13.
 */
class OtherChangePasswordActivity : CommonActivity<ActivityOtherChangePasswordBinding>(), OtherChangePasswordContract.IView {

    private lateinit var mContext: Context
    private lateinit var mPresenter: OtherChangePasswordPresenter

    private var mIsFocusNewPassword = false//输入新密码是否获取焦点
    private var mIsFocusNewPasswordReenter = false//重新输入新密码是否获取焦点

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        initView()
    }

    override fun getBinding(): ActivityOtherChangePasswordBinding {
        return ActivityOtherChangePasswordBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.et_new_password -> {//输入新密码
                log("输入新密码获取焦点")
                mIsFocusNewPassword = true
                mIsFocusNewPasswordReenter = false
                mBinding.etNewPassword.setBackgroundResource(R.drawable.other_shape_password_focus_bg)
                mBinding.etNewPasswordReenter.setBackgroundResource(R.drawable.other_shape_password_bg)
                mBinding.keyBoard.visibility = View.VISIBLE
                val password = mBinding.etNewPassword.text.toString()
                mBinding.keyBoard.clear()
                mBinding.keyBoard.input(password)
            }
            R.id.et_new_password_reenter -> {//重新输入新密码
                log("重新输入新密码获取焦点")
                setClickNewPasswordView()
            }
        }
    }

    override fun onChangeSuccess() {
        log("onChangeSuccess")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setTitle(getString(R.string.other_change_password_change_success))
            .setButton(getString(R.string.common_close))
            .setListener(object : CommonDialog.OnClickListener {
                override fun onConfirm() {
                    Router.getInstance().getService(LoginModuleService::class.java)?.goToLoginActivity(mContext)
                    finish()
                }

                override fun onCancel() {
                }

            })
            .createAndShow()
    }

    override fun onChangeFail(msg: String?) {
        log("onChangeFail|msg = $msg")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setTitle(getString(R.string.other_change_password_change_fail))
            .setDescription(msg)
            .setButton(getString(R.string.common_close))
            .createAndShow()
    }

    private fun initData() {
        mContext = this
        mPresenter = OtherChangePasswordPresenter(mContext)
    }

    private fun initView() {
        //数字键盘
        mBinding.keyBoard.setMaxLength(6)
        mBinding.keyBoard.setButtonClickable(false)
        mBinding.keyBoard.setKeyBoardClickListener(object : CommonCustomNumberInputKeyboard.KeyBoardClickListener {
            override fun onOkTouch() {
                //如果是当前焦点在输入密码，则点击Next后，光标移动到下一个输入框
                if (mIsFocusNewPassword) {
                    setClickNewPasswordView()
                    return
                }
                mIsFocusNewPasswordReenter = false
                mIsFocusNewPassword = false
                mBinding.etNewPassword.setBackgroundResource(R.drawable.other_shape_password_bg)
                mBinding.etNewPasswordReenter.setBackgroundResource(R.drawable.other_shape_password_bg)
                mBinding.keyBoard.clear()
                mPresenter.changePassword(mBinding.etNewPassword.text.toString(), mBinding.etNewPasswordReenter.text.toString())
            }

            override fun onInputNumberTouch(inputNumber: String?) {
                if (mIsFocusNewPassword) {
//                    log("输入密码获取焦点，inputNumber = $inputNumber")
                    mBinding.etNewPassword.text = Editable.Factory.getInstance().newEditable(inputNumber)
                }
                if (mIsFocusNewPasswordReenter) {
//                    log("重新输入密码获取焦点，inputNumber = $inputNumber")
                    mBinding.etNewPasswordReenter.text = Editable.Factory.getInstance().newEditable(inputNumber)
                }
                //两个输入框任意一个未空，或两个不匹配则为灰色
//                if (TextUtils.isEmpty(mBinding.etNewPassword.text.toString())
//                    || TextUtils.isEmpty(mBinding.etNewPasswordReenter.text.toString())
                if (mBinding.etNewPassword.text.toString().length < 6
                    || mBinding.etNewPasswordReenter.text.toString().length < 6
                    || mBinding.etNewPassword.text.toString() != mBinding.etNewPasswordReenter.text.toString()
                ) {
                    mBinding.keyBoard.setButtonClickable(false)
                } else {
                    mBinding.keyBoard.setButtonClickable(true)
                }
            }

        })

        //设置两个输入框不可获取焦点
        mBinding.etNewPassword.isFocusable = false
        mBinding.etNewPasswordReenter.isFocusable = false
        //将两个输入框的掩码字符修改为星号
        mBinding.etNewPassword.transformationMethod = PasswordCharSequenceStyle()
        mBinding.etNewPasswordReenter.transformationMethod = PasswordCharSequenceStyle()
    }

    /**
     * 设置点击 重新输入密码 后的view
     */
    private fun setClickNewPasswordView() {
        mIsFocusNewPasswordReenter = true
        mIsFocusNewPassword = false
        mBinding.etNewPassword.setBackgroundResource(R.drawable.other_shape_password_bg)
        mBinding.etNewPasswordReenter.setBackgroundResource(R.drawable.other_shape_password_focus_bg)
        mBinding.keyBoard.visibility = View.VISIBLE
        val reEnter = mBinding.etNewPasswordReenter.text.toString()
//        log("reEnter = $reEnter")
        mBinding.keyBoard.clear()
        mBinding.keyBoard.input(reEnter)
    }

}