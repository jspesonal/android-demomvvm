package com.wiseasy.smartpos.other.setting.password.change

import com.wiseasy.smartpos.base.mvp.BaseIView

/**
 * @author Belle(Baiyunyan)
 * @date 2022/02/07
 */
interface OtherChangePasswordContract {

    interface IView : BaseIView {
        /**
         * 修改成功
         */
        fun onChangeSuccess()

        /**
         * 修改失败
         */
        fun onChangeFail(msg: String?)
    }

    interface IPresenter {
        /**
         * 修改密码
         */
        fun changePassword(newPassword: String, reEnterNewPassword: String)
    }

}