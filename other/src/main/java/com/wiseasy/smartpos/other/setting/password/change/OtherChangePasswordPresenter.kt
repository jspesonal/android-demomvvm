package com.wiseasy.smartpos.other.setting.password.change

import android.content.Context
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.kotlin.withIOContext
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.comm.http.wisecloud.IWiseCloudAPI
import com.wiseasy.smartpos.comm.http.wisecloud.RetrofitClientWiseCloud
import com.wiseasy.smartpos.comm.http.wisecloud.util.getMD5String
import com.wiseasy.smartpos.other.R
import okhttp3.Response

/**
 * @author Belle(Baiyunyan)
 * @date 2022/02/07
 */
class OtherChangePasswordPresenter(context: Context) : BasePresenter(), OtherChangePasswordContract.IPresenter {

    private var mView: OtherChangePasswordContract.IView

    init {
        mContext = context
        mView = context as OtherChangePasswordContract.IView
    }

    override fun changePassword(newPassword: String, reEnterNewPassword: String) {
        log("setPassword|newPassword = $newPassword, reEnterNewPassword = $reEnterNewPassword")
        mView.showLoading()
        if (newPassword != reEnterNewPassword) {
            mView.onChangeFail(mContext?.getString(R.string.login_set_password_error))
            return
        }
        launchRx {
            val json = JSONObject()
            json["sn"] = CommonUtil.getSN(mContext)
            json["loginAccount"] = SPUtil.getSmartPayPhone(mContext)
            json["loginPassword"] = getMD5String(newPassword)

            withIOContext {
                RetrofitClientWiseCloud.create(IWiseCloudAPI::class.java)
                    .changePassword(RetrofitClientWiseCloud.buildRequestBody(json))
                    .dataConvert(Response::class.java)
            }
            mView.onChangeSuccess()
        }.await {
            it.printStackTrace()
            mView.onChangeFail(it.message)
        }
    }

}