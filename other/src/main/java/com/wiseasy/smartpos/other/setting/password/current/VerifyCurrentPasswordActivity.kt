package com.wiseasy.smartpos.other.setting.password.current

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonCustomNumberInputKeyboard
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.other.R
import com.wiseasy.smartpos.other.databinding.ActivityOtherInputPassword2Binding
import com.wiseasy.smartpos.other.setting.password.change.OtherChangePasswordActivity
import com.wiseasy.smartpos.other.view.PasswordCharSequenceStyle

/**
 * 验证当前密码
 * Created by Bella on 2022/1/14.
 */
class VerifyCurrentPasswordActivity : CommonActivity<ActivityOtherInputPassword2Binding>(), VerifyCurrentPasswordContract.IView {

    private lateinit var mContext: Context
    private lateinit var mPresenter: VerifyCurrentPasswordPresenter

    override fun getBinding(): ActivityOtherInputPassword2Binding {
        return ActivityOtherInputPassword2Binding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        initView()
    }

    override fun onVerifySuccess() {
        log("onVerifySuccess")
        hideLoading()
        startActivity(Intent(mContext, OtherChangePasswordActivity::class.java))
        finish()
    }

    override fun onVerifyFail(msg: String?) {
        log("onVerifyFail|msg = $msg")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setTitle(getString(R.string.other_password_wrong))
            .setDescription(msg)
            .setButton(getString(R.string.common_ok))
            .createAndShow()
    }

    private fun initData() {
        mContext = this
        mPresenter = VerifyCurrentPasswordPresenter(mContext)
    }

    private fun initView() {
        mBinding.tvPassword.setBackgroundResource(R.drawable.other_shape_password_focus_bg)
        //数字键盘
        mBinding.keyBoard.setMaxLength(6)
        mBinding.keyBoard.setButtonClickable(false)
        mBinding.keyBoard.setKeyBoardClickListener(object : CommonCustomNumberInputKeyboard.KeyBoardClickListener {
            override fun onOkTouch() {
//                Log.i(TAG, "onOkTouch: ")
                mPresenter.verify(mBinding.tvPassword.text.toString())

            }

            override fun onInputNumberTouch(inputNumber: String?) {
//                Log.i(TAG, "onInputNumberTouch|inputNumber = $inputNumber ")
                mBinding.tvPassword.text = inputNumber
//                if (TextUtils.isEmpty(inputNumber)) {
                if (inputNumber?.length!! < 6) {
                    mBinding.keyBoard.setButtonClickable(false)
                } else {
                    mBinding.keyBoard.setButtonClickable(true)
                }
            }

        })

        //将输入框的字符修改为*号
        mBinding.tvPassword.transformationMethod = PasswordCharSequenceStyle()
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.tv_password -> {
                //输入密码获取焦点
                mBinding.keyBoard.visibility = View.VISIBLE
                val password = mBinding.tvPassword.text.toString()
                mBinding.keyBoard.clear()
                mBinding.keyBoard.input(password)
            }
        }
    }

}