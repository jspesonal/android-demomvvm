package com.wiseasy.smartpos.other.setting.password.current

import com.wiseasy.smartpos.base.mvp.BaseIView

/**
 * @author Belle(Baiyunyan)
 * @date 2022/02/07
 */
interface VerifyCurrentPasswordContract {

    interface IView : BaseIView {
        /**
         * 验证成功
         */
        fun onVerifySuccess()

        /**
         * 验证失败
         */
        fun onVerifyFail(msg: String?)
    }

    interface IPresenter {
        /**
         * 验证当前用户
         */
        fun verify(password: String)
    }

}