package com.wiseasy.smartpos.other.setting.password.current

import android.content.Context
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.base.util.withIOContext
import com.wiseasy.smartpos.comm.bean.VerifyPassword
import com.wiseasy.smartpos.comm.http.wisecloud.IWiseCloudAPI
import com.wiseasy.smartpos.comm.http.wisecloud.RetrofitClientWiseCloud
import com.wiseasy.smartpos.comm.http.wisecloud.util.getMD5String

/**
 * @author Belle(Baiyunyan)
 * @date 2022/02/07
 */
class VerifyCurrentPasswordPresenter(context: Context) : BasePresenter(), VerifyCurrentPasswordContract.IPresenter {

    private var mView: VerifyCurrentPasswordContract.IView

    init {
        mContext = context
        mView = context as VerifyCurrentPasswordContract.IView
    }

    override fun verify(password: String) {
        val phone = SPUtil.getSmartPayPhone(mContext)
        log("login|number = $phone, password = $password")
        mView.showLoading()
        launchRx {
            val json = JSONObject()
            json["sn"] = CommonUtil.getSN(mContext)
            json["loginAccount"] = phone
            json["loginPassword"] = getMD5String(password)
            withIOContext {
                RetrofitClientWiseCloud.create(IWiseCloudAPI::class.java).verifyPassword(RetrofitClientWiseCloud.buildRequestBody(json)).dataConvert(VerifyPassword::class.java)
            }
            mView.onVerifySuccess()

        }.await {
            it.printStackTrace()
            mView.onVerifyFail(it.message)
        }
    }

}