package com.wiseasy.smartpos.other.setting.update;

import android.text.TextUtils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Wiseasy ljl.
 * Date: 2022/1/26
 * Desc：Signature utility class.
 */
public class SignUtil {

    /**
     * Get MD5
     *
     * @param inStr
     * @return
     */
    public static String string2MD5(String inStr) {
        byte[] contentBytes = getContentBytes(inStr, "UTF-8");
        return bytes2Md5String(contentBytes);
    }

    private static byte[] getContentBytes(String content, String charset) {
        if (TextUtils.isEmpty(charset)) {
            return content.getBytes();
        }
        try {
            return content.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("An error occurred during the MD5 signature. " +
                    "The specified encoding set is incorrect. The encoding set you specified is: " + charset);
        }
    }

    private static String bytes2Md5String(byte[] data) {
        return toHexString(md5(data));
    }

    private static String toHexString(byte[] bytes) {
        StringBuilder hexValue = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            int val = ((int) bytes[i]) & 0xff;
            if (val < 16) {
                hexValue.append("0");
            }
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();
    }

    private static byte[] md5(byte[] data) {
        return getMd5Digest().digest(data);
    }

    private static MessageDigest getMd5Digest() {
        return getDigest("MD5");
    }

    private static MessageDigest getDigest(String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException var2) {
            throw new IllegalArgumentException(var2);
        }
    }

}
