package com.wiseasy.smartpos.other.setting.update;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.text.TextUtils;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;


/**
 * Created by Wiseasy ljl.
 * Date: 2022/1/26
 * Desc：VersionMD5 utility class.
 */
public class VersionMD5Util {

    /**
     * Get versionM5.
     * Use this method to get VersionMD5.
     *
     * @param context
     * @param packageName
     * @return
     */
    public static String getVersionMd5(Context context, String packageName) {
        String publicKey = getPublicKey(context, packageName);
        if (TextUtils.isEmpty(publicKey)) {
            return null;
        } else {
            String str = packageName + publicKey;
            return SignUtil.string2MD5(str);
        }
    }

    /**
     * Get the application signature public key.
     *
     * @param context
     * @param packageName
     * @return
     */
    public static String getPublicKey(Context context, String packageName) {
        byte[] sign = getSign(context, packageName);
        String key = null;
        if (sign != null) {
            key = getPublicKeyBySign(sign);
        }
        return key;
    }

    /**
     * Get the application signature public key.
     *
     * @param signature
     * @return
     */
    public static String getPublicKeyBySign(byte[] signature) {
        try {
            CertificateFactory certFactory = CertificateFactory
                    .getInstance("X.509");
            X509Certificate cert = (X509Certificate) certFactory
                    .generateCertificate(new ByteArrayInputStream(signature));
            byte[] publickey = cert.getPublicKey().getEncoded();
            return bytesToHex(publickey);
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get the Application Signature.
     *
     * @param context
     * @param packageName
     * @return
     */
    public static byte[] getSign(Context context, String packageName) {
        Signature[] signs = getSignatures(context, packageName);
        if (signs == null) {
            return null;
        }
        return signs[0].toByteArray();
    }

    /**
     * Returns the signature information of the corresponding packet.
     *
     * @param context
     * @param packageName
     * @return
     */
    public static Signature[] getSignatures(Context context, String packageName) {
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
            return packageInfo.signatures;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Converts byte arrays to characters.
     *
     * @param bs
     * @return
     */
    public static String bytesToHex(byte[] bs) {
        if (bs == null) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        String hex = "";
        for (int i = 0; i < bs.length; i++) {
            hex = Integer.toHexString(bs[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex);
        }
        return sb.toString();
    }

}
