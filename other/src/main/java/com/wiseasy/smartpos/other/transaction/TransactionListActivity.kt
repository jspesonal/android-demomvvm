package com.wiseasy.smartpos.other.transaction

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.EditorInfo
import com.liaoinstan.springview.widget.SpringView
import com.wiseasy.smartpos.base.util.DateUtil
import com.wiseasy.smartpos.comm.bean.TransListDetailBean
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.other.R
import com.wiseasy.smartpos.other.transaction.adapter.TransactionListAdapter
import com.wiseasy.smartpos.other.databinding.ActivityTransactionListBinding
import com.wiseasy.smartpos.other.view.CustomFooter
import com.wiseasy.smartpos.other.view.CustomHeader
import java.text.SimpleDateFormat
import java.util.*

/**
 * 交易列表界面
 * @author Belle(Baiyunyan)
 * @date 2021/12/28
 */
class TransactionListActivity : CommonActivity<ActivityTransactionListBinding>(), TransactionListContact.IView {

    private lateinit var mContext: Context
    private lateinit var mPresenter: TransactionListPresenter
    private lateinit var mAdapter: TransactionListAdapter
    private var mData = mutableListOf<TransListDetailBean>()
    private val format = SimpleDateFormat("yyyy-MM-dd")

    private var mSelectDate: Date = DateUtil.StringToDate(DateUtil.getCurDateStr(), "yyyy-MM-dd")
    private var mPageNum = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        initView()
    }

    override fun getBinding(): ActivityTransactionListBinding {
        return ActivityTransactionListBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        mPageNum = 1
        when (v.id) {
            R.id.tv_time -> {//选择的日期
                val dp = DatePickerDialog(this, { datePicker, year, month, day ->
                    mSelectDate = DateUtil.setDate(mSelectDate, year, month, day)
                    mBinding.tvTime.text = format.format(mSelectDate)
                    mPresenter.queryList(getDate(), mPageNum, mBinding.etInputName.text.toString())
                }, mSelectDate.year + 1900, mSelectDate.month, mSelectDate.date) //year是从1900后开始的，所以要加1
                dp.show()
            }
            R.id.iv_pre -> {//前一天
                //前一天 previous day
                mSelectDate = DateUtil.getPreviousDay(mSelectDate)
                mBinding.tvTime.text = format.format(mSelectDate)
                onChangeDate()
                mPresenter.queryList(getDate(), mPageNum, mBinding.etInputName.text.toString())
            }
            R.id.iv_next -> {//后一天
                mSelectDate = DateUtil.getNextDay(mSelectDate)
                mBinding.tvTime.text = format.format(mSelectDate)
                onChangeDate()
                mPresenter.queryList(getDate(), mPageNum, mBinding.etInputName.text.toString())
            }
            R.id.iv_search -> {//搜索
                mAdapter.clear()
                mPresenter.queryList(getDate(), mPageNum, mBinding.etInputName.text.toString())
            }
            R.id.iv_clear -> {//清除按钮
                mBinding.etInputName.text = Editable.Factory.getInstance().newEditable("")
            }
            R.id.btn_close -> {//底部关闭
                finish()
            }
        }
    }

    override fun onStartQuery() {
        log("onStartQuery")
    }

    override fun onQueryListSuccess(date: MutableList<TransListDetailBean>, isLoadMore: Boolean) {
        log("onQueryListSuccess")
        hideLoading()
        mBinding.rlEmpty.visibility = GONE
        mBinding.svTransRefresh.visibility = VISIBLE
        mBinding.svTransRefresh.onFinishFreshAndLoad()
        if (isLoadMore) {
            mData.addAll(date)
        } else {
            mData = date
        }
        mAdapter.setData(mData)
    }

    override fun onQueryListFail(msg: String?) {
        log("onQueryListFail|msg = $msg")
        hideLoading()
        mBinding.rlEmpty.visibility = VISIBLE
        mBinding.svTransRefresh.visibility = GONE
        mBinding.svTransRefresh.onFinishFreshAndLoad()
        mBinding.tvError.text = msg
    }

    override fun onNoData() {
        log("onNoData")
        hideLoading()
        mAdapter.clear()
        mBinding.rlEmpty.visibility = VISIBLE
        mBinding.svTransRefresh.visibility = GONE
        mBinding.svTransRefresh.onFinishFreshAndLoad()
    }

    private fun initView() {
        mBinding.lvTrans.adapter = mAdapter
        mBinding.tvTime.text = getDate()
        mBinding.svTransRefresh.header = CustomHeader()
        mBinding.svTransRefresh.footer = CustomFooter()
        mBinding.svTransRefresh.type = SpringView.Type.FOLLOW
        mBinding.svTransRefresh.setListener(object : SpringView.OnFreshListener {
            override fun onRefresh() {
                log("下拉刷新")
                mPageNum = 1
                mPresenter.queryList(format.format(mSelectDate), mPageNum)
            }

            override fun onLoadmore() {
                log("上拉加载")
                mPresenter.queryList(format.format(mSelectDate), ++mPageNum)
            }

        })
        mBinding.etInputName.setOnEditorActionListener { _, actionId, _ ->
            when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    log("点击了软键盘上右下角的搜索按钮")
                }
            }
            true
        }
        mBinding.etInputName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (TextUtils.isEmpty(s)) {
                    mBinding.ivClear.visibility = GONE
                } else {
                    mBinding.ivClear.visibility = VISIBLE
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })
    }

    private fun initData() {
        mContext = this
        mPresenter = TransactionListPresenter(mContext)
        mAdapter = TransactionListAdapter(mContext, mData)

        mPresenter.queryList(getDate(), 1)
    }

    /**
     * 改变日期并加载数据时View显示
     */
    private fun onChangeDate() {
        log("onChangeDate")
        mAdapter.clear()
        mBinding.rlEmpty.visibility = GONE
    }

    /**
     * 获取时间
     */
    private fun getDate(): String {
        return DateUtil.date2Str(mSelectDate, "yyyy-MM-dd")
    }

}