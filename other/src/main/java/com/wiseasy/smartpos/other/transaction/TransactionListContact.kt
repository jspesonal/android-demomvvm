package com.wiseasy.smartpos.other.transaction

import com.wiseasy.smartpos.base.mvp.BaseIView
import com.wiseasy.smartpos.comm.bean.TransListDetailBean

/**
 * @author Belle(Baiyunyan)
 * @date 2021/12/28
 */
interface TransactionListContact {

    interface IView : BaseIView {

        fun onStartQuery()

        fun onQueryListSuccess(date: MutableList<TransListDetailBean>, isLoadMore: Boolean)

        fun onQueryListFail(msg: String?)

        fun onNoData()
    }

    interface IPresenter {
        fun queryList(data: String, pageNum: Int, searchText: String = "")
    }

}