package com.wiseasy.smartpos.other.transaction

import android.content.Context
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.base.util.withIOContext
import com.wiseasy.smartpos.comm.bean.TransListBean
import com.wiseasy.smartpos.comm.bean.TransListDetailBean
import com.wiseasy.smartpos.comm.http.smartpay.ISmartPayAPI
import com.wiseasy.smartpos.comm.http.smartpay.RetrofitClientSmartPay
import com.wiseasy.smartpos.comm.util.putBasicAuth

/**
 * @author Belle(Baiyunyan)
 * @date 2021/12/28
 */
class TransactionListPresenter(context: Context) : BasePresenter(), TransactionListContact.IPresenter {

    private var mView: TransactionListContact.IView
    private val LIMIT = 20//每一页的数量

    init {
        mContext = context
        mView = context as TransactionListContact.IView
    }

    override fun queryList(data: String, pageNum: Int, searchText: String) {
        log("选择的日期$data, 页数：$pageNum")
        mView.showLoading()
        launchRx {
            val basicAuth = JSONObject()
            mContext?.let { putBasicAuth(it, basicAuth) }

            val json = JSONObject()
            json["basicAuth"] = basicAuth
            json["searchText"] = searchText
            json["typeTrans"] = "all"
            json["fromDate"] = data
            json["toDate"] = data
            json["start"] = (pageNum - 1) * LIMIT
            json["limit"] = LIMIT

            var result: TransListBean? = null
            var list = mutableListOf<TransListDetailBean>()
            withIOContext {
                result = RetrofitClientSmartPay.create(ISmartPayAPI::class.java).getTransList(RetrofitClientSmartPay.buildRequestBody(json)).dataConvert(TransListBean::class.java)
                list = result?.list!!
            }

            //如果当前页是第一页并且列表是0，则说明交易无数据
            if (list.size == 0 && pageNum == 1) {
                mView.onNoData()
                return@launchRx
            }

            //是否是加载更多
            if (pageNum == 1) {
                mView.onQueryListSuccess(list, false)
            } else {
                mView.onQueryListSuccess(list, true)
            }

        }.await {
            it.printStackTrace()
            mView.onQueryListFail(it.message)
        }

    }

}