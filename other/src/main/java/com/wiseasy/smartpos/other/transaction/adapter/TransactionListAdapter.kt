package com.wiseasy.smartpos.other.transaction.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.wiseasy.android.utils.decimalFormat
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.comm.Constant
import com.wiseasy.smartpos.comm.bean.TransListDetailBean
import com.wiseasy.smartpos.comm.module.service.MobileModuleService
import com.wiseasy.smartpos.comm.util.getIconByType
import com.wiseasy.smartpos.comm.util.getStatusNameByStatus
import com.wiseasy.smartpos.other.R

/**
 * @author Belle(Baiyunyan)
 * @date 2021/12/28
 */
class TransactionListAdapter(private val context: Context, private var data: MutableList<TransListDetailBean>) : BaseAdapter() {

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return data[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder: ViewHolder
        var contentView = convertView
        if (contentView == null) {
            contentView = LayoutInflater.from(context).inflate(R.layout.item_trans_list, null)
            holder = ViewHolder()
            holder.rlItem = contentView.findViewById(R.id.rl_item)
            holder.ivIcon = contentView.findViewById(R.id.iv_icon)
            holder.tvName = contentView.findViewById(R.id.tv_name)
            holder.tvStatus = contentView.findViewById(R.id.tv_status)
            holder.tvAmount = contentView.findViewById(R.id.tv_amount)
            holder.tvTime = contentView.findViewById(R.id.tv_time)
            contentView.tag = holder
        } else {
            holder = contentView.tag as ViewHolder
        }

        val transaction = data[position]
        transaction.typeTrans?.let {
            holder.ivIcon.setImageDrawable(getIconByType(context, it))
        }
        holder.tvName.text = "${context.getString(R.string.other_trans_list_received_from)} ${transaction.customerName}"
        holder.tvStatus.text = getStatusNameByStatus(context, transaction.status)
        holder.tvAmount.text = decimalFormat(transaction.amount.toString()) + Constant.UNIT
        holder.tvTime.text = transaction.date

        when (transaction.status) {
            Constant.TRANS_STATUS_LIST_SUCCESS -> holder.tvStatus.setTextColor(context.resources.getColor(R.color.comm_theme))
            Constant.TRANS_STATUS_LIST_FAIL -> holder.tvStatus.setTextColor(context.resources.getColor(R.color.comm_note_red_color))
            Constant.TRANS_STATUS_LIST_SETTLEMENT -> holder.tvStatus.setTextColor(context.resources.getColor(R.color.comm_text_hint_color))
            Constant.TRANS_STATUS_LIST_PROCESSING -> holder.tvStatus.setTextColor(context.resources.getColor(R.color.common_dialog_btn_yellow))
            else -> holder.tvStatus.setTextColor(context.resources.getColor(R.color.comm_text_hint_color))
        }

        holder.rlItem.setOnClickListener {
            Router.getInstance().getService(MobileModuleService::class.java)?.goToCodePayDetailActivity(context, transaction.refNo!!)
        }

        return contentView!!
    }

    inner class ViewHolder() {
        lateinit var rlItem: RelativeLayout
        lateinit var ivIcon: ImageView
        lateinit var tvName: TextView
        lateinit var tvStatus: TextView
        lateinit var tvAmount: TextView
        lateinit var tvTime: TextView
    }

    fun setData(data1: MutableList<TransListDetailBean>) {
        data = data1
        notifyDataSetChanged()
    }

    fun clear() {
        if (data != null) {
            data.clear()
            notifyDataSetChanged()
        }
    }

}