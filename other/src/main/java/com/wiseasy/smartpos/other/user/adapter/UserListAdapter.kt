package com.wiseasy.smartpos.other.user.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wiseasy.smartpos.comm.bean.UserBean
import com.wiseasy.smartpos.comm.util.getUserStatusName
import com.wiseasy.smartpos.other.R

/**
 * Created by Bella on 2022/1/7.
 */
class UserListAdapter(
    private val context: Context, private var data: MutableList<UserBean>,
    listener: OnItemClickListener
) : RecyclerView.Adapter<UserListAdapter.ViewHolder>() {
    val TAG = "UserListAdapter"
    private var mListener: OnItemClickListener = listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        Log.i(TAG, "onCreateViewHolder: ")
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_user_list, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.i(TAG, "onBindViewHolder: ")
        val data = data.get(position)

        //员工职务：0.超管 1.店长， 2.普通店员
        if (data.position == 0) {
            holder.ivIcon.setBackgroundResource(R.mipmap.other_user_yellow_icon)
        } else if (data.position == 1) {
            holder.ivIcon.setBackgroundResource(R.mipmap.other_user_blue_icon)
        } else {
            holder.ivIcon.setBackgroundResource(R.mipmap.other_user_pink_icon)
        }

        holder.tvAccount.text = data.loginAccount
        holder.tvStore.text = data.storeName
        if (data.status == 1) {
            //Active
            holder.tvStatus.setTextColor(context.resources.getColor(R.color.comm_theme))
            holder.tvStatusDot.setBackgroundResource(R.drawable.comm_shape_user_status_active_bg)
        } else {
            //disable
            holder.tvStatus.setTextColor(context.resources.getColor(R.color.comm_text_hint_color))
            holder.tvStatusDot.setBackgroundResource(R.drawable.comm_shape_user_status_disable_bg)
        }
        holder.tvStatus.text = getUserStatusName(context, data.status)

        holder.itemView.setOnClickListener {
            mListener.onItemSelect(data)
        }
    }


    override fun getItemCount(): Int {
        Log.i(TAG, "getItemCount: ")
        return data?.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivIcon: ImageView = itemView.findViewById(R.id.iv_user_icon)
        val tvAccount: TextView = itemView.findViewById(R.id.tv_user_account)
        val tvStore: TextView = itemView.findViewById(R.id.tv_user_store)
        val tvStatus: TextView = itemView.findViewById(R.id.tv_user_status)
        val tvStatusDot: TextView = itemView.findViewById(R.id.tv_user_status_dot)
    }

    fun setData(data1: MutableList<UserBean>) {
        data = data1
        notifyDataSetChanged()
    }

    fun clear() {
        Log.i(TAG, "clear: ")
        if (data != null) {
            data.clear()
            notifyDataSetChanged()
        }
    }

    interface OnItemClickListener {
        fun onItemSelect(user: UserBean)
    }

}