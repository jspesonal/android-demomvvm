package com.wiseasy.smartpos.other.user.add

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.google.gson.Gson
import com.wiseasy.smartpos.base.event.FreshUserListEvent
import com.wiseasy.smartpos.base.event.RxBus
import com.wiseasy.smartpos.base.mvp.BaseActivity
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.bean.StoreBean
import com.wiseasy.smartpos.comm.bean.UserBean
import com.wiseasy.smartpos.comm.util.getUserRoleCode
import com.wiseasy.smartpos.comm.util.isCorrectPhone
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.comm.view.CommonSelectRoleDialog
import com.wiseasy.smartpos.comm.view.CommonSelectStoreDialog
import com.wiseasy.smartpos.other.R
import com.wiseasy.smartpos.other.databinding.ActivityUserAddBinding

/**
 * Created by Bella on 2022/1/10.
 */
class UserAddActivity : CommonActivity<ActivityUserAddBinding>(), UserAddContract.IView,
    View.OnClickListener {
    lateinit var mContext: Context
    lateinit var mPresenter: UserAddPresenter
    var userBean: UserBean = UserBean()

    lateinit var mStoreList: MutableList<StoreBean>
    lateinit var roleList: MutableList<String>

    val roleLisener = object : CommonSelectRoleDialog.OnSelectResult {
        override fun onResult(data: String) {
            mBinding.rlUserAddRole.visibility = View.GONE
            mBinding.rlUserAddRoleWithMessage.visibility = View.VISIBLE
            mBinding.etUserAddRoleValue.text = data
            userBean.position = getUserRoleCode(mContext, data)
        }

    }

    val storeListener = object : CommonSelectStoreDialog.OnSelectResult {
        override fun onResult(store: StoreBean) {
            mBinding.rlUserAddStore.visibility = View.GONE
            mBinding.rlUserAddStoreWithMessage.visibility = View.VISIBLE
            mBinding.etUserAddStoreValue.text = store.storeName
            userBean.storeNo = store.storeNo!!
            userBean.storeName = store.storeName!!
        }

    }

    override fun getBinding(): ActivityUserAddBinding {
        return ActivityUserAddBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
        initView()

    }

    fun initData() {
        mContext = this
        mPresenter = UserAddPresenter(mContext)
    }

    fun initView() {
        checkInformationEmpty()
        mBinding.etUserAddStoreValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                checkInformationEmpty()
            }

        })

        mBinding.etUserAddAccountValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                checkInformationEmpty()
            }

        })

        mBinding.etUserAddNameValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                checkInformationEmpty()
            }

        })

        mBinding.etUserAddRoleValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                checkInformationEmpty()
            }

        })

        val role = SPUtil.getSmartPayUserPosition(mContext)
        when (role) {
            0 -> {
                //只有一个超管，超管可以创建很多店长
                roleList = mutableListOf<String>(
                    getString(R.string.other_user_info_role_manager),
                    getString(R.string.other_user_info_role_clerk)
                )
            }
            1 -> {
                //一个店只有一个店长
                roleList = mutableListOf<String>(
                    getString(R.string.other_user_info_role_clerk)
                )
            }
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.rl_user_add_store, R.id.rl_user_add_store_with_message -> {
                mPresenter.getStoreList(CommonUtil.getSN(mContext))

            }
            R.id.rl_user_add_account -> {
                mBinding.rlUserAddAccountWithMessage.visibility = View.VISIBLE
                mBinding.rlUserAddAccount.visibility = View.GONE
            }
            R.id.rl_user_add_name -> {
                mBinding.rlUserAddNameWithMessage.visibility = View.VISIBLE
                mBinding.rlUserAddName.visibility = View.GONE
            }
            R.id.rl_user_add_role, R.id.rl_user_add_role_with_message -> {
                val dialog = CommonSelectRoleDialog(
                    this,
                    getString(R.string.other_user_info_role_select),
                    getString(R.string.other_user_info_role_select_tip),
                    roleList, roleLisener
                )
                dialog.show()
            }
            R.id.btn_ok -> {
                val number = mBinding.etUserAddAccountValue.text.toString()
                if (number.length < 10 || !isCorrectPhone(number)) {
                    mDialog = CommonDialog.Builder(mContext)
                        .setTitle(getString(R.string.login_phone_error))
                        .setButton(getString(R.string.common_close))
                        .createAndShow()
                    return
                }

                userBean.loginAccount = mBinding.etUserAddAccountValue.text.toString()
                userBean.staffName = mBinding.etUserAddNameValue.text.toString()
                val userInfo = Gson().toJson(userBean)
                mPresenter.addUser(userInfo)
            }
        }
    }

    override fun onAddUserSuccess() {
        Log.i(TAG, "onAddUserSuccess: ")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setTitle(getString(R.string.common_success))
            .setButton(getString(R.string.common_ok))
            .setListener(object : CommonDialog.OnClickListener {
                override fun onConfirm() {
                    finish()
                }

                override fun onCancel() {
                }

            })
            .createAndShow()
        RxBus.getInstance().post(FreshUserListEvent())
    }

    override fun onAddUserFail(msg: String?) {
        Log.i(TAG, "onAddUserFail: ")
        hideLoading()
        mDialog = CommonDialog.Builder(mContext)
            .setTitle(msg)
            .setButton(getString(R.string.common_ok))
            .createAndShow()
    }

    override fun onGetStoreListSuccess(storeList: MutableList<StoreBean>) {
        Log.i(TAG, "onGetStoreListSuccess: ")
        hideLoading()
        mStoreList = storeList
        val dialog = CommonSelectStoreDialog(
            this, getString(R.string.other_user_info_store_select),
            getString(R.string.other_user_info_role_select_tip),
            mStoreList, storeListener
        )
        dialog.show()
    }

    override fun onGetStoreListFail(msg: String?) {
        Log.i(TAG, "onGetStoreListFail: ")
        hideLoading()
        val dialog = CommonDialog.Builder(mContext)
            .setTitle(getString(R.string.other_user_info_get_role_fail))
            .setDescription(msg)
            .setButton(getString(R.string.common_close))
            .createAndShow()
    }

    fun checkInformationEmpty() {
        if (TextUtils.isEmpty(mBinding.etUserAddStoreValue.text.toString())
            || TextUtils.isEmpty(mBinding.etUserAddAccountValue.text.toString())
            || TextUtils.isEmpty(mBinding.etUserAddNameValue.text.toString())
            || TextUtils.isEmpty(mBinding.etUserAddRoleValue.text.toString())
        ) {
            mBinding.btnOk.isClickable = false
            mBinding.btnOk.setBackgroundResource(R.color.common_gray_button_bg)
        } else {
            mBinding.btnOk.isClickable = true
            mBinding.btnOk.setBackgroundResource(R.drawable.common_select_btn_bg_theme)
        }
    }

}