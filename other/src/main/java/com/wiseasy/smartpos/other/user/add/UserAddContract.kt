package com.wiseasy.smartpos.other.user.add

import com.wiseasy.smartpos.base.mvp.BaseIView
import com.wiseasy.smartpos.comm.bean.StoreBean

/**
 * Created by Bella on 2022/1/19.
 */
interface UserAddContract {
    interface IView : BaseIView {
        /**
         * 用户添加成功
         */
        fun onAddUserSuccess()

        /**
         * 用户添加失败
         */
        fun onAddUserFail(msg: String?)

        /**
         * 获取门店列表成功
         */
        fun onGetStoreListSuccess(storeList: MutableList<StoreBean>)

        /**
         * 获取门店列表失败
         */
        fun onGetStoreListFail(msg: String?)
    }

    interface IPresenter {
        /**
         * 增加新用户
         */
        fun addUser(userInfo: String)

        /**
         * 获取商户门店列表
         */
        fun getStoreList(sn: String)


    }
}