package com.wiseasy.smartpos.other.user.add

import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.alibaba.fastjson.JSONObject
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.base.util.withIOContext
import com.wiseasy.smartpos.comm.bean.MerchantMappingInfo
import com.wiseasy.smartpos.comm.bean.StoreBean
import com.wiseasy.smartpos.comm.bean.UserBean
import com.wiseasy.smartpos.comm.http.wisecloud.IWiseCloudAPI
import com.wiseasy.smartpos.comm.http.wisecloud.RetrofitClientWiseCloud
import okhttp3.Response

/**
 * Created by Bella on 2022/1/19.
 */
class UserAddPresenter(context: Context) : BasePresenter(), UserAddContract.IPresenter {
    private var mView: UserAddContract.IView

    init {
        mContext = context
        mView = context as UserAddContract.IView
    }

    override fun addUser(userInfo: String) {
        Log.i(TAG, "addUser: $userInfo")
        mView.showLoading()
        val gson = Gson()
        val user = gson.fromJson(userInfo, UserBean::class.java)
        launchRx {
            val json = JSONObject()
            json["sn"] = CommonUtil.getSN(mContext)
            json["loginAccount"] = user.loginAccount
            json["merchantNo"] = user.merchantNo
            json["position"] = user.position
            json["staffName"] = user.staffName
            json["storeName"] = user.storeName
            json["storeNo"] = user.storeNo
            withIOContext {
                val result = RetrofitClientWiseCloud.create(IWiseCloudAPI::class.java).addUser(
                    RetrofitClientWiseCloud.buildRequestBody(json)
                ).dataConvert(Response::class.java)
            }
            mView.onAddUserSuccess()
        }.await {
            it.printStackTrace()
            mView.onAddUserFail(it.message)
        }
    }

    override fun getStoreList(sn: String) {
        Log.i(TAG, "getStoreList: $sn")
        mView.showLoading()
        launchRx {
            val json = JSONObject()
            json["sn"] = sn
            var result: MutableList<StoreBean>? = null
            withIOContext {
                val typeTemp = object : TypeToken<MutableList<StoreBean>>() {}.type

                result = RetrofitClientWiseCloud.create(IWiseCloudAPI::class.java)
                    .getStoreList(RetrofitClientWiseCloud.buildRequestBody(json)).dataConvertByList(typeTemp)
                Log.i(TAG, "getStoreList: $result")
            }
            result?.let {
                if (SPUtil.getSmartPayUserPosition(mContext) == 1) {
                    val merchantMappingInfoJson = SPUtil.getWiseCloudMerchantInfo(mContext)
                    if (!TextUtils.isEmpty(merchantMappingInfoJson)) {
                        val merchantInfo: MerchantMappingInfo =  Gson().fromJson(merchantMappingInfoJson, MerchantMappingInfo::class.java)
                        val storeBean = StoreBean(merchantInfo.merchantNo, merchantInfo.storeNo, merchantInfo.storeName)
                        val newResult = mutableListOf(storeBean)
                        mView.onGetStoreListSuccess(newResult)
                    } else {
                        mView.onGetStoreListSuccess(it)
                    }
                } else {
                    mView.onGetStoreListSuccess(it)
                }
            }
        }.await {
            it.printStackTrace()
            mView.onGetStoreListFail(it.message)
        }
    }


}