package com.wiseasy.smartpos.other.user.display

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.gson.Gson
import com.wiseasy.smartpos.base.event.FreshUserListEvent
import com.wiseasy.smartpos.base.event.RxBus
import com.wiseasy.smartpos.base.mvp.BaseActivity
import com.wiseasy.smartpos.base.router.Router
import com.wiseasy.smartpos.comm.bean.UserBean
import com.wiseasy.smartpos.comm.module.service.LoginModuleService
import com.wiseasy.smartpos.comm.util.getUserRoleName
import com.wiseasy.smartpos.comm.util.getUserStatusName
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.other.R
import com.wiseasy.smartpos.other.databinding.ActivityUserInformationBinding
import com.wiseasy.smartpos.other.user.edit.UserEditActivity
import com.wiseasy.smartpos.other.user.list.UserListActivity

/**
 * Created by Bella on 2022/1/11.
 */
class UserInformationActivity() : CommonActivity<ActivityUserInformationBinding>(),
    UserInformationContract.IView, View.OnClickListener {
    lateinit var userBean: UserBean
    lateinit var userString: String
    var mPresenter: UserInformationPresenter
    var mContext: Context

    val gson = Gson()
    var mIsSuccess: Boolean = false

    val listener = object : CommonDialog.OnClickListener {
        override fun onConfirm() {
            Log.i(TAG, "onConfirm: ")
            if (mIsSuccess) {
                //删除成功，回到列表页
                mDialog.dismiss()
                RxBus.getInstance().post(FreshUserListEvent())
                finish()
            } else {
                //删除失败，停在当页
                mDialog.dismiss()
            }
        }

        override fun onCancel() {
            Log.i(TAG, "onCancel: ")
        }

    }

    companion object {
        fun actionStart(context: Context, user: String) {
            val intent = Intent(context, UserInformationActivity::class.java)
            intent.putExtra("user", user)
            context.startActivity(intent)
        }
    }

    init {
        mContext = this
        mPresenter = UserInformationPresenter(mContext)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userString = intent.getStringExtra("user").toString()
        userBean = gson.fromJson(userString, UserBean::class.java)
        initView()
    }

    fun initView() {
        mBinding.tvAccountValue.text = userBean.loginAccount
        mBinding.tvStoreValue.text = userBean.storeName
        mBinding.tvNameValue.text = userBean.staffName
        mBinding.tvRole.text = getUserRoleName(this, userBean.position)
        mBinding.tvStatusValue.text = getUserStatusName(this, userBean.status)
        mBinding.btnDelete.setOnClickListener(this)
        mBinding.btnEdit.setOnClickListener(this)
        mBinding.btnBackToHome.setOnClickListener(this)
    }

    override fun getBinding(): ActivityUserInformationBinding {
        return ActivityUserInformationBinding.inflate(layoutInflater)
    }


    @SuppressLint("StringFormatInvalid")
    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btn_edit -> {
//                UserEditActivity.actionStart(this, userString)
                val intent = Intent(this, UserEditActivity::class.java)
                intent.putExtra("user", userString)
                startActivityForResult(intent, 1)
//                finish()
            }
            R.id.btn_delete -> {
                mDialog = CommonDialog.Builder(mContext)
                    .setTitle(getString(R.string.other_user_whether_delete_tip))
                    .setButton(getString(R.string.common_yes, R.string.common_no))
                    .setListener(object : CommonDialog.OnClickListener {
                        override fun onConfirm() {
                            mPresenter.deleteUser(userBean.id, userBean.loginAccount!!)
                            mDialog.dismiss()
                        }

                        override fun onCancel() {
                            mDialog.dismiss()
                        }

                    }).createAndShow()
            }
            R.id.btn_back_to_home -> {
                Router.getInstance().getService(LoginModuleService::class.java)
                    ?.goToHomeActivity(this)
            }
        }
    }

    override fun onDeteleUserResult(isSuccess: Boolean, msg: String?) {
        Log.i(TAG, "onDeteleUserResult: $isSuccess, $msg")
        hideLoading()
        mIsSuccess = isSuccess
        mDialog = CommonDialog.Builder(this)
            .setTitle(msg)
            .setButton(getString(R.string.common_close))
            .setListener(listener)
            .createAndShow()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        log("onActivityResult|requestCode = $requestCode, resultCode = $resultCode")
        if (requestCode == 1 && resultCode == RESULT_OK) {
            userString = data?.getStringExtra("user").toString()
            userBean = gson.fromJson(userString, UserBean::class.java)
            log("onActivityResult|更新界面, userString = $userString")
            initView()
        } else {
            log("不更新界面")
        }
    }

}