package com.wiseasy.smartpos.other.user.display

import com.wiseasy.smartpos.base.mvp.BaseIView

/**
 * Created by Bella on 2022/1/24.
 */
interface UserInformationContract {
    interface IView: BaseIView {
        /**
         * 删除员工成功
         */
        fun onDeteleUserResult(isSuccess: Boolean, msg: String?)

    }

    interface IPresenter {
        /**
         * 删除员工
         */
        fun deleteUser(id: Int, account: String)
    }
}