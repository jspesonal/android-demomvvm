package com.wiseasy.smartpos.other.user.display

import android.content.Context
import android.util.Log
import com.alibaba.fastjson.JSONObject
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.base.util.withIOContext
import com.wiseasy.smartpos.comm.http.wisecloud.IWiseCloudAPI
import com.wiseasy.smartpos.comm.http.wisecloud.RetrofitClientWiseCloud
import com.wiseasy.smartpos.other.R
import okhttp3.Response

/**
 * Created by Bella on 2022/1/24.
 */
class UserInformationPresenter(context: Context): BasePresenter(), UserInformationContract.IPresenter {
    private var mView: UserInformationContract.IView
    
    init {
        mContext = context
        mView = context as UserInformationContract.IView
    }
    
    override fun deleteUser(id: Int, account: String) {
        Log.i(TAG, "deleteUser: $id")
        mView.showLoading()
        if (account == SPUtil.getSmartPayPhone(mContext)) {
            mView.onDeteleUserResult(false, mContext?.getString(R.string.other_user_delete_tip_2))
            return
        }
        launchRx {
            val json = JSONObject()
            json["sn"] = CommonUtil.getSN(mContext)
            json["id"] = id
            var result: Response? = null
            withIOContext {
                val result = RetrofitClientWiseCloud.create(IWiseCloudAPI::class.java).deleteUser(
                    RetrofitClientWiseCloud.buildRequestBody(json)).dataConvert(Response::class.java)
                Log.i(TAG, "updateUserInfo: ${result.toString()}")
            }
            mView.onDeteleUserResult(true, mContext?.getString(R.string.other_user_delete_tip))

        }.await {
            it.printStackTrace()
            mView.onDeteleUserResult(false, it.message)
        }
    }
}