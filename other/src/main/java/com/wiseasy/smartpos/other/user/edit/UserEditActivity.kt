package com.wiseasy.smartpos.other.user.edit

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.google.gson.Gson
import com.wiseasy.smartpos.base.event.FreshUserListEvent
import com.wiseasy.smartpos.base.event.RxBus
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.comm.bean.StoreBean
import com.wiseasy.smartpos.comm.bean.UserBean
import com.wiseasy.smartpos.comm.util.*
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.comm.view.CommonDialog
import com.wiseasy.smartpos.comm.view.CommonSelectRoleDialog
import com.wiseasy.smartpos.comm.view.CommonSelectStoreDialog
import com.wiseasy.smartpos.other.R
import com.wiseasy.smartpos.other.databinding.ActivityUserEditBinding
import com.wiseasy.smartpos.other.user.display.UserInformationActivity

/**
 * Created by Bella on 2022/1/13.
 */
class UserEditActivity : CommonActivity<ActivityUserEditBinding>(), UserEditContract.IView,
    View.OnClickListener {
    lateinit var mContext: Context
    lateinit var mPresenter: UserEditPresenter

    lateinit var userBean: UserBean
    lateinit var userString: String
    val gson = Gson()

    lateinit var mStoreList: MutableList<StoreBean>

    lateinit var roleList: MutableList<String>
    lateinit var statusList: MutableList<String>


    val roleListener = object : CommonSelectRoleDialog.OnSelectResult {
        override fun onResult(data: String) {
            mBinding.tvUserEditRoleValue.text = data
            userBean.position = getUserRoleCode(mContext, data)
        }

    }

    val storeListener = object : CommonSelectStoreDialog.OnSelectResult {
        override fun onResult(store: StoreBean) {
            mBinding.tvUserEditStoreValue.text = store.storeName
            userBean.storeName = store.storeName
            userBean.storeNo = store.storeNo
        }


    }

    val statusListener = object : CommonSelectRoleDialog.OnSelectResult {
        override fun onResult(data: String) {
            mBinding.tvUserEditStatusValue.text = data
            userBean.status = getUserStatusCode(mContext, data)
        }

    }

    companion object {
        fun actionStart(context: Context, user: String) {
            val intent = Intent(context, UserEditActivity::class.java)
            intent.putExtra("user", user)
            context.startActivity(intent)
        }
    }

    override fun getBinding(): ActivityUserEditBinding {
        return ActivityUserEditBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
        initView()

        statusList = mutableListOf<String>(
            getString(R.string.other_user_info_status_active),
            getString(R.string.other_user_info_status_disable)
        )
    }

    fun initView() {
        mBinding.etUserEditAccountValue.text = userBean.loginAccount
        mBinding.tvUserEditStoreValue.text = userBean.storeName
        mBinding.etUserEditNameValue.setText(userBean.staffName)
        val name = mBinding.etUserEditNameValue.text.toString()
        if (!TextUtils.isEmpty(name)) {
            mBinding.etUserEditNameValue.setSelection(name.length)
        }

        mBinding.tvUserEditRoleValue.text = getUserRoleName(this, userBean.position)
        mBinding.tvUserEditStatusValue.text = getUserStatusName(this, userBean.status)
        mBinding.etUserEditNameValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                Log.i(TAG, "beforeTextChanged: ")
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Log.i(TAG, "onTextChanged: ")
            }

            override fun afterTextChanged(s: Editable?) {
                Log.i(TAG, "afterTextChanged: ")
                if (TextUtils.isEmpty(mBinding.etUserEditNameValue.text.toString())) {
                    mBinding.btnOk.isClickable = false
                    mBinding.btnOk.setBackgroundResource(R.color.common_gray_button_bg)
                } else {
                    mBinding.btnOk.isClickable = true
                    mBinding.btnOk.setBackgroundResource(R.drawable.common_select_btn_bg_theme)
                }
            }

        })

    }

    private fun initData() {
        userString = intent.getStringExtra("user").toString()
        userBean = gson.fromJson(userString, UserBean::class.java)
        mContext = this
        mPresenter = UserEditPresenter(mContext)
        val role = SPUtil.getSmartPayUserPosition(mContext)
        when (role) {
            0 -> {
                //只有一个超管，超管可以创建很多店长
                roleList = mutableListOf<String>(
                    getString(R.string.other_user_info_role_manager),
                    getString(R.string.other_user_info_role_clerk)
                )
            }
            1 -> {
                //一个店只有一个店长
                roleList = mutableListOf<String>(
                    getString(R.string.other_user_info_role_clerk)
                )
            }
        }
    }

    override fun onActionBarLeft() {
        finish()
    }


    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.et_user_edit_account_value -> {

            }
            R.id.rl_user_edit_store -> {
                //超管不可编辑门店
                if (SPUtil.getSmartPayUserPosition(mContext) == 0) {
                    toast(getString(R.string.other_user_admin_can_not_edit))
                    return
                }
                mPresenter.getStoreList(CommonUtil.getSN(mContext))
            }
            R.id.et_user_edit_name_value -> {
            }
            R.id.rl_user_edit_role -> {
                //超管不可编辑角色
                if (SPUtil.getSmartPayUserPosition(mContext) == 0) {
                    toast(getString(R.string.other_user_admin_can_not_edit))
                    return
                }
                val dialog = CommonSelectRoleDialog(
                    this,
                    getString(R.string.other_user_info_role_select),
                    getString(R.string.other_user_info_role_select_tip),
                    roleList,
                    roleListener
                )
                dialog.show()
            }
            R.id.rl_user_edit_status -> {
                val dialog = CommonSelectRoleDialog(
                    this,
                    getString(R.string.other_user_info_status_select),
                    getString(R.string.other_user_info_role_select_tip),
                    statusList,
                    statusListener
                )
                dialog.show()
            }
            R.id.btn_ok -> {
                //保存更改信息，并更新
                Log.i(TAG, "onClick: ${userBean}")
                userBean.staffName = mBinding.etUserEditNameValue.text.toString()
                val userInfo: String = gson.toJson(userBean)
                mPresenter.updateUserInfo(userInfo)
            }
        }
    }

    override fun onGetStoreListSuccess(storeList: MutableList<StoreBean>) {
        Log.i(TAG, "onGetStoreListSuccess: $storeList")
        hideLoading()
        mStoreList = storeList

        val dialog = CommonSelectStoreDialog(
            this,
            getString(R.string.other_user_info_store_select),
            getString(R.string.other_user_info_store_select),
            mStoreList,
            storeListener
        )
        dialog.show()
    }

    override fun onGetStoreListFail(msg: String?) {
        Log.i(TAG, "onGetStoreListFail: ")
        hideLoading()
        val dialog = CommonDialog.Builder(mContext)
            .setTitle(getString(R.string.other_user_info_get_role_fail))
            .setDescription(msg)
            .setButton(getString(R.string.common_close))
            .createAndShow()
    }

    override fun onUpdateUserInfoSuccess() {
        Log.i(TAG, "onGetUpdateUserInfoSuccess: ")
        hideLoading()
        val dialog = CommonDialog.Builder(mContext)
            .setTitle(getString(R.string.other_user_info_update_success))
            .setListener(object : CommonDialog.OnClickListener {
                override fun onConfirm() {
                    finish()
                }

                override fun onCancel() {
                }

            })
            .setButton(getString(R.string.common_ok))
            .createAndShow()
        RxBus.getInstance().post(FreshUserListEvent())
        val intent = Intent()
        val userStr = gson.toJson(userBean)
        log("需要回传的user = $userStr")
        intent.putExtra("user", userStr)
        setResult(RESULT_OK, intent)
    }

    override fun onUpdateUserInfoFail(msg: String?) {
        Log.i(TAG, "onGetUpdateUserInfoFail: ")
        hideLoading()
        val dialog = CommonDialog.Builder(mContext)
            .setTitle(getString(R.string.other_user_info_update_fail))
            .setDescription(msg)
            .setButton(getString(R.string.common_ok))
            .createAndShow()
    }

//    override fun finish() {
//        //跳转到上个界面(信息展示界面)，并携带当前更新后的信息
//        val userStr = gson.toJson(userBean)
//        val intent = Intent(this, UserInformationActivity::class.java)
//        intent.putExtra("user", userStr)
//        setResult(mIsChange, intent)
////        startActivityForResult(intent, 1)
//
//        super.finish()
//    }
}