package com.wiseasy.smartpos.other.user.edit

import com.wiseasy.smartpos.base.mvp.BaseIView
import com.wiseasy.smartpos.comm.bean.StoreBean
import com.wiseasy.smartpos.comm.bean.UserBean

/**
 * Created by Bella on 2022/1/21.
 */
interface UserEditContract {
    interface IView: BaseIView {
        /**
         * 获取门店列表成功
         */
        fun onGetStoreListSuccess(storeList: MutableList<StoreBean>)

        /**
         * 获取门店列表失败
         */
        fun onGetStoreListFail(msg: String?)


        fun onUpdateUserInfoSuccess()

        fun onUpdateUserInfoFail(msg: String?)

    }

    interface IPresenter {
        /**
         * 获取商户门店列表
         */
        fun getStoreList(sn: String)

        /**
         * 修改员工信息
         */
        fun updateUserInfo(userStr: String)
    }
}