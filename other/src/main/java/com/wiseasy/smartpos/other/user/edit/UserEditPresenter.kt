package com.wiseasy.smartpos.other.user.edit

import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.alibaba.fastjson.JSONObject
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.base.util.withIOContext
import com.wiseasy.smartpos.comm.bean.MerchantMappingInfo
import com.wiseasy.smartpos.comm.bean.StoreBean
import com.wiseasy.smartpos.comm.bean.UserBean
import com.wiseasy.smartpos.comm.http.wisecloud.BaseResult
import com.wiseasy.smartpos.comm.http.wisecloud.IWiseCloudAPI
import com.wiseasy.smartpos.comm.http.wisecloud.RetrofitClientWiseCloud
import okhttp3.Response

/**
 * Created by Bella on 2022/1/21.
 */
class UserEditPresenter(context: Context) : BasePresenter(), UserEditContract.IPresenter {
    private var mView: UserEditContract.IView

    init {
        mContext = context
        mView = context as UserEditContract.IView
    }

    override fun getStoreList(sn: String) {
        Log.i(TAG, "getStoreList: ")
        mView.showLoading()
        launchRx {
            val json = JSONObject()
            json["sn"] = sn
            var result: MutableList<StoreBean>? = null
            withIOContext {
                val typeTemp = object : TypeToken<MutableList<StoreBean>>() {}.type

                result = RetrofitClientWiseCloud.create(IWiseCloudAPI::class.java)
                    .getStoreList(RetrofitClientWiseCloud.buildRequestBody(json)).dataConvertByList(typeTemp)
                Log.i(TAG, "getStoreList: $result")
            }
            result?.let {
                if (SPUtil.getSmartPayUserPosition(mContext) == 1) {
                    val merchantMappingInfoJson = SPUtil.getWiseCloudMerchantInfo(mContext)
                    if (!TextUtils.isEmpty(merchantMappingInfoJson)) {
                        val merchantInfo: MerchantMappingInfo =  Gson().fromJson(merchantMappingInfoJson, MerchantMappingInfo::class.java)
                        val storeBean = StoreBean(merchantInfo.merchantNo, merchantInfo.storeNo, merchantInfo.storeName)
                        val newResult = mutableListOf(storeBean)
                        mView.onGetStoreListSuccess(newResult)
                    } else {
                        mView.onGetStoreListSuccess(it)
                    }
                } else {
                    mView.onGetStoreListSuccess(it)
                }
            }
        }.await {
            it.printStackTrace()
            mView.onGetStoreListFail(it.message)
        }

    }

    override fun updateUserInfo(userStr: String) {
        Log.i(TAG, "updateUserInfo: $userStr")
        mView.showLoading()
        val gson = Gson()
        val user = gson.fromJson(userStr, UserBean::class.java)
        launchRx {
            val json = JSONObject()
            json["sn"] = CommonUtil.getSN(mContext)
            json["id"] = user.id
            json["loginAccount"] = user.loginAccount
            json["position"] = user.position
            json["staffName"] = user.staffName
            json["storeName"] = user.storeName
            json["storeNo"] = user.storeNo
            json["status"] = user.status
            withIOContext {
                val result = RetrofitClientWiseCloud.create(IWiseCloudAPI::class.java).updateUserInfo(
                    RetrofitClientWiseCloud.buildRequestBody(json)
                ).dataConvert(Response::class.java)
                Log.i(TAG, "updateUserInfo: ${result.toString()}")
            }
            mView.onUpdateUserInfoSuccess()

        }.await {
            it.printStackTrace()
            mView.onUpdateUserInfoFail(it.message)
        }
    }
}