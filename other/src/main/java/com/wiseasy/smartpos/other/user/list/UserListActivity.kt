package com.wiseasy.smartpos.other.user.list

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.liaoinstan.springview.widget.SpringView
import com.wiseasy.smartpos.base.event.FinishFirstLoginEvent
import com.wiseasy.smartpos.base.event.FreshUserListEvent
import com.wiseasy.smartpos.base.event.RxBus
import com.wiseasy.smartpos.base.util.ToastUtil
import com.wiseasy.smartpos.comm.bean.UserBean
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.other.R
import com.wiseasy.smartpos.other.databinding.ActivityUserListBinding
import com.wiseasy.smartpos.other.user.display.UserInformationActivity
import com.wiseasy.smartpos.other.user.adapter.UserListAdapter
import com.wiseasy.smartpos.other.user.add.UserAddActivity
import com.wiseasy.smartpos.other.view.CustomFooter
import com.wiseasy.smartpos.other.view.CustomHeader
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Bella on 2022/1/6.
 */
class UserListActivity : CommonActivity<ActivityUserListBinding>(), UserListContract.IView,
    UserListAdapter.OnItemClickListener {
    lateinit var mContext: Context
    lateinit var mPresenter: UserListPresenter
    lateinit var mAdapter: UserListAdapter
    private val mCompositeDisposable = CompositeDisposable()
    private var mData = mutableListOf<UserBean>()
    private var mPageNum = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
        initView()
        subscribeEvent()
    }

    private fun initData() {
        Log.i(TAG, "initData: ")
        mContext = this
        mPresenter = UserListPresenter(mContext)
        val layoutManager = LinearLayoutManager(mContext)
        mBinding.rvUsers.layoutManager = layoutManager
        mAdapter = UserListAdapter(this, mData, this)
        mBinding.rvUsers.adapter = mAdapter
        mPageNum = 1
        mPresenter.queryUserList("", mPageNum)
    }

    private fun initView() {
        Log.i(TAG, "initView: ")
        mBinding.rvUsers.adapter = mAdapter
        mBinding.svUserRefresh.header = CustomHeader()
        mBinding.svUserRefresh.footer = CustomFooter()
        mBinding.svUserRefresh.type = SpringView.Type.FOLLOW

        mBinding.ivSearch.setOnClickListener(this)
        mBinding.ivClearSearchInformation.setOnClickListener(this)

        mBinding.svUserRefresh.setListener(object : SpringView.OnFreshListener {
            override fun onRefresh() {
                Log.i(TAG, "下拉刷新")
                mPageNum = 1
                mPresenter.queryUserList("", mPageNum)
            }

            override fun onLoadmore() {
                Log.i(TAG, "上拉加载")
                mPresenter.queryUserList("", ++mPageNum)
            }

        })

        mBinding.etInputName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (TextUtils.isEmpty(s)) {
                    mBinding.ivClearSearchInformation.visibility = GONE
                } else {
                    mBinding.ivClearSearchInformation.visibility = VISIBLE
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })

    }

    /**
     * 订阅事件
     */
    private fun subscribeEvent() {
        log("subscribeEvent订阅事件")
        mPageNum = 1
        mCompositeDisposable.add(
            RxBus.getInstance().toObservable(FreshUserListEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    log("收到刷新用户列表")
                    mPresenter.queryUserList(mBinding.etInputName.text.toString(), mPageNum)
                })
    }

    override fun onActionBarRight() {
        super.onActionBarRight()
        startActivity(Intent(this, UserAddActivity::class.java))
    }

    override fun getBinding(): ActivityUserListBinding {
        return ActivityUserListBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        mPageNum = 1
        when (v.id) {
            R.id.iv_search -> {
                //根据员工姓名搜索
                if (TextUtils.isEmpty(mBinding.etInputName.text.toString())) {
                    ToastUtil.showShort(mContext, getString(R.string.other_user_list_search_name))
                    return
                }
                mAdapter.clear()
                mPresenter.queryUserList(mBinding.etInputName.text.toString(), mPageNum)
            }

            R.id.iv_clear_search_information -> {
                mBinding.etInputName.text = Editable.Factory.getInstance().newEditable("")
            }

            R.id.btn_close -> {//底部关闭
                finish()
            }
        }
    }

    override fun onDestroy() {
        mCompositeDisposable.dispose()
        super.onDestroy()
    }

    override fun onQueryListSuccess(datas: MutableList<UserBean>, isLoadMore: Boolean) {
        Log.i(TAG, "onQueryListSuccess: $datas")
        hideLoading()
        mBinding.rlEmpty.visibility = View.GONE
        mBinding.rvUsers.visibility = View.VISIBLE
        mBinding.svUserRefresh.visibility = View.VISIBLE
        mBinding.svUserRefresh.onFinishFreshAndLoad()
        if (isLoadMore) {
            mData.addAll(datas)
        } else {
            mData = datas
        }

        mAdapter.setData(mData)
    }

    override fun onQueryListFail(msg: String?) {
        Log.i(TAG, "onQueryListFail: $msg")
        hideLoading()
        mBinding.rlEmpty.visibility = View.VISIBLE
        mBinding.svUserRefresh.visibility = View.GONE
        mBinding.svUserRefresh.onFinishFreshAndLoad()
        mBinding.tvError.text = msg
    }

    override fun onNoData() {
        Log.i(TAG, "onNoData: ")
        hideLoading()
        mAdapter.clear()
        mBinding.rlEmpty.visibility = VISIBLE
        mBinding.svUserRefresh.visibility = GONE
        mBinding.svUserRefresh.onFinishFreshAndLoad()
    }

    val gson = Gson()
    override fun onItemSelect(user: UserBean) {
        val userInfo: String = gson.toJson(user)
        UserInformationActivity.actionStart(this, userInfo)
    }


}