package com.wiseasy.smartpos.other.user.list

import com.wiseasy.smartpos.base.mvp.BaseIView
import com.wiseasy.smartpos.comm.bean.UserBean

/**
 * Created by Bella on 2022/1/6.
 */
interface UserListContract {

    interface IView: BaseIView {
        fun onQueryListSuccess(data: MutableList<UserBean>, isLoadMore: Boolean)

        fun onQueryListFail(msg: String?)

        fun onNoData()
    }

    interface IPresenter {
        /**
         * 查询用户列表
         */
        fun queryUserList(userName: String?, pageNum: Int)
    }
}