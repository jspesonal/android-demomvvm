package com.wiseasy.smartpos.other.user.list

import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.alibaba.fastjson.JSONObject
import com.google.gson.reflect.TypeToken
import com.wiseasy.smartpos.base.mvp.BasePresenter
import com.wiseasy.smartpos.base.util.CommonUtil
import com.wiseasy.smartpos.base.util.SPUtil
import com.wiseasy.smartpos.base.util.launchRx
import com.wiseasy.smartpos.base.util.withIOContext
import com.wiseasy.smartpos.comm.bean.UserBean
import com.wiseasy.smartpos.comm.http.wisecloud.IWiseCloudAPI
import com.wiseasy.smartpos.comm.http.wisecloud.RetrofitClientWiseCloud

/**
 * Created by Bella on 2022/1/6.
 */
class UserListPresenter(context: Context) : BasePresenter(), UserListContract.IPresenter {
    private var mView: UserListContract.IView

    init {
        mContext = context
        mView = context as UserListContract.IView
    }


    override fun queryUserList(userName: String?, pageNum: Int) {
        Log.i(TAG, "queryUserList: ")
        mView.showLoading()

        launchRx {
            val json = JSONObject()
            json["sn"] = CommonUtil.getSN(mContext)
            json["currentPosition"] = SPUtil.getSmartPayUserPosition(mContext)
            json["staffName"] = userName
            json["page"] = pageNum
            json["size"] = 20
            var result: MutableList<UserBean>? = null
            withIOContext {
                val typeTemp = object : TypeToken<MutableList<UserBean>>() {}.type

                result = RetrofitClientWiseCloud.create(IWiseCloudAPI::class.java).getUserList(
                    RetrofitClientWiseCloud.buildRequestBody(json)
                ).dataConvertByList(typeTemp)
                Log.i(TAG, "queryUserList: $result")
            }
            result?.let {
                if (result!!.size == 0 && pageNum == 1) {
                    mView.onNoData()
                    return@launchRx
                }
                if (pageNum == 1) {
                    mView.onQueryListSuccess(it, false)
                } else {
                    mView.onQueryListSuccess(it, true)
                }

            }

        }.await {
            it.printStackTrace()
            mView.onQueryListFail(it.message)
        }


    }
}