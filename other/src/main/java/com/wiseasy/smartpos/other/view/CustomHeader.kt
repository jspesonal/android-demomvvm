package com.wiseasy.smartpos.other.view

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.liaoinstan.springview.container.BaseHeader
import com.liaoinstan.springview.container.DefaultHeader
import com.wiseasy.smartpos.base.util.RotateAnim
import com.wiseasy.smartpos.other.R

/**
 * 下拉刷新
 * @author Belle(Baiyunyan)
 * @date 2021/12/28
 */
class CustomHeader : BaseHeader() {

    private val TAG = "CustomHeader ->"
    private lateinit var mIvHeader: ImageView

    /**
     * 获取Header
     */
    override fun getView(inflater: LayoutInflater, viewGroup: ViewGroup?): View {
        Log.d(TAG, "getView")
        val view = inflater.inflate(R.layout.view_refresh_header, viewGroup, true)
        mIvHeader = view.findViewById(R.id.iv_header)
        return view
    }

    /**
     * 拖拽开始前回调
     */
    override fun onPreDrag(rootView: View?) {
        Log.d(TAG, "onPreDrag")
    }

    /**
     * 手指拖拽过程中不断回调，dy为拖拽的距离，可以根据拖动的距离添加拖动过程动画
     */
    override fun onDropAnim(rootView: View?, dy: Int) {
        Log.d(TAG, "onDropAnim")
        val rotateAnimation = RotateAnim(duretion = 1100)
        mIvHeader.startAnimation(rotateAnimation)
    }

    /**
     * 手指拖拽过程中每次经过临界点时回调
     * @param rootView View
     * @param upORdown Boolean 是向上经过还是向下经过
     */
    override fun onLimitDes(rootView: View?, upORdown: Boolean) {
        Log.d(TAG, "onLimitDes")
    }

    /**
     * 拉动超过临界点后松开时回调
     */
    override fun onStartAnim() {
        Log.d(TAG, "onStartAnim")
        val rotateAnimation = RotateAnim(duretion = 1100)
        mIvHeader.startAnimation(rotateAnimation)
    }

    /**
     * 头部已经全部弹回时回调
     */
    override fun onFinishAnim() {
        Log.d(TAG, "onFinishAnim")
        mIvHeader.clearAnimation()
    }
}