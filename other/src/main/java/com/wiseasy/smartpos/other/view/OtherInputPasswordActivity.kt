package com.wiseasy.smartpos.other.view

import android.view.View
import android.widget.TextView
import com.wiseasy.smartpos.comm.view.CommonActivity
import com.wiseasy.smartpos.other.databinding.ActivityOtherInputPasswordBinding

/**
 * Created by Bella on 2022/1/4.
 */
class OtherInputPasswordActivity : CommonActivity<ActivityOtherInputPasswordBinding>() {
    var mPasswordString: String? = null


    override fun getBinding(): ActivityOtherInputPasswordBinding {
        return ActivityOtherInputPasswordBinding.inflate(layoutInflater)
    }

    override fun onClick(v: View?) {
        super.onClick(v)

    }

    fun initView() {
        val title = intent.getStringExtra("title")
        mBinding.actionBar.setTitle(title)

    }
}