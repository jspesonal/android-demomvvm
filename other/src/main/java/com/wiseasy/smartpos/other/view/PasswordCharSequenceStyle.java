package com.wiseasy.smartpos.other.view;

import android.text.method.PasswordTransformationMethod;
import android.view.View;

/**
 * 将输入框EditText的掩码字符改为星号
 *
 * @author Belle(Baiyunyan)
 * @date 2022/01/14
 */
public class PasswordCharSequenceStyle extends PasswordTransformationMethod {

    @Override
    public CharSequence getTransformation(CharSequence source, View view) {
        return new PasswordCharSequence(source);
    }

    private class PasswordCharSequence implements CharSequence {
        private CharSequence mSource;

        public PasswordCharSequence(CharSequence source) {
            mSource = source;
        }

        public char charAt(int index) {
            return '*';
        }

        public int length() {
            return mSource.length();
        }

        public CharSequence subSequence(int start, int end) {
            return mSource.subSequence(start, end);
        }
    }
}
